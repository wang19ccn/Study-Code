var express = require("express"); //引用包
var query = require("querystring");

var app = express();  //初始化
app.listen("8080", function () {  //监听
    console.log("服务已经启动，端口是8080")
});

var data2 = "";
app.post(
    '/login',
    function (request, response) {
        response.header("Access-Control-Allow-Origin", "*");
        request.on("data", function (datas) {   //接收用户发来的数据,里面的data是不变的，就得叫data
            console.log(datas);
            data2 = "";
            // console.log(datas)  ; //<Buffer 75 73 65 72 6e 61 6d 65 3d 26 70 61 73 73 77 6f 72 64 3d
            // >这样打印的结果是Buffer 类型的，所以我们得拼接一下,用querystring这个模块转成字符串
            data2 += datas;
        });
        request.on("end", function () {  //接收用户发来的数据之后开始解析
            console.log(query.parse(data2));  //{ username: '海燕', password: '123' }
            var username = query.parse(data2).username;
            var password = query.parse(data2).password;
            if (username == "test" && password == "123") {
                response.send(true)//如果登录成功就把数据返回给前端
            } else {
                response.send(false)
            }
        })

    }
);