var template = require('art-template');
var path = require('path')
var fs = require('fs')
var jwt = require('jsonwebtoken');

const listModel = require('../model/list')

// 应用中间价
const list = (req, res, next) => {
    // // 服务端渲染
    // let data = '<ul>'
    // for (let i = 0; i < 100; i++) {
    //     data += `<li>line ${i + 1}</li>`
    // }
    // data += '</ul>'

    // let data = '{"ret": true, "data":'

    // let dataObj = {
    //     ret: true,
    //     data: []
    // }
    // for (let i = 0; i < 100; i++) {
    //     dataObj.data.push('line' + i)
    // }

    // let dataArray = []
    // for (let i = 0; i < 200; i++) {
    //     dataArray.push('line ' + i)
    // }

    // res.set('content-type', 'application/json;charset=utf-8')

    // express-art-template 提供的render方法
    // res.render('list', {
    //     data: JSON.stringify(dataArray)
    // })

    // res.render('list-html', {
    //     data: dataArray
    // })

    var html = template(path.join(__dirname, '../view/list-html.art'), {
        data: listModel.dataArray
    });

    fs.writeFileSync(path.join(__dirname, '../public/list.html'), html)

    res.send('pages has been compile.')
}

const token = (req, res, next) => {
    // 对称加密
    const tk = jwt.sign({ username: 'admin' }, 'I love you')
    const decoded = jwt.verify(tk, 'I love you')
    res.send(decoded)

    // 非对称加密

}


exports.list = list
exports.token = token