// public 全部为静态资源，且处于浏览器环境，不支持node

$.ajax({
    url: '/api/list',
    success(result) {
        let templateStr = `
            <ul>
                {{each data}}
                <li>{{$value}}</li>
                {{/each}}
            </ul>
            <div>
                <b>{{x}}</b>
            </div>
        `
        let html = template.render(templateStr, {
            data: result.data,
            x: 'hello'
        })
        $('#list').html(html)

        // // 客户端渲染数据
        // let html = '<ul>'
        // $.each(result.data, (index, value) => {
        //     html += '<li>' + value + '</li>'
        // })
        // html += "</ul>"

        // let str = template.render('<div>{{data}}</div>', { data: 100 })
        // console.log(str)

        // 浏览器不支持读文件
        // let str = template.render('./list.art', { data: 100 })
        // console.log(str)

        // $('#list').html(html)
    }
})