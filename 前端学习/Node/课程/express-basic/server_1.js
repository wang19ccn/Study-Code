const express = require('express')

const app = express()

const middlewares = [(req, res, next) => {
    // res.send('hello')
    console.log(0)
    next()
}, (req, res, next) => {
    console.log(1)
    next()
}, (req, res, next) => {
    console.log(2)
    next()
}]

// express 回调函数被称为中间件
// 中间件栈
// 可用数组封装
// 凡是路径匹配，会自动组在一起
app.use('/',middlewares)
app.use('/ajax',(req,res,next) => {
    console.log('ajax')
    // next()
})
app.use('/api',(req,res) => {
    res.send('world')
})


app.listen(8080, () => {
    console.log('localhost:8080')
})