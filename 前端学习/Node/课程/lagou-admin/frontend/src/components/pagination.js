import usersListPageTpl from '../views/users-pages.art'
import page from '../databus/page'

const _setCurrentPage = (index) => {
    $('#users-page #users-page-list li:not(:first-child,:last-child)')
        .eq(index - 1) // 只找index对应的元素
        .addClass('active')
        .siblings()
        .removeClass('active')
}

// 显示分页效果
const pagination = (data, pageSize) => {
    const total = data.length
    const pageCount = Math.ceil(total / pageSize)
    // 让模板能循环生成 li  (还可以使用from生成数组)
    const pageArray = new Array(pageCount)
    console.log(pageArray)

    const htmlPage = usersListPageTpl({
        pageArray
    })

    $('#users-page').html(htmlPage)

    _setCurrentPage(page.curPage)

    _bindEvent(data, pageSize)
}

const _bindEvent = (data, pageSize) => {
    // 分页事件绑定
    $('#users-page').on('click', '#users-page-list li:not(:first-child,:last-child)', function () {
        const index = $(this).index()
        page.setCurPage(index)
        $('body').trigger('changeCurPage', index)
        _setCurrentPage(index)
    })
    $('#users-page').on('click', '#users-page-list li:first-child', function () {
        if (page.curPage > 1) {
            page.setCurPage(page.curPage - 1)
            $('body').trigger('changeCurPage', page.curPage)
            _setCurrentPage(page.curPage)
        }
    })
    $('#users-page').on('click', '#users-page-list li:last-child', function () {
        if (page.curPage < Math.ceil(data.length / pageSize)) {
            page.setCurPage(page.curPage + 1)
            $('body').trigger('changeCurPage', page.curPage)
            _setCurrentPage(page.curPage)
        }
    })
}

export default pagination