import indexTpl from '../views/index.art'

import usersTpl from '../views/users.art'
import usersListTpl from '../views/users-list.art'

import pagination from '../components/pagination'
import page from '../databus/page'

const htmlIndex = indexTpl({})

const pageSize = page.PageSize
let dataList = []

// 注册功能
const _signup = () => {
    const $btnClose = $('#users-close')

    // 提交表单
    const data = $('#users-form').serialize()
    $.ajax({
        url: '/api/users',
        type: 'post',
        data,
        success(res) {
            page.setCurPage(1)
            // 添加数据后渲染
            _loadData()
        }
    })

    // 单击关闭模态框
    $btnClose.click()
}

// 装填list数据
const _list = (pageNo) => {
    let start = (pageNo - 1) * pageSize
    $('#users-list').html(usersListTpl({
        data: dataList.slice(start, start + pageSize)
    }))
}

// 从后端加载数据
const _loadData = () => {
    // ajax返回一个promise
    return $.ajax({
        url: 'api/users',
        // async: false, // 方法二 使ajax变为同步，但浏览器会给（警告：可能会影响用户体验）
        success(result) {
            dataList = result.data
            // 分页
            pagination(result.data, pageSize)
            // 数据渲染
            _list(page.curPage)
        }
    })
}

const _methods = () => {
    // 删除事件绑定
    $('#users-list').on('click', '.remove', function () { // jquery语法：代理绑定
        $.ajax({
            url: '/api/users',
            type: 'delete',
            data: {
                id: $(this).data('id')
            },
            success() {
                _loadData()

                const isLastPage = Math.ceil(dataList.length / pageSize) === page.curPage
                const restOne = dataList.length % pageSize === 1
                const notPageFirst = page.curPage > 0

                if (isLastPage && restOne && notPageFirst) {
                    page.setCurPage(page.curPage - 1)
                }
            }
        })
    })

    // 登出事件绑定
    $('#users-signout').on('click', (e) => {
        // 因为模板中a标签有href，会跳转，拦截一下 或者 把href改成 href="javascript:void(0)"
        e.preventDefault()
        $.ajax({
            url: '/api/users/signout',
            dataType: 'json',
            success(result) {
                if (result.ret) {
                    console.log(1)
                    location.reload()
                }
            }
        })
    })

    // 点击保存，提交表单
    $('#users-save').on('click', _signup)
}

const _subscribe = () => {
    $('body').on('changeCurPage', (e, index) => {
        _list(index)
        console.log(page.curPage)
    })
}

const index = (router) => {
    const loadIndex = (res) => {
        // 渲染首页
        res.render(htmlIndex)

        // window resize，让页面撑满整个屏幕
        $(window, '.wrapper').resize()

        // 填充用户列表
        $('#content').html(usersTpl())

        // 初次渲染list
        _loadData()

        // 页面事件绑定
        _methods()

        // 订阅事件
        _subscribe()
    }

    return (req, res, next) => {
        $.ajax({
            url: '/api/users/isAuth',
            dataType: 'json',
            success(result) {
                if (result.ret) {
                    loadIndex(res)
                } else {
                    router.go('/signin')
                }
            }
        })


    }
}


export default index
