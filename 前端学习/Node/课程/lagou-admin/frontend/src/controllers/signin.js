import signinTpl from '../views/signin.art'
const htmlSignin = signinTpl({})

// 用下划线开头表示内部自己使用，不会暴露在外面
const _handleSubmit = (router) => {
    return (e) => {
        // 该方法将通知 Web 浏览器不要执行与事件关联的默认动作（如果存在这样的动作）。
        // 例如，如果 type 属性是 "submit"，在事件传播的任意阶段可以调用任意的事件句柄，
        // 通过调用该方法，可以阻止提交表单。
        e.preventDefault()

        const data = $('#signin').serialize()
        $.ajax({
            url: '/api/users/signin',
            type: 'post',
            dataType: 'json',
            data,
            success(res, textStatus, jqXHR) {
                const token = jqXHR.getResponseHeader('X-Access-Token')
                localStorage.setItem('lg-token', token)
                if (res.ret) {
                    router.go('/index')
                }
            }
        })
    }
}

// 登录模块
// 函数柯里化
const signin = (router) => {
    return (req, res, next) => {
        res.render(htmlSignin)
        $('#signin').on('submit', _handleSubmit(router))
    }
}

export default signin