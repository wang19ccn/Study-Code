class Page {
    constructor() {
        this.curPage = 1
        this.PageSize = 3
    }

    setCurPage(curPage) {
        this.curPage = curPage
    }
}

export default new Page()