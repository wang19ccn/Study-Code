import SMERouter from 'sme-router'

const router = new SMERouter('root')

// 如果在appjs中，自己渲染dom节点的方法
// $('#root').html(html)

import index from '../controllers/index'
import signin from '../controllers/signin'

// 路由守卫
router.use((req) => {
    console.log(1)
    // 打开第一个页面
    $.ajax({
        url: '/api/users/isAuth',
        dataType: 'json',
        headers: {
            'X-Access-Token': localStorage.getItem('lg-token') || ''
        },
        success(result) {
            console.log(1)
            if (result.ret) {
                router.go('/index')
            } else {
                router.go('/signin')
            }
        }
    })
})

router.route('/', () => { })

router.route('/signin', signin(router))

router.route('/index', index(router))

// router.route('/signin', signin(router))

export default router