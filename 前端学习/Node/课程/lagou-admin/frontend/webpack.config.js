const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require("copy-webpack-plugin")
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
    // 配置环境
    mode: 'development',

    // 
    devtool: 'source-map',

    // 配置入口
    entry: {
        'js/app': './src/app.js'
    },

    // 配置出口
    output: {
        path: path.join(__dirname, './dist'),
        filename: '[name]-[hash:6].js'
    },

    // 
    module: {
        rules: [{
            test: /\.art$/,
            exclude: /(node_modules)/,
            use: {
                loader: 'art-template-loader'
            }
        }, {
            test: /\.css$/,
            // 从后向前解析
            loaders: ['style-loader', 'css-loader']
        }]
    },

    // 配置插件
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, './public/index.html'),
            filename: 'index.html',
            inject: true
        }),
        // 负责整合public中的静态文件打包
        new CopyPlugin({
            patterns: [
                {
                    from: './public/*.ico',
                    to: path.join(__dirname, './dist/favicon.ico'),
                },
                {
                    from: './public/libs',
                    to: path.join(__dirname, './dist/libs'),
                }
            ]
        }),
        // 把打包之前的文件清楚
        new CleanWebpackPlugin()
    ],

    // 配置server
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 8080,
        // 代理（非常重要）
        proxy: {
            "/api": {
                target: "http://localhost:3000",
            }
        }
    },

}