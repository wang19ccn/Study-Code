# 一 数据库操作

# 查询当前数据库

dp

# 查询所有数据库

show dbs

# 创建/切换数据库

use <db_name>

# 删除数据库

db.dropDatabase()

# 显示当前数据库的状态

db.stats()

# 查看当前数据库的版本

db.version()

# 查看当前数据库的链接机器地址

db.getMongo()

# 二 集合操作

# 创建一个集合

db.createCollection("collName")
给定容量：
db.createCollection("collName",{size:20,capped:true,max:100})
db.collName.isCapped() //判断集合是否为定容量

# 得到指定名称的集合 (了解)

db.getCollection("collName")

# 得到当前 db 的所有集合

db.getCollectionNames()

# 显示当前 db 所有集合的状态 (了解)

db.printCollectionStats()

# 文档操作

# 增删改查

# （1）添加

插入数据
db.<collName>.insert([{name:'m1',release:'2020-12-05"}])
db.<collName>.insert([{name:'m2',release:'2020-12-06"},{name:'m3',release:'2020-12-07"}])
db.<collName>.save([{name:'m4',release:'2020-12-08"},{name:'m5',release:'2020-12-09"}])
db.<collName>.insert([{name:'m1',release:'2020-12-05",publishNum:100}])

# （2）修改

db.<collName>.updata(条件),{$set:{}} set-修改
db.<collName>.updata({name:'m1'}),{$set:{release:'2020-12-02'}}
db.<collName>.updata(条件),{$set:{}} inc-增加
db.<collName>.updata({name:'m1'}),{$inc:{publisheNume:100}}
第一个 bool 型参数，如果未查到是否增加
db.<collName>.updata({name:'m1'}),{$inc:{publisheNume:100},true}
第二个 bool 型参数，是否把所有匹配到的修改
db.<collName>.updata({name:'m1'}),{$inc:{publisheNume:100},true,true}

# （3）删除

db.<collName>.remove({name:'m1000'})

# （4）查询

db.<collName>.find() 查询所有记录
db.<collName>.distinct('name') 查询去重后数据
db.<collName>.find({release:'2020-12-05'}) 查询 release = '2020-12-05' 的记录
db.<collName>.find({release:{$gt:'2020-12-05'}}) 查询 release > '2020-12-05' 的记录
db.<collName>.find({release:{$gte:'2020-12-05'}}) 查询 release >= '2020-12-05' 的记录
db.<collName>.find({release:{$lt:'2020-12-05'}}) 查询 release < '2020-12-05' 的记录
db.<collName>.find({release:{$lte:'2020-12-05'}}) 查询 release <= '2020-12-05' 的记录
db.<collName>.find({release:{$gte:'2020-12-05',$lte:'2020-12-05'}}) 查询 release >= '2020-12-05' 并且 age <= 26
db.<collName>.find({name:/^1/}) 查询 name 中以 1 开头的数据
db.<collName>.find({name:/1$/}) 查询 name 中以 1 结尾的数据
db.<collName>.find({},{_id:0,publishNum:0}) 查询指定列的数据 0-隐藏 1-显示
db.<collName>.find({name:/1$/},{\_id:0,publishNum:0}) 查询指定的列，且符合特定条件
db.<collName>.find().sort({release:1}) 升序
db.<collName>.find().sort({release:-1}) 降序
db.<collName>.find().sort({release:1}).limit(3).skip(6) 升序排序，取三条，跳过 6 条
db.<collName>.find().limit(3).skip(6).sort({release:1}) 取三条，跳过 6 条，升序排序
db.<collName>.find({$or:[{release:'2020-12-04'},{release:'2020-12-05'}]} or 与 查询
db.<collName>.findOne() 查询第一条数据
db.<collName>.find({name:'m1'}).count() 查询某个结果集的记录条数
