const http = require('http')

const server = http.createServer((Request,Response)=>{
    let url = Request.url
    Response.write(url)
    Response.end()
})

server.listen(8090,'localhost',()=>{
    console.log('localhost:8090')
})