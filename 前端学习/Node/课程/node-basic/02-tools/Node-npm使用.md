# 全局安装/卸载 package
- npm install forever --global (-g)
- npm uninstall forever --global

# 初始化 package.json
- npm init -y

# 项目中（局部）安装 package
- 开发环境 npm install underscore --save-dev （-D）
- 生产环境 npm install underscore --save （-S）

# node package version
- 13.4.6
- major:13（主版本号）, minor:4（次版本号）, patch:6（补丁号，偶数稳定，奇数测试）

# npm 版本号
- ^（版本号） 锁定主版本号
- ~（版本号） 锁定主版本号、次版本号
-  （版本号） 无符号 锁定所有版本号包括补丁（严格锁死）
- *          默认为最新版本
# 涉及版本的命令
- npm view [package-name] versions 查看该包全部版本号
- npm outdated 查看存在过期版本的包
- npm update 升级包

# 清除缓存
- npm cache clean --force

# 模块发布
- package.json 中 main 定义的文件为 暴露文件
- npm publish 上传包
- npm unpublish 卸载包
- 注意坑：npm源
