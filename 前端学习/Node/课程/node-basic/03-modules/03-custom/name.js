// 1.定义模块
// 2.暴露模块接口
// 3.引用模块
// 4.调用模块

const name = {
    surname: 'zhang',
    sayName() {
        console.log(this.surname)
    }
}

const age = {
    age: 100
}

// module.exports 只能在一个文件里出现一次
// module.exports = {
//     name,
//     age
// }

// const exports = module.exports

// exports.name = name
// exports.age = age

// 会相当于切断之前 赋值给 exports 的 module.exports 
// 重新赋值给 exports 会报错
// exports = {
//     name,
//     age
// }

// exports.default = {

// }

// module.exports = {
//     default
// }