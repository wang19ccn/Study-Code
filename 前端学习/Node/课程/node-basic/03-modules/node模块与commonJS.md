# CommonJS 与 ES6 的区别

- 1.应用场景

- CommonJS 只适用于服务端（浏览器不提供），也就是运行在nodejs中
- ES6 适用于浏览器，在语言标准层面实现了模块功能， 完全可以取代CommonJS（nodejs目前主流还是采用CommonJS规范）。

不过在v13.2版本，nodejs已经实现了ES6模块语法，还未正式替换，在考察阶段。v13.2版本将js文件以 .mjs结尾，nodejs将它视为ES6模块。以 .cjs结尾则视为CommonJS模块。也可以在包的package.json文件中增加 "type": "module"信息。nodejs则将整个包都视为ES6模块来加载运行。

- 2.使用方式

- CommonJS

引入使用：require("path")

1.如果是第三方模块 只需要填入模块名

2.自己定义的模块 需要使用相对路径或者绝对路径

导出模块使用：exports.xxx 或 module.exports.xxx 或 module.exports=xxx 或 this.xxx

！！不管是使用 exports 还是 this  ，都需要用点语法的形式导出，因为 他们两个是module.exports的指针 重新赋值将会切断关联

- ES6 模块

默认导出   export default 变量或者函数或者对象

默认引入  import name from "相对或绝对路径" 

导出的名字和引入的名字可以不一致

按需导出  export 需要声明 变量用const var let  函数用function

按需引入  import {变量名或函数名} form "路径"    

全部引入 使用  import * as 自定义name "路径"

会将默认导出和按需导出 全部引入
