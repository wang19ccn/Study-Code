const logger = require('../../utils/log')
const querystring = require('querystring')

const http = require('http')
const https = require('https')

const server = http.createServer((request, response) => {
    const url = request.url

    // logger.debug(response)
    // debugger; // 断点

    // let data = ''
    // request.on('data', (chunk) => {
    //     data += chunk
    // })
    // request.on('end', () => {
    //     response.writeHead(200, {
    //         // 默认返回html：'content-type': 'text/html'
    //         // 返回文本：'content-type': 'text/plain',
    //         // 返回json字符串：'content-type': 'application/json;charset=uft-8',
    //         'content-type': 'application/json;charset=uft-8',
    //     })
    //     // 写法一
    //     // response.write(`{"url":"${url}"}`)
    //     response.write(JSON.stringify(querystring.parse(data)));
    //     response.end()
    //     // 写法二
    //     // response.end('{"x":100}')
    // })

    https.get('https://www.xiaomiyoupin.com/mtop/mf/cat/list', (result) => {
        let data = ''
        result.on('data', (chunk) => {
            data += chunk
        })
        result.on('end', () => {
            response.writeHead(200, {
                'content-type': 'application/json;charset=uft-8',
            })
            response.write(data);
            response.end()
        })
    })

})

server.listen(8080, () => {
    console.log('localhost:8080')
})

// node --inspect server.js 打开nodejs检查器
// node --inspect --inspect-brk server.js 
// 浏览器中打开 edge://inspect/#devices