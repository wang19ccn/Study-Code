const http = require('http')
const querystring = require('querystring')

const postData = querystring.stringify({
    province: '上海',
    city: '上海',
    district: '宝山区',
    address: '同济支路199号智慧七立方3号楼2-4层',
    latitude: 43.0,
    longitude: 160.0,
    message: '求购一条小鱼',
    contact: '13666666',
    type: 'sell',
    time: 1571217561
})

const options = {
    // 选择协议，注意冒号
    protocol: 'http:',
    // 主机名
    hostname: 'localhost',
    // 方法
    method: 'post',
    // 端口号
    port: 3000,
    // 路径
    path: '/data',
    // headers配置
    headers: {
        // 首部字段
        'content-type': 'application/x-www-form-urlencoded',
        // 数据长度
        'Content-Length': Buffer.byteLength(postData)
    }
}

const server = http.createServer((req, res) => {
    const request = http.request(options, (result) => {

    })
    // 给后端数据
    request.write(postData)
    // 告诉后端传输结束
    request.end()

    res.end()
})

server.listen(8080, () => {
    console.log('localhost:8080')
})

