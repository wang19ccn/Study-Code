const fs = require('fs')

// 版本10以上
const fsPromises = require('fs').promises

// 错误优先的回调
// 异步操作（鲜明特点：回调）
// I/0操作 用回调函数

// --------------文件夹 操作-----------------------

// 创建文件夹 C
// fs.mkdir('logs',(err) => {
//     if(err) throw err
//     console.log('文件夹创建成功。')
// })

// 修改文件/文件夹名字 U
// fs.rename('./logs','./log',() => {
//     console.log('文件夹名字修改成功。')
// })

// 删除文件夹（目录） D
// fs.rmdir('./log', () => {
//     console.log('文件夹删除成功。')
// })

// 查询(读取) R
// fs.readdir('logs', (err,result) => {
//     if (err) throw err
//     console.log(result)
// })

// --------------文件 操作-----------------------

// 创建
// fs.writeFile('./logs/log1.log', 'hello\nworld', (err) => {
//     console.log('done.')
// })

// 修改
// fs.appendFile('./logs/log1.log', '!!!', (err) => {
//     console.log('done.')
// })

// 删除
// fs.unlink('./logs/log1.log', (err) => {
//     console.log('done.')
// })

// 查询
// 数据流->字符 方法一
// fs.readFile('./logs/log1.log', 'utf-8', (err, content) => {
//     // content -> <Buffer 68 65 6c 6c 6f 0a 77 6f 72 6c 64> 数据流
//     console.log(content)
// })

// 数据流->字符 方法二
// fs.readFile('./logs/log1.log', (err, content) => {
//     // content -> <Buffer 68 65 6c 6c 6f 0a 77 6f 72 6c 64> 数据流
//     console.log(content.toString())
// })

// --------------异步 同步-----------------------

// fs.readFile('./logs/log1.log', (err,content) => {
//     console.log(content.toString())
// })

// const content = fs.readFileSync('./logs/log1.log')
// console.log(content.toString())
// console.log('continue...')

// -------------- promises -----------------------

// 立即调用函数表达式
// ;(async() => {
//     let result = await fsPromises.readFile('./logs/log1.log')
//     console.log(result.toString())
// })()
// 分号注意事项：如果不加分号，注意（ 和 [ 作为第一行时要加分号

// -------------- 批量操作 -----------------------

// 批量创建文件
// for (var i = 0; i < 10; i++) {
//     fs.writeFile(`./logs/log-${i}.log`, `log-${i}`, (err) => {
//         console.log('done.')
//     })
// }

// 批量读取文件
// function readDir(dir){
//     fs.readdir(dir, (err, content) => {
//         content.forEach((value, index) => {
//             let joinDir = `${dir}/${value}`
//             fs.stat(joinDir, (err, stats) => {
//                 if(stats.isDirectory()){
//                     readDir(joinDir)
//                 }else{
//                     fs.readFile(joinDir,(err,content)=>{
//                         console.log(content.toString())
//                     })
//                 }
//             })
//         })
//     })
// }

// readDir('./')

// -------------- watach 方法 -----------------------

fs.watch('./logs/log-0.log', (err) => {
    console.log('file has changed!')
})

