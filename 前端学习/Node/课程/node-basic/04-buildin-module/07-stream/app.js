const fs = require('fs')
const zlip = require('zlib')

const gzip = zlip.createGzip()

const readStream = fs.createReadStream('./logs.txt')
const writeStream = fs.createWriteStream('./logs2.gzip')

readStream
    .pipe(gzip)
    .pipe(writeStream)

// WriteStream.write(readStream)