# node的浏览器端调试
- node --inspect server.js 打开nodejs检查器
- node --inspect --inspect-brk server.js 
- 浏览器中打开 edge://inspect/#devices

# node进程管理工具
- supervisor
- nodemon（本地端）  nodemon server.js
- forever
- pm2（服务器端重点）