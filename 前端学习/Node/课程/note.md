# express temple

-   ejs
-   pug
-   jade
-   art-template

# 页面 render

-   SSR （Server Side Render）
-   CSR （Client Side Render）

# mongodb

robo 3T

# node.js 项目

### 前端（Frontend）

-   前端工程化环境（webpack）
-   CSS 预处理工具 （sass）
-   JS 模块化：ES Module，CommonJS Module
-   JS 库：jQuery
-   SPA：single page application，路由：SME-Router
-   UI 组件库：BootStrap(AdminLTE)
-   RMVC:Art-templalte

### 后端（backend）

-   Node.js
-   Express
-   MongoDB(mongoose)
-   EJS
-   jwt（json web token）
-   RMVP

### 开发架构

-   前后端分离的架构
