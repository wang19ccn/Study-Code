import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: { //提供唯一的公共数据源，所有共享的数据都要统一放到Store的State中进行存储
    count:0
  },
  //只有 mutations 中定义的函数，才有权力修改 state 中的数据
  mutations: { //用于变更Store中的数据
    add(state){ // 不要在 mutations 函数中，执行异步操作
      state.count++
      // setTimeout(() => {
      //   state.count++
      // },1000)
    },
    addN(state,step){
      state.count += step
    },
    sub(state){
      state.count--
    },
    subN(state,step){
      state.count -= step
    },
  },
  actions: {
    addAsync(context){
      setTimeout(() => {
        context.commit('add')
      }, 1000);
    },
    addNAsync(context,step){
      setTimeout(() => {
        context.commit('addN',step)
      }, 1000);
    },
    subAsync(context){
      setTimeout(() => {
        context.commit('sub')
      }, 1000);
    },
    subNAsync(context,step){
      setTimeout(() => {
        context.commit('subN',step)
      }, 1000);
    },
  },
  getters: {
    showNum(state){
      return '当前最新的数量是'+state.count
    }
  },
  modules: {
  }
})

/*
组件中
*/