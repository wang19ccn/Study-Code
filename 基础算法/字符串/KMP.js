/**
 * 计算前缀表
 * @param {*} pattern 模式串
 * @param {*} prefix 前缀表
 * @param {*} n 模式串长度
 */
var prefix_table = function (pattern, prefix, n) {
    prefix[0] = 0 // 规定第一位等于 0
    let len = 0 // 比较时使用的长度
    let i = 1 // 第一个位置已经为0，从1开始
    while (i < n) {
        if (pattern[i] == pattern[len]) {
            len++
            prefix[i] = len
            i++
        } else {
            if (len > 0) len = prefix[len - 1]
            else {
                prefix[i] = 0
                i++
            }
        }
    }
    // console.log(move_prefix_table(prefix, n))
}

/**
 * 移动前缀表，去掉最后一位，并后移，空缺的第一位补为-1
 * @param {*} prefix 
 * @param {*} n 
 */
var move_prefix_table = function (prefix, n) {
    for (let i = n - 1; i > 0; i--) {
        prefix[i] = prefix[i - 1]
    }
    prefix[0] = -1
    // console.log(prefix)
}

/**
 * KMP算法
 * @param {*} text 主串
 * @param {*} pattern 模式串
 */
var kmp_search = function (text, pattern) {
    let m = text.length
    let n = pattern.length
    let prefix = new Array(n)
    prefix_table(pattern, prefix, n)
    move_prefix_table(prefix, n)
    console.log(prefix)

    // 规定
    // text[i]        长度  len(text)  m
    // pattern[j]     长度  len(pattern)  n
    let i = 0
    let j = 0
    while (i < m) {
        if (j == n - 1 && text[i] == pattern[j]) {
            console.log("Found pattern at" + (i - j))
            j = prefix[j]
        }
        if (text[i] == pattern[j]) {
            ++i
            ++j
        } else {
            j = prefix[j]
            if (-1 == j) {
                i++
                j++
            }
        }
    }

}

// console.log(prefix_table("ABABCABAA", [], 9))

// console.log(kmp_search("ABABABCABAABABABAB", "ABABCABAA"))
// console.log(kmp_search("ABABABCABAABABCABAA", "ABABCABAA"))
console.log(kmp_search("ABABABABCABAAB", "ABABCABAA"))