var preorderTraversal = function (root) {
    let res = []
    var recursion = function (root) {
        if (!root) return
        res.push(root.val)
        recursion(root.left)
        recursion(root.right)
    }
    recursion(root)
    return res
};
