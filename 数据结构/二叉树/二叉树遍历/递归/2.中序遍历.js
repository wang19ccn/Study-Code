var inorderTraversal = function (root) {
    let res = []
    var recursion = function (root) {
        if (!root) return
        recursion(root.left)
        res.push(root.val)
        recursion(root.right)
    }
    recursion(root)
    return res
};
