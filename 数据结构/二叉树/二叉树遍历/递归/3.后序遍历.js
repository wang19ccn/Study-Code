var postorderTraversal = function (root) {
    let res = []
    var recursion = function (root) {
        if (!root) return
        recursion(root.left)
        recursion(root.right)
        res.push(root.val)
    }
    recursion(root)
    return res
};