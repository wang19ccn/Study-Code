/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrder = function (root) {
    let res = new Array()
    if (root) {
        dfs(res, root, 0)
    }
    return res
};

var dfs = function (res, node, level) {
    if (res.length - 1 < level) {
        res.push([])
    }
    res[level].push(node.val)
    if (node.left != null) {
        dfs(res, node.left, level + 1)
    }
    if (node.right != null) {
        dfs(res, node.right, level + 1)
    }
}