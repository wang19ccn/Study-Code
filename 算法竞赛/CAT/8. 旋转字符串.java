public class newSolution {
    /**
     * @param str:    An array of char
     * @param offset: An integer
     * @return: nothing
     */
    public static void rotateString(char[] str, int offset) {
        // write your code here
        int len = str.length;
        if (str == null || str.length == 0) return;

        offset %= len;
        reverse(str, 0, len - offset - 1);
        System.out.println(str);
        reverse(str, len - offset, len - 1);
        System.out.println(str);
        reverse(str, 0, len - 1);
        System.out.println(str);
    }

    private static void reverse(char[] str, int start, int end) {
        for (int i = start, j = end; i < j; i++, j--) {
            char temp = str[j];
            str[j] = str[i];
            str[i] = temp;
        }
    }

    public static void main(String[] args) {
        char[] arr = {'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        rotateString(arr, 3);
        System.out.println(arr);
    }
}