/**
 * @param {number[]} coins
 * @return {number}
 */
var minCount = function (coins) {
    let sum = 0
    for (let coin of coins) {
        sum += parseInt(coin / 2) + coin % 2
        // sum += parseInt((coin + 1) / 2)
    }
    return sum
};

console.log(minCount([4, 2, 1]))