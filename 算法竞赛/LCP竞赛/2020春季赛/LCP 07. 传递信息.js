/**
 * @param {number} n
 * @param {number[][]} relation
 * @param {number} k
 * @return {number}
 */
var numWays = function (n, relation, k) {
    let dp = new Array(k + 1)
    for (let i = 0; i <= k; i++) {
        dp[i] = new Array(n).fill(0)
    }

    dp[0][0] = 1
    for (let i = 0; i < k; i++) {
        for (let r of relation) {
            dp[i + 1][r[1]] += dp[i][r[0]]
        }
    }

    return dp[k][n - 1]
};