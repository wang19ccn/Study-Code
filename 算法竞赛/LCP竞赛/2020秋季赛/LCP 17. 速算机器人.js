/**
 * 参赛作答
 * @param {string} s
 * @return {number}
 */
var calculate = function(s) {
    let len = s.length
    let x = 1
    let y = 0
    for(let i = 0; i<len;i++){
        if(s[i]=='A'){
            x = 2*x+y
        }else{
            y = 2*y +x
        } 
    }
    return x + y
};