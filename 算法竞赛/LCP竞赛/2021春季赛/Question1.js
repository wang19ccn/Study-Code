/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var purchasePlans = function (nums, target) {
    let count = 0
    nums.sort((a, b) => a - b)

    let left = 0
    let right = nums.length - 1
    while (left < right) {
        if (nums[left] + nums[right] <= target) {
            count += right - left
            left++
        } else {
            right--
        }
    }

    return count % 1000000007
};


console.log(purchasePlans([2, 2, 1, 9], 10))