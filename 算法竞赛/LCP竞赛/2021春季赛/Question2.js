/**
 * @param {number} num
 * @param {number} xPos
 * @param {number} yPos
 * @return {number}
 */
var orchestraLayout = function (num, xPos, yPos) {
    let grid = new Array(num)
    for (let i = 0; i < num; i++) {
        grid[i] = new Array(num)
    }

    let count = 1
    let left = 0, right = num - 1, top = 0, bottom = num - 1
    while (left <= right && top <= bottom) {
        // 上
        for (let i = left; i <= right; i++) {
            if (count == 9) {
                grid[top][i] = 9
            } else {
                grid[top][i] = count % 9
            }
            count++
        }
        // 右
        for (let i = top + 1; i <= bottom; i++) {
            if (count == 9) {
                grid[i][right] = 9
            } else {
                grid[i][right] = count % 9
            }
            count++
        }
        if (left < right && top < bottom) {
            // 下
            for (let i = right - 1; i > left; i--) {
                if (count == 9) {
                    grid[bottom][i] = 9
                } else {
                    grid[bottom][i] = count % 9
                }
                count++
            }
            // 左
            for (let i = bottom; i > top; i--) {
                if (count == 9) {
                    grid[i][left] = 9
                } else {
                    grid[i][left] = count % 9
                }
                count++
            }
        }
        [left, right, top, bottom] = [left + 1, right - 1, top + 1, bottom - 1];
    }

    return grid[xPos][yPos]
};

console.log(orchestraLayout(90000, 400, 201))