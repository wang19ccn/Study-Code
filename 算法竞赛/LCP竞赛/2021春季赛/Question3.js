/**
 * @param {number[]} nums
 * @return {number}
 */
var magicTower = function (nums) {
    if (nums.length == 0) return -1

    let res = 0
    let count = 0

    dfs(nums, 0, count, res)
    return res
};

var dfs = function (nums, start, count, res) {
    if (start == nums.length) {
        res = Math.min(count, res)
        return res
    }

    for (let i = start; i < nums.length; i++) {
        if (nums[i] > 0) {
            life += nums[i]
        } else if (nums[i] < 0) {
            life += nums[i]
            if (life < 0) {
                count++
            } else {
                dfs(nums, i + 1, count, res)
            }
        }
    }


};