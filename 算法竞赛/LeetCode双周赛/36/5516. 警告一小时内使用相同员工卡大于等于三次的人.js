/**
 * @param {string[]} keyName
 * @param {string[]} keyTime
 * @return {string[]}
 */
var alertNames = function (keyName, keyTime) {
    let map = new Map()
    let arr = []
    let res = []
    for (let i = 0; i < keyName.length; i++) {
        if (map.has(keyName[i])) {
            let arr = map.get(keyName[i])
            arr.push(keyTime[i])
            map.set(keyName[i], arr)
        } else {
            arr.push(keyName[i])
            map.set(keyName[i], [keyTime[i]])
        }
    }
    for (let a of arr) {
        let cur = map.get(a)
        for (let i = 0; i < cur.length; i++) {
            cur[i] = divid(cur[i])
        }
        //console.log(cur)
        let count = 1
        for (let i = 0; i < cur.length; i++) {
            let t1 = cur[i][0]
            let t2 = cur[i][1]
            for (let j = 0; j < cur.length; j++) {
                if (j == i) continue
                if (t1 == cur[j][0] && t2 <= cur[j][1]) {
                    count++
                }
                if (t1 + 1 == cur[j][0] && t2 >= cur[j][1]) {
                    count++
                }
                // if (t1 - 1 == cur[j][0] && t2 <= cur[j][1]) {
                //     count++
                //     continue
                // }
                if (count == 3) {
                    res.push(a)
                    break
                }
            }
            if (count == 3) {
                break
            }
            count = 1
        }
    }
    res.sort()
    return res
};

var divid = function (str) {
    let arr = str.split("")
    let res = []
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] == ":") {
            res[0] = Number(arr.slice(0, i).join(''))
            res[1] = Number(arr.slice(i + 1).join(''))
        }
    }
    return res
}

console.log(alertNames(["leslie", "leslie", "leslie", "clare", "clare", "clare", "clare"],
    ["13:00", "13:20", "14:00", "18:00", "18:51", "19:30", "19:49"]))

console.log(alertNames(["a", "a", "a", "a", "a", "a", "a", "b", "b", "b", "b", "b", "c", "c", "c", "c", "c", "d", "d", "d", "d", "d", "d"],
    ["19:15", "14:31", "14:57", "07:49", "16:21", "12:52", "12:08", "02:51", "14:40", "16:19", "03:12", "02:05", "05:03", "14:41", "00:33", "08:41", "06:22", "13:43", "03:42", "05:44", "10:27", "13:17", "15:35"]))

console.log(alertNames(["b", "b", "b", "b", "b"], ["02:51", "14:40", "16:19", "03:12", "02:05"]))