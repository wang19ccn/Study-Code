/**
 * @param {number[]} nums
 * @return {boolean}
 */
// var canBeIncreasing = function (nums) {
//     let count = 0
//     if (nums.length == 2) return true
//     for (let i = 1; i < nums.length; i++) {
//         if (nums[i - 1] >= nums[i]) {
//             count++
//             if ((i >= 2 && nums[i - 2] >= nums[i]) || count >= 2) {
//                 return false
//             }
//         }
//     }
//     return true
// };


// var canBeIncreasing = function (nums) {
//     let flag = true

//     if (nums.length == 2) return true
//     for (let i = 1; i < nums.length; i++) {
//         if (nums[i - 1] >= nums[i]) {
//             flag = false
//             if ((i >= 2 && nums[i - 2] < nums[i]) || i == 1) {
//                 for (let j = i + 1; j < nums.length; j++) {
//                     if (nums[i - 1] >= nums[i]) {
//                         flag = false
//                         break
//                     }
//                     flag = true
//                 }
//             } else {
//                 if (!flag && nums[i - 1] < nums[i + 1]) {
//                     for (let j = i + 2; j < nums.length; j++) {
//                         if (nums[i - 1] >= nums[i]) {
//                             flag = false
//                             break
//                         }
//                     }
//                     flag = true
//                 }
//             }
//             break
//         }
//     }
//     return flag
// };


/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canBeIncreasing = function (nums) {
    if (nums.length == 2) return true
    let stack = [nums[0]]
    let i = 1
    while (i < nums.length) {
        if (stack[stack.length - 1] < nums[i]) {
            stack.push(nums[i])
        } else {
            if (stack[stack.length - 1] < nums[i + 1] || i == nums.length - 1) {
                if ((i + 1) == nums.length - 1 || i == nums.length - 1) return true
                for (let j = i + 2; j < nums.length; j++) {
                    if (nums[j - 1] > nums[j]) {
                        break
                    }
                    if (j == nums.length - 1 && nums[j - 1] < nums[j]) {
                        return true
                    }
                }
            }

            let count = 0
            while (stack.length != 0 && stack[stack.length - 1] > nums[i]) {
                stack.pop()
                count++
                if (count > 1) return false
            }
            for (let j = i + 1; j < nums.length; j++) {
                if (nums[j - 1] > nums[j]) {
                    return false
                }
                if (j == nums.length - 1 && nums[j - 1] < nums[j]) {
                    return true
                }
            }
            return false
        }
        i++
    }
    return true
};

console.log(canBeIncreasing([89, 384, 691, 680, 111, 756]))
console.log(canBeIncreasing([23, 297, 427, 949, 945]))
console.log(canBeIncreasing([105, 924, 32, 968]))
console.log(canBeIncreasing([1, 2, 3]))
console.log(canBeIncreasing([1, 1]))
console.log(canBeIncreasing([2, 3, 1, 2]))
