/**
 * @param {string} s
 * @param {string} part
 * @return {string}
 */
var removeOccurrences = function (s, part) {
    let visit = []
    let arr = s.split('')
    let match = part.split('')

    let len = part.length

    let i = 0
    while (i < arr.length) {
        if (arr[i] == part[0]) {
            let j = i + 1
            for (; j < arr.length && (j - i) < len; j++) {
                if (arr[j] != match[j - i]) {
                    visit.push(i)
                    break
                }
            }
            if ((j - i) == len) arr.splice(i, len)
        }
        i++
    }
    console.log(arr)
    console.log(visit)

    for (let i = visit.length - 1; i >= 0; i--) {
        let index = visit[i]
        if (arr[index] == part[0]) {
            let j = index + 1;
            for (; j < arr.length && j < len; j++) {
                if (arr[j] != match[j - index]) {
                    visit.push(index)
                }
            }
            if ((j - index) == len) arr.splice(index, len)
        }
    }

    return arr.join('')
}

console.log(removeOccurrences("daabcbaabcbc", "abc"))