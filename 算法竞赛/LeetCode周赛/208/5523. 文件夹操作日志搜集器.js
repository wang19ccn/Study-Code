/**
 * @param {string[]} logs
 * @return {number}
 */
var minOperations = function (logs) {
    let count = 0
    for (let log of logs) {
        if (log == "../") {
            if (count == 0) {
                continue
            }
            count--
        }
        else if (log == './') {
            continue
        }
        else {
            count++
        }
    }
    return count
};

console.log(minOperations(["d1/", "d2/", "../", "d21/", "./"]))