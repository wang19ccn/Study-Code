/**
 * @param {number[]} customers
 * @param {number} boardingCost
 * @param {number} runningCost
 * @return {number}
 */
var minOperationsMaxProfit = function (customers, boardingCost, runningCost) {
    let max = -Infinity
    let income = 0
    let count = 0
    let minCount = 0
    let len = customers.length
    for (let i = 0; i < len; i++) {
        let num = parseInt(customers[i] / 4)
        let temp = customers[i] % (4 * num)

        if (num != 0 && i != len - 1) {
            customers[i] = 4
            customers[i + 1] += 4 * (num - 1) + temp
        }

        if (num != 0 && i == len - 1) {
            customers[i] = 4
            let j = 1
            for (; j < num; j++) {
                customers.splice(i + j, 0, 4)
            }
            if (temp != 0) {
                customers.splice(i + j + 1, 0, temp)
            }
            len = customers.length
        }

        count++
        income += customers[i] * boardingCost - runningCost
        if (max < income) {
            max = income
            minCount = count
        }
    }
    return max < 0 ? -1 : minCount
};

console.log(minOperationsMaxProfit([8, 3], 5, 6))
console.log(minOperationsMaxProfit([10, 9, 6], 6, 4))
console.log(minOperationsMaxProfit([3, 4, 0, 5, 1], 1, 92))
console.log(minOperationsMaxProfit([10, 10, 6, 4, 7], 3, 8))