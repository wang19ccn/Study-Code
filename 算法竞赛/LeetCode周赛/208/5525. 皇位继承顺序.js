/**
 * @param {string} kingName
 */
var ThroneInheritance = function (kingName) {
    this.children = new Map()
    this.dead = new Set()

    this.king = kingName
    this.curOrder = []

    this.dfs = function (u) {
        if (!this.dead.has(u)) {
            this.curOrder.push(u)
        }
        if (this.children.has(u)) {
            for (let v of this.children.get(u)) {
                this.dfs(v)
            }
        }
    }

};

/** 
 * @param {string} parentName 
 * @param {string} childName
 * @return {void}
 */
ThroneInheritance.prototype.birth = function (parentName, childName) {
    if (this.children.has(parentName)) {
        this.children.get(parentName).push(childName)
    } else {
        this.children.set(parentName, [childName])
    }
};

/** 
 * @param {string} name
 * @return {void}
 */
ThroneInheritance.prototype.death = function (name) {
    this.dead.add(name)
};

/**
 * @return {string[]}
 */
ThroneInheritance.prototype.getInheritanceOrder = function () {
    this.curOrder = []
    this.dfs(this.king)
    return this.curOrder
};

/**
 * Your ThroneInheritance object will be instantiated and called as such:
 * var obj = new ThroneInheritance(kingName)
 * obj.birth(parentName,childName)
 * obj.death(name)
 * var param_3 = obj.getInheritanceOrder()
 */
