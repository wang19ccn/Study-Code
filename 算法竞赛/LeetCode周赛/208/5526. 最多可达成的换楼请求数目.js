/**
 * @param {number} n
 * @param {number[][]} requests
 * @return {number}
 */
var maximumRequests = function (n, requests) {
    let nums = new Array(n + 1).fill(0)
    let res = 0

    var dfs = function (start, count) {
        if (check()) res = Math.max(res, count)
        for (let i = start; i < requests.length; i++) {
            // 回溯
            nums[requests[i][0]]--
            nums[requests[i][1]]++
            dfs(i + 1, count + 1)
            nums[requests[i][0]]++
            nums[requests[i][1]]--
        }
    }

    var check = function () {
        for (let i = 0; i < nums.length; i++) {
            if (nums[i] != 0) return false
        }
        return true
    }

    var main = function () {
        dfs(0, 0)
        return res
    }

    return main()
};