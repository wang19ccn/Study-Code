/**
 * @param {string} a
 * @param {string} b
 * @return {boolean}
 */
var checkPalindromeFormation = function (a, b) {
    let left = parseInt(a.length / 2 - 1)
    left = Math.min(check(a, a, left), check(b, b, left))
    left = Math.min(check(a, b, left), check(b, a, left))
    return left == -1
};

var check = function (str_left, str_right, left) {
    let right = str_left.length - 1 - left
    while (left >= 0 && right < str_left.length) {
        if (str_left[left] != str_right[right]) break
        left--
        right++
    }
    return left
};