/**
 * @param {string} a
 * @param {string} b
 * @return {boolean}
 */
var checkPalindromeFormation = function (a, b) {
    a = a.split("")
    b = b.split("")
    for (let i = 0; i <= a.length; i++) {
        a_pre = a.slice(0, i)
        a_suf = a.slice(i)
        b_pre = b.slice(0, i)
        b_suf = b.slice(i)
        if ((isHuiwen(a_pre.concat(b_suf))) || (isHuiwen(b_pre.concat(a_suf)))) {
            return true
        }
    }
    return false
};

var isHuiwen = function (s) {
    // let left = 0
    // let right = s.length - 1
    // while (left < right) {
    //     if (s[left] != s[right]) {
    //         return false
    //     }
    //     left++
    //     right--
    // }
    // return true
    let mid = parseInt(s.length / 2)
    let s1 = s.slice(0, mid)
    let s2
    if (s.length % 2 != 0) {
        s2 = s.slice(mid + 1)
    } else {
        s2 = s.slice(mid)
    }
    let str1 = s1.join("")
    let str2 = s2.reverse().join("")
    
    if (str1 == str2) {
        return true
    }
    return false
}

 console.log(checkPalindromeFormation("askxrrnhyddrlmcgymtichivmwyjfpyqqxmiimxqqypfjywmvihcitmygcmlryczoygimgii", "iigmigyozcyfxgfzkwpvjuxbjphbbmwlhdcavhtjhbpccsxaaiyitfbzljvhjoytfqlqrohv"))
// console.log(checkPalindromeFormation("ulacfd", "jizalu"))
// console.log(checkPalindromeFormation("cdeoo", "oooab"))
// console.log(isHuiwen(["u", "l", "a", "a", "l", "u"]))
// console.log(checkPalindromeFormation("x", "y"))