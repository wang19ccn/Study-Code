/**
 * 理解错题意
 * @param {string} s
 * @return {number}
 */
var maxDepth = function (s) {
    let leftCount = 0
    let rightCount = 0
    let res = 0
    for (let i = 0; i < s.length; i++) {
        if (!isNaN(Number(s[i]))) {
            let left = i - 1
            let right = i + 1
            leftCount = 0
            rightCount = 0
            for (; left >= 0; left--) {
                if (s[left] == "(") {
                    leftCount++
                }
                else if (s[left] == ")") {
                    leftCount--
                }
            }
            for (; right < s.length; right++) {
                if (s[right] == ")") {
                    rightCount++
                }
                else if (s[right] == "(") {
                    rightCount--
                }
            }
            let depth = Math.min(leftCount, rightCount)
            res = Math.max(res, depth)
        }
    }
    return res
};

console.log(maxDepth("((8+7)*(3+9)-0)*(1*6)"))