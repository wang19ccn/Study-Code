/**
 * @param {string} s
 * @return {number}
 */
var maxDepth = function (s) {
    let depth = 0
    let maxDepth = 0
    for (let c of s) {
        if (c == "(") {
            depth++
            maxDepth = Math.max(depth, maxDepth)
        }
        else if (c == ")") {
            depth--
        }
    }
    return maxDepth
};