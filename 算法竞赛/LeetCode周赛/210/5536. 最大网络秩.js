/**
 * @param {number} n
 * @param {number[][]} roads
 * @return {number}
 */
var maximalNetworkRank = function (n, roads) {
    let res = 0

    let arr = Array(n)
    for (let i = 0; i < n; i++) {
        arr[i] = []
    }
    for (let road of roads) {
        arr[road[0]].push(road[1])
        arr[road[1]].push(road[0])
    }

    for (let i = 0; i < n; i++) {
        let isVisit = new Array(n).fill(false)
        for (let k of arr[i]) {
            isVisit[k] = true
            res = Math.max(arr[i].length + arr[k].length - 1, res)
        }
        for (let j = i + 1; j < n; ++j) {
            if (!isVisit[j]) {
                res = Math.max(arr[i].length + arr[j].length, res)
            }
        }
    }

    return res
};

// console.log(maximalNetworkRank(8, [[0, 1], [1, 2], [2, 3], [2, 4], [5, 6], [5, 7]]))

console.log(maximalNetworkRank(5, [[0, 1], [0, 3], [1, 2], [1, 3], [2, 3], [2, 4]]))