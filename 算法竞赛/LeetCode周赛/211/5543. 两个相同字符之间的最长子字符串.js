/**
 * @param {string} s
 * @return {number}
 */
var maxLengthBetweenEqualCharacters = function (s) {
    let map = new Map()
    for (let i = 0; i < s.length; i++) {
        if (map.has(s[i])) {
            let arr = map.get(s[i])
            arr.push(i)
            map.set(s[i], arr)
        } else {
            map.set(s[i], [i])
        }
    }
    let max = -1
    map.forEach((value, key) => {
        if (value.length < 2) return
        let x = value.shift()
        let y = value.pop()
        max = Math.max(max, y - x - 1)
    })
    return max
};

console.log(maxLengthBetweenEqualCharacters("dabca"))