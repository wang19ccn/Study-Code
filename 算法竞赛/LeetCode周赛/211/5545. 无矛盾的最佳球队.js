/**
 * @param {number[]} scores
 * @param {number[]} ages
 * @return {number}
 */
var bestTeamScore = function (scores, ages) {
    let len = ages.length
    let info = []
    for (let i = 0; i < len; i++) {
        info[i] = {}
    }
    for (let i = 0; i < len; i++) {
        info[i].age = ages[i]
        info[i].score = scores[i]
    }
    info.sort(compare)
    // console.log(info)

    let dp = []
    let res = 0
    for (let i = 0; i < len; ++i) {
        dp[i] = info[i].score;
        for (let j = i - 1; j >= 0; --j) {
            if (info[j].score <= info[i].score) {
                dp[i] = Math.max(dp[j] + info[i].score, dp[i])
            }
        }
        res = Math.max(res, dp[i])
    }

    return res
};


function compare(a, b) {
    if (a.age < b.age) {
        return -1;
    }
    if (a.age > b.age) {
        return 1;
    }
    if (a.age = b.age) {
        if (a.score < b.score) {
            return -1;
        }
        if (a.score > b.score) {
            return 1;
        }
    }
    return 0;
}


console.log(bestTeamScore([1, 3, 5, 10, 15], [1, 2, 3, 4, 5]))
console.log(bestTeamScore([1, 2, 3, 5], [8, 9, 10, 1]))
console.log(bestTeamScore([9, 2, 8, 8, 2], [4, 1, 3, 3, 5]))
console.log(bestTeamScore([319776, 611683, 835240, 602298, 430007, 574, 142444, 858606, 734364, 896074], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]))
console.log(bestTeamScore([1, 3, 7, 3, 2, 4, 10, 7, 5], [4, 5, 2, 1, 1, 2, 4, 1, 4]))
