/**
 * @param {number[]} releaseTimes
 * @param {string} keysPressed
 * @return {character}
 */
var slowestKey = function (releaseTimes, keysPressed) {
    let max = releaseTimes[0]
    let res = []
    for (let i = 1; i < releaseTimes.length; i++) {
        max = Math.max(max, releaseTimes[i] - releaseTimes[i - 1])
    }
    if (releaseTimes[0] == max) {
        res.push(keysPressed[0])
    }
    for (let i = 1; i < releaseTimes.length; i++) {
        if (releaseTimes[i] - releaseTimes[i - 1] == max) {
            res.push(keysPressed[i])
        }
    }
    res.sort((a, b) => a - b)
    return res[res.length - 1]
};

console.log(slowestKey([23, 34, 43, 59, 62, 80, 83, 92, 97], "qgkzzihfc"))