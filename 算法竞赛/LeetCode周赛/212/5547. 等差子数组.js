/**
 * @param {number[]} nums
 * @param {number[]} l
 * @param {number[]} r
 * @return {boolean[]}
 */
var checkArithmeticSubarrays = function (nums, l, r) {
    let answer = []
    let agree = true
    for (let i = 0; i < l.length; i++) {
        let arr = nums.slice(l[i], r[i] + 1)
        arr.sort((a, b) => a - b)
        if (arr.length > 2) {
            for (let i = 1; i < arr.length - 1; i++) {
                if (arr[i] - arr[i - 1] != arr[i + 1] - arr[i]) {
                    answer.push(false)
                    agree = false
                    break
                }
            }
        }
        if (agree) {
            answer.push(true)
        }
        agree = true
    }
    return answer
};

console.log(checkArithmeticSubarrays([-12, -9, -3, -12, -6, 15, 20, -25, -20, -15, -10], [0, 1, 6, 4, 8, 7], [4, 4, 9, 7, 9, 10]))