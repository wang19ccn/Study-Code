/**
 * @param {number[][]} heights
 * @return {number}
 */
var minimumEffortPath = function (heights) {
    let dx = [-1, 0, 1, 0]
    let dy = [0, -1, 0, 1]
    let m = heights.length
    let n = heights[0].length
    // 边界判断
    var isBorder = function (x, y) {
        return x >= 0 && x < m && y >= 0 && y < n
    }
    // DFS
    var dfs = function (x, y, max, heights, visit) {
        if (x == m - 1 && y == n - 1) {
            return true
        }
        visit[x][y] = true
        for (let i = 0; i < 4; i++) {
            let new_x = x + dx[i]
            let new_y = y + dy[i]
            if (isBorder(new_x, new_y) && !visit[new_x][new_y] && Math.abs(heights[new_x][new_y] - heights[x][y]) <= max) {
                if (dfs(new_x, new_y, max, heights, visit)) return true
            }
        }
        return false
    };

    // 二分查找来确定值
    let left = 0
    let right = 1000000

    let visit = new Array(m)
    for (let i = 0; i < m; i++) {
        visit[i] = new Array(n).fill(false)
    }

    while (left < right) {
        let mid = parseInt(left + (right - left) / 2)
        for (let i = 0; i < m; i++) {
            visit[i] = new Array(n).fill(false)
        }
        if (!dfs(0, 0, mid, heights, visit)) { // 相邻最大值大于mid
            left = mid + 1
        } else {// 相邻最大值小于等于mid
            right = mid
        }
    }

    return left
}


console.log(minimumEffortPath([[1, 2, 2], [3, 8, 2], [5, 3, 5]]))

// 1 2 2
// 3 8 2
// 5 3 5