/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var swapNodes = function (head, k) {
    let cur = head
    let preFirst = head, first = head
    let preSecond = head, second = head

    let count = 0
    while (cur != null) {
        count++
        if (count == k - 1) preFirst = cur
        if (count == k) first = cur
        cur = cur.next
    }
    if (count == 1) return head
    if (count == 2) {
        let temp = head.next
        head.next.next = head
        head.next = null
        return temp
    }

    cur = head
    let newK = count - k + 1
    count = 0
    while (cur != null) {
        count++
        if (count == newK - 1) preSecond = cur
        if (count == newK) second = cur
        cur = cur.next
    }

    if (k != 1 && (count - k) != 0) {
        preFirst.next = second
        preSecond.next = first

        let temp = head
        temp = first.next
        first.next = second.next
        second.next = temp

        return head
    } else {
        if((count-k)==0){
            let t1 = second
            second = first
            first = t1
            preSecond = preFirst
        }
        
        let temp = head
        temp = first.next
        first.next = second.next
        second.next = temp

        preSecond.next = first

        return second
    }
};

// console.log(swapNodes(
//     head = {
//         val: 1,
//         next: { val: 2, next: { val: 3, next: { val: 4, next: { val: 5, next: null } } } }
//     }, 2
// ))
console.log(swapNodes(
    head = {
        val: 1,
        next: { val: 2, next: null }
    }, 1
))