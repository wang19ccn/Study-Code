/**
 * @param {number[]} encoded
 * @param {number} first
 * @return {number[]}
 */
var decode = function (encoded, first) {
    let len = encoded.length
    let arr = new Array(len + 1)

    arr[0] = first
    for (let i = 1; i < len + 1; i++) {
        arr[i] = encoded[i - 1] ^ arr[i - 1]
    }

    return arr
};

console.log(decode([6, 2, 7, 3], 4))

// 1 ^ 0 = 1
// 0 ^ 2 = 2
// 2 ^ 1 = 3