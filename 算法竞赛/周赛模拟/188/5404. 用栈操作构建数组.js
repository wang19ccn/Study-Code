/**
 * @param {number[]} target
 * @param {number} n
 * @return {string[]}
 */
var buildArray = function (target, n) {
    let x = 1;
    let res = [];

    let k = 0
    for (let i = 0; i < target[target.length - 1]; i++) {
        if (target[k] == x) {
            res.push("Push")
            k++
            x++
            continue
        }
        res.push("Push")
        res.push("Pop")
        x++
    }

    return res
};