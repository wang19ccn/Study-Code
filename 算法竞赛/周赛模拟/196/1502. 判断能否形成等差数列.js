/**
 * @param {number[]} arr
 * @return {boolean}
 */
var canMakeArithmeticProgression = function (arr) {
    arr.sort((a, b) => { return a - b })
    for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i] - arr[i + 1] != arr[0] - arr[1]) return false
    }
    return true
};

