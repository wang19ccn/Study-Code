/**
 * @param {number[]} nums
 * @return {number}
 */
var specialArray = function (nums) {
    let count = 0
    for (let i = 0; i <= nums.length; ++i) {
        for (let k = 0; k < nums.length; ++k) {
            if (nums[k] >= i) {
                count++
            }
        }
        if (count == i) return i
        count = 0
    }
    return -1
};