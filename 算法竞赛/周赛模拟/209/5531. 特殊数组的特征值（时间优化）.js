/**
 * @param {number[]} nums
 * @return {number}
 */
var specialArray = function (nums) {
    let count = nums.length
    nums.sort((a, b) => a - b)
    if (count <= nums[0]) return count
    count--
    for (let i = 1; i <= nums.length; ++i) {
        if (count > nums[i - 1] && count <= nums[i]) {
            return count
        } else {
            count--
        }
    }
    return -1
};

console.log(specialArray([3, 6, 7, 7, 0]))