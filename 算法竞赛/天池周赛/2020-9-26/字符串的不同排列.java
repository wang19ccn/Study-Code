
import com.sun.deploy.util.StringUtils;

import java.util.*;

public class Solution {
    /**
     * @param str: A string
     * @return: all permutations
     */
    public static List<String> stringPermutation(String str) {
        int len = str.length();
        List<String> res = new ArrayList<String>();

        if (len == 0) {
            res.add("");
            return res;
        }

        char[] arr = str.toCharArray();
        Arrays.sort(arr);
        str = String.valueOf(arr);

        boolean[] used = new boolean[len];
        Deque<String> path = new ArrayDeque<>(len);
        dfs(str, len, 0, used, path, res);

        return res;
    }

    private static void dfs(String str, int len, int depth, boolean[] used, Deque<String> path, List<String> res) {

        if (depth == len) {
            ArrayList<String> list = new ArrayList<String>(path);
            String newString = String.join("", list);
            res.add(newString);
            return;
        }

        for (int i = 0; i < len; ++i) {
            if (used[i]) {
                continue;
            }

            if (i > 0 && str.charAt(i) == str.charAt(i - 1) && !used[i - 1]) {
                continue;
            }

            path.addLast(str.charAt(i) + "");
            used[i] = true;

            dfs(str, len, depth + 1, used, path, res);
            used[i] = false;
            path.removeLast();
        }
    }

    public static void main(String[] args) {
        System.out.println(stringPermutation("abcdefg"));
    }
}