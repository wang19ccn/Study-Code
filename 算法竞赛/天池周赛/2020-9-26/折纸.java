public class new1Solution {
    // 找规律题
    // 0
    // 001
    // mid = 1 left = 0 right = 1
    // left + 0 + right + 0 + left + 1 + right
    // 0 0 1 0 0 1 1
    // 0010011

    /**
     * @param n: The folding times
     * @return: the 01 string
     */
    public String getString(int n) {
        String res = "0";
        for (int i = 2; i <= n; i++) {
            int mid = res.length() / 2;
            String left = res.substring(0, mid);
            String right = res.substring(mid + 1);
            res = left + "0" + right + "0" + left + "1" + right;
        }
        return res;
    }
}
