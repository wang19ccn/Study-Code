public class Solution {
    /**
     * @param values: a vector of integers
     * @return: a boolean which equals to true if the first player will win
     */
    public boolean firstWillWin(int[] values) {
        int len = values.length;

        int[][] dp = new int[len][len];

        for (int i = 0; i < len; i++) {
            dp[i][i] = values[i];
        }

        for (int i = len - 2; i >= 0; i--) {
            for (int j = i + 1; j < len; j++) {
                int pickA = values[i] - dp[i + 1][j]; // 选择左端
                int pickB = values[j] - dp[i][j - 1]; // 选择右端
                dp[i][j] = Math.max(pickA, pickB);
            }
        }

        return dp[0][len - 1] >= 0;
    }
}