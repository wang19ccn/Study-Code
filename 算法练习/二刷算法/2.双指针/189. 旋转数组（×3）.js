/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
// 方法1 超时 （通过37/37）？
var rotate = function (nums, k) {
    let count = k % nums.length
    for (let i = 0; i < count; i++) {
        let temp = nums.pop()
        nums.unshift(temp)
    }
    return nums
};

// 方法2 
var reverse = function (nums, start, end) {
    while (start < end) {
        [nums[start], nums[end]] = [nums[end], nums[start]];
        start += 1
        end -= 1
    }
};
var rotate = function (nums, k) {
    let count = k % nums.length
    // 7 6 5 4 3 2 1
    nums.reverse()
    // 5 6 7 4 3 2 1
    reverse(nums, 0, count - 1)
    reverse(nums, count, nums.length - 1)
};

console.log(rotate([1, 2, 3, 4, 5, 6, 7], 3))
console.log(rotate([-1, -100, 3, 99], 2))