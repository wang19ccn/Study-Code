/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
// 方法1：交换
var moveZeroes = function (nums) {
    let len = nums.length
    let left = 0
    let right = 0
    while (right < len) {
        if (nums[right]) {
            ;[nums[left], nums[right]] = [nums[right], nums[left]]
            left++
        }
        right++
    }
};

// 方法2：覆盖
var moveZeroes = function (nums) {
    let len = nums.length
    let count = 0
    for (let i = 0; i < len; i++) {
        if (nums[i] != 0) {
            nums[count] = nums[i]
            count++
        }
    }
    for (let i = count; i < nums.length; i++) {
        nums[i] = 0
    }
};

console.log(moveZeroes([0, 1, 0, 3, 12]))