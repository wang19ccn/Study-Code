/**
 * @param {string} s
 * @return {string}
 */
// 方法一 利用库函数
var reverseWords = function (s) {
    let s_arr = s.split(' ')
    for (let i = 0; i < s_arr.length; i++) {
        let temp = s_arr[i].split('')
        temp.reverse()
        s_arr[i] = temp.join('')
    }
    // return s.split("").reverse().join("").split(" ").reverse().join(" ")
    return s = s_arr.join(' ')
};

// 方法二 双指针
/**
 * @param {string} s
 * @return {string}
 */
var reverseWords = function (s) {
    let arr = s.split('')
    let i = 0
    while (i < arr.length) {
        let start = i
        while (i < arr.length && arr[i] != ' ') {
            ++i
        }
        let left = start
        let right = i - 1
        while (left < right) {
            ;[arr[left], arr[right]] = [arr[right], arr[left]]
            left++
            right--
        }
        i++
    }
    return arr.join('')
};

console.log(reverseWords("Let's take LeetCode contest"))