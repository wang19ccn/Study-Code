/**
 * @param {number[]} nums
 * @return {number[]}
 */
// 方法1 进阶 O(n) 的复杂度
var sortedSquares = function (nums) {
    for (let i = 0; i < nums.length; i++) {
        nums[i] *= nums[i]
    }

    let left = 0
    let right = nums.length - 1
    let stack = []
    while (left <= right) {
        if (nums[left] < nums[right]) {
            stack.push(nums[right])
            right--
        } else {
            stack.push(nums[left])
            left++
        }
    }

    return stack.reverse()
};

// 方法2 直接排序
var sortedSquares = function (nums) {
    for (let i = 0; i < nums.length; i++) {
        nums[i] *= nums[i]
    }
    return nums.sort((a, b) => a - b)
};

console.log(sortedSquares([-4, -1, 0, 3, 10]))