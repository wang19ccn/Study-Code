/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var permute = function(nums) {
    let len = nums.length;
    let res = [];
    if(len==0){
        return res;
    }

    let path=[]
    let used=new Array(len).fill(false)

    dfs(nums,len,0,path,used,res)

    return res;
};

var dfs = function(nums,len,depth,path,used,res){
    if(depth==len){
        res.push([...path])
        return;
    }

    for(let i = 0;i<len;i++){
        if(used[i]){
            continue;
        }
        path.push(nums[i]);
        used[i] = true;
        dfs(nums,len,depth+1,path,used,res);
        path.pop();
        used[i]=false;
    }
}