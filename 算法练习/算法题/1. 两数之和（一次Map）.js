/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    let myMap=new Map()

    for(let i=0;i<nums.length;i++){
        const key=target-nums[i]
        if(myMap.has(key)){
            return [myMap.get(key),i]
        }
        myMap.set(nums[i],i)
    }
};

console.log(twoSum([2, 7, 11, 15],9))
