/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (nums, target) {
    let myMap = new Map()

    for (let i = 0; i < nums.length; i++) {
        myMap.set(nums[i], i)
    }

    for (let i = 0; i < nums.length; i++) {
        const key = target - nums[i]
        if (myMap.has(key) && myMap.get(key) != i) {
            return [i,myMap.get(key)]
        }
    }
};

console.log(twoSum([3,2,4], 6))