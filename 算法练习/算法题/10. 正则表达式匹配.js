/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */
var isMatch = function (s, p) {
    if (s == null || p == null) return false

    let s_len = s.length
    let p_len = p.length

    let dp = new Array(s_len + 1)
    for (let i = 0; i < dp.length; i++) dp[i] = new Array(p_len + 1)

    dp[0][0] = true
    // s字符串前i个与p规则前0个，无法匹配
    for (let i = 1; i < s_len + 1; i++) dp[i][0] = false
    // s字符串前0个与p规则前j个
    // [0][0]是true，*可以让字符出现0次
    for (let j = 1; j < p_len + 1; j++) {
        if (p[j - 1] == '*') {
            dp[0][j] = dp[0][j - 2]
        } else {
            dp[0][j] = false
        }
    }

    for (let i = 1; i < s_len + 1; i++) {
        for (let j = 1; j < p_len + 1; j++) {
            if (s[i - 1] == p[j - 1] || p[j - 1] == ".") {
                dp[i][j] = dp[i - 1][j - 1]
            }
            else if (p[j - 1] == "*") {
                if (s[i - 1] == p[j - 2] || p[j - 2] == ".") {
                    // dp[i-1][j]  多个字符匹配的情况
                    // dp[i][j-1]  单个字符匹配的情况
                    // dp[i][j-2]  没有匹配的情况
                    // 满足其一即可
                    dp[i][j] = dp[i][j - 2] || dp[i][j - 1] || dp[i - 1][j]
                } else {
                    // 如果和dp[i-2]不相同，直接*为0，等于删掉该字符
                    dp[i][j] = dp[i][j - 2]
                }
            }
            else {
                dp[i][j] = false
            }
        }
    }
    return dp[s_len][p_len]
};