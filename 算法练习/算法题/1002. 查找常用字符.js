/**
 * @param {string[]} A
 * @return {string[]}
 */
var commonChars = function (A) {
    let min = new Array(26).fill(Infinity)

    for (let a of A) {
        let count = new Array(26).fill(0)
        for (let i = 0; i < a.length; ++i) {
            count[a[i].charCodeAt() - 97]++
        }
        for (let i = 0; i < 26; ++i) {
            min[i] = Math.min(min[i], count[i])
        }
    }

    let res = []
    for (let i = 0; i < 26; ++i) {
        for (let j = 0; j < min[i]; ++j) {
            res.push(String.fromCharCode(i + 97))
        }
    }
    return res
};

console.log(commonChars(["bella", "label", "roller"]))