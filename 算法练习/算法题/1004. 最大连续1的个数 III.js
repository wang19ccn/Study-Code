/**
 * @param {number[]} A
 * @param {number} K
 * @return {number}
 */
var longestOnes = function (A, K) {
    let left = 0
    let right = 0

    let zoneSum = A[0] ? 0 : 1

    while (right < A.length) {
        if (zoneSum > K) {
            if (A[left] == 0) zoneSum--
            left++
        }
        right++
        if (A[right] == 0 && right < A.length) zoneSum++
    }

    return right - left
};

console.log(longestOnes([0, 0], 2))