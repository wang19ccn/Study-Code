/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
// 注：下面用的是栈的结构，依然可以检查是否对称
var isSymmetric = function (root) {
    let queue = []
    queue.push(root)
    queue.push(root)
    while (queue.length != 0) {
        let t1 = queue.pop()
        let t2 = queue.pop()
        if (t1 == null && t2 == null) continue
        if (t1 == null || t2 == null) return false
        if (t1.val != t2.val) return false
        queue.push(t1.right)
        queue.push(t2.left)
        queue.push(t1.left)
        queue.push(t2.right)
    }
    return true
};

// 使用队列
var isSymmetric = function (root) {
    if (root == null || (root.left == null && root.right == null)) {
        return true;
    }
    let queue = []
    queue.push(root.left)
    queue.push(root.right)
    while (queue.length != 0) {
        let lTree = queue.shift()
        let rTree = queue.shift()
        if (lTree == null && rTree == null) continue
        if (lTree == null || rTree == null) return false
        if (lTree.val != rTree.val) return false
        queue.push(lTree.left)
        queue.push(rTree.right)
        queue.push(lTree.right)
        queue.push(rTree.left)
    }
    return true
};