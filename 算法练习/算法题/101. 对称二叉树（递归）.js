/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function (root) {
    // if(!root) return true
    // return check(root.right, root.left)
    return check(root, root)
};

var check = function (t1, t2) {
    if (!t1 && !t2) return true
    if (!t1 || !t2) return false
    return t1.val === t2.val && check(t1.right, t2.left) && check(t1.left, t2.right)
}