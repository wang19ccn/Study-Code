/**
 * @param {number[]} A
 * @return {boolean}
 */
// 贪心算法
var canThreePartsEqualSum = function (A) {
    let sum = 0
    for (let a of A) {
        sum += a
    }

    if (sum % 3 != 0) {
        return false
    }

    let target = sum / 3
    let len = A.length
    let minIndex = 0
    let cur = 0

    while (minIndex < len) {
        cur += A[minIndex]
        if (cur == target) {
            break
        }
        ++minIndex
    }

    if (cur != target) {
        return false
    }

    let k = minIndex + 1
    cur = 0
    while (k + 1 < len) { // 确保第三部分至少有一个元素
        cur += A[k]
        if (cur == target) {
            return true
        }
        ++k
    }

    return false
};

console.log(canThreePartsEqualSum([0, 2, 1, -6, 6, -7, 9, 1, 2, 0, 1]))