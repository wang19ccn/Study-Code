/**
 * @param {number[]} A
 * @return {number}
 */
var maxScoreSightseeingPair = function (A) {
    let temp = A[0]
    let ans = 0
    for (let j = 1; j < A.length; j++) {
        ans = Math.max(ans, A[j] + (--temp))
        temp = Math.max(temp, A[j])
    }
    return ans;
};