/**
 * @param {number[]} A
 * @return {boolean[]}
 */
// 根据余数定理

// (a+b)%q=(a%q+b%q)%q

// (a * b) % p = (a % p * b % p) % p

var prefixesDivBy5 = function (A) {
    let list = []
    let prefix = 0
    let length = A.length
    for (let i = 0; i < length; i++) {
        prefix = ((prefix << 1) + A[i]) % 5
        list.push(prefix === 0)
    }
    return list
};
