/**
 * @param {number[][]} clips
 * @param {number} T
 * @return {number}
 */
var videoStitching = function (clips, T) {
    let max = new Array(T).fill(0)
    let last = 0
    let pre = 0
    let res = 0
    for (let clip of clips) {
        if (clip[0] < T) {
            max[clip[0]] = Math.max(max[clip[0]], clip[1])
        }
    }
    // console.log(max)
    for (let i = 0; i < T; i++) {
        last = Math.max(last, max[i])
        if (i == last) {
            return -1
        }
        if (i == pre) {
            res++
            pre = last
        }
    }
    return res
};

console.log(videoStitching([[0, 2], [4, 6], [8, 10], [1, 9], [1, 5], [5, 9]], 10))
console.log(videoStitching([[0, 1], [1, 2]], 5))