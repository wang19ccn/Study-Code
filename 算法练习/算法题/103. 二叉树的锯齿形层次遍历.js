/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var zigzagLevelOrder = function (root) {
    let res = []
    if (!root) return res

    let dir = false

    let q = []
    q.push(root)
    while (q.length !== 0) {
        let levelLength = q.length
        res.push([])
        for (let i = 0; i < levelLength; i++) {
            let node = q.shift() //FIFO队列特性
            res[res.length - 1].push(node.val)
            if (node.left) q.push(node.left)
            if (node.right) q.push(node.right)
        }
        if (dir) {
            res[res.length - 1].reverse()
            dir = false
        } else {
            dir = true
        }

    }

    return res;
};