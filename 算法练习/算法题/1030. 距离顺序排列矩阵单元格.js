/**
 * @param {number} R
 * @param {number} C
 * @param {number} r0
 * @param {number} c0
 * @return {number[][]}
 */
var allCellsDistOrder = function (R, C, r0, c0) {
    let maxDiStance = Math.max(r0, R - 1 - r0) + Math.max(c0, C - 1 - c0)
    let barrel = new Array(maxDiStance + 1)
    for (let i = 0; i <= maxDiStance; i++) {
        barrel[i] = []
    }
    for (let r = 0; r < R; r++) {
        for (let c = 0; c < C; c++) {
            let distance = Math.abs(r - r0) + Math.abs(c - c0)
            barrel[distance].push([r, c])
        }
    }
    let res = []
    for (let i = 0; i <= maxDiStance; i++) {
        for (let b of barrel[i]) {
            res.push(b)
        }
    }
    return res
};