/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var maxUncrossedLines = function (nums1, nums2) {
    let m = nums1.length;
    let n = nums2.length;

    let dp = new Array(m + 1);
    for (let i = 0; i < m + 1; i++) {
        dp[i] = new Array(n + 1).fill(0);
    }

    for (let i = 1; i <= m; i++) {
        for (let j = 1; j <= n; j++) {
            if (nums1[i - 1] == nums2[j - 1]) {
                dp[i][j] = dp[i - 1][j - 1] + 1;
            } else {
                dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
            }
        }
    }

    return dp[m][n];
};

console.log(maxUncrossedLines([10, 5, 2, 1, 5, 2], [2, 5, 1, 2, 5]))

// [2,5,1,2,5]

// [10,5,2,1,5,2]

//   0   1 2 3 4 5 6
// 0 0   0 0 0 0 0 0
//       - - - - - -
// 1 0 | 0 0 1 1 1 1
// 2 0 | 0 1 1 1 
// 3 0 |
// 4 0 |
// 5 0 |


// 状态转移方程
// f[nums1_index][nums2_index]

// f[i][j] = Math.min(f[i][j - 1], f[i - 1][j])