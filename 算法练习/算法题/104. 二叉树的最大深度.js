/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
// var maxDepth = function (root) {
//     if (root == null) return 0
//     var dfs = function (root, depth) {
//         if (root == null) return depth
//         let left = dfs(root.left, depth + 1)
//         let right = dfs(root.right, depth + 1)
//         return Math.max(left, right)
//     }
//     return dfs(root, 0)
// };

var maxDepth = function (root) {
    return root == null ? 0 : Math.max(maxDepth(root.left), maxDepth(root.right)) + 1
};

// var maxDepth = function (root) {
//     var dfs = function (root, depth) {
//         if (!root) return depth - 1
//         let left = dfs(root.left, depth + 1)
//         let right = dfs(root.right, depth + 1)
//         return Math.max(left, right)
//     }
//     return dfs(root, 1)
// };