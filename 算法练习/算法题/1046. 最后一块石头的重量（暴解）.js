/**
 * @param {number[]} stones
 * @return {number}
 */
var lastStoneWeight = function (stones) {
    let len = stones.length

    stones.sort((a, b) => a - b)
    while (len > 1) {
        if (stones[len - 1] == stones[len - 2]) {
            stones.pop()
            stones.pop()
        }
        else if (stones[len - 1] > stones[len - 2]) {
            stones[len - 2] = stones[len - 1] - stones[len - 2]
            stones.pop()
        }
        stones.sort((a, b) => a - b)
        len = stones.length
    }

    return len == 0 ? 0 : stones[0]
};

console.log(lastStoneWeight([2, 7, 4, 1, 8, 1]))