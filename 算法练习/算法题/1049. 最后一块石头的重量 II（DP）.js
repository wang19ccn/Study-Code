/**
 * @param {number[]} stones
 * @return {number}
 */
// 问题转化：
// 从 stonesstones 数组中选择，凑成总和不超过 sum/2 的最大价值。
var lastStoneWeightII = function (stones) {
    let len = stones.length;

    let sum = 0;
    for (let s of stones) {
        sum += s;
    }

    let target = parseInt(sum / 2);
    let dp = new Array(target + 1).fill(0);
    for (let i = 1; i <= len; i++) {
        let temp = stones[i - 1];
        for (let j = target; j >= temp; j--) {
            dp[j] = Math.max(dp[j], dp[j - temp] + temp);
        }
    }

    // dp[target] <= sum/2
    return Math.abs(sum - dp[target] - dp[target])
};

console.log(lastStoneWeightII([89, 23, 100, 93, 82, 98, 91, 85, 33, 95, 72, 98, 63, 46, 17, 91, 92, 72, 77, 79, 99, 96, 55, 72, 24, 98, 79, 93, 88, 92]))
