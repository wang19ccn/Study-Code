/**
 * @param {number[]} stones
 * @return {number}
 */
var lastStoneWeightII = function (stones) {
    let len = stones.length
    let res = Infinity;

    var dfs = function (start, sum) {
        if (start == len - 1) {
            let temp = Math.min(res, sum);
            if (temp >= 0) {
                res = temp;
            }
            return;
        }
        dfs(start + 1, sum + stones[start + 1]);
        dfs(start + 1, sum - stones[start + 1]);
    };

    dfs(0, -stones[0]);
    dfs(0, stones[0]);

    return res;
};

console.log(lastStoneWeightII([89, 23, 100, 93, 82, 98, 91, 85, 33, 95, 72, 98, 63, 46, 17, 91, 92, 72, 77, 79, 99, 96, 55, 72, 24, 98, 79, 93, 88, 92]))
