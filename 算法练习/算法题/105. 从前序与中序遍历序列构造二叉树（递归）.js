/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {number[]} preorder
 * @param {number[]} inorder
 * @return {TreeNode}
 */
var buildTree = function (preorder, inorder) {
    let preLen = preorder.length
    let inLen = inorder.length

    if (preLen != inLen) return null

    let map = new Map()
    for (let i = 0; i < inLen; i++) {
        map.set(inorder[i], i)
    }
    return buildTreeRes(preorder, 0, preLen - 1, map, 0, inLen - 1)
};

/**
 * 
 * @param {*} preorder 前序遍历序列
 * @param {*} preLeft 前序遍历子区间左边界
 * @param {*} preRight 前序遍历子区间右边界
 * @param {*} map 中序遍历，数值与下标的对应关系
 * @param {*} inLeft 中序遍历子区间左边界
 * @param {*} inRight  中序遍历子区间右边界
 */
var buildTreeRes = function (preorder, preLeft, preRight, map, inLeft, inRight) {
    if (preLeft > preRight || inLeft > inRight) return null
    let rootVal = preorder[preLeft]
    let root = new TreeNode(rootVal)
    let pIndex = map.get(rootVal)
    root.left = buildTreeRes(preorder, preLeft + 1, pIndex - inLeft + preLeft, map, inLeft, pIndex - 1)
    root.right = buildTreeRes(preorder, pIndex - inLeft + preLeft + 1, preRight, map, pIndex + 1, inRight)
    return root
};

function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}

console.log(buildTree([3, 9, 20, 15, 7], [9, 3, 15, 20, 7]))