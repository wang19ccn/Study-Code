/**
 * @param {number[]} customers
 * @param {number[]} grumpy
 * @param {number} X
 * @return {number}
 */
var maxSatisfied = function (customers, grumpy, X) {
    let len = customers.length

    let sum = 0
    for (let i = 0; i < len; i++) {
        if (grumpy[i] == 0) {
            sum += customers[i]
        }
    }

    let left = 0
    let right = X - 1
    let add = 0
    for (let i = 0; i < X; i++) {
        if (grumpy[i] == 1) {
            add += customers[i]
        }
    }

    let sumAdd = add
    while (right < len) {
        if (grumpy[left] == 1) {
            add -= customers[left]
        }
        left++
        right++
        if (grumpy[right] == 1) {
            add += customers[right]
        }
        sumAdd = Math.max(add, sumAdd)
    }

    return sumAdd + sum
};

console.log(maxSatisfied([1, 0, 1, 2, 1, 1, 7, 5], [0, 1, 0, 1, 0, 1, 0, 1], 3))