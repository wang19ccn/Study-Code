function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}

/**
 * @param {number[]} inorder
 * @param {number[]} postorder
 * @return {TreeNode}
 */
var buildTree = function (inorder, postorder) {
    let inLen = inorder.length
    let postLen = postorder.length
    return buildNewTree(inorder, 0, inLen - 1, postorder, 0, postLen - 1)
};

var buildNewTree = function (inorder, inLeft, inRight, postorder, postLeft, postRight) {

    if (inLeft > inRight || postLeft > postRight) {
        return null
    }

    let pivot = postorder[postRight]
    let pivotIndex = inLeft

    while (inorder[pivotIndex] != pivot) {
        pivotIndex++
    }

    let root = new TreeNode(pivot)

    root.left = buildNewTree(inorder, inLeft, pivotIndex - 1,
        postorder, postLeft, postRight - inRight + pivotIndex - 1)

    root.right = buildNewTree(inorder, pivotIndex + 1, inRight,
        postorder, postRight - inRight + pivotIndex, postRight - 1)

    return root
}

console.log(buildTree([9, 3, 15, 20, 7], [9, 15, 7, 20, 3]))