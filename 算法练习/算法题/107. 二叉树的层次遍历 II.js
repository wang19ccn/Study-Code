/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrderBottom = function (root) {
    if (root == null) return []
    let queue = [root]
    let res = []
    while (queue.length) {
        let level = []
        let len = queue.length
        for (let i = 0; i < len; i++) {
            let cur = queue.shift()
            level.push(cur.val)
            if (cur.left) {
                queue.push(cur.left)
            }
            if (cur.right) {
                queue.push(cur.right)
            }
        }
        res.unshift(level)
    }
    return res
};