/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {number}
 */
var numSubmatrixSumTarget = function (matrix, target) {
    let res = 0;
    let m = matrix.length, n = matrix[0].length;
    for (let i = 0; i < m; i++) {
        let sum = new Array(n).fill(0);
        for (let j = i; j < m; j++) {
            for (let c = 0; c < n; c++) {
                sum[c] += matrix[j][c];
            }
            res += subarraySum(sum, target);
        }
    }
    return res;
};

const subarraySum = (nums, k) => {
    const map = new Map();
    map.set(0, 1);
    let count = 0, pre = 0;
    for (const x of nums) {
        pre += x;
        if (map.has(pre - k)) {
            count += map.get(pre - k);
        }
        map.set(pre, (map.get(pre) || 0) + 1);
    }
    return count;
}
