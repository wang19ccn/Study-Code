/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {ListNode} head
 * @return {TreeNode}
 */
var sortedListToBST = function (head) {
    let len = 0
    let h = head
    while (head) {
        len++
        head = head.next
    }

    var buildBST = function (start, end) {
        if (start > end) return null
        let mid = parseInt(start + (end - start + 1) / 2)
        let left = buildBST(start, mid - 1)
        let root = new TreeNode(h.val)
        h = h.next
        root.left = left
        root.right = buildBST(mid + 1, end)
        return root
    }

    return buildBST(1, len)
};

