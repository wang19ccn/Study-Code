/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {ListNode} head
 * @return {TreeNode}
 */
var sortedListToBST = function (head) {
    let arr = []
    while (head) {
        arr.push(head.val)
        head = head.next
    }
    return buildBST(arr, 0, arr.length - 1)
};

var buildBST = function (arr, start, end) {
    if (start > end) return null
    let mid = parseInt(start + (end - start + 1) / 2)
    let root = new TreeNode(arr[mid])
    root.left = buildBST(arr, start, mid - 1)
    root.right = buildBST(arr, mid + 1, end)
    return root
}