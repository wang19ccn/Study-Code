/**
 * @param {number[]} height
 * @return {number}
 */
var maxArea = function(height) {
    let i=0
    let res=0
    let j=height.length-1
    while(i<j){
        if(height[i]<height[j]){
            res=Math.max(res,height[i]*(j-i))
            i+=1
        }else{
            res=Math.max(res,height[j]*(j-i))
            j-=1
        }
    }
    return res
};

console.log(maxArea([1,8,6,2,5,4,8,3,7]))