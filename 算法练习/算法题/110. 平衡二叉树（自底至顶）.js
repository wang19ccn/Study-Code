/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isBalanced = function (root) {
    return depth(root) != -1
};

var depth = function (root) {
    if (root == null) return 0
    let leftDepth = depth(root.left)
    if (leftDepth == -1) return -1
    let rightDepth = depth(root.right)
    if (rightDepth == -1) return -1
    return Math.abs(leftDepth - rightDepth) < 2 ? Math.max(leftDepth, rightDepth) + 1 : -1
}