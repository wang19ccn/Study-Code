/**
 * @param {string} seq
 * @return {number[]}
 */
var maxDepthAfterSplit = function(seq) {
    let str=seq.split("")
    console.log(str)
    let ans=[];
    let index=0;

    for(item of str){
        console.log(item)
        //遍历括号字符串，左括号按照自身index的奇偶来分配到0或1
        //右括号和对应的左括号index奇偶颠倒，所以自身索引加1的奇偶性来分配到0或1
        ans[index++]=item=='('? index & 1 : ((index+1) & 1);
        console.log(index)
    }
    return ans;
};

console.log(maxDepthAfterSplit("(()(())())"))