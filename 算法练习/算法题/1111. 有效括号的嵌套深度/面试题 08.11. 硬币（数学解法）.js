/**
 * @param {number} n
 * @return {number}
 */
var waysToChange = function(n) {
    let ans=0
    let mod=1e9+7

    for(let i=0;i<=n/25;i++){
        let x=n-i*25
        let y=parseInt(x/5+1)
        let z=parseInt(x/10+1);
        ans += (y-z+1)*z
        ans %= mod
    }
    return ans;
};

console.log(waysToChange(5))