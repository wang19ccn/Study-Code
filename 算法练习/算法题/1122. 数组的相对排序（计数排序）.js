/**
 * @param {number[]} arr1
 * @param {number[]} arr2
 * @return {number[]}
 */
var relativeSortArray = function (arr1, arr2) {
    let maxLen = 0
    for (let a of arr1) {
        maxLen = Math.max(maxLen, a)
    }
    let fre = new Array(maxLen + 1).fill(0)
    for (let x of arr1) {
        ++fre[x]
    }
    let ans = new Array()
    for (let x of arr2) {
        for (let i = 0; i < fre[x]; ++i) {
            ans.push(x)
        }
        fre[x] = 0
    }
    for (let i = 0; i <= maxLen; ++i) {
        for (let j = 0; j < fre[i]; ++j) {
            ans.push(i)
        }
    }
    return ans
};
