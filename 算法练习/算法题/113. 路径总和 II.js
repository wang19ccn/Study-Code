/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} sum
 * @return {number[][]}
 */
var pathSum = function (root, sum) {
    let res = []
    let path = []
    let num = 0
    dfs(root, num, path, sum, res)
    return res
};

var dfs = function (root, num, path, sum, res) {
    if (!root) return

    num += root.val
    path.push(root.val)

    if (num == sum && !root.left && !root.right) {
        res.push([...path])
        return path.pop()
    }

    dfs(root.left, num, path, sum, res)
    dfs(root.right, num, path, sum, res)
    path.pop()
}