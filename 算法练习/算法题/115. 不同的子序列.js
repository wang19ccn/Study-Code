/**
 * @param {string} s
 * @param {string} t
 * @return {number}
 */
var numDistinct = function (s, t) {
    let sLen = s.length, tLen = t.length

    let memo = new Array(sLen)
    for (let i = 0; i < sLen; i++) {
        memo[i] = new Array(tLen)
        for (let j = 0; j < tLen; j++) {
            memo[i][j] = -1
        }
    }

    var helper = function (i, j) {
        if (j < 0) {
            return 1
        }
        if (i < 0) {
            return 0
        }
        if (memo[i][j] != -1) {
            return memo[i][j]
        }
        if (s[i] == t[j]) {
            memo[i][j] = helper(i - 1, j) + helper(i - 1, j - 1)
        } else {
            memo[i][j] = helper(i - 1, j)
        }
        return memo[i][j]
    }

    return helper(sLen - 1, tLen - 1)
};