/**
 * // Definition for a Node.
 * function Node(val, left, right, next) {
 *    this.val = val === undefined ? null : val;
 *    this.left = left === undefined ? null : left;
 *    this.right = right === undefined ? null : right;
 *    this.next = next === undefined ? null : next;
 * };
 */

/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function (root) {
    if (!root) return root

    let queue = [root]

    while (queue.length) {
        let len = queue.length
        for (let i = 0; i < len; i++) {
            let cur = queue.shift()
            if (cur.left) {
                queue.push(cur.left)
            }
            if (cur.right) {
                queue.push(cur.right)
            }
            if (i + 1 < len) {
                cur.next = queue.shift()
                queue.unshift(cur.next)
            } else {
                cur.next = null
            }
        }
    }

    return root
};  