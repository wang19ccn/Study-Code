/**
 * @param {number[][]} grid
 * @return {number}
 */
var maxDistance = function (grid) {
    let dx = [-1, 1, 0, 0];
    let dy = [0, 0, -1, 1];

    let queue = new Array();
    let m = grid.length; //获取所有元素数目
    let n = grid[0].length; //获取单行元素数目

    //获取所有陆地
    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            if (1 == grid[i][j]) {
                queue.push([i,j]);
            }
        }
    }

    //console.log(queue.shift());

    let hasOcean = false;

    let point =[];

    while(0!=queue.length){
        point=queue.shift();
        let x=point[0];
        let y=point[1];
        //console.log("x="+x,"y="+y);
        for(let i=0;i<4;i++){
            let newX = x + dx[i];
            let newY = y + dy[i];
            //console.log("grid["+newX+"]["+newY+"]=");
            //console.log("grid["+newX+"]["+newY+"]="+grid[newX][newY]);
            if(newX<0||newX>=m||newY<0||newY>=n||grid[newX][newY]!=0){
                continue;
            }
            grid[newX][newY]=grid[x][y]+1;
            hasOcean=true;
            queue.push([newX,newY]);
        }
    }

    if(point==[]||!hasOcean){
        return -1;
    }
    //console.log(grid)
    //return console.log(grid[point[0]][point[1]] - 1);
    return grid[point[0]][point[1]] - 1;
};

maxDistance([[1,0,0],[0,0,0],[0,0,0]]);

