/**
 * // Definition for a Node.
 * function Node(val, left, right, next) {
 *    this.val = val === undefined ? null : val;
 *    this.left = left === undefined ? null : left;
 *    this.right = right === undefined ? null : right;
 *    this.next = next === undefined ? null : next;
 * };
 */

/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function (root) {
    if (root == null || (root.right == null && root.left == null)) {
        return root
    }
    if (root.left != null && root.right != null) {
        root.left.next = root.right
        root.right.next = getRight(root)
    }
    if (root.left == null) {
        root.right.next = getRight(root)
    }
    if (root.right == null) {
        root.left.next = getRight(root)
    }

    root.right = connect(root.right)
    root.left = connect(root.left)

    return root
};

var getRight = function (root) {
    while (root.next != null) {
        if (root.next.left != null) {
            return root.next.left
        }
        if (root.next.right != null) {
            return root.next.right
        }
        root = root.next
    }
    return null
}