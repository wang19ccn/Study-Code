/**
 * @param {string} s
 * @return {string}
 */

//  输入：s = "(u(love)i)"
//  输出："iloveu"
var reverseParentheses = function (s) {
    let stack = [];
    let str = ''
    for (let c of s) {
        if (c == '(') {
            stack.push(str);
            str = '';
        }
        else if (c == ')') {
            str = str.split('').reverse().join('');
            str = stack.pop() + str;
        } else {
            str += c
        }
    }
    return str
};

console.log(reverseParentheses("(u(love)i)"))