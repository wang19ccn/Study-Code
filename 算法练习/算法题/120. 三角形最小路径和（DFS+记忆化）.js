/**
 * @param {number[][]} triangle
 * @return {number}
 */
var minimumTotal = function (triangle) {

    let maxDepth = triangle.length

    let memo = new Array(maxDepth)
    for (let i = 0; i < maxDepth; i++) {
        memo[i] = new Array(maxDepth).fill(null)
    }

    var dfs = function (val, index, depth) {
        if (depth == maxDepth) return triangle[depth - 1][index]

        if (memo[depth - 1][index] != null) return memo[depth - 1][index]

        let one = dfs(triangle[depth][index], index, depth + 1) + val
        let two = dfs(triangle[depth][index + 1], index + 1, depth + 1) + val

        return memo[depth - 1][index] = Math.min(one, two)
    }

    return dfs(triangle[0][0], 0, 1)

};

console.log(minimumTotal([[2], [3, 4], [6, 5, 7], [4, 1, 8, 3]]))