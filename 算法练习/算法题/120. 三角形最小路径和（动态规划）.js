/**
 * @param {number[][]} triangle
 * @return {number}
 */
var minimumTotal = function (triangle) {

    let maxDepth = triangle.length

    let dp = new Array(maxDepth)
    for (let i = 0; i < maxDepth; i++) {
        dp[i] = new Array(maxDepth).fill(null)
    }

    for (let i = maxDepth - 1; i >= 0; i--) {
        for (let j = 0; j <= i; j++) {
            if (i == maxDepth - 1) {
                dp[i][j] = triangle[i][j]
            }
            else {
                dp[i][j] = Math.min(dp[i + 1][j], dp[i + 1][j + 1]) + triangle[i][j]
            }
        }
    }

    return dp[0][0]
};

console.log(minimumTotal([[2], [3, 4], [6, 5, 7], [4, 1, 8, 3]]))