// class UnionFind {
//     constructor(n) {
//         this.parent = new Array(n)
//         this.rank = new Array(n)
//         for (let i = 0; i < n; i++) {
//             this.parent[i] = i
//             this.rank[i] = 1.0
//         }
//     }
//     union(x, y, value) {
//         let rootX = this.find(x)
//         let rootY = this.find(y)
//         if (rootX == rootY) {
//             return
//         }

//         if (rank[rootX] == rank[rootY]) {
//             parent[rootX] = rootY;
//             // 此时以 rootY 为根结点的树的高度仅加了 1
//             rank[rootY]++;
//         } else if (rank[rootX] < rank[rootY]) {
//             parent[rootX] = rootY;
//             // 此时以 rootY 为根结点的树的高度不变
//         } else {
//             // 同理，此时以 rootX 为根结点的树的高度不变
//             parent[rootY] = rootX;
//         }
//     }
//     /**
//     * 路径压缩
//     * @param x
//     * @return 根结点的 id
//     */
//     find(x) {
//         if (x != this.parent[x]) {
//             let origin = this.parent[x]
//             this.parent[x] = this.find(this.parent[x])
//             this.weight[x] *= this.weight[origin]
//         }
//         return this.parent[x]
//     }
// }

// /**
//  * @param {string} s
//  * @param {number[][]} pairs
//  * @return {string}
//  */
// var smallestStringWithSwaps = function (s, pairs) {
//     if (pairs.length == 0) {
//         return s
//     }
//     let len = s.length()
//     let unionFind = new UnionFind(len)

//     // 优先队列 
// };

var smallestStringWithSwaps = function (s, pairs) {
    const fa = new Array(100010).fill(0);

    const find = (x) => {
        return x === fa[x] ? x : fa[x] = find(fa[x]);
    }

    const n = s.length;
    for (let i = 0; i < n; i++) {
        fa[i] = i;
    }
    for (let i = 0; i < pairs.length; ++i) {
        const x = pairs[i][0], y = pairs[i][1];
        const ux = find(x), uy = find(y);
        if (ux ^ uy) {
            fa[ux] = uy;
        }
    }

    const vec = new Array(n).fill(0).map(() => new Array());
    for (let i = 0; i < n; i++) {
        fa[i] = find(i);
        vec[fa[i]].push(s[i]);
    }

    for (let i = 0; i < n; ++i) {
        if (vec[i].length > 0) {
            vec[i].sort((a, b) => a.charCodeAt() - b.charCodeAt());
        }
    }
    const p = new Array(n).fill(0);
    let ans = [];
    for (let i = 0; i < n; ++i) {
        ans.push('1');
    }

    for (let i = 0; i < n; ++i) {
        ans[i] = vec[fa[i]][p[fa[i]]];
        p[fa[i]]++;
    }

    return ans.join('');
};

// 作者：LeetCode-Solution
// 链接：https://leetcode-cn.com/problems/smallest-string-with-swaps/solution/jiao-huan-zi-fu-chuan-zhong-de-yuan-su-b-qdn9/
