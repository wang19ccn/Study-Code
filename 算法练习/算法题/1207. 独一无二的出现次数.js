/**
 * @param {number[]} arr
 * @return {boolean}
 */
var uniqueOccurrences = function (arr) {
    let map = new Map()
    for (let a of arr) {
        if (map.has(a)) {
            map.set(a, map.get(a) + 1)
        } else {
            map.set(a, 1)
        }
    }
    let set = new Set()
    // map.forEach((value) => {
    //     set.add(value)
    // })
    for(let [key,value] of map){
        set.add(value)
    }
    return set.size === map.size
};