/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    let maxfit=0
    let min=Infinity

    for(let i=0;i<prices.length;i++){
        if(min>prices[i]){
            min=prices[i]
        }
        else if((prices[i]-min)>maxfit){
            maxfit=prices[i]-min
        }
    }

    return maxfit
};