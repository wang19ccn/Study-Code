/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
    let len = prices.length
    if (len < 2) { return 0 }
    let preCash = 0
    let preHold = -prices[0]
    for(let i=1;i<len;i++){
        let cash = Math.max(preCash,preHold+prices[i])
        let hold = Math.max(preHold,preCash-prices[i])
        preCash = cash
        preHold = hold
    }
    return cash
};