/**
 * @param {number[]} prices
 * @return {number}
 */
/* 
状态：
    0. 没有操作（不记录）
    1. 第一次买入
    2. 第一次卖出
    3. 第二次买入
    4. 第二次卖出
*/
var maxProfit = function (prices) {
    let buy_1 = -prices[0], sell_1 = 0
    let buy_2 = -prices[0], sell_2 = 0

    for (let i = 1; i < prices.length; i++) {
        buy_1 = Math.max(buy_1, - prices[i])
        sell_1 = Math.max(sell_1, buy_1 + prices[i]);
        buy_2 = Math.max(buy_2, sell_1 - prices[i]);
        sell_2 = Math.max(sell_2, buy_2 + prices[i]);

    }

    return sell_2
};

console.log(maxProfit([3, 3, 5, 0, 0, 3, 1, 4]))