/**
 * @param {number[][]} coordinates
 * @return {boolean}
 */
var checkStraightLine = function (coordinates) {
    let n = coordinates.length
    let dx = coordinates[1][0] - coordinates[0][0]
    let dy = coordinates[1][1] - coordinates[0][1]
    for (let i = 2; i < n; i++) {
        let newX = coordinates[i][0] - coordinates[0][0]
        let newY = coordinates[i][1] - coordinates[0][1]
        // dy/dx = newY/newX 避免浮点精度问题
        if (dy * newX != newY * dx) return false
    }
    return true
};