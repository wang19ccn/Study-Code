/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */

var maxPathSum = function (root) {
    let maxPath = -Infinity
    var dfs = function (root) {
        if (root == null) return 0
        let left = Math.max(0, dfs(root.left))
        let right = Math.max(0, dfs(root.right))
        maxPath = Math.max(maxPath, left + root.val + right)
        return root.val + Math.max(left, right)
    }
    dfs(root)
    return maxPath
};

console.log(maxPathSum(root = {
    val: 0,
    left: null,
    right: null
}))

