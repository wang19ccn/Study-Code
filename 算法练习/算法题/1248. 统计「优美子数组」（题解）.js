// let nums = [2,2,2,1,2,2,1,2,2,2], k = 2

// 奇数索引数组[3, 6]

// k为2, 有2奇数, 满足条件.   左边有3个偶数  右边有3个偶数  总组合为 (3 + 1) * (3 + 1) = 16 

// 因为 先看左边 三位, 我们可以取 0/1/2/3 这么多位偶数组合, 右边一样 0/1/2/3 , 结合左右两边 直接相乘就行了

// 如当两边都是取 0 时, 得   1,2,2,1
// 如当两边都是取 1 时, 得 2,1,2,2,1,2 
// ...
// 如当左取0,右取 1 时, 得   1,2,2,1,2 
// ...

// 循环, 因为只有两位 所以只要循环 [3, 6].length - k = 0, 从0开始循环 直到 0, 只要循环一次

// 使 left 计左边有多少偶数, right 计 右边有多少偶数

// 当i = 0时, 从的nums[odd[0]] = nums[3] 位置往左边找, 从 nums[odd[0 + k - 1]] = nums[6]位置往右边找

// 在nums的范围内, 往左找 直到不是偶数的为止 期间每找到一个left就 加1 , 

// 往右找 直到不是偶数的为止, 期间每找到一个right就 加1

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var numberOfSubarrays = function(nums, k) {
    let len = nums.length
    let odd = []                    // 存储奇数 索引
    for(let i = 0; i < len; i++){   // 找出所有奇数的索引
      if(nums[i] % 2 === 1){
        odd.push(i)
      }
    }
    if(odd.length < k) return 0                 // 当奇数的个数小于k时 直接返回, 其实不用这段也可以,下面根本不会进循环
    let total = 0                               // 接下来就是找出k位奇数两边有多少个偶数了, 计总数
    for(let i = 0; i <= odd.length - k; i++){   // 循环奇数索引数组, 找出左右两边偶数
      let left = 0, right = 0                                   // 用来统计左右两边偶数
      let left_index = odd[i], right_index = odd[i + k - 1]     // 开始位置
      while(left_index > 0 && nums[left_index - 1] % 2 === 0){  // 往左找到不是偶数的为止
        left_index--
        left++
      }
      while(right_index < len && nums[right_index + 1] % 2 === 0){ // 往右找到不是偶数的为止
        right++
        right_index++
      }
      total += (left + 1) * (right + 1)
    }
    return total
  };
  
  // 作者：shetia
