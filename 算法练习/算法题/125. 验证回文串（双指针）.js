/**
 * @param {string} s
 * @return {boolean}
 */
var isPalindrome = function (s) {
    s = s.replace(/[^0-9a-zA-Z]/g, '').toLowerCase();
    let i = 0
    let j = s.length - 1
    while (i < j) {
        if (s.charAt(i) != s.charAt(j)) {
            return false
        }
        i++
        j--
    }
    return true
};