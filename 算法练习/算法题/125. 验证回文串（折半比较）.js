/** 时间约136ms
 * @param {string} s
 * @return {boolean}
 */
var isPalindrome = function (s) {
    s = s.replace(/[^0-9a-zA-Z]/g, '').toLowerCase();
    let arr = s.split('')
    let len = arr.length
    let mid = parseInt(len / 2)
    let str1 = arr.slice(0, mid)
    let str2
    if (len % 2 != 0) {
        str2 = arr.slice(mid + 1, len)
    } else {
        str2 = arr.slice(mid, len)
    }
    str2 = str2.reverse()
    console.log(str1, str2)
    return str1.toString() == str2.toString()
};

console.log(isPalindrome("A man, a plan, a canal: Panama"))