/**
 * @param {number} steps
 * @param {number} arrLen
 * @return {number}
 */
// 动态规划
// 状态：f[i][j] i --> 当前剩余操作数   j --> 当前所在位置  f --> 方案数
// 初始化条件： f[step][0] = 1
// 求：f[0]][0]=1
// 状态转移方程
// 由「原地」操作到达当前状态，消耗一次操作，此时由状态 f[i + 1][j]f[i+1][j] 转移而来
// 由「向左」操作到达当前状态，消耗一次操作，此时由状态 f[i + 1][j + 1]f[i+1][j+1] 转移而来
// 由「向右」操作到达当前状态，消耗一次操作，此时由状态 f[i + 1][j - 1]f[i+1][j−1] 转移而来
var numWays = function (steps, arrLen) {
    let mod = 1e9 + 7;
    let max = parseInt(Math.min(steps / 2, arrLen - 1));

    let res = new Array(steps + 1);
    for (let i = 0; i < steps + 1; i++) {
        res[i] = new Array(max + 1).fill(0);
    }

    res[steps][0] = 1
    for (let i = steps - 1; i >= 0; i--) {
        for (let j = 0; j <= max; j++) {
            // 原地
            res[i][j] = res[i + 1][j] % mod;
            // 向左
            if (j - 1 >= 0) res[i][j] = (res[i][j] + res[i + 1][j - 1]) % mod;
            // 向右
            if (j + 1 <= max) res[i][j] = (res[i][j] + res[i + 1][j + 1]) % mod;
        }
    }

    return res[0][0]
};

console.log(numWays(3, 3))