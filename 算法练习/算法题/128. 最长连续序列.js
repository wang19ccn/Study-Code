/**
 * @param {number[]} nums
 * @return {number}
 */
var longestConsecutive = function (nums) {
    let setNums = new Set(nums)
    let max = 0
    for (let i = 0; i < nums.length; i++) {
        if (!setNums.has(nums[i] - 1)) {
            let temp = nums[i]
            let count = 1
            while (setNums.has(temp + 1)) {
                temp++
                count++
            }
            max = Math.max(max, count)
        }
    }
    return max
};