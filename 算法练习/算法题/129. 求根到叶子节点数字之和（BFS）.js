/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var sumNumbers = function (root) {
    if (root == null) {
        return 0
    }
    let sum = 0
    let nodeQueue = [root]
    let numQueue = [root.val]
    while (nodeQueue.length) {
        let node = nodeQueue.shift()
        let num = numQueue.shift()
        let left = node.left
        let right = node.right
        if (left === null && right === null) {
            sum += num
        } else {
            if (left !== null) {
                nodeQueue.push(left)
                numQueue.push(num * 10 + left.val)
            }
            if (right !== null) {
                nodeQueue.push(right)
                numQueue.push(num * 10 + right.val)
            }
        }
    }
    return sum
};