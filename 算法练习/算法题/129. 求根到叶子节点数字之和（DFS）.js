/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var sumNumbers = function (root) {
    if (!root) return 0
    let sum = 0
    var dfs = function (root, add) {
        if (!root.left && !root.right) {
            sum += add * 10 + root.val
            return
        }
        if (root.right) {
            dfs(root.right, add * 10 + root.val)
        }
        if (root.left) {
            dfs(root.left, add * 10 + root.val)
        }
    }
    dfs(root, 0)
    return sum
};