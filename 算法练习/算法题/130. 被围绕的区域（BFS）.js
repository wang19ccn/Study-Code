/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solve = function (board) {
    if (board == null || board.length == 0) return
    let m = board.length
    let n = board[0].length
    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            if (i == 0 || i == m - 1 || j == 0 || j == n - 1) {
                if (board[i][j] == 'O') bfs(board, i, j);
            }
        }
    }
    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            if (board[i][j] === '#') board[i][j] = 'O';
            else if (board[i][j] === 'O') board[i][j] = 'X';
        }
    }
};

var bfs = function (board, i, j) {
    let dx = [1, -1, 0, 0]
    let dy = [0, 0, 1, -1]
    let queue = [[i, j]]
    board[i][j] = '#'
    while (queue.length) {
        let [x, y] = queue.shift()
        for (let i = 0; i < 4; i++) {
            let new_x = x + dx[i]
            let new_y = y + dy[i]
            if (new_x < 0 || new_x >= board.length || new_y < 0 || new_y >= board[0].length) {
                continue
            }
            if (board[new_x][new_y] == 'O') {
                board[new_x][new_y] = '#'
                queue.push([new_x, new_y])
            }
        }
    }
}

console.log(solve([["O", "O"], ["O", "O"]]))