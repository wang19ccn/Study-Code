/**
 * @param {number[]} arr
 * @param {number} target
 * @return {number}
 */
var findBestValue = function (arr, target) {
    // 排序
    arr.sort((a, b) => a - b);

    let len = arr.length
    let curSum = 0
    for (let i = 0; i < len; i++) {
        let curAveInt = parseInt((target - curSum) / (len - i))
        if (curAveInt <= arr[i]) {
            let curAve = (target - curSum) / (len - i)
            if (curAve - curAveInt <= 0.5) {
                return curAveInt
            } else {
                return curAveInt + 1
            }
        }
        curSum += arr[i]
    }
    return arr[len - 1]
};

console.log(findBestValue([4, 9, 3], 10))