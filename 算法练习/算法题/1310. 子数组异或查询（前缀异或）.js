/**
 * @param {number[]} arr
 * @param {number[][]} queries
 * @return {number[]}
 */
var xorQueries = function (arr, queries) {
    // 计算前缀和
    let XOR = new Array(arr.length + 1).fill(0);
    for (let i = 0; i < arr.length; i++) {
        XOR[i + 1] = XOR[i] ^ arr[i];
    }
    // 查询 Q[left,right] = X0R[left] ^ X0R[right+1]     
    // Q[2,6] -->  0 1 2 3 4 5 6
    // XOR --> 0 1(0) 2(1) 3(2) 4(3) 5(4) 6(5) 7(6) 
    let ans = new Array(queries.length).fill(0);
    for (let i = 0; i < queries.length; i++) {
        ans[i] = XOR[queries[i][0]] ^ XOR[queries[i][1] + 1];
    }
    return ans
};

console.log(xorQueries([1, 3, 4, 8], [[0, 3], [0, 1], [3, 3], [1, 2]]))
