/**
 * @param {number} n
 * @param {number[][]} connections
 * @return {number}
 */
var makeConnected = function (n, connections) {
    if (connections.length < n - 1) return -1
    let unionFid = new UnionFind(n)
    for (let i = 0; i < connections.length; i++) {
        unionFid.union(connections[i][0], connections[i][1])
    }
    return unionFid.getGroupNum() - 1
};

class UnionFind {
    /**
     * 初始化并查集，并使其顶点指向-1
     * @param {number} num 顶点个数 
     */
    constructor(num) {
        this.roots = new Array(num).fill(-1)
        this.group = num
    }

    /**
     * 查找顶点x的根节点
     * @param {number} x 
     */
    findRoot(x) {
        let x_root = x
        while (this.roots[x_root] != -1) {
            x_root = this.roots[x_root]
        }
        return x_root
    }

    /**
     * 合并顶点x和顶点y所在的集合
     * @param {number} x 
     * @param {number} y 
     */
    union(x, y) {
        // 查找各自根节点
        let x_root = this.findRoot(x)
        let y_root = this.findRoot(y)
        // 如果根节点相同，说明已经在一个集合内
        if (x_root != y_root) {
            this.group--
            this.roots[x_root] = y_root
        }
    }

    getGroupNum() {
        return this.group
    }
}

// 模板
// class UnionFind {
//     /**
//      * 初始化并查集，并使其顶点指向-1
//      * @param {number} num 顶点个数 
//      */
//     constructor(num) {
//         this.roots = new Array(num).fill(-1)
//         this.ranks = new Array(num).fill(0)
//     }

//     /**
//      * 查找顶点x的根节点
//      * @param {number} x 
//      */
//     findRoot(x) {
//         let x_root = x
//         while (this.roots[x_root] != -1) {
//             x_root = this.roots[x_root]
//         }
//         return x_root
//     }

//     /**
//      * 合并顶点x和顶点y所在的集合
//      * @param {number} x 
//      * @param {number} y 
//      */
//     union(x, y) {
//         // 查找各自根节点
//         let x_root = this.findRoot(x)
//         let y_root = this.findRoot(y)
//         // 如果根节点相同，说明已经在一个集合内
//         if (x_root == y_root) return

//         let x_rank = this.ranks[x_root]
//         let y_rank = this.ranks[y_root]
//         if (x_rank < y_rank) {    // 谁高度大，谁就作为根节点
//             this.roots[x_root] = y_root
//         } else if (y_rank < x_rank) {
//             this.roots[y_root] = x_root
//         } else {                  // 一样高，谁作为根节点都行
//             this.roots[y_root] = x_root
//             this.ranks[x_root]++ // 作为根节点的，高度会+1
//         }

//     }
// }