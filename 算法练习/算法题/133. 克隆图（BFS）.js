/**
 * // Definition for a Node.
 * function Node(val, neighbors) {
 *    this.val = val === undefined ? 0 : val;
 *    this.neighbors = neighbors === undefined ? [] : neighbors;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */
var cloneGraph = function (node) {
    if (node == null) return null
    let visit = new Map()
    let queue = []
    let cloneNode = new Node(node.val, [])

    queue.push(node)
    visit.set(node, cloneNode)
    while (queue.length) {
        let curNode = queue.shift()
        for (let neighborsNode of curNode.neighbors) {
            if (!visit.has(neighborsNode)) {
                queue.push(neighborsNode)
                let cloneNeighbor = new Node(neighborsNode.val, [])
                visit.set(neighborsNode, cloneNeighbor)
            }
            let curCloneNode = visit.get(curNode)
            let cloneNeighborNode = visit.get(neighborsNode)
            curCloneNode.neighbors.push(cloneNeighborNode)
        }
    }

    return cloneNode
};