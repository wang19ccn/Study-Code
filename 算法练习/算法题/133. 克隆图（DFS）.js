/**
 * // Definition for a Node.
 * function Node(val, neighbors) {
 *    this.val = val === undefined ? 0 : val;
 *    this.neighbors = neighbors === undefined ? [] : neighbors;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */
var cloneGraph = function (node) {
    let visit = new Map()
    return dfs(visit, node)
};

var dfs = function (visit, node) {
    if (node == null) return null
    if (visit.has(node.val)) return visit.get(node.val)
    let cloneNode = new Node(node.val, [])
    visit.set(node.val, cloneNode)
    for (let neighborsNode of node.neighbors) {
        let cloneNeighbor = dfs(visit, neighborsNode)
        cloneNode.neighbors.push(cloneNeighbor)
    }
    return cloneNode
}