/**
 * @param {number[]} gas
 * @param {number[]} cost
 * @return {number}
 */
var canCompleteCircuit = function (gas, cost) {
    let n = gas.length
    for (let i = 0; i < n; i++) {
        let j = i
        let remain = gas[i]
        while (remain - cost[j] >= 0) {
            remain = remain - cost[j] + gas[(j + 1) % n]
            j = (j + 1) % n
            if (j == i) {
                return i
            }
        }
        if (j < i) {
            return -1
        }
        i = j
    }
    return -1
};

