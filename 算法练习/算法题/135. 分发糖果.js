/**
 * @param {number[]} ratings
 * @return {number}
 */
var candy = function (ratings) {
    let n = ratings.length
    let left = new Array(n).fill(0)

    // 左规则: ratings[i] > ratings[i - 1] 
    for (let i = 0; i < n; i++) {
        if (i > 0 && ratings[i] > ratings[i - 1]) {
            left[i] = left[i - 1] + 1
        } else {
            left[i] = 1
        }
    }

    // 右规则: ratings[i] > ratings[i + 1]
    let right = 0
    let res = 0
    for (let i = n - 1; i > -1; i--) {
        if (i < n - 1 && ratings[i] > ratings[i + 1]) {
            right++
        } else {
            right = 1
        }
        // 选最大值，使其同时满足左规则和右规则
        res += Math.max(left[i], right)
    }

    return res
};