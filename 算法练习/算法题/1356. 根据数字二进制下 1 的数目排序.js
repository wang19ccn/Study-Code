/**
 * @param {number[]} arr
 * @return {number[]}
 */
var sortByBits = function (arr) {
    arr.sort((a, b) => {
        let res = sortNum(a) - sortNum(b)
        if (res == 0) return a - b
        return res
    })
    return arr
};

var sortNum = function (num) {
    let res = 0
    while (num) {
        if (num & 1) {
            res++
        }
        num = num >> 1
    }
    return res
}