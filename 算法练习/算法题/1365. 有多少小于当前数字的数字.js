/**
 * @param {number[]} nums
 * @return {number[]}
 */
var smallerNumbersThanCurrent = function (nums) {
    let res = []
    let arr = []
    for (let i = 0; i < nums.length; i++) {
        arr.push([nums[i], i])
    }
    arr.sort((a, b) => a[0] - b[0])
    let pre = -1
    for (let i = 0; i < nums.length; i++) {
        if (pre == -1 || arr[i - 1][0] != arr[i][0]) {
            pre = i
        }
        res[arr[i][1]] = pre
    }
    return res
};

console.log(smallerNumbersThanCurrent([8, 1, 2, 2, 3]))