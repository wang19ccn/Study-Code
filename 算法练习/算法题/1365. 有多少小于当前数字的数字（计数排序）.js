/**
 * @param {number[]} nums
 * @return {number[]}
 */
var smallerNumbersThanCurrent = function (nums) {
    let cnt = new Array(101).fill(0)
    let res = []
    let n = nums.length
    for (let i = 0; i < n; ++i) {
        cnt[nums[i]]++
    }
    for (let i = 1; i < 100; ++i) {
        cnt[i] += cnt[i - 1]
    }
    for (let i = 0; i < n; ++i) {
        res.push(nums[i] == 0 ? 0 : cnt[nums[i] - 1])
    }
    return res
};

console.log(smallerNumbersThanCurrent([0, 0, 0, 8, 1, 2, 2, 3]))