/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNumber = function (nums) {
    nums.sort((a, b) => a - b);

    let count = 1;
    let i = 1
    for (; i < nums.length; i++) {
        if (nums[i - 1] == nums[i]) {
            count++;
        }
        else if (nums[i - 1] != nums[i] && count == 1) {
            return nums[i - 1];
        }
        else {
            count = 1;
        }
    }

    if (i == nums.length && count == 1) {
        return nums[i - 1];
    }
};

console.log(singleNumber([0, 1, 0, 1, 0, 1, 99]))