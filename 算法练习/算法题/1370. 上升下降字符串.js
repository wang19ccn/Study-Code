/**
 * @param {string} s
 * @return {string}
 */
var sortString = function (s) {
    let count = new Array(26).fill(0)
    for (let S of s) {
        count[S.charCodeAt() - 97]++
    }
    let res = []
    while (res.length < s.length) {
        for (let i = 0; i < 26; i++) {
            if (count[i] != 0) {
                res.push(String.fromCharCode(i + 97))
                count[i]--
            }
        }
        for (let i = 25; i >= 0; i--) {
            if (count[i] != 0) {
                res.push(String.fromCharCode(i + 97))
                count[i]--
            }
        }
    }
    return res.join('')
};

console.log(sortString("rat"))