/**
 * @param {string} s
 * @return {number}
 */
var findTheLongestSubstring = function (s) {
    let res = 0
    let state = 0
    let vowel = { a: 1, e: 2, i: 4, o: 8, u: 16 }
    let map = { 0: -1 }
    for (let i = 0; i < s.length; i++) {
        let char = s[i]
        if (vowel[char] !== undefined) {
            state ^= vowel[char]
            if (map[state] === undefined) {
                map[state] = i
            }
        }
        let len = i - map[state]
        res = Math.max(res, len)
    }
    return res
};

console.log(findTheLongestSubstring("eoeoecccccc"))