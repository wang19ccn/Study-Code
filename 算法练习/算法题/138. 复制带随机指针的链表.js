/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function (head) {
    if (head == null) return null
    let map = new Map()
    let p = head
    while (p != null) {
        let newNode = new Node(p.val)
        map.set(p, newNode)
        p = p.next
    }
    p = head
    while (p != null) {
        let newNode = map.get(p)
        if (p.next != null) {
            newNode.next = map.get(p.next)
        }
        if (p.random != null) {
            newNode.random = map.get(p.random)
        }
        p = p.next
    }
    return map.get(head)
};

