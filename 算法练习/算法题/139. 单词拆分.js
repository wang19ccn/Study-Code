/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {boolean}
 */
var wordBreak = function (s, wordDict) {
    let wordSet = new Set(wordDict)
    let memo = new Array(s.length)

    var check = function (s, wordSet, start, memo) {
        if (start > s.length - 1) return true
        if (memo[start] !== undefined) return memo[start]
        for (let end = start + 1; end <= s.length; end++) {
            let word = s.slice(start, end)
            if (wordSet.has(word) && check(s, wordSet, end, memo)) {
                memo[start] = true
                return true
            }
        }
        memo[start] = false
        return false
    }

    return check(s, wordSet, 0, memo)
};