/**
 * 6-15的每日一题（之前已经练习过 => 复习本题)
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function (strs) {
    if (strs.length == 0) return ""

    let res = strs[0]

    for (let i = 1; i < strs.length; i++) {
        let j = 0;
        for (; j < strs[i].length && j < res.length; j++) {
            if (res[j] != strs[i][j]) {
                break
            }
        }
        res = res.substring(0, j)

        if (res === "") return res
    }

    return res;
};