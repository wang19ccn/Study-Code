/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {string[]}
 */
var wordBreak = function (s, wordDict) {
    let len = s.length
    let dict = new Set(wordDict)
    let memo = new Array(len)

    var dfs = function (start) {
        if (memo[start]) {
            return memo[start]
        }
        if (start > s.length - 1) {
            return [[]]
        }
        let res = []
        for (let i = start + 1; i <= len; i++) {
            let word = s.substring(start, i)
            if (dict.has(word)) {
                let temp = dfs(i)
                for (let t of temp) {
                    res.push([word].concat(t))
                }
            }
        }
        memo[start] = res
        return res
    }

    return dfs(0).map((words) => {
        return words.join(' ')
    })
};