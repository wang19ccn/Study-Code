/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
var hasCycle = function (head) {
    let check = new Map()
    while (head) {
        if (check.has(head)) return true
        check.set(head)
        head = head.next
    }
    return false
};