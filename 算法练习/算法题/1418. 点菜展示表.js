/**
 * @param {string[][]} orders
 * @return {string[][]}
 */
// orders[i] = [customerNamei, tableNumberi, foodItemi]，
// 1. customerNamei 是客户的姓名
// 2. tableNumberi 是客户所在餐桌的桌号
// 3. foodItemi 是客户点的餐品名称

var displayTable = function (orders) {
    let map = new Map()
    for (let i = 0; i < orders.length; i++) {
        if (map.has(orders[i][2])) {
            let temp = map.get(orders[i][2])
            temp.push(orders[i][1])
            map.set(orders[i][2], temp)
        } else {
            map.set(orders[i][2], [orders[i][1]])
        }
    }
    console.log(map)

    let arr = []
    map.forEach((v, k) => {
        arr.push([k, v])
    })

    arr.sort()
    console.log(arr)

    // 生成表头
    let title = ['Table']
    for (let i = 0; i < arr.length; i++) {
        title.push(arr[i][0])
    }
    console.log('Title:  ' + title)

    // 生成表的内容
    let finalContent = new Array()
    for (let i = 0; i < arr.length; i++) {
        let content = new Array()
        for (let j = 0; j < arr[i][1].length; j++) {
            content[j] = new Array(title.length).fill(0)
            content[j][0] = arr[i][1][j]
            content[j][i + 1] = 1
        }
        finalContent.push(content)
    }
    console.log(finalContent)

    // 拆成独立表
    let soloContent = []
    for (let i = 0; i < finalContent.length; i++) {
        for (let j = 0; j < finalContent[i].length; j++) {
            soloContent.push(finalContent[i][j])
        }
    }

    // 排序
    soloContent.sort((a, b) => {
        return Number(a[0]) - Number(b[0])
    })
    console.log(soloContent)

    // 按桌分类
    let mapContent = new Map()
    for (let i = 0; i < soloContent.length; i++) {
        let arrTemp = soloContent[i].slice(1, soloContent[i].length)
        if (mapContent.has(soloContent[i][0])) {
            let temp = mapContent.get(soloContent[i][0])
            temp.push(arrTemp)
            mapContent.set(soloContent[i][0], temp)
        } else {
            mapContent.set(soloContent[i][0], [arrTemp])
        }
    }
    console.log(mapContent)

    // 按桌合并点菜情况
    let resContent = []
    mapContent.forEach((v, k) => {
        console.log(v)
        let temp = new Array(v[0].length).fill(0)
        for (let i = 0; i < v.length; i++) {
            for (let j = 0; j < v[i].length; j++) {
                temp[j] += v[i][j]
            }
        }
        resContent.push([k, ...temp])
    })

    // 将表头和表内容组装在一起
    let res = [title]
    for (let i = 0; i < resContent.length; i++) {
        for (let k = 1; k < resContent[i].length; k++) {
            resContent[i][k] = resContent[i][k] + ""
        }
        res.push(resContent[i])
    }

    return res
};

console.log(displayTable(orders = [["James", "12", "Fried Chicken"], ["Ratesh", "12", "Fried Chicken"], ["Amadeus", "12", "Fried Chicken"], ["Adam", "1", "Canadian Waffles"], ["Brianna", "1", "Canadian Waffles"]]))

// console.log(displayTable([["David", "3", "Ceviche"], ["Corina", "10", "Beef Burrito"], ["David", "3", "Fried Chicken"], ["Carla", "5", "Water"], ["Carla", "5", "Ceviche"], ["Rous", "3", "Ceviche"]]))

// console.log(displayTable([["Laura", "2", "Bean Burrito"], ["Jhon", "2", "Beef Burrito"], ["Melissa", "2", "Soda"]]))