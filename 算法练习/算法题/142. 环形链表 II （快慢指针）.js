/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var detectCycle = function (head) {
    if (head == null) {
        return null;
    }
    let slow = head
    let fast = head
    do {
        // 考虑无环
        if (fast == null || fast.next == null) return null
        slow = slow.next
        fast = fast.next.next
    } while (slow != fast)
    slow = head
    while (slow != fast) {
        slow = slow.next
        fast = fast.next
    }
    return slow
};