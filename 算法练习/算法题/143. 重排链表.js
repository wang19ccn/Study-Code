function ListNode(val, next) {
    this.val = (val === undefined ? 0 : val)
    this.next = (next === undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function (head) {
    if (!head) return
    let l1 = head

    let mid = middleNode(head)
    // 注意，这里的变动要调整middle函数的判断条件
    // 要在偶数时取到靠右的数
    let l2 = mid.next
    // L1从mid断开
    mid.next = null

    l2 = reverseList(l2)
    mergeList(l1, l2)

};

var middleNode = function (head) {
    let slow = head
    let fast = head
    while (fast.next !== null && fast.next.next !== null) {
        slow = slow.next
        fast = fast.next.next
    }
    return slow
};

var reverseList = function (head) {
    let pre = new ListNode()
    let cur = head
    pre = null
    while (cur != null) {
        let tmp = cur.next
        cur.next = pre
        pre = cur
        cur = tmp
    }
    return pre
};

var mergeList = function (l1, l2) {
    let l1_temp = null
    let l2_temp = null
    while (l1 != null && l2 != null) {
        l1_temp = l1.next
        l2_temp = l2.next

        l1.next = l2
        l1 = l1_temp

        l2.next = l1
        l2 = l2_temp
    }
};


console.log(reorderList(head = {
    val: 1,
    next: {
        val: 2,
        next: {
            val: 3,
            next: {
                val: 4,
                next: null
            }
        }
    }
}))