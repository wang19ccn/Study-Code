function ListNode(val, next) {
    this.val = (val === undefined ? 0 : val)
    this.next = (next === undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function (head) {
    if (!head) return
    let list = []
    let cur = head
    while (cur) {
        // js链表其实是嵌套结构，不宜把next也引入，所以由数组自然顺序接替链表顺序,next清空
        const temp = cur
        cur = cur.next
        temp.next = null
        list.push(temp)
    }
    let i = 0
    let j = list.length - 1
    while (i < j) {
        list[i].next = list[j]
        i++
        if (i == j) break
        list[j].next = list[i]
        j--
    }
    list[i].next = null
};

console.log(reorderList(head = {
    val: 1,
    next: {
        val: 2,
        next: {
            val: 3,
            next: {
                val: 4,
                next: null
            }
        }
    }
}))