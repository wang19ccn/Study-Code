function ListNode(val, next) {
    this.val = (val === undefined ? 0 : val)
    this.next = (next === undefined ? null : next)
}

/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function (head) {
    if (head == null || head.next == null || head.next.next == null) {
        return;
    }
    let len = 0;
    let h = head;
    //求出节点数
    while (h != null) {
        len++;
        h = h.next;
    }

    reorderListHelper(head, len);
    console.log(head)
};

var reorderListHelper = function (head, len) {
    if (len == 1) {
        let outTail = head.next;
        head.next = null;
        return outTail;
    }
    if (len == 2) {
        let outTail = head.next.next;
        head.next.next = null;
        return outTail;
    }
    //得到对应的尾节点，并且将头结点和尾节点之间的链表通过递归处理
    let tail = reorderListHelper(head.next, len - 2);
    let subHead = head.next;//中间链表的头结点
    head.next = tail;
    let outTail = tail.next;  //上一层 head 对应的 tail
    tail.next = subHead;
    return outTail;
}

console.log(reorderList(head = {
    val: 1,
    next: {
        val: 2,
        next: {
            val: 3,
            next: {
                val: 4,
                next: null
            }
        }
    }
}))