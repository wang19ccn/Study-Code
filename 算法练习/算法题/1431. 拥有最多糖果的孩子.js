/**
 * @param {number[]} candies
 * @param {number} extraCandies
 * @return {boolean[]}
 */
var kidsWithCandies = function (candies, extraCandies) {
    let max = 0
    for (let candie of candies) {
        max = Math.max(max, candie)
    }
    for (let i = 0; i < candies.length; i++) {
        candies[i] = candies[i] + extraCandies >= max
    }
    return candies
};