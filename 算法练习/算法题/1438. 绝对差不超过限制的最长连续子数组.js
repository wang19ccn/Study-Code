/**
 * @param {number[]} nums
 * @param {number} limit
 * @return {number}
 */
var longestSubarray = function (nums, limit) {
    let left = 0
    let right = 0

    let queue_max = new Array()
    let queue_min = new Array()

    let maxLen = 0
    while (right < nums.length) {
        while (queue_max.length && queue_max[queue_max.length - 1] < nums[right]) queue_max.pop()
        while (queue_min.length && queue_min[queue_min.length - 1] > nums[right]) queue_min.pop()
        queue_max.push(nums[right])
        queue_min.push(nums[right])
        while (queue_max.length && queue_min.length && queue_max[0] - queue_min[0] > limit) {
            if (nums[left] == queue_max[0]) queue_max.shift()
            if (nums[left] == queue_min[0]) queue_min.shift()
            left++
        }
        maxLen = Math.max(maxLen, right - left + 1)
        right++
    }

    return maxLen
};

console.log(longestSubarray([8, 2, 4, 7], 4))