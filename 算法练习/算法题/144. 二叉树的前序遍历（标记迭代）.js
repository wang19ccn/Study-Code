/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function (root) {
    let white = 0
    let gray = 1
    let res = []
    let stack = [[white, root]]
    while (stack.length != 0) {
        let cur = stack.pop()
        let color = cur[0]
        let node = cur[1]
        if (!node) continue
        if (color == white) {
            stack.push([white, node.right])
            stack.push([white, node.left])
            stack.push([gray, node])
        } else {
            res.push(node.val)
        }
    }
    return res
};