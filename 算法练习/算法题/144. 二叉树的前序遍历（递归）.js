/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function (root) {
    let res = []
    var recursion = function (root) {
        if (!root) return
        res.push(root.val)
        recursion(root.left)
        recursion(root.right)
    }
    recursion(root)
    return res
};