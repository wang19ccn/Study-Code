/**
 * @param {number[]} arr
 * @return {number}
 */
var countTriplets = function (arr) {
    let n = arr.length;
    let res = 0;
    let preXOR = new Array(n + 1).fill(0);

    for (let i = 0; i < n; ++i) {
        preXOR[i + 1] = preXOR[i] ^ arr[i];
    }

    for (let i = 1; i <= n; ++i) {
        for (let j = i + 1; j <= n; ++j) {
            for (let k = j; k <= n; ++k) {
                let a = preXOR[j - 1] ^ preXOR[i - 1];
                let b = preXOR[k] ^ preXOR[j - 1];
                res += (a == b);
            }
        }
    }

    return res;
};