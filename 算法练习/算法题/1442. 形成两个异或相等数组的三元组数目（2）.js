/**
 * @param {number[]} arr
 * @return {number}
 */
// a = arr[i]−arr[j−1]
// b = arr[j] - arr[k]
// a ⊕ b = 0
// arr[i] ^...^ arr[j-1]^ arr[j] ^...^ arr[k] = 0
// 因为【i，k】的区间异或值为0，可以得到： preXor[i-1] == preXor[k]
var countTriplets = function (arr) {
    let n = arr.length;
    let res = 0;
    let preXOR = new Array(n + 1).fill(0);

    for (let i = 0; i < n; ++i) {
        preXOR[i + 1] = preXOR[i] ^ arr[i];
    }

    for (let i = 1; i <= n; ++i) {
        for (let k = i + 1; k <= n; ++k) {
            if (preXOR[i - 1] == preXOR[k]) {
                res += k - i;
            }
        }

    }

    return res;
};

