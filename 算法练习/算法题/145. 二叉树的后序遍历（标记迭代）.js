/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var postorderTraversal = function (root) {
    let white = 0
    let gray = 1
    let res = []
    let stack = [[white, root]]
    while (stack.length != 0) {
        let cur = stack.pop()
        let color = cur[0]
        let node = cur[1]
        if (!node) continue
        if (color == white) {
            stack.push([gray, node])
            stack.push([white, node.right])
            stack.push([white, node.left])
        } else {
            res.push(node.val)
        }
    }
    return res
};