// 利用Map和其有序性
// 根据Map的有序性，头部为最近最少使用

/**
 * @param {number} capacity
 */
var LRUCache = function (capacity) {
    this.cache = new Map()
    this.capacity = capacity
};

/** 
 * @param {number} key
 * @return {number}
 */
LRUCache.prototype.get = function (key) {
    let val = this.cache.get(key)
    // 判断不存在，直接返回-1
    if (typeof val === 'undefined') return -1
    // 存在，则删除后重新插入尾部
    this.cache.delete(key)
    this.cache.set(key, val)
    return val
};

/** 
 * @param {number} key 
 * @param {number} value
 * @return {void}
 */
LRUCache.prototype.put = function (key, value) {
    // 存在，则删除后重新插入（调整访问顺序）
    if (this.cache.has(key)) this.cache.delete(key)
    // 如果储存量等于设定值则为满，删除头部第一个元素，利用Map的有序性（使用keys()方法）
    else if (this.cache.size === this.capacity) this.cache.delete(this.cache.keys().next().value)
    // 插入（更新）键值对
    this.cache.set(key, value)
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * var obj = new LRUCache(capacity)
 * var param_1 = obj.get(key)
 * obj.put(key,value)
 */