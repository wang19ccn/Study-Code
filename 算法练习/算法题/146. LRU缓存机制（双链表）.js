/**
 * 双链表定义
 * 定义 ListNode 类
 */
class ListNode {
    constructor(key, value) {
        this.key = key // 存key
        this.value = value // 存value
        this.next = null
        this.prev = null
    }
}

class LRUCache {
    constructor(capacity) { // 容量由用户设置
        this.capacity = capacity // 缓存的容量
        this.hashTable = {} // 哈希表用对象实现
        this.count = 0 // 缓存数目，初始为0
        this.dummyHead = new ListNode() // 虚拟头结点
        this.dummyTail = new ListNode() // 虚拟尾节点
        this.dummyHead.next = this.dummyTail
        this.dummyTail.prev = this.dummyHead // 相联系
    }

    // get()方法
    get(key) {
        let node = this.hashTable[key] // 从哈希表中获取对应的节点
        if (node == null) return -1 // 如果节点不存在，返回-1
        this.moveToHead(node) // 因为被读取了，所以该节点移动到链表头部
        return node.value // 返回出节点的value
    }

    moveToHead(node) { // 将节点移动到链表头部
        this.removeFromList(node) // 从链表中删除节点
        this.addToHead(node) // 添加到链表的头部
    }

    removeFromList(node) { // 删除node节点
        let tempForPrev = node.prev // 暂存它的后继节点
        let tempForNext = node.next // 暂存它的前驱节点
        tempForPrev.next = tempForNext // 前驱节点的next指向后继节点
        tempForNext.prev = tempForPrev // 后继节点的prev指向前驱节点
    }

    addToHead(node) { // 插入到虚拟头结点和真实头结点之间
        node.prev = this.dummyHead // node的prev指针指向虚拟头结点
        node.next = this.dummyHead.next // node的next指针指向原来的真实头结点
        this.dummyHead.next.prev = node // 原来的真实头结点的prev指向node
        this.dummyHead.next = node // 虚拟头结点的next指针指向node
    }

    // put()方法
    put(key, value) {
        let node = this.hashTable[key] // 获取链表中的node
        if (node == null) { // 如果不存在于链表
            let newNode = new ListNode(key, value) // 为键值对创建新的节点
            this.hashTable[key] = newNode // 存入哈希表
            this.addToHead(newNode) // 将节点添加到链表头部
            this.count++ // 缓存数目+1
            if (this.count > this.capacity) { // 超出缓存容量
                this.removeLRUItem() // 移除最远一次使用的item
            }
        } else { // 如果已经存在于链表
            node.value = value // 更新value
            this.moveToHead(node) // “使用”了数据，将节点移到链表头部
        }
    }

    removeLRUItem() { // 删除LRU item
        let tail = this.popTail() // 执行popTail，将它从链表尾部删除
        delete this.hashTable[tail.key] // 哈希表中也将它删除
        this.count-- // 缓存数目-1
    }

    popTail() { // 删除链表尾节点
        let tailItem = this.dummyTail.prev // 即 虚拟尾节点的上一节点
        this.removeFromList(tailItem) // 删除该真实尾节点
        return tailItem // 返回被删除的节点
    }
}

//   题解
//   作者：hyj8
//   链接：https://leetcode-cn.com/problems/lru-cache/solution/bu-yong-yu-yan-nei-jian-de-map-gua-dang-feng-zhuan/
//   来源：力扣（LeetCode）
//   著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。