/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var insertionSortList = function (head) {
    let dummyHead = new ListNode(0)

    let cur = head
    let prev = null
    let temp = null

    dummyHead.next = head
    while (cur && cur.next) {
        if (cur.val <= cur.next.val) {
            cur = cur.next
        } else {
            temp = cur.next
            cur.next = cur.next.next

            prev = dummyHead
            while (prev.next.val <= temp.val) {
                prev = prev.next
            }

            temp.next = prev.next
            prev.next = temp
        }
    }

    return dummyHead.next
};