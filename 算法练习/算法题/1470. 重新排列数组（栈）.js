/**
 * @param {number[]} nums
 * @param {number} n
 * @return {number[]}
 */
var shuffle = function (nums, n) {
    let res = new Array()
    for (let i = 0, j = nums.length / 2; i < nums.length && j < nums.length; i++, j++) {
        res.push(nums[i])
        res.push(nums[j])
    }
    return res
};