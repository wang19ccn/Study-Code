/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var sortList = function (head) {
    if (!head || !head.next) {
        return head
    }

    let mid = middleNode(head)
    let temp = mid.next

    mid.next = null
    let left = head
    let right = temp

    left = sortList(left)
    right = sortList(right)

    return mergeTwoLists(left, right)
};

// 中点
var middleNode = function (head) {
    let slow = head
    let fast = head
    while (fast.next !== null && fast.next.next !== null) {
        slow = slow.next
        fast = fast.next.next
    }
    return slow
}

// 合并
let mergeTwoLists = function (list_1, list_2) {
    let pre = new ListNode(-1)
    let cur = pre
    while (list_1 && list_2) {
        if (list_1.val < list_2.val) {
            cur.next = list_1
            list_1 = list_1.next
        } else {
            cur.next = list_2
            list_2 = list_2.next
        }
        cur = cur.next
    }
    cur.next = list_1 || list_2
    return pre.next
}