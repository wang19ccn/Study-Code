/** 
 * 6-12号每日一题,之前已经练习过，回家后复习这道题
 * @param {number[]} nums
 * @return {number[][]}
 */
var threeSum = function (nums) {
    let ans = []
    let len = nums.length

    if (nums == null || len < 3) return ans

    //排序
    nums.sort((a, b) => a - b);

    //特殊情况
    if (nums[0] > 0 || nums[len - 1] < 0) return ans

    for (let i = 0; i < len; i++) {
        if (nums[i] > 0) break //如果当前数字大于零，则三数之和一定大于零
        if (i > 0 && nums[i] == nums[i - 1]) continue
        let L = i + 1
        let R = len - 1
        while (L < R) {
            let sum = nums[i] + nums[L] + nums[R]
            if (sum == 0) {
                ans.push([nums[i], nums[L], nums[R]])
                while (L < R && nums[L] == nums[L + 1]) L++
                while (L < R && nums[R] == nums[R - 1]) R--
                L++
                R--
            }
            else if (sum < 0) L++
            else if (sum > 0) R--
        }
    }

    return ans
};