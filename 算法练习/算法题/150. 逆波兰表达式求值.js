/**
 * @param {string[]} tokens
 * @return {number}
 */
var evalRPN = function (tokens) {
    let stack = []

    for (let t of tokens) {
        if (t == '+' || t == '-' || t == '*' || t == '/') {
            let num_1 = parseInt(stack.pop())
            let num_2 = parseInt(stack.pop())
            switch (t) {
                case '+':
                    stack.push(num_1 + num_2)
                    break
                case '-':
                    stack.push(num_2 - num_1)
                    break
                case '*':
                    stack.push(num_1 * num_2)
                    break
                case '/':
                    stack.push(num_2 / num_1 > 0 ? Math.floor(num_2 / num_1) : Math.ceil(num_2 / num_1))
                    break
            }
        } else {
            stack.push(t)
        }
    }

    return stack.pop()
};

console.log(evalRPN(["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]))