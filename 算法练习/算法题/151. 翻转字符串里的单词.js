/**
 * @param {string} s
 * @return {string}
 */
//正则
var reverseWords = function(s) {
    console.log("正则")
    return s.trim().replace(/\s+/g,' ').split(' ').reverse().join(' ')
};

//filter
var reverseWords_1 = function(s) {
    console.log("filter")
    return s.trim().split(' ').filter(v=>v !=='').reverse().join(' ')
};
console.log(reverseWords_1("the sky is blue"));