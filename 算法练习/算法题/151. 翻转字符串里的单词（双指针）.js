/**
 * @param {string} s
 * @return {string}
 */
var reverseWords = function (s) {
    let len = s.length
    if (len < 1) return s

    let left = 0
    let right = 0

    s = s.trim()
    let res = []
    while (right < len) {
        if (s[right] == ' ' || right == len - 1) {
            let temp
            if (right == len - 1) {
                temp = s.slice(left, right + 1)
            }
            else {
                temp = s.slice(left, right)
            }

            res.push(temp)
            right++
            left = right
        }
        while (s[right] == ' ' && s[right - 1] == ' ') {
            right++
            left = right
        }
        right++
    }

    return res.reverse().join(' ')
};

console.log(reverseWords("  the   sky is blue"))