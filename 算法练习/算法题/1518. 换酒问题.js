/**
 * @param {number} numBottles
 * @param {number} numExchange
 * @return {number}
 */
var numWaterBottles = function (numBottles, numExchange) {
    let res = numBottles
    let num = numBottles
    while (num >= numExchange) {
        res += parseInt(num / numExchange)
        num = num % numExchange + parseInt(num / numExchange)
    }
    return res
};

console.log(numWaterBottles(5, 5))