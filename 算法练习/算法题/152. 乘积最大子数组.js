/**
 * @param {number[]} nums
 * @return {number}
 */
var maxProduct = function (nums) {
    let dpMax = nums[0]
    let dpMin = nums[0]
    let max = nums[0]
    for (let i = 1; i < nums.length; i++) {
        //更新 dpMin 的时候需要 dpMax 之前的信息，所以先保存起来
        let preMax = dpMax
        dpMax = Math.max(dpMin * nums[i], dpMax * nums[i], nums[i])
        dpMin = Math.min(dpMin * nums[i], preMax * nums[i], nums[i])
        max = Math.max(max, dpMax)
    }
    return max
};