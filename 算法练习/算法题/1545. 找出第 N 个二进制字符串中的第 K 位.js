/**
 * @param {number} n
 * @param {number} k
 * @return {character}
 */
var findKthBit = function (n, k) {
    let s = "0"
    for (let i = 1; i < n; ++i) {
        s = s + "1" + reverse(s)
    }
    return s[k - 1]
};

var reverse = function (s) {
    let arr = s.split("")
    for (let i = 0; i < arr.length; ++i) {
        arr[i] = arr[i] ^ 1
    }
    return arr.reverse().join("")
}

console.log(findKthBit(3, 1))