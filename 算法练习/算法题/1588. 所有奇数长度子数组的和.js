/**
 * @param {number[]} arr
 * @return {number}
 */
var sumOddLengthSubarrays = function (arr) {
    let sum = 0
    for (let i = 1; i <= arr.length; i += 2) {
        for (let j = 0; j <= arr.length - i; j++) {
            let count = j
            while (count < i + j) {
                sum += arr[count]
                count++
            }
        }
    }
    return sum
};

console.log(sumOddLengthSubarrays([1, 4, 2, 5, 3]))