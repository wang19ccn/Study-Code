/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var threeSumClosest = function (nums, target) {
    //排序
    nums.sort((a, b) => a - b)
    //初始
    let ans = nums[0] + nums[1] + nums[2]

    for (let i = 0; i < nums.length; i++) {
        if (i > 0 && nums[i] == nums[i - 1]) continue
        let start = i + 1
        let end = nums.length - 1
        while (start < end) {
            let sum = nums[i] + nums[start] + nums[end]
            if (Math.abs(target - sum) < Math.abs(target - ans)) {
                ans = sum
            }
            if (sum > target) {
                end--
            }
            else if (sum < target) {
                start++
            }
            else {
                return ans
            }
        }
    }
    return ans
};