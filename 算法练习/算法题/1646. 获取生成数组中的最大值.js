/**
 * @param {number} n
 * @return {number}
 */
var getMaximumGenerated = function (n) {
    if (n == 0) return 0

    let nums = new Array(n + 1)

    nums[0] = 0
    nums[1] = 1

    for (let i = 0; i < n; i++) {
        if (2 * i <= n) nums[2 * i] = nums[i]
        if (2 * i + 1 <= n) nums[2 * i + 1] = nums[i] + nums[i + 1]
    }

    let res = 0
    for (let i of nums) {
        res = Math.max(res, i)
    }

    return res
};

console.log(getMaximumGenerated(20))