/**
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (numbers, target) {
    for (let i = 0; i < numbers.length; i++) {
        let left = i + 1
        let right = numbers.length - 1
        while (left <= right) {
            let mid = (left + parseInt((right - left) / 2))
            if (numbers[mid] < target - numbers[i]) {
                left = mid + 1
            }
            else if (numbers[mid] > target - numbers[i]) {
                right = mid - 1
            }
            else {
                return [i + 1, mid + 1]
            }
        }
    }
};

console.log(twoSum([2, 7, 11, 15], 17))