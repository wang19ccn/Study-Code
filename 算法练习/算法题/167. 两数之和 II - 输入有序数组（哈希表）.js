/** 
 * 用数组模拟实现
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (numbers, target) {
    let check = []
    for (let i = 0; i < numbers.length; i++) {
        let tmp = target - numbers[i]
        if (check[tmp] !== undefined) return [check[tmp] + 1, i + 1]
        check[numbers[i]] = i
    }
    return [-1, -1]
};

console.log(twoSum([2, 7, 11, 15], 26))