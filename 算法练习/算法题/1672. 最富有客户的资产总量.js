/**
 * @param {number[][]} accounts
 * @return {number}
 */
var maximumWealth = function (accounts) {
    let max = 0
    for (let account of accounts) {
        let sum = 0
        for (let a of account) {
            sum += a
        }
        max = Math.max(max, sum)
    }
    return max
};