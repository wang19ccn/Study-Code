/**
 * @param {number} columnNumber
 * @return {string}
 */
var convertToTitle = function (columnNumber) {
    let res = []
    while (columnNumber) {
        let remainder = columnNumber % 26
        if (remainder == 0) {
            remainder = 26
            columnNumber -= 26
        }
        res.push(String.fromCharCode(remainder + 64))
        columnNumber = parseInt(columnNumber / 26)
    }
    return res.reverse().join('')
};
console.log(convertToTitle(52))
console.log(convertToTitle(701))