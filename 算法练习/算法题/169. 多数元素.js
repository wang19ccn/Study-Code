/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function (nums) {
    if (nums.length <= 1) return nums[0]
    let maxNum = parseInt(nums.length / 2)
    let count = 1
    nums.sort((a, b) => a - b)
    for (let i = 1; i < nums.length; i++) {
        if (nums[i - 1] == nums[i]) {
            count++
        } else {
            count = 1
        }
        if (count > maxNum) {
            return nums[i]
        }
    }
};

console.log(majorityElement([1, 2, 1]))