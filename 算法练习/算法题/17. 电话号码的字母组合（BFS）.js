/**
 * @param {string} digits
 * @return {string[]}
 */
var letterCombinations = function (digits) {
    if (digits.length == 0) return []
    let map = {
        2: 'abc',
        3: 'def',
        4: 'ghi',
        5: 'jkl',
        6: 'mno',
        7: 'pqrs',
        8: 'tuv',
        9: 'wxyz'
    }
    let queue = []
    queue.push('')
    for (let i = 0; i < digits.length; i++) {
        let num = queue.length
        for (let j = 0; j < num; j++) {
            let cur = queue.shift()
            let letters = map[digits[i]]
            for (let l of letters) {
                queue.push(cur + l)
            }
        }
    }
    return queue
};


