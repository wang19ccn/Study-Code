/**
 * @param {string} digits
 * @return {string[]}
 */
var letterCombinations = function (digits) {
    if (digits.length == 0) return []
    let res = []
    let map = {
        2: 'abc',
        3: 'def',
        4: 'ghi',
        5: 'jkl',
        6: 'mno',
        7: 'pqrs',
        8: 'tuv',
        9: 'wxyz'
    }
    dfs(digits, map, res, '', 0)
    return res
};

var dfs = function (digits, map, res, str, point) {
    if (point > digits.length - 1) {
        res.push(str)
        return
    }
    let letters = map[digits[point]]
    for (let l of letters) {
        dfs(digits, map, res, str + l, point + 1)
    }
}
