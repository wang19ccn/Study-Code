/**
 * @param {number[]} deliciousness
 * @return {number}
 */
var countPairs = function (deliciousness) {
    const len = deliciousness.length
    const MOD = 1000000007;
    // 1. 寻找循环限制条件，任意两个数之和不会超过最大值x2
    let maxValue = 0
    for (let d of deliciousness) {
        maxValue = Math.max(maxValue, d)
    }
    const maxSum = maxValue * 2
    // 2. 寻找组合数
    let pairs = 0
    let map = new Map()
    for (let i = 0; i < len; i++) {
        const val = deliciousness[i]
        for (let sum = 1; sum <= maxSum; sum *= 2) {
            let count = map.get(sum - val) || 0
            pairs = (pairs + count) % MOD
        }
        // 倒序匹配 （1 —> 0，1 —> 1，1 —> 2）
        map.set(val, (map.get(val) || 0) + 1)
    }
    return pairs
};

console.log(countPairs([1, 1, 1, 3, 3, 3, 7]))