/**
 * @param {number[]} encoded
 * @return {number[]}
 */
var decode = function (encoded) {
    let len = encoded.length;
    let perm = new Array(len + 1);

    let ABCDE = 0;
    // 前 n 个正整数的排列，且 n 是奇数。
    for (let i = 1; i <= len + 1; i++) {
        ABCDE ^= i;
    }

    let BCDE = 0;
    for (let i = 1; i < len; i += 2) {
        BCDE ^= encoded[i];
    }

    // 参考 1720 题
    perm[0] = ABCDE ^ BCDE;
    for (let i = 1; i <= len; i++) {
        perm[i] = perm[i - 1] ^ encoded[i - 1];
    }

    return perm;
};


// 解题思路
// https://leetcode-cn.com/problems/decode-xored-permutation/solution/ji-shuang-yi-wen-dai-ni-shua-liang-dao-j-mujs/


// 注：
// ABCDE (A⊕B⊕C⊕D⊕E)
// BCDE (B⊕C⊕D⊕E)
// 异或运算法则 a ⊕ b = b ⊕ a
// 则 A⊕B⊕C⊕D⊕E ⊕ B⊕C⊕D⊕E = A

// 第一步 perm = [A, B, C, D, E]，encoded = [AB, BC, CD, DE]；
// 第二步 根据perm，我们可以得到ABCDE,根据encoded的BC和DE，我们可以得到BCDE；
// 第三步 求得perm[0]，参考1720
