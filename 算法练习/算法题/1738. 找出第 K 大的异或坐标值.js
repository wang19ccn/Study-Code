/**
 * @param {number[][]} matrix
 * @param {number} k
 * @return {number}
 */
var kthLargestValue = function (matrix, k) {
    let max = -Infinity;

    let m = matrix.length;
    let n = matrix[0].length;

    for (let i = 0; i < m; i++) {
        for (let j = 1; j < n; j++) {
            matrix[i][j] ^= matrix[i][j - 1];
        }
    }

    let res = []
    for (let j = 0; j < n; j++) {
        res.push(matrix[0][j])
        for (let i = 1; i < m; i++) {
            matrix[i][j] ^= matrix[i - 1][j];
            res.push(matrix[i][j])
        }
    }

    res.sort((a, b) => a - b)
    return res[res.length - k]
};

console.log(kthLargestValue([[5, 2], [1, 6]], 1))