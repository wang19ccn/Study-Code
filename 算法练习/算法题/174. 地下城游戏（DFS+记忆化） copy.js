/**
 * @param {number[][]} dungeon
 * @return {number}
 */
var calculateMinimumHP = function (dungeon) {
    let m = dungeon.length
    let n = dungeon[0].length

    let memo = new Array(m)
    for (let i = 0; i < m; i++) {
        memo[i] = new Array(n)
    }

    var dfs = function (i, j) {
        if (i == m - 1 && j == n - 1) {
            return Math.max(1 - dungeon[i][j], 1)
        }
        if (memo[i][j] > 0) return memo[i][j]
        let minRes = 0
        if (i == m - 1) {
            minRes = Math.max(dfs(i, j + 1) - dungeon[i][j], 1)
        }
        else if (j == n - 1) {
            minRes = Math.max(dfs(i + 1, j) - dungeon[i][j], 1)
        }
        else {
            minRes = Math.max(Math.min(dfs(i + 1, j), dfs(i, j + 1)) - dungeon[i][j], 1)
        }
        memo[i][j] = minRes
        return memo[i][j]
    }

    return dfs(0, 0)
};