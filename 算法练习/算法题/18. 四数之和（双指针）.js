/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[][]}
 */
var fourSum = function (nums, target) {
    if (nums.length < 4) return []

    let res = []
    let len = nums.length

    nums.sort((a, b) => a - b)
    for (let a = 0; a <= len - 4; a++) {
        if (a > 0 && nums[a] == nums[a - 1]) continue
        for (let b = a + 1; b <= len - 3; b++) {
            if (b > a + 1 && nums[b] == nums[b - 1]) continue
            let c = b + 1
            let d = len - 1
            while (c < d) {
                if (nums[a] + nums[b] + nums[c] + nums[d] < target) {
                    c++
                }
                else if (nums[a] + nums[b] + nums[c] + nums[d] > target) {
                    d--
                }
                else {
                    res.push([nums[a], nums[b], nums[c], nums[d]])
                    while (c < d && nums[c + 1] == nums[c]) {
                        c++
                    }
                    while (c < d && nums[d - 1] == nums[d]) {
                        d--
                    }
                    c++
                    d--
                }
            }
        }
    }

    return res
}

console.log(fourSum([-1, 0, -5, -2, -2, -4, 0, 1, -2], -9))