/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[][]}
 */
var fourSum = function (nums, target) {
    if (nums.length == 0) {
        return []
    }
    let set = new Set(nums)
    let res = []  // 结果集合
    let path = [] // 路径结合
    nums.sort((a, b) => a - b) // 排序
    dfs(nums, target, 0, 0, path, res, set)
    return res
};

var dfs = function (nums, target, start, depth, path, res, set) {
    if (depth == 3) {
        // 剪枝4
        if ((set.has(target) && target > nums[start - 1]) || (target == nums[start])) {
            path.push(target)
            res.push([...path])
            path.pop()
        }
        return
    }
    for (let i = start; i < nums.length; i++) {
        // 剪枝1 
        if (i > start && nums[i] == nums[i - 1]) {
            continue
        }

        // 剪枝2
        if (nums[i] + (3 - depth) * nums[i + 1] > target) return
        // 剪枝3
        if (nums[i] + (3 - depth) * nums[nums.length - 1] < target) continue

        path.push(nums[i])
        dfs(nums, target - nums[i], i + 1, depth + 1, path, res, set)
        path.pop()
    }
}

console.log(fourSum([1, -2, -5, -4, -3, 3, 3, 5], -11))