/**
 * @param {number[]} nums
 * @return {number}
 */
var arraySign = function (nums) {
    let product = 1;
    for (let x of nums) {
        if (0 == x) {
            return 0;
        }
        if (x < 0) {
            product *= -1;
        }
    }
    return product;
};
