/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function (head, n) {
    let pre = new ListNode(0)
    pre.next = head
    let start = pre
    let end = pre

    while (n != 0) {
        start = start.next
        n--
    }
    while (start.next != null) {
        start = start.next
        end = end.next
    }

    end.next = end.next.next
    return pre.next
};