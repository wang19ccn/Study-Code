/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
    let len = nums.length
    if (0 == len) return 0
    if (1 == len) return nums[0]

    // let dp = []
    // dp[0] = nums[0]
    // dp[1] = Math.max(nums[0], nums[1])
    // for (let i = 2; i < len; i++) {
    //     dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i])
    // }
    // return dp[len - 1]

    //滚动数组 将空间复杂度 O(n) 降到 O(1)
    let first = nums[0]
    let second = Math.max(nums[0], nums[1])
    for (let i = 2; i < len; i++) {
        let temp = second
        second = Math.max(second, first + nums[i])
        first = temp
    }
    return second
}

console.log(rob([2, 1, 1, 2]))