/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */

var rightSideView = function (root) {   
    dfs(root,0,res=[])
    return res;
};

var dfs = function(root,depth){
    if(!root){
        return []
    }
    if(!res[depth]){
        res.push(root.val)
    }
    depth++;
    dfs(root.right,depth);
    dfs(root.left,depth);
};



console.log(rightSideView([]))

function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}