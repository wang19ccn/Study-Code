/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
    let arr = s.split("")
    let queue = []
    if (arr.length % 2 != 0) return false
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] == '(') {
            queue.push(arr[i])
            continue
        }
        if (arr[i] == ')') {
            if (queue.pop() != '(') return false
            continue
        }
        if (arr[i] == '[') {
            queue.push(arr[i])
            continue
        }
        if (arr[i] == ']') {
            if (queue.pop() != '[') return false
            continue
        }
        if (arr[i] == '{') {
            queue.push(arr[i])
            continue
        }
        if (arr[i] == '}') {
            if (queue.pop() != '{') return false
            continue
        }
    }
    if (queue.length == 0) {
        return true
    } else {
        return false
    }
};

console.log(isValid("(]"))