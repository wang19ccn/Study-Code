/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
    let stack = []
    for (let c of s) {
        if (c == '(' || c == '[' || c == '{') {
            stack.push(c)
            continue
        }
        if (c == ')') {
            if (stack.pop() != '(') return false
            continue
        }
        if (c == ']') {
            if (stack.pop() != '[') return false
            continue
        }
        if (c == '}') {
            if (stack.pop() != '{') return false
            continue
        }
    }
    return stack.length == 0 ? true : false
};

console.log(isValid("(]"))