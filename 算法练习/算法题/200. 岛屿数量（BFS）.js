/**
 * @param {character[][]} grid
 * @return {number}
 */
var numIslands = function (grid) {
    if (grid == null || grid.length == 0) {
        return 0;
    }

    let dx = [-1, 1, 0, 0];
    let dy = [0, 0, -1, 1];

    let queue = new Array();
    let m = grid.length; //获取所有元素数目
    let n = grid[0].length; //获取单行元素数目


    let sum = 0;
    let point = [];

    //从每个元素开始都做一遍bfs
    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            if (1 == grid[i][j]) {
                sum++;
                queue.push([i, j]);
                grid[i][j]=0;//标志当前岛屿已经来过
                while (0 != queue.length) {
                    point = queue.shift();
                    let x = point[0];
                    let y = point[1];
                    for (let i = 0; i < 4; i++) {
                        let newX = x + dx[i];
                        let newY = y + dy[i];
                        if (newX < 0 || newX >= m || newY < 0 || newY >= n || grid[newX][newY] == 0) {
                            continue;
                        }
                        grid[newX][newY] = 0;
                        queue.push([newX, newY]);
                    }
                }

            }
        }
    }

    return sum;
};

console.log(numIslands([[1,1,1,1,0],
    [1,1,0,1,0],
    [1,1,0,0,0],
    [0,0,0,0,0]]))
    
console.log(numIslands([[1,1,0,0,0],
    [1,1,0,0,0],
    [0,0,1,0,0],
    [0,0,0,1,1]]))
    