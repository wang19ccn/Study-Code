/**
 * @param {number} n
 * @return {boolean}
 */
var isHappy = function(n) {
    let set = new Set();
    while(n!=1&&!set.has(n)){
        set.add(n)
        n=bitSquareSum(n)
    }
    return n==1;
};

var bitSquareSum = function(x){
    let sum=0
    while(x>0){
        let bit = x%10
        x=parseInt(x/10)
        sum+=bit*bit
    }
    return sum
};

console.log(isHappy(19))