/**
 * @param {number[]} rolls
 * @param {number} mean
 * @param {number} n
 * @return {number[]}
 */
var missingRolls = function (rolls, mean, n) {
    const len = rolls.length

    let temp = 0
    for (let r of rolls) temp += r

    let sum = mean * (len + n) - temp

    if (sum < n || sum > 6 * n) return []

    let res = new Array(n)
    res.fill(Math.floor(sum / n))
    if (Math.floor(sum / n) * n < sum) {
        let d = sum - (Math.floor(sum / n) * n)
        let idx = 0
        while (d > 0) {
            res[idx]++
            idx++
            d--
        }
    }
    return res
};

console.log(missingRolls([3, 2, 4, 3], 4, 2))