/**
 * 需要剪枝
 * @param {number[]} rolls
 * @param {number} mean
 * @param {number} n
 * @return {number[]}
 */
var missingRolls = function (rolls, mean, n) {
    const len = rolls.length

    let temp = 0
    for (let r of rolls) {
        console.log(r)
        temp += r
    }
    let sum = mean * (len + n) - temp
    let res = []
    let flag = 0

    let dfs = function (x, surplus) {
        res.push(x)
        surplus -= x
        if (surplus == 0 && res.length - 1 < n) return
        if (res.length - 1 > n) return
        if (res.length - 1 == n && surplus == 0) {
            debugger
            return flag = 1
        }

        for (let i = 1; i <= 6; i++) {
            if (flag) return
            dfs(i, surplus)
            if (flag) return
            res.pop()
        }

    }

    dfs(0, sum)

    res.shift()
    return res
};

console.log(missingRolls([3, 2, 4, 3], 4, 2))

/*

rolls = [3,2,4,3], mean = 4, n = 2

(3+2+4+3+x+y) / 6 = 4

24 - 3 - 2 - 4 - 3 = 12

y = 12 - x 

*/
