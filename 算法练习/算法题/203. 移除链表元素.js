/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} val
 * @return {ListNode}
 */
var removeElements = function (head, val) {
    let init = new ListNode(0)
    init.next = head

    let cur = head
    let per = init
    while (cur != null) {
        if (cur.val == val) {
            per.next = cur.next
        }
        else {
            per = cur
        }
        cur = cur.next
    }

    return init.next
};