var StockPrice = function () {
    this.map = new Map()
    this.time = -Infinity
};

/** 
 * @param {number} timestamp 
 * @param {number} price
 * @return {void}
 */
StockPrice.prototype.update = function (timestamp, price) {
    this.map.set(timestamp, price)
    if (timestamp > this.time) this.time = timestamp
};

/**
 * @return {number}
 */
StockPrice.prototype.current = function () {
    return this.map.get(this.time)
};

/**
 * @return {number}
 */
StockPrice.prototype.maximum = function () {
    let max = -Infinity
    for (let item of this.map.values()) {
        if (max < item) max = item
    }
    return max
};

/**
 * @return {number}
 */
StockPrice.prototype.minimum = function () {
    let min = Infinity
    for (let item of this.map.values()) {
        if (min > item) min = item
    }
    return min
};

/**
 * Your StockPrice object will be instantiated and called as such:
 * var obj = new StockPrice()
 * obj.update(timestamp,price)
 * var param_2 = obj.current()
 * var param_3 = obj.maximum()
 * var param_4 = obj.minimum()
 */

let obj = new StockPrice()
obj.update(1, 10)
obj.update(2, 5)
obj.current()

console.log(obj)
console.log(obj.current())