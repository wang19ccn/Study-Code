/** 76ms 35.2MB
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var reverseList = function (head) {
    //递归终止条件是当前为空，或者下一个节点为空
    if (head == null || head.next == null) return head
    //cur是最后一个节点
    let cur = reverseList(head.next)
    head.next.next = head
    head.next = null
    return cur
};