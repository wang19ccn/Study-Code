/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {boolean}
 */
var canFinish = function (numCourses, prerequisites) {
    let arr = new Array()
    for (let i = 0; i < numCourses; i++) {
        arr[i] = new Array()
    }
    let flags = new Array(numCourses).fill(0)
    for (let p of prerequisites) {
        arr[p[1]].push(p[0])
    }
    for (let i = 0; i < numCourses; i++) {
        if (!dfs(arr, flags, i)) return false
    }
    return true
};

var dfs = function (arr, flags, i) {
    if (flags[i] == 1) return false
    if (flags[i] == -1) return true
    flags[i] = 1
    for (let k of arr[i]) {
        if (!dfs(arr, flags, k)) return false
    }
    flags[i] = -1
    return true
}

console.log(canFinish(2, [[1, 0], [0, 1]]))