/**
 * @param {number} s
 * @param {number[]} nums
 * @return {number}
 */
var minSubArrayLen = function (s, nums) {
    let left = 0
    let right = 0
    let sum = 0
    let min = Infinity
    while (right < nums.length) {
        sum += nums[right++]
        while (sum >= s) {
            min = Math.min(min, right - left)
            sum -= nums[left++]
        }
    }
    return min == Infinity ? 0 : min
};