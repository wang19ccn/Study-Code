/** 延用了23题的解法，试试别的解法
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var mergeTwoLists = function(l1, l2) {
    let lists=[l1,l2]
    let arr=[]
    for(let i=0;i<lists.length;i++){
        let item=lists[i]
        while(item){
            arr.push(item.val)
            item=item.next
        }
    }

    arr.sort((a,b)=>a-b)
    let head=new ListNode()
    let cur=head

    for(let i=0;i<arr.length;i++){
        cur.next=new ListNode(arr[i])
        cur=cur.next
    }

    return head.next
};