/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {number[]}
 */
var findOrder = function (numCourses, prerequisites) {
    let inDegree = new Array(numCourses).fill(0)
    let graph = {} // 存放一个课程和其后续对应的课成
    for (let i = 0; i < prerequisites.length; i++) {
        inDegree[prerequisites[i][0]]++ // 构建入度数组，当前课程需要几门先学课程
        if (graph[prerequisites[i][1]]) {
            graph[prerequisites[i][1]].push(prerequisites[i][0])
        } else {
            let list = []
            list.push(prerequisites[i][0])
            graph[prerequisites[i][1]] = list
        }
    }

    let res = []
    let queue = []
    // 存入度为0的课
    for (let i = 0; i < numCourses; i++) {
        if (inDegree[i] === 0) queue.push(i)
    }
    while (queue.length) {
        let cur = queue.shift() // 选择队列中第一个课程
        res.push(cur)
        let checkQueue = graph[cur] // 查看哈希表，获取对应的后续课程
        if (checkQueue && checkQueue.length) { // 确保有后续课程
            for (let i = 0; i < checkQueue.length; i++) { // 遍历后续课程
                inDegree[checkQueue[i]]-- // 将后续课程的入度-1
                if (inDegree[checkQueue[i]] === 0) {
                    queue.push(checkQueue[i])
                }
            }
        }
    }
    // 可能会有多个正确的顺序，你只要返回一种就可以了。如果不可能完成所有课程，返回一个空数组。
    return res.length === numCourses ? res : []
};