/**
 * @param {string} s
 * @return {string}
 */
var shortestPalindrome = function (s) {
    let rev = s.split('').reverse().join('')
    let str = s + '#' + rev
    let prefix = new Array(str.length)

    // 计算前后缀
    prefix_table(str, prefix, str.length)

    let maxLen = prefix[str.length - 1]
    let add_s = rev.substring(0, s.length - maxLen)
    return add_s + s
};


/**
 * 计算前缀表
 * @param {*} pattern 模式串
 * @param {*} prefix 前缀表
 * @param {*} n 模式串长度
 */
var prefix_table = function (pattern, prefix, n) {
    prefix[0] = 0 // 规定第一位等于 0
    let len = 0 // 比较时使用的长度
    let i = 1 // 第一个位置已经为0，从1开始
    while (i < n) {
        if (pattern[i] == pattern[len]) {
            len++
            prefix[i] = len
            i++
        } else {
            if (len > 0) len = prefix[len - 1]
            else {
                prefix[i] = 0
                i++
            }
        }
    }
    // console.log(move_prefix_table(prefix, n))
}

console.log(shortestPalindrome("abcd"))