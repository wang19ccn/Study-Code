/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findKthLargest = function (nums, k) {
    let len = nums.length
    let left = 0
    let right = len - 1
    let target = len - k
    while (true) {
        let index = partition(nums, left, right)
        if (index == target) {
            return nums[index]
        } else if (index < target) {
            left = index + 1
        } else {
            right = index - 1
        }
    }
};

var partition = function (nums, left, right) {
    let pivot = nums[left]
    let j = left
    for (let i = left + 1; i <= right; i++) {
        if (nums[i] < pivot) {
            j++
            [nums[i], nums[j]] = [nums[j], nums[i]]
        }
    }
    [nums[left], nums[j]] = [nums[j], nums[left]]
    return j
}

