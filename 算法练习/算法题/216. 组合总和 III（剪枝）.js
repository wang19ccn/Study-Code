/**
 * @param {number} k
 * @param {number} n
 * @return {number[][]}
 */
var combinationSum3 = function (k, n) {
    if (k < 1 || n < 1) return []
    let res = []
    let path = []
    dfs(k, n, 1, 0, path, res)
    return res
};

var dfs = function (k, n, start, sum, path, res) {
    if (path.length == k) {
        if (sum == n) {
            res.push([...path])
        }
        return
    }
    for (let i = start; i < 10; i++) {
        if (sum + i > n || path.length + (n - start + 1) < k) {
            break
        }
        path.push(i)
        dfs(k, n, i + 1, sum + i, path, res)
        path.pop()
    }
};

console.log(combinationSum3(1, 4))