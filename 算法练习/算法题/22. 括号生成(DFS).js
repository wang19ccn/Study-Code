/**
 * @param {number} n
 * @return {string[]}
 */
var generateParenthesis = function (n) {
    let sum = [];
    dfs(n, n, "",sum);
    //console.log(sum)
    return sum;
};

var dfs = function (left, right, str,sum) {
    if (left == 0 && right == 0) {
        sum.push(str);
        return;
    }

    if (left > 0) {
        dfs(left - 1, right, str + "(",sum);
    }


    if (right > left) {
        dfs(left, right - 1, str + ")",sum)
    }
}

console.log(generateParenthesis(3));