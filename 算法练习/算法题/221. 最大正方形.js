/** 二维dp，思考一下怎么降维
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalSquare = function (matrix) {
    if (matrix == null || matrix.length < 1 || matrix[0].length < 1) return 0

    let width = matrix[0].length
    let height = matrix.length
    let maxSide = 0

    let dp = []
    for (let i = 0; i <= height; i++) {
        dp[i] = []
        for (let j = 0; j <= width; j++) {
            dp[i][j] = [0]
        }
    }

    for (let row = 0; row < height; row++) {
        for (let col = 0; col < width; col++) {
            if (matrix[row][col] == '1') {
                dp[row + 1][col + 1] = Math.min(dp[row + 1][col], dp[row][col + 1], dp[row][col]) + 1
            }
            maxSide = Math.max(maxSide, dp[row + 1][col + 1])
        }
    }
    
    console.log(dp)
    return maxSide * maxSide
};

console.log(maximalSquare([["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]))