/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var countNodes = function (root) {
    if (root == null) return 0
    let left_depth = countDepths(root.left)
    let right_depth = countDepths(root.right)
    // 左子树深度等于右子树深度, 则左子树是满二叉树
    if (left_depth == right_depth) {
        return countNodes(root.right) + (1 << left_depth)
    }
    // 左子树深度大于右子树深度, 则右子树是满二叉树
    else {
        return countNodes(root.left) + (1 << right_depth)
    }
};

var countDepths = function (root) {
    let depths = 0
    while (root) {
        root = root.left
        depths++
    }
    return depths
}

// 利用位运算，完成2的幂次方运算
// 将1左移多少位
// 1<<2 = 0x04
// 1<<3 = 0x08
// 左移一位值就乘以2