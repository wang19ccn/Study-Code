/**
 * @param {number[]} nums
 * @return {string[]}
 */
var summaryRanges = function (nums) {
    let start = 0
    let end = 0
    let arr = []
    while (end < nums.length) {
        if (nums[end] + 1 == nums[end + 1]) {
            end++
        }
        else if (start == end) {
            arr.push(nums[start] + "")
            start++
            end++
        }
        else {
            arr.push(nums[start] + "->" + nums[end])
            start = end + 1
            end++
        }
    }
    return arr
};

console.log(summaryRanges([0, 1, 2, 4, 5, 7]))