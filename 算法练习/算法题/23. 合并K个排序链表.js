/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */
var mergeKLists = function (lists) {
    let arr=[]
    for(let i=0;i<lists.length;i++){
        let item=lists[i]
        while(item){
            arr.push(item.val)
            item=item.next
        }
    }

    arr.sort((a,b)=>a-b)
    let head=new ListNode()
    let cur=head

    for(let i=0;i<arr.length;i++){
        cur.next=new ListNode(arr[i])
        cur=cur.next
    }

    return head.next
}

function ListNode(val) {
    this.val = val;
    this.next = null;
}

//正确的测试用例
console.log(mergeKLists([{val:1,next:{val:4,next:{val:5,next:null}}},
    {val:1,next:{val:3,next:{val:1,next:4}}},
    ,{val:2,next:{val:6,next:null}}]))