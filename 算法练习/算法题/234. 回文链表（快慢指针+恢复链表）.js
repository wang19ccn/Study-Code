/**
 * 原地算法
 * O(n) 时间复杂度
 * O(1) 空间复杂度
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {boolean}
 */
var isPalindrome = function (head) {
    if (head == null || head.next == null) {
        return true
    }
    let slow = head
    let fast = head
    let pre = head
    let prepre = null
    // 找中间点
    while (fast != null && fast.next != null) {
        pre = slow
        slow = slow.next
        fast = fast.next.next
        pre.next = prepre
        prepre = pre
    }
    // 当链表节点为奇数时
    let newSlow = slow
    if (fast != null) {
        newSlow = slow.next
    }
    //恢复链表
    var recovery = function () {
        let rev = slow
        let temp = pre
        while (pre != null) {
            temp = pre.next
            pre.next = rev
            rev = pre
            pre = temp
        }
    }
    // 对比数值
    let cur = pre
    let index = newSlow
    while (cur != null) {
        if (cur.val != index.val) {
            recovery()
            return false
        }
        cur = cur.next
        index = index.next
    }
    // 回文链表
    recovery()
    return true
};

// console.log(isPalindrome({ val: 1, next: { val: 2, next: { val: 1, next: null } } }))
console.log(isPalindrome({ val: 1, next: { val: 2, next: null } }))