/**
 * 原地算法
 * O(n) 时间复杂度
 * O(1) 空间复杂度
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {boolean}
 */
var isPalindrome = function (head) {
    if (head == null || head.next == null) {
        return true
    }
    let slow = head
    let fast = head
    let pre = head
    let prepre = null
    while (fast != null && fast.next != null) {
        pre = slow
        slow = slow.next
        fast = fast.next.next
        pre.next = prepre
        prepre = pre
    }
    // 当链表节点为奇数时
    if (fast != null) {
        slow = slow.next
    }
    while (pre != null) {
        if (pre.val != slow.val) return false
        pre = pre.next
        slow = slow.next
    }
    return true
};

// console.log(isPalindrome({ val: 1, next: { val: 2, next: { val: 1, next: null } } }))
console.log(isPalindrome({ val: 1, next: { val: 2, next: null } }))