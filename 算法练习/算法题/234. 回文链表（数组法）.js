/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {boolean}
 */
var isPalindrome = function (head) {
    let res = []
    while (head) {
        res.push(head.val)
        head = head.next
    }
    let left = 0
    let right = res.length - 1
    while (left < right) {
        if (res[left] != res[right]) return false
        left++
        right--
    }
    return true
};

console.log(isPalindrome({ val: 1, next: null }))