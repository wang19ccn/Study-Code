/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
    let a = root
    while (true) {
        if (p.val < a.val && q.val < a.val) {
            a = a.left
        } else if (p.val > a.val && q.val > a.val) {
            a = a.right
        } else {
            return a
        }
    }
};