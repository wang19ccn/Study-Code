/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
    let path_p = getPath(root, p)
    let path_q = getPath(root, q)
    let a = new TreeNode()
    for (let i = 0; i < path_p.length && i < path_q.length; ++i) {
        if (path_p[i] == path_q[i]) {
            a = path_p[i]
        } else {
            // 这里不能直接return，因为如果其中一个节点本身是祖先节点，
            // 那么此时i已经到达了长度极限，不会再进for循环，这样会让函数无返回值
            break
        }
    }
    return a
};

var getPath = function (root, t) {
    let path = []
    let node = root
    while (node != t) {
        path.push(node)
        if (t.val < node.val) {
            node = node.left
        } else {
            node = node.right
        }
    }
    path.push(node)
    return path
}