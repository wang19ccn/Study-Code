/** 时间复杂度 O(n) 空间复杂度 O(1)
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function (nums) {
    let len = nums.length
    let res = []
    let right = 1
    let left = 1
    for (let i = 0; i < len; i++) {
        res[i] = left
        left *= nums[i]
    }
    for (let j = len - 1; j > -1; j--) {
        res[j] *= right
        right *= nums[j]
    }
    return res
};