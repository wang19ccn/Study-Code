/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
class m_queue {
    constructor() {
        this.queue = []
    }

    // 单调队列 弹出
    m_queue_pop(value) {
        if (this.queue.length != 0 && value == this.queue[0]) {
            this.queue.shift()
        }
    };

    // 单调队列 进入
    m_queue_push(value) {
        while (this.queue.length != 0 && value > this.queue[this.queue.length - 1]) {
            this.queue.pop()
        }
        this.queue.push(value)
    };

    // 返回最大值
    m_queue_max() {
        return this.queue[0]
    }
}
var maxSlidingWindow = function (nums, k) {
    let res = []

    // 第一次 建立初始单调栈
    let window = new m_queue()
    for (let i = 0; i < k && i < nums.length; i++) {
        window.m_queue_push(nums[i])
    }
    res.push(window.m_queue_max())

    // 第二次 滑动窗口
    for (let i = k; i < nums.length; i++) {
        window.m_queue_pop(nums[i - k])
        window.m_queue_push(nums[i])
        res.push(window.m_queue_max())
    }

    return res
};

console.log(maxSlidingWindow([7, 2, 4], 2))