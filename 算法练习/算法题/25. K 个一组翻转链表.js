/** 92ms,36.8MB
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var reverseKGroup = function (head, k) {
    let pre = null
    let cur = head
    // 判断长度是否够翻转的
    let p = head
    for (let i = 0; i < k; i++) {
        if (p == null) return head
        p = p.next
    }
    // 对K个链表节点进行反转
    for (let j = 0; j < k; j++) {
        let temp = cur.next
        cur.next = pre
        pre = cur
        cur = temp
    }
    // head已经成为反转后的最后一个元素，去链接下一段链表
    // pre是反转链表的第一个元素，当前cur是下一段链接第一个元素
    head.next = reverseKGroup(cur, k)
    return pre
};