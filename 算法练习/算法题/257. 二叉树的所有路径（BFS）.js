/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string[]}
 */
var binaryTreePaths = function (root) {
    if (root == null) return []

    let res = []
    let node_queue = [root]
    let path_queue = [root.val.toString()]

    while (node_queue.length) {
        let node = node_queue.shift()
        let path = path_queue.shift()
        if (node.left == null && node.right == null) {
            res.push(path)
        } else {
            if (node.left) {
                node_queue.push(node.left)
                path_queue.push(path + "->" + node.left.val)
            }
            if (node.right) {
                node_queue.push(node.right)
                path_queue.push(path + "->" + node.right.val)
            }
        }
    }

    return res
};
