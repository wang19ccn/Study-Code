/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string[]}
 */
var binaryTreePaths = function (root) {
    if (root == null) return []
    let res = []
    let path = ""
    dfs(root, path, res)
    return res
};

var dfs = function (root, path, res) {
    if (root.left == null && root.right == null) {
        path += root.val
        res.push(path)
        return
    } else {
        path += root.val + "->"
        if (root.left) dfs(root.left, path, res)
        if (root.right) dfs(root.right, path, res)
    }
}