/** 自己得算法 执行用时176ms，内存消耗37.2，感觉用时太长
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
    let like=0
    for(let i=0;i<nums.length;i++){
        for(let j=i+1;j<nums.length;j++){
            if(nums[i]==nums[j]) like++
        }
        nums.splice(i,like)
        like=0
        j=0
    }

    return nums.length
};