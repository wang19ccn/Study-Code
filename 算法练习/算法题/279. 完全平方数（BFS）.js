/**
 * @param {number} n
 * @return {number}
 */
var numSquares = function (n) {
    let queue = new Array()
    let visit = new Set()
    queue.push(n)
    visit.add(n)

    let level = 0
    while (!queue.length == 0) {
        level++
        let len = queue.length

        for (let i = 0; i < len; i++) {
            let cur = queue.shift()
            for (let j = 0; j * j <= cur; j++) {
                let temp = cur - j * j
                if (temp == 0) return level
                if (!visit.has(temp)) queue.push(temp)
                visit.add(temp)
            }
        }
    }

    return level
};