/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function (haystack, needle) {
    let h_len = haystack.length
    let n_len = needle.length
    for (let i = 0; i < h_len - n_len + 1; i++) {
        if (haystack.substring(i, i + n_len) == needle) {
            return i
        }
    }
    return -1
};