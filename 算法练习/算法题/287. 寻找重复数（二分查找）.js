// 抽屉原理：桌上有十个苹果，要把这十个苹果放到九个抽屉里，无论怎样放，我们会发现至少会有一个抽屉里面放不少于两个苹果。
/**
 * @param {number[]} nums
 * @return {number}
 */
var findDuplicate = function (nums) {
    let len = nums.length
    let left = 1;
    let right = len - 1;
    while (left < right) {
        // 右移一位等于除2
        let mid = (left + right) >> 1
        let cnt = 0
        for (num of nums) {
            // cnt += num <= mid;
            if (num <= mid) {
                cnt += 1
            }
        }
        // 根据抽屉原理，小于等于 4 的个数如果严格大于 4 个
        // 此时重复元素一定出现在 [1, 4] 区间里
        if (cnt > mid) {
            // 重复元素位于区间 [left, mid]
            right = mid
        } else {
            // if 分析正确了以后，else 搜索的区间就是 if 的反面
            // [mid + 1, right]
            left = mid + 1
        }
    }
    return left
};