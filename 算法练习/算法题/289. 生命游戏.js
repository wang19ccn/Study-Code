/**
 * @param {number[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var gameOfLife = function (board) {

    //方向数组
    const dx = [0, 1, 0, -1, -1, -1, 1, 1];
    const dy = [1, 0, -1, 0, 1, -1, 1, -1];

    // 数组副本
    const CopyBoard = board.map(ary => {
        return [...ary];
    });

    //遍历每个细胞
    for (let i = 0; i < CopyBoard.length; i++) {
        for (let j = 0; j < CopyBoard[i].length; j++) {
            let liveNum = 0;
            for (let k = 0; k < 8; k++) {
                let x = i + dx[k];
                let y = j + dy[k];

                //边界
                if (x < 0 || y < 0 || x >= CopyBoard.length || y >= CopyBoard[i].length) continue

                liveNum += CopyBoard[x][y] ? 1 : 0;
            }

            //生死判断
            if (liveNum < 2 || liveNum > 3) {
                board[i][j] = 0;
            } else if (liveNum <= 3 && CopyBoard[i][j]) {
                board[i][j] = 1;
            } else if (liveNum === 3 && !CopyBoard[i][j]) {
                board[i][j] = 1;
            }
        }
    }
    
};

console.log(gameOfLife([
[0, 1, 0],
[0, 0, 1],
[1, 1, 1],
[0, 0, 0]]))