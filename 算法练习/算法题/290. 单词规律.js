/**
 * @param {string} pattern
 * @param {string} s
 * @return {boolean}
 */
var wordPattern = function (pattern, s) {
    const word_ch = new Map()
    const ch_word = new Map()
    const words = s.split(' ')

    if (pattern.length !== words.length) {
        return false
    }

    // entries() 方法返回一个新的包含 [key, value] 对的 Iterator 对象
    for (const [i, word] of words.entries()) {
        const ch = pattern[i]
        if (word_ch.has(word) && word_ch.get(word) != ch || ch_word.has(ch) && ch_word.get(ch) !== word) {
            return false
        }
        word_ch.set(word, ch)
        ch_word.set(ch, word)
    }

    return true
};