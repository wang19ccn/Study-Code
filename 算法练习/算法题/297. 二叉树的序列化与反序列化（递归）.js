/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * Encodes a tree to a single string.
 *
 * @param {TreeNode} root
 * @return {string}
 */
var serialize = function (root) {
    if (root == null) return '_,'
    let leftSerialize = serialize(root.left)
    let rightSerialize = serialize(root.right)
    return root.val + ',' + leftSerialize + rightSerialize
};

/**
 * Decodes your encoded data to tree.
 *
 * @param {string} data
 * @return {TreeNode}
 */
var deserialize = function (data) {
    let list = data.split(',')
    return bulidTree(list)
};

var bulidTree = function (list) {
    let node_val = list.shift()
    if (node_val == '_') return null
    let node = new TreeNode(node_val)
    node.left = bulidTree(list)
    node.right = bulidTree(list)
    return node
};

/**
 * Your functions will be called as such:
 * deserialize(serialize(root));
 */

function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}

//二叉树测试用例
console.log(
    serialize(
        root = {
            val: 1,
            left: { val: 2, left: null, right: null },
            right: {
                val: 3,
                left: { val: 4, left: null, right: null },
                right: { val: 5, left: null, right: null }
            }
        }
    )
)

