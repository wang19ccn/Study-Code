/** 滑动窗格
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function (s) {
    let isHave = new Set()
    let len = s.length
    let left = -1
    let max = 0
    for (let i = 0; i < len; i++) {
        if (i != 0) {
            isHave.delete(s.charAt(i - 1))
        }
        while (left + 1 < len && !isHave.has(s.charAt(left + 1))) {
            isHave.add(s.charAt(left + 1))
            left++
        }
        max = Math.max(max, left - i + 1)
        console.log(isHave)
    }
    return max
};

console.log(lengthOfLongestSubstring("abcabcbb"))