/**
 * @param {number[]} nums
 * @return {number}
 */
var lengthOfLIS = function (nums) {
    if (nums.length < 1) return 0
    let dp = new Array(nums.length).fill(1)
    let maxLen = 0
    for (let i = 0; i < nums.length; i++) {
        for (let j = 0; j < i; j++) {
            if (nums[j] < nums[i]) dp[i] = Math.max(dp[i], dp[j] + 1)
        }
        maxLen = Math.max(maxLen, dp[i])
    }
    return maxLen
};