/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
    if (prices.length == 0) return 0

    let len = prices.length
    let dp = new Array(len)
    for (let i = 0; i < len; i++) {
        dp[i] = new Array(3)
    }

    dp[0][0] = -prices[0]  // dp[i][0]: 手上持有股票的最大收益
    dp[0][1] = 0           // dp[i][1]: 手上不持有股票，并且处于冷冻期中的累计最大收益
    dp[0][2] = 0           // dp[i][2]: 手上不持有股票，并且不在冷冻期中的累计最大收益
    for (let i = 1; i < len; ++i) {
        dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][2] - prices[i])
        dp[i][1] = dp[i - 1][0] + prices[i]
        dp[i][2] = Math.max(dp[i - 1][1], dp[i - 1][2])
    }

    return Math.max(dp[len - 1][1], dp[len - 1][2])
};

console.log(maxProfit([4,0,8]))