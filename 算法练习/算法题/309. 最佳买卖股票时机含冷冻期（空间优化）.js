/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
    if (prices.length == 0) return 0

    let len = prices.length
    let dp = new Array(len)
    for (let i = 0; i < len; i++) {
        dp[i] = new Array(3)
    }

    f0 = -prices[0]
    f1 = 0
    f2 = 0
    for (let i = 1; i < len; ++i) {
        let f0_temp = Math.max(f0, f2 - prices[i])
        let f1_temp = f0 + prices[i]
        let f2_temp = Math.max(f1, f2)
        f0 = f0_temp
        f1 = f1_temp
        f2 = f2_temp
    }

    return Math.max(f1, f2)
};

console.log(maxProfit([1, 2, 3, 0, 2]))