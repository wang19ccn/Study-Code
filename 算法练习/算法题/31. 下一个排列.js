/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var nextPermutation = function (nums) {
    let len = nums.length
    if (len <= 1) return
    let i = len - 2
    let k = len - 1

    // find A[i] < A[j]
    while (i >= 0 && nums[i] >= nums[i + 1]) {
        i--
    }

    if (i >= 0) {
        // find A[i] < A[k]
        // 这里需要大于等于吗？nums[i] >= nums[k]
        // 需要，因为会有重复的数
        while (nums[i] >= nums[k]) {
            k--
        };
        // swap A[i] A[k]
        [nums[i], nums[k]] = [nums[k], nums[i]]
    }

    reverse(nums, i + 1, len - 1)
    console.log(nums)
};

var reverse = function (nums, start, end) {
    while (start < end) {
        [nums[start], nums[end]] = [nums[end], nums[start]]
        ++start
        --end
    }
}

console.log(nextPermutation([1, 2, 3, 8, 5, 7, 6, 4]))

// 1 2 3 8 6 4 5 7 