/**
 * @param {string} s
 * @return {string}
 */
var removeDuplicateLetters = function (s) {
    let len = s.length
    let offset = 'a'.charCodeAt()

    let charArray = s.split("")
    let lastIndex = new Array(26).fill(0)
    for (let i = 0; i < len; i++) {
        lastIndex[charArray[i].charCodeAt() - offset] = i
    }

    let stack = new Array()
    let visited = new Array(26).fill(false)
    for (let i = 0; i < len; i++) {
        if (visited[charArray[i].charCodeAt() - offset]) {
            continue
        }
        while (stack.length != 0 && stack[stack.length - 1] > charArray[i] && lastIndex[stack[stack.length - 1].charCodeAt() - offset] > i) {
            let top = stack.pop()
            visited[top.charCodeAt() - offset] = false
        }
        stack.push(charArray[i])
        visited[charArray[i].charCodeAt() - offset] = true
    }

    return stack.join('')
};