/** 动态规划
 * 状态转移方程 dp[i] = 2 + dp[i-1] + dp[i-dp[i-1]-2]
 * 2 -- 基础长度
 * dp[i-1] -- 内部最长字串长度
 * dp[i-dp[i-1]-2] -- 外部最长字串长度
 * （ -- 对应dp为0
 * @param {string} s
 * @return {number}
 */
var longestValidParentheses = function (s) {
    let res = 0
    let len = s.length
    let dp = new Array(len).fill(0)
    for (let i = 1; i < len; i++) {
        if (s[i] == ')') {
            // 当遇见"("右括号时 
            if (s[i - 1] == '(') {
                if (i - 2 >= 0) { // 防止数组越界
                    dp[i] = 2 + dp[i - 2]
                } else {
                    dp[i] = 2
                }
            }
            // 如果左侧第一个不是"(",则当排除掉内部最长字串之后遇见"("括号时
            else if (s[i - dp[i - 1] - 1] == '(' && i - dp[i - 1] - 1 >= 0) {
                if (i - dp[i - 1] - 2 >= 0) {
                    dp[i] = 2 + dp[i - 1] + dp[i - dp[i - 1] - 2]
                } else {
                    dp[i] = dp[i - 1] + 2
                }
            }
        }
        // 如果 s[i] == "(" 无法构成子串，保持为0
        // 如果 s[i-dp[i-1]-1]==')' 无法构成子串，保持为0
        res = Math.max(res, dp[i])
    }
    return res
};

console.log(longestValidParentheses("(()"))

