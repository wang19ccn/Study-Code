/** 
 * @param {string} s
 * @return {number}
 */
var longestValidParentheses = function (s) {
    let res = 0
    let stack = [-1] // -1 充当参照物
    for (let i = 0; i < s.length; i++) {
        if (s[i] == '(') {
            stack.push(i)
        } else {
            stack.pop()
            if (stack.length == 0) {
                stack.push(i) // 栈为空时，当前下标作为新的参照物
            } else {
                res = Math.max(res, i - stack[stack.length - 1])
            }
        }
    }
    return res
};

console.log(longestValidParentheses("(()"))

