/** 
 * @param {string} s
 * @return {number}
 */
var longestValidParentheses = function (s) {
    let left = 0 // 左括号数目 
    let right = 0 // 右括号数目
    let res = 0
    for (let i = 0; i < s.length; i++) {
        if (s[i] == '(') left++
        if (s[i] == ')') right++
        if (left == right) {
            res = Math.max(res, 2 * right)
        } else if (right > left) {
            left = right = 0
        }
    }
    left = right = 0
    for (let i = s.length - 1; i > -1; i--) {
        if (s[i] == '(') left++
        if (s[i] == ')') right++
        if (left == right) {
            res = Math.max(res, 2 * left)
        } else if (left > right) {
            left = right = 0
        }
    }
    return res
};

console.log(longestValidParentheses("(()"))

