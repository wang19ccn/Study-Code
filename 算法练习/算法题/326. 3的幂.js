/**
 * @param {number} n
 * @return {boolean}
 */
var isPowerOfThree = function (n) {
    let t = 1
    while (t < n) {
        t *= 3
    }
    return t == n
};