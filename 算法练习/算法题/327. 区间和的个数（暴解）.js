/**
 * @param {number[]} nums
 * @param {number} lower
 * @param {number} upper
 * @return {number}
 */
var countRangeSum = function (nums, lower, upper) {
    let sum = 0
    let n = 0
    for (let i = 0; i < nums.length; i++) {
        for (let j = i; j < nums.length; j++) {
            sum = j == i ? nums[i] : sum + nums[j]
            if (sum >= lower && sum <= upper) {
                n++
            }
        }
    }
    return n++
};