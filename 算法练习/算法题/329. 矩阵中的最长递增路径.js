/**
 * @param {number[][]} matrix
 * @return {number}
 */
var longestIncreasingPath = function (matrix) {
    if (matrix.length == 0) return 0
    let dx = [0, 1, 0, -1]
    let dy = [1, 0, -1, 0]
    let m = matrix.length
    let n = matrix[0].length
    let memo = Array.from(Array(m), () => Array(n))
    let res = 1

    var dfs = function (matrix, i, j, m, n) {
        if (memo[i][j]) return memo[i][j]
        let max = 1
        for (let k = 0; k < 4; k++) {
            let x = i + dx[k]
            let y = j + dy[k]
            if (x >= 0 && x < m && y >= 0 && y < n && matrix[x][y] > matrix[i][j]) {
                max = Math.max(max, 1 + dfs(matrix, x, y, m, n))
            }
        }
        return memo[i][j] = max
    }

    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            res = Math.max(res, dfs(matrix, i, j, m, n))
        }
    }
    return res
};

console.log(longestIncreasingPath([[9, 9, 4], [6, 6, 8], [2, 1, 1]]))