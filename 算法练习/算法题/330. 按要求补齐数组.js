/**
 * @param {number[]} nums
 * @param {number} n
 * @return {number}
 */
var minPatches = function (nums, n) {
    //累加的总和    
    let total = 1
    //需要补充的数字个数
    let count = 0
    //访问的数组下标索引
    let index = 0

    while (total <= n) {
        if (index < nums.length && nums[index] <= total) {
            //如果数组能组成的数字范围是[1,total)，那么加上nums[index]
            //就变成了[1,total)U[nums[index],total+nums[index])
            //结果就是[1,total+nums[index])
            total += nums[index]
            index++
        } else {
            //添加一个新数字，并且count加1
            total *= 2
            count++
        }
    }

    return count
};

// 作者：sdwwld
// 链接：https://leetcode-cn.com/problems/patching-array/solution/an-yao-qiu-bu-qi-shu-zu-tan-xin-suan-fa-b4bwr/
