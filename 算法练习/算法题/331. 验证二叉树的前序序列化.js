/**
 * @param {string} preorder
 * @return {boolean}
 */
// 利用出度和入度
var isValidSerialization = function (preorder) {
    if (preorder == '#') return true
    let nodes = preorder.split(",")

    let indegree = 0
    let outdegree = 0

    for (let i = 0; i < nodes.length; i++) {
        // 处理根节点
        if (i == 0) {
            if (nodes[i] == '#') return false
            outdegree += 2
            continue
        }
        // null节点，入度+1
        if (nodes[i] == "#") {
            indegree += 1
        } else {
            indegree += 1
            outdegree += 2
        }
        if (i != nodes.length - 1 && indegree >= outdegree) {
            return false
        }
    }

    return indegree == outdegree
};