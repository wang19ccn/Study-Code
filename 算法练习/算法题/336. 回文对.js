/**
 * @param {string[]} words
 * @return {number[][]}
 */
var palindromePairs = function (words) {
    // 单词反转
    let revWords = new Map()
    for (let i = 0; i < words.length; i++) {
        let revWord = words[i].split('').reverse().join('')
        revWords.set(revWord, i)
    }
    // 寻找回文
    let res = []
    for (let i = 0; i < words.length; i++) {
        let temp = words[i]
        if (isPalindrome(temp) && revWords.has('') && temp !== '') {
            res.push([revWords.get(''), i]);
        }
        for (let j = 0; j < temp.length; j++) {
            let left = temp.substring(0, j)
            let right = temp.substring(j)
            if (isPalindrome(left) && revWords.has(right) && revWords.get(right) !== i) {
                res.push([revWords.get(right), i])
            }
            if (isPalindrome(right) && revWords.has(left) && revWords.get(left) !== i) {
                res.push([i, revWords.get(left)])
            }
        }
    }

    return res
};

var isPalindrome = function (str) {
    let left = 0
    let right = str.length - 1
    while (left < right) {
        if (str[left] != str[right]) return false
        left++
        right--
    }
    return true
}

console.log(palindromePairs(["a", "abc", "aba", ""]))