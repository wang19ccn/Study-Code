/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var rob = function (root) {

    var dfs = function (node) {
        if (node == null) return [0, 0]

        let left = dfs(node.left)
        let right = dfs(node.right)

        let dp = []

        dp[0] = Math.max(left[0], left[1]) + Math.max(right[0], right[1])
        dp[1] = node.val + left[0] + right[0]
        return dp
    }

    // 后序遍历
    let res = dfs(root)
    return Math.max(res[0], res[1])
};