/**
 * @param {number} n
 * @return {number}
 */
var integerBreak = function (n) {
    let dp = [0, 1, 1]
    for (let i = 3; i < n + 1; i++) {
        dp[i % 3] = Math.max(
            1 * Math.max(dp[(i - 1) % 3], i - 1),
            2 * Math.max(dp[(i - 2) % 3], i - 2),
            3 * Math.max(dp[(i - 3) % 3], i - 3))
    }
    return dp[n % 3]
};
console.log(integerBreak(9))