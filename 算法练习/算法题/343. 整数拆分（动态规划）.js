/**
 * @param {number} n
 * @return {number}
 */
var integerBreak = function (n) {
    let dp = new Array(n + 1).fill(0)
    for (let i = 2; i < n + 1; i++) {
        for (let j = 1; j < i; j++) {
            dp[i] = Math.max(dp[i], j * dp[i - j], j * (i - j))
        }
    }
    return dp[n]
};

console.log(integerBreak(10))