/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function (nums, k) {
    let map = new Map()
    for (let num of nums) {
        if (map.has(num)) {
            map.set(num, map.get(num) + 1)
        } else {
            map.set(num, 1)
        }
    }

    if (map.size <= k) {
        return [...map.keys()]
    }

    let i = 0
    let heap = [,]
    map.forEach((value, key) => {
        if (i < k) {
            heap.push(key)
            if (i === k - 1) buildHeap(heap, map, k)
        }
        else if (map.get(heap[1]) < value) {
            heap[1] = key
            adjustHeap(heap, map, k, 1)
        }
        i++
    })
    heap.shift()
    return heap
};

//建立小顶堆
var buildHeap = function (heap, map, k) {
    if (k == 1) return
    for (let i = parseInt(k / 2); i >= 1; i--) {
        adjustHeap(heap, map, k, i);
    }
};

//调整小顶堆
var adjustHeap = function (heap, map, k, i) {
    while (true) {
        let minIndex = i
        if (2 * i <= k && map.get(heap[2 * i]) < map.get(heap[i])) {
            minIndex = 2 * i
        }
        if (2 * i + 1 <= k && map.get(heap[2 * i + 1]) < map.get(heap[minIndex])) {
            minIndex = 2 * i + 1
        }
        if (minIndex !== i) {
            change(heap, i, minIndex)
            i = minIndex
        } else {
            break
        }
    }
};

//交换元素
var change = function (nums, x, y) {
    let temp = nums[x];
    nums[x] = nums[y];
    nums[y] = temp;
};

console.log(topKFrequent([4, 1, -1, 2, -1, 2, 3], 2))