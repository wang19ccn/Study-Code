/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function (nums, k) {
    let map = new Map()
    for (let num of nums) {
        if (map.has(num)) {
            map.set(num, map.get(num) + 1)
        } else {
            map.set(num, 1)
        }
    }

    if (map.size <= k) {
        return [...map.keys()]
    }

    let res = []
    let barrel = new Array(nums.length + 1).fill(null)
    for (let key of map.keys()) {
        let i = map.get(key)
        if (barrel[i] == null) {
            barrel[i] = new Array()
        }
        barrel[i].push(key)
    }

    for (let i = barrel.length - 1; i >= 0 && res.length < k; i--) {
        if (barrel[i] == null) continue
        res.push(...barrel[i])
    }

    return res
};

console.log(topKFrequent([4, 1, -1, 2, -1, 2, 3], 2))