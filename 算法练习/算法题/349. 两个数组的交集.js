/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersection = function (nums1, nums2) {
    if (nums1.length == 0 || nums2.length == 0) return []
    let set = new Set(nums1)
    let res = new Set()
    for (let num of nums2) {
        if (set.has(num)) {
            res.add(num)
        }
    }
    return Array.from(res)
};

console.log(intersection([1, 2, 2, 1], [2, 2]))