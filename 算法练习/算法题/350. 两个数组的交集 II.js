/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersect = function (nums1, nums2) {
    let res = []
    let map = new Map()
    for (let num1 of nums1) {
        if (map.has(num1)) {
            map.set(num1, (map.get(num1) + 1))
        } else {
            map.set(num1, 1)
        }
    }
    for (let num2 of nums2) {
        if (map.get(num2) > 0) {
            res.push(num2)
            map.set(num2, map.get(num2) - 1)
        }
    }
    return res
};

console.log(intersect([4, 9, 5], [9, 4, 9, 8, 4]))