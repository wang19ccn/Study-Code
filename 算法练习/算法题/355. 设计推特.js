/**
 * Initialize your data structure here.
 */
var Twitter = function() {
    this.followMap = {} //用户关注列表， 用 Set 数据类型不需要去处理重复数据，取消关注（从列表删除）也会更方便；
    this.postMap = new Map() //postMap：用户推文列表
    this.lastetPosId=0 //推文的自增id，用于后续获取推文列表时排序；
};

/**
 * 创建一条新的推文
 * Compose a new tweet. 
 * @param {number} userId 
 * @param {number} tweetId
 * @return {void}
 */
Twitter.prototype.postTweet = function(userId, tweetId) {
    const postTime = this.lastetPosId++
    let tweetList = [{tweetId,postTime}]
    if(this.postMap.has(userId)){
        tweetList = tweetList.concat(this.postMap.get(userId))
    }
    this.postMap.set(userId,tweetList)
};

/**
 * 检索最近的十条推文。每个推文都必须是由此用户关注的人或者是用户自己发出的。推文必须按照时间顺序由最近的开始排序。
 * Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent. 
 * @param {number} userId
 * @return {number[]}
 */
Twitter.prototype.getNewsFeed = function(userId) {
    const followeeIdList = this.followMap[userId] ? [...this.followMap[userId]] : []
    const tweeList = []
    const userIds=[...new Set(followeeIdList.concat([userId]))]
    userIds.forEach(uid=>{
        if(this.postMap.has(uid)){
            tweeList.push(...this.postMap.get(uid).slice(0,10))
        }
    })
    tweeList.sort((a,b)=>b.postTime-a.postTime)
    return tweeList.slice(0,10).map(item=>item.tweetId)
};

/**
 * 关注一个用户
 * Follower follows a followee. If the operation is invalid, it should be a no-op. 
 * @param {number} followerId 
 * @param {number} followeeId
 * @return {void}
 */
Twitter.prototype.follow = function(followerId, followeeId) {
    if(this.followMap[followerId]){
        this.followMap[followerId].add(followeeId)
    }else{
        this.followMap[followerId]=new Set([followeeId])
    }
};

/**
 * 取消关注一个用户
 * Follower unfollows a followee. If the operation is invalid, it should be a no-op. 
 * @param {number} followerId 
 * @param {number} followeeId
 * @return {void}
 */
Twitter.prototype.unfollow = function(followerId, followeeId) {
    if (this.followMap[followerId]) {
        this.followMap[followerId].delete(followeeId)
    }
};

/**
 * Your Twitter object will be instantiated and called as such:
 * var obj = new Twitter()
 * obj.postTweet(userId,tweetId)
 * var param_2 = obj.getNewsFeed(userId)
 * obj.follow(followerId,followeeId)
 * obj.unfollow(followerId,followeeId)
 */

twitter = new Twitter();

// 用户1发送了一条新推文 (用户id = 1, 推文id = 5).
twitter.postTweet(1, 5);

// 用户1的获取推文应当返回一个列表，其中包含一个id为5的推文.
twitter.getNewsFeed(1);

// 用户1关注了用户2.
twitter.follow(1, 2);

// 用户2发送了一个新推文 (推文id = 6).
twitter.postTweet(2, 6);

// 用户1的获取推文应当返回一个列表，其中包含两个推文，id分别为 -> [6, 5].
// 推文id6应当在推文id5之前，因为它是在5之后发送的.
twitter.getNewsFeed(1);

// 用户1取消关注了用户2.
twitter.unfollow(1, 2);

// 用户1的获取推文应当返回一个列表，其中包含一个id为5的推文.
// 因为用户1已经不再关注用户2.
twitter.getNewsFeed(1);
