/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solveSudoku = function (board) {
    let rows = new Array(9)
    let cols = new Array(9)
    let blocks = new Array(9)
    let options = ['1', '2', '3', '4', '5', '6', '7', '8', '9']

    // 初始化
    for (let i = 0; i < 9; i++) { // 集合的初始化
        rows[i] = new Set(options)
        cols[i] = new Set(options)
        blocks[i] = new Set(options)
    }

    // 根据坐标，获取所在的小框的索引
    let getBlockIndex = function (i, j) {
        return (i / 3 | 0) * 3 + j / 3 | 0   // |0 是向下取整 
    }

    // 删掉已存在的数
    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            if (board[i][j] != ".") {
                rows[i].delete(board[i][j])
                cols[j].delete(board[i][j])
                blocks[getBlockIndex(i, j)].delete(board[i][j])
            }
        }
    }

    let fill = function (i, j) {
        // 列越界，就填下一行
        if (j == 9) {
            i++
            j = 0
            if (i == 9) return true   // 都填完了 返回true
        }

        // 如果不是空白格，递归填下一格
        if (board[i][j] != ".") return fill(i, j + 1)

        // 获取所在小框的索引
        const blockIndex = getBlockIndex(i, j)

        // 枚举出所有选择：1-9
        for (let num = 1; num <= 9; num++) {
            const s = String(num)
            // 当前选择必须在三个set中都存在，如果有一个不存在，就说明发生了冲突，跳过该选择
            if (!rows[i].has(s) || !cols[j].has(s) || !blocks[blockIndex].has(s)) continue

            // 作出选择
            board[i][j] = s
            rows[i].delete(s)
            cols[j].delete(s)
            blocks[blockIndex].delete(s)

            if (fill(i, j + 1)) return true  // 如果基于当前选择，填下一个，最后可解出数独，直接返回真

            // 回溯
            board[i][j] = "."
            rows[i].add(s)
            cols[j].add(s)
            blocks[blockIndex].add(s)
        }
        return false
    }

    fill(0, 0)
    return board
}

// 参考
// https://leetcode-cn.com/problems/sudoku-solver/solution/shou-hua-tu-jie-jie-shu-du-hui-su-suan-fa-sudoku-s/