/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var combinationSum4 = function (nums, target) {
    let dp = new Array(target + 1).fill(0)
    // dp[0] = 1，表示，如果那个硬币的面值刚刚好等于需要凑出的价值，这个就成为 1 种组合方案
    dp[0] = 1
    for (let i = 1; i < target + 1; i++) {
        for (let num of nums) {
            if (num <= i) {
                dp[i] += dp[i - num]
            }
        }
    }
    return dp[target]
};

console.log(combinationSum4([1, 2, 3], 4))