/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var combinationSum4 = function (nums, target) {
    if (nums.length == 0 || target < 1) return 0
    let memo = new Array(target + 1)

    memo[0] = 1
    nums.sort((a, b) => a - b)

    var dfs = function (target) {
        if (memo[target] === undefined) {
            let sum = 0 // 当前层目标数，能有多少种方法得到
            for (let num of nums) {
                if (target < num) break
                sum += dfs(target - num)
            }
            memo[target] = sum
        }
        return memo[target]
    };

    return dfs(target)
};

console.log(combinationSum4([3, 33, 333], 10000))