/**
 * @param {number[][]} matrix
 * @param {number} k
 * @return {number}
 */
var getRankInMatrix = function (matrix, midVal) {
    let n = matrix.length
    let rank = 0
    let row = 0
    let col = n - 1
    while (row < n && col >= 0) {
        if (midVal >= matrix[row][col]) {
            rank += col + 1
            row++
        } else {
            col--
        }
    }
    return rank
}

var kthSmallest = function (matrix, k) {
    let n = matrix.length
    let low = matrix[0][0]
    let high = matrix[n - 1][n - 1]
    while (low < high) {
        let midVal = parseInt(low + (high - low) / 2)
        let rank = getRankInMatrix(matrix, midVal)
        if (rank < k) {
            low = midVal + 1
        } else {
            high = midVal
        }
    }
    return low
};

console.log(kthSmallest([[0, 2, 5], [7, 8, 9], [10, 11, 12]], 3))

/*

关于为什么二分查找最后返回的值一定在矩阵中

*/

/*

证明如下：
设min^{t}为第t轮二分的min，mid^{t}为第t轮二分的mid,max^{t}为第t轮二分的max,target是我们要查找的值。

因此min^{t}=min^{t-1}或者min^{t}=mid^{t-1}+1。如果min^{t}=mid^{t-1}+1,说明小于等于mid^{t-1}的矩阵中元素的个数小于k,说明mid^{t-1}<target,那么min^{t}=mid^{t-1}+1<target+1,即min^{t}<=target。因此，只要其中有一次的min是由上一轮的mid转化而来的，那么就可以保证min始终<=target。如果min一直保持着初始状态，从来没有从上一轮mid转化而来过，那么min^{t}=min{1}<=target。因此，min始终小于等于target。

同时，max^{t}=mid^{t-1}或者max^{t}=max^{t-1}。如果max^{t}=mid^{t-1},说明小于等于mid^{t-1}的矩阵中的元素的个数>=k，说明mid^{t-1}>=target。因此，只要其中有一次的max是由上一轮的mid转化而来的，那么就可以保证max始终>=target。如果max一直保持着初始状态，从来没有从上一轮mid转化而来过，那么max^{t}=max{1}>=target。因此，max始终大于等于target。

此外，由于min和max构成的区间是在不断缩小的，所以最终肯定可以达到min=max的状态，从而退出循环。此时，由于min<=target,max>=target,而min=max，所以min=max=target。

得证。

作者：shimura233
链接：https://leetcode-cn.com/problems/kth-smallest-element-in-a-sorted-matrix/solution/guan-yu-wei-shi-yao-er-fen-cha-zhao-zui-hou-fan-hu/
来源：力扣（LeetCode）
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

*/