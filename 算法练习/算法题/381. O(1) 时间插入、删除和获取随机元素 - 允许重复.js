/**
 * Initialize your data structure here.
 */
var RandomizedCollection = function () {
    this.map = new Map()
    this.res = new Array()
};

/**
 * Inserts a value to the collection. Returns true if the collection did not already contain the specified element. 
 * @param {number} val
 * @return {boolean}
 */
RandomizedCollection.prototype.insert = function (val) {
    this.res.push(val)
    let set = this.map.has(val) ? this.map.get(val) : new Set()
    set.add(this.res.length - 1)
    this.map.set(val, set)
    return set.size == 1
};

/**
 * Removes a value from the collection. Returns true if the collection contained the specified element. 
 * @param {number} val
 * @return {boolean}
 */
RandomizedCollection.prototype.remove = function (val) {
    if (!this.map.has(val)) return false
    if (!this.map.get(val).size) return false
    // 获取删除元素的下标信息
    let set = this.map.get(val)
    // 取set第一个元素
    let setIter = set.values();
    let index = setIter.next().value
    set.delete(index)
    // 更新删除元素的下标信息
    this.map.set(val, set)
    // 删除元素
    if (index < this.res.length - 1) {
        // 获取数组表最后一个值
        let last = this.res[this.res.length - 1]
        // 调整last的位置信息表
        this.map.get(last).delete(this.res.length - 1)
        this.map.get(last).add(index);
        // 交换元素
        [this.res[index], this.res[this.res.length - 1]] = [this.res[this.res.length - 1], this.res[index]]; // 前面一定要加分号
        // this.res[this.res.length-1] = this.res[index] 
        // this.res[index] = last
    }
    this.res.pop()
    return true
};

/**
 * Get a random element from the collection.
 * @return {number}
 */
RandomizedCollection.prototype.getRandom = function () {
    let index = Math.floor(Math.random() * this.res.length)
    return this.res[index]
};

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * var obj = new RandomizedCollection()
 * var param_1 = obj.insert(val)
 * var param_2 = obj.remove(val)
 * var param_3 = obj.getRandom()
 */