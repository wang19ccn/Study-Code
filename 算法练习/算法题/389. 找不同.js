/**
 * @param {string} s
 * @param {string} t
 * @return {character}
 */
var findTheDifference = function (s, t) {
    let map = new Map()
    for (let str_s of s) {
        if (!map.has(str_s)) {
            map.set(str_s, 1)
        } else {
            map.set(str_s, map.get(str_s) + 1)
        }
    }
    for (let str_t of t) {
        if (!map.has(str_t) || map.get(str_t) == 0) {
            return str_t
        }
        map.set(str_t, map.get(str_t) - 1)
    }
};