/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum = function (candidates, target) {
    if (0 == candidates.length) return res

    let len = candidates.length
    let res = new Array()
    let path = new Array()

    dfs(candidates, 0, len, target, path, res)
    return res
};

var dfs = function (candidates, start, len, target, path, res) {
    if (0 > target) {
        return
    }
    if (0 == target) {
        res.push([...path])
        return
    }
    for (let i = start; i < len; i++) {
        path.push(candidates[i])
        // 注意开始起点，依然是i，是因为每个元素可以重复利用
        // 同时，控制起点，从而避免出现重复，例如[2,3,3]，当进入3时，i > 2,不可能出现[3,3,2]
        dfs(candidates, i, len, target - candidates[i], path, res)
        path.pop()
    }
}

console.log(combinationSum([2, 3, 6, 7], 7))