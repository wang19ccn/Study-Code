/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum = function (candidates, target) {
    if (0 == candidates.length) return res

    let len = candidates.length
    let res = new Array()
    let path = new Array()

    // 排序来进行剪枝
    candidates.sort((a, b) => a - b);
    dfs(candidates, 0, len, target, path, res)
    return res
};

var dfs = function (candidates, start, len, target, path, res) {
    if (0 == target) {
        res.push([...path])
        return
    }

    for (let i = start; i < len; i++) {
        // 数组有序，直接跳过后面
        if (target - candidates[i] < 0) {
            break
        }

        path.push(candidates[i])
        dfs(candidates, i, len, target - candidates[i], path, res)
        path.pop()
    }
}

console.log(combinationSum([2, 3, 6, 7], 7))