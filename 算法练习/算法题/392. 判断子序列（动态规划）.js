/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isSubsequence = function (s, t) {
    let n = s.length
    let m = t.length

    let dp = new Array(m + 1)
    for (let k = 0; k < m + 1; k++) {
        dp[k] = new Array(26)
    }

    //初始化最后一行
    for (let h = 0; h < 26; h++) {
        dp[m][h] = m
    }

    for (let i = m - 1; i >= 0; i--) {
        for (let j = 0; j < 26; j++) {
            if (t.charAt(i) == String.fromCharCode(97 + j)) {
                dp[i][j] = i
            } else {
                dp[i][j] = dp[i + 1][j]
            }
        }
    }
    //console.log(dp)

    let res = 0
    for (let i = 0; i < n; i++) {
        if (dp[res][s.charAt(i).charCodeAt() - 97] == m) {
            return false
        }
        res = dp[res][s.charAt(i).charCodeAt() - 97] + 1
    }
    return true
};

console.log(isSubsequence("abc", "ahbgdc"))