/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isSubsequence = function (s, t) {
    if (s.length == 0 && t.length == 0) return true
    for (let k = 0, i = 0; k < t.length; k++) {
        if (s[i] == t[k]) {
            i++
        }
        if (i == s.length) {
            return true
        }
    }
    return false
};

console.log(isSubsequence("abcd", "ahbgdc"))