/**
 * @param {string} s
 * @return {string}
 */
var decodeString = function (s) {
    let numStack = [] // 倍数num的等待栈
    let strStack = [] // 待拼接的str的等待栈
    let num = 0
    let result = ''
    for (let char of s) {
        if (!isNaN(char)) { // 判断是否是数字
            num = num * 10 + +char // js中+可以将数字字符转为数字
        }
        else if (char === '[') {
            strStack.push(result)
            numStack.push(num)
            result = ''
            num = ''
        }
        else if (char === ']') {
            let times = numStack.pop() // 获取拷贝次数
            result = strStack.pop() + result.repeat(times) // repeat()根据参数值重复字符串
        }
        else {
            result += char
        }
    }
    return result
};

console.log(decodeString("3[a2[c]]"))
