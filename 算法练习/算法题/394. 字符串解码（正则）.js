/**
 * @param {string} s
 * @return {string}
 */
var decodeString = function (s) {
    // 正则匹配
    // 分组1 (\d+) [0~9]至少一次，在replace里对应n
    // 分组2 ([a-zA-Z]+) [a~z][A~Z]至少一次，在replace里对应str
    let reg = /(\d+)\[([a-zA-Z]+)\]/g
    while (reg.test(s)) {
        s = s.replace(reg, (match, n, str) => str.repeat(n))
    }
    return s
};

console.log(decodeString("3[a2[c]]"))
