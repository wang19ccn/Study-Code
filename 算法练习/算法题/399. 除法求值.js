/**
 * @param {string[][]} equations
 * @param {number[]} values
 * @param {string[][]} queries
 * @return {number[]}
 */
class UnionFind {
    constructor(n) {
        this.parent = new Array(n)
        this.weight = new Array(n)
        for (let i = 0; i < n; i++) {
            this.parent[i] = i
            this.weight[i] = 1.0
        }
    }
    union(x, y, value) {
        let rootX = this.find(x)
        let rootY = this.find(y)
        if (rootX == rootY) {
            return
        }
        this.parent[rootX] = rootY
        this.weight[rootX] = this.weight[y] * value / this.weight[x]
    }
    /**
    * 路径压缩
    * @param x
    * @return 根结点的 id
    */
    find(x) {
        if (x != this.parent[x]) {
            let origin = this.parent[x]
            this.parent[x] = this.find(this.parent[x])
            this.weight[x] *= this.weight[origin]
        }
        return this.parent[x]
    }
    isConnected(x, y) {
        let rootX = this.find(x)
        let rootY = this.find(y)
        if (rootX == rootY) {
            return this.weight[x] / this.weight[y]
        } else {
            return -1.0
        }
    }
}

var calcEquation = function (equations, values, queries) {
    let equationsSize = equations.length

    let unionFind = new UnionFind(2 * equationsSize);
    // 第 1 步：预处理，将变量的值与 id 进行映射，使得并查集的底层使用数组实现，方便编码
    let hashMap = new Map()
    let id = 0
    for (let i = 0; i < equationsSize; i++) {
        let equation = equations[i]
        let var1 = equation[0]
        let var2 = equation[1]
        if (!hashMap.has(var1)) {
            hashMap.set(var1, id)
            id++
        }
        if (!hashMap.has(var2)) {
            hashMap.set(var2, id)
            id++
        }
        unionFind.union(hashMap.get(var1), hashMap.get(var2), values[i])
    }
    // 第 2 步：做查询
    let queriesSize = queries.length
    let res = new Array(queriesSize)
    for (let i = 0; i < queriesSize; i++) {
        let var1 = queries[i][0]
        let var2 = queries[i][1]

        let id1 = hashMap.get(var1)
        let id2 = hashMap.get(var2)

        if (id1 == null || id2 == null) {
            res[i] = -1.0
        } else {
            res[i] = unionFind.isConnected(id1, id2)
        }
    }
    return res
};

console.log(calcEquation([["a", "b"], ["b", "c"]],
    [2.0, 3.0],
    [["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"]]))

// 参考答案：
// 作者：LeetCode
// 链接：https://leetcode-cn.com/problems/evaluate-division/solution/399-chu-fa-qiu-zhi-nan-du-zhong-deng-286-w45d/
