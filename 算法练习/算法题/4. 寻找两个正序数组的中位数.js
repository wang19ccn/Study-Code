/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var findMedianSortedArrays = function (nums1, nums2) {
    if (nums1.length > nums2.length) {
        let temp = nums1
        nums1 = nums2
        nums2 = temp
    }

    let m = nums1.length
    let n = nums2.length

    // 分割线左边的所有元素需要满足的个数 m + (n - m + 1) / 2
    // 已经包含奇偶两种情况
    let totalLeft = parseInt((m + n + 1) / 2)

    // 在 nums1 的区间 [0,m] 里查找恰当的分割线
    // 使得 nums[i - 1] <= nums2[j] && nums2[j - 1] <= nums1[i]
    let left = 0
    let right = m

    while (left < right) {
        // 定义：分割线在第1个数组右边的第1个元素的下标 i = 分割线在第1个数组左边的元素个数
        // 定义：分割线在第2个数组右边的第1个元素的下标 j = 分割线在第2个数组左边的元素个数
        // 定义：i, j 表示分割线右边元素下标，i - 1, j - 1 表示分割线左边元素
        let i = left + parseInt((right - left + 1) / 2)
        let j = totalLeft - i
        if (nums1[i - 1] > nums2[j]) {
            // 下一轮搜索的区间 [left,i - 1]
            right = i - 1
        } else {
            // 下一轮搜索的区间 [i,right]
            left = i
        }
    }

    // while (left < right) {
    //     let i = left + parseInt((right - left) / 2)
    //     let j = totalLeft - i
    //     if (nums2[j - 1] > nums1[i]) {
    //         // 下一轮搜索的区间 [i + 1, right]
    //         left = i + 1
    //     } else {
    //         // 下一轮搜索的区间 [left, i]
    //         right = i
    //     }
    // }

    let i = left
    let j = totalLeft - i
    let nums1_LeftMax = (i == 0 ? -Infinity : nums1[i - 1])
    let nums1_RightMin = (i == m ? Infinity : nums1[i])
    let nums2_LeftMax = (j == 0 ? -Infinity : nums2[j - 1])
    let nums2_RightMin = (j == n ? Infinity : nums2[j])

    if (((m + n) % 2) == 1) {
        return Math.max(nums1_LeftMax, nums2_LeftMax)
    } else {
        return ((Math.max(nums1_LeftMax, nums2_LeftMax) + Math.min(nums1_RightMin, nums2_RightMin))) / 2;
    }
};

// console.log(findMedianSortedArrays([3, 8, 9, 10], [2, 4, 6, 12, 18, 20]))
console.log(findMedianSortedArrays([1, 3], [2]))