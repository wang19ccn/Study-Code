/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum2 = function (candidates, target) {
    if (candidates.length == 0) {
        return []
    }
    let res = []
    let path = []
    candidates.sort((a, b) => a - b)
    dfs(candidates, target, 0, path, res)
    return res
};

var dfs = function (candidates, target, start, path, res) {
    if (target <= 0) {
        if (target == 0) {
            res.push([...path])
        }
        return
    }
    for (let i = start; i < candidates.length; i++) {
        if (target - candidates[i] < 0) {
            break
        }
        if (i > start && candidates[i] == candidates[i - 1]) {
            continue
        }
        path.push(candidates[i])
        dfs(candidates, target - candidates[i], i + 1, path, res)
        path.pop()
    }
};

console.log(combinationSum2([10, 1, 2, 7, 6, 1, 5], 8))