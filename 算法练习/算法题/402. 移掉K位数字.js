/**
 * @param {string} num
 * @param {number} k
 * @return {string}
 */
var removeKdigits = function (num, k) {
    if (num.length <= k) return '0'
    let arr = num.split('')
    let stack = [arr[0]]
    for (let i = 1; i < num.length; i++) {
        while (stack[stack.length - 1] > num[i] && stack.length > 0 && k > 0) {
            stack.pop()
            k--
        }
        stack.push(num[i])
    }
    while (k > 0) {
        stack.pop()
        k--
    }
    while (stack[0] == 0) {
        stack.shift()
    }
    return stack.join('') == '' ? '0' : stack.join('')
};

console.log(removeKdigits("1432219", 3))
console.log(removeKdigits("10000200", 1))
console.log(removeKdigits("112", 1))
console.log(removeKdigits("10", 1))