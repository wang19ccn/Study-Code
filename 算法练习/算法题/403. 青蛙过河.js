/**
 * @param {number[]} stones
 * @return {boolean}
 */
var canCross = function (stones) {
    let set = new Set();
    return dfs(stones, 0, 0, set);
};

var dfs = function (stones, k, index, set) {
    const key = index * 1000 + k // 构造唯一的key，代表当前子问题
    if (set.has(key)) {
        return false;
    } else {
        set.add(key);
    }
    for (let i = index + 1; i < stones.length; i++) {
        let gap = stones[i] - stones[index];
        if (k - 1 <= gap && gap <= k + 1) {
            if (dfs(stones, gap, i, set)) {
                return true;
            }
        }
        else if (gap > k + 1) {
            break;
        }
    };
    return index == stones.length - 1
};

console.log(canCross([0, 1, 2, 3, 4, 8, 9, 11]))