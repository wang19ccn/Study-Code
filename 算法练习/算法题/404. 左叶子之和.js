/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var sumOfLeftLeaves = function (root) {
    let sum = 0
    var dfs = function (root) {
        if (!root) return
        if (root.left) {
            if (!root.left.left && !root.left.right) {
                sum += root.left.val
            }
            dfs(root.left)
        }
        if (root.right) {
            dfs(root.right)
        }
    }
    dfs(root)
    return sum
};