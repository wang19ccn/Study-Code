/**
 * @param {string} s
 * @return {number}
 */
var longestPalindrome = function (s) {
    let map = new Map()
    let set = new Set()
    for (let c of s) {
        if (map.has(c)) {
            map.set(c, map.get(c) + 1)
        } else {
            map.set(c, 1)
            set.add(c)
        }
    }
    let res = 0
    let bit = false
    for (let v of set) {
        if (map.get(v) % 2) {
            res += map.get(v) - 1
            bit = true
        } else {
            res += map.get(v)
        }
    }
    return bit ? res + 1 : res
};

console.log(longestPalindrome("abccccd"))