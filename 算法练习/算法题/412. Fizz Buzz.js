/**
 * @param {number} n
 * @return {string[]}
 */
var fizzBuzz = function (n) {
    let res = []
    for (let i = 1; i <= n; i++) {
        let str = []
        if (i % 3 === 0) {
            str.push("Fizz");
        }
        if (i % 5 === 0) {
            str.push("Buzz");
        }
        if (str.length === 0) {
            str.push(i);
        }
        res.push(str.join(''));
    }
    return res
};