/**
 * @param {number[]} nums
 * @return {number}
 */
var thirdMax = function (nums) {
    let set = new Set(nums)
    let arr = Array.from(set)
    arr.sort((a, b) => b - a)
    return arr.length >= 3 ? arr[2] : arr[0]
};

console.log(thirdMax([1, 2]))