/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var addStrings = function (num1, num2) {
    let res = ""
    let i = num1.length - 1
    let j = num2.length - 1
    let carry = 0
    while (i >= 0 || j >= 0) {
        let n1 = i >= 0 ? num1.charAt(i) : 0
        let n2 = j >= 0 ? num2.charAt(j) : 0
        let temp = Number(n1) + Number(n2) + carry
        carry = parseInt(temp / 10)
        res = temp % 10 + res
        i--
        j--
    }
    if (carry == 1) res = "1" + res
    return res
};

console.log(addStrings("72", "35"))