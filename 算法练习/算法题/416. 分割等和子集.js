/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canPartition = function (nums) {
    let len = nums.length
    if (len == 0) {
        return false
    }

    let sum = 0
    for (let num of nums) {
        sum += num
    }

    if ((sum & 1) == 1) {
        return false
    }

    let target = sum / 2

    let dp = new Array(len)
    for (let i = 0; i < len; i++) {
        dp[i] = new Array(target + 1).fill(false)
    }

    if (nums[0] <= target) {
        dp[0][nums[0]] = true
    }

    for (let i = 1; i < len; i++) {
        for (let j = 0; j <= target; j++) {
            // 如果dp[0][0] = 0 这步就可以不写，但下一个判断要改成 nums[i] < j
            if (nums[i] == j) {
                dp[i][j] = true
                continue
            }
            if (nums[i] < j) {
                dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i]]
            }
        }
    }

    return dp[len - 1][target]
};

console.log(canPartition([11, 5, 1, 5]))