/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canPartition = function (nums) {
    let len = nums.length
    if (len == 0) {
        return false
    }

    let sum = 0
    for (let num of nums) {
        sum += num
    }

    if ((sum & 1) == 1) {
        return false
    }

    let target = sum / 2

    let dp = new Array(len)
    for (let i = 0; i < len; i++) {
        dp[i] = new Array(target + 1).fill(false)
    }

    // 初始化
    // dp[0][0] 虽然不符合状态方程的定义，
    // 但是如果 j - nums[i] = 0 就说明找到一个和j一样的数，应该判断为true
    dp[0][0] = true
    if (nums[0] == target) {
        return true
    }
    // 其实不用这段代码依然能求出正确结果
    // 但是如果不写 状态转移方程的初始化不完全，中间部分应该是true会算成false
    if (nums[0] < target) {
        dp[0][nums[0]] = true
    }
    // 同理dp[0][0]
    // 可在状态转移的循环中，直接继承上一次状态在修正
    for (let i = 1; i < len; i++) {
        dp[i][0] = true
    }

    for (let i = 1; i < len; i++) {
        for (let j = 0; j <= target; j++) {
            if (nums[i] <= j) {
                dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i]]
            }
        }

        if (dp[i][target]) {
            return true
        }
    }

    return dp[len - 1][target]
};

console.log(canPartition([1, 5, 11, 5]))