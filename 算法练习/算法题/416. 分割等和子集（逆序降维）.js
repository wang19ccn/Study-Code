/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canPartition = function (nums) {
    let len = nums.length
    if (len == 0) {
        return false
    }

    let sum = 0
    for (let num of nums) {
        sum += num
    }

    if ((sum & 1) == 1) {
        return false
    }

    let target = sum / 2

    let dp = new Array(target + 1).fill(false)

    dp[0] = true
    if (nums[0] == target) {
        return true
    }
    if (nums[0] < target) {
        dp[nums[0]] = true
    }

    for (let i = 1; i < len; i++) {
        // 逆序，因为计算dp[j]用到的是上一行两个状态，如果正序会受到影响
        for (let j = target; nums[i] <= j; j--) {
            if (dp[target]) return true
            dp[j] = dp[j] || dp[j - nums[i]]
        }
    }

    return dp[target]
};

console.log(canPartition([1, 2, 5]))