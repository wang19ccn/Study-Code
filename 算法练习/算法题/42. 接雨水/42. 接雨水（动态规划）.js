/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
    let sum = 0;
    let max_left = [];
    let max_right = [];

    for (let i = 0; i < height.length; i++) {
        max_left[i] = 0;
        max_right[i] = 0;
    }
    for (let i = 1; i < height.length - 1; i++) {
        max_left[i] = Math.max(max_left[i - 1], height[i - 1])
        //console.log(max_left);
    }
    for (let i = height.length - 2; i >= 0; i--) {
        max_right[i] = Math.max(max_right[i + 1], height[i + 1]);
    }
    for (let i = 1; i < height.length - 1; i++) {
        let min = Math.min(max_left[i], max_right[i]);
        if (min > height[i]) {
            sum = sum + (min - height[i]);
        }
    }

    return sum
};

console.log(trap([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]))