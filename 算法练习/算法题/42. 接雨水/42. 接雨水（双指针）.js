/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
    let sum = 0;
    let max_left = 0;
    let max_right = 0;

    //指针
    let left = 1;
    let right = height.length - 2;

    for (let i = 1; i < height.length - 1; i++) {
        //从左到右更，木桶原理，
        //只需要找到右边能比左边高的墙
        //即可去算出当前左柱子（-1是左柱子左边的墙）能承载多少水
        //若左边高于找到的右边墙，就去右边计算
        if (height[left - 1] < height[right + 1]) {
            max_left = Math.max(max_left, height[left - 1]);
            let min = max_left;//此处是表达这时候左边墙小于右边墙
            if (min > height[left]) {
                sum += min - height[left];
            }
            left++;
        //从右到左更
        } else {
            max_right = Math.max(max_right, height[right + 1]);
            let min = max_right;
            if (min > height[right]) {
                sum += min - height[right];
            }
            right--;
        }
    }
    return sum;
};

console.log(trap([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]))