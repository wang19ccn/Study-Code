/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var characterReplacement = function (s, k) {
    let len = s.length
    if (len < 2) return len

    let left = 0
    let right = 0

    let maxCount = 0
    let freq = new Array(26).fill(0)
    let res = 0
    while (right < len) {
        freq[s[right].charCodeAt() - 65]++
        maxCount = Math.max(maxCount, freq[s[right].charCodeAt() - 65])
        right++

        if (right - left > maxCount + k) {
            freq[s[left].charCodeAt() - 65]--
            left++
        }

        res = Math.max(res, right - left)
    }

    return res
};

console.log(characterReplacement("ABAB", 2))