/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var multiply = function (num1, num2) {
    if (num1.length < num2.length) [num1, num2] = [num2, num1];
    let res = new Array(num1.length + num2.length + 1).fill(0)
    let i = num2.length - 1
    let j = 0
    let carry = 0
    let bitCarry = 0
    while (i >= 0) {
        for (let k = j, last = num1.length - 1; k < num1.length + j; k++, last--) {
            let bit = parseInt(num1[last]) * parseInt(num2[i])
            let num = parseInt(res[k]) + parseInt(bit % 10) + carry + bitCarry
            bitCarry = parseInt(bit / 10)
            carry = parseInt(num / 10)
            res[k] = parseInt(num % 10)
        }
        if (carry == 1 || bitCarry > 0) {
            let temp = res[num1.length + j] + carry + bitCarry
            res[num1.length + j] = temp % 10
            carry = parseInt(temp / 10)
            bitCarry = 0
        }
        if (carry == 1) {
            resres[num1.length + j + 1] = 1
            carry = 0
        }
        j++
        --i
    }
    if (carry == 1 || bitCarry > 0) {
        res[num1.length + j] = carry + bitCarry
    }
    while (res.length > 1 && res[res.length - 1] === 0) {
        res.pop();
    }
    return res.reverse().join('')
};

console.log(multiply("123456789", "987654321"))
// console.log(multiply("98765", "56789"))