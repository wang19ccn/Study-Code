/**
 * // Definition for a Node.
 * function Node(val,prev,next,child) {
 *    this.val = val;
 *    this.prev = prev;
 *    this.next = next;
 *    this.child = child;
 * };
 */

/**
 * @param {Node} head
 * @return {Node}
 */
var flatten = function (head) {
    let prev = null
    var dfs = function (head) {
        if (head == null) return
        let next = head.next
        if (prev != null) {
            prev.next = head
            head.prev = prev
        }
        prev = head
        dfs(head.child)
        head.child = null
        dfs(next)
    }
    dfs(head)
    return head
};