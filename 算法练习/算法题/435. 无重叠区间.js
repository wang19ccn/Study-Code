/** 贪心算法
 * @param {number[][]} intervals
 * @return {number}
 */
// 思路：
// 1. 进行右排序
// 2. 从左向右开始遍历，选取右边界最小的作为分割点
// 3. 最后 总区间数 - 非交叉区间数目
// 注：本题和 [贪心算法：用最少数量的箭引爆气球] 相似
var eraseOverlapIntervals = function (intervals) {
    if (intervals.length == 0) return 0
    // 1. 以右边界排序
    intervals.sort((a, b) => a[1] - b[1])
    // 2. 从左向右开始遍历
    // 非交叉区间数目
    let count = 1
    let end = intervals[0][1]
    for (let i = 1; i < intervals.length; i++) {
        if (end <= intervals[i][0]) {
            end = intervals[i][1]
            count++
        }
    }
    // 3. 总区间数 - 非交叉区间数目
    return intervals.length - count
};