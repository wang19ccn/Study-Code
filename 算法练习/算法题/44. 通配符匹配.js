/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */
var isMatch = function (s, p) {
    let s_len = s.length
    let p_len = p.length

    // 构建dp数组
    let dp = new Array(s_len + 1)
    for (let i = 0; i < s_len + 1; i++) {
        dp[i] = new Array(p_len + 1).fill(false)
    }

    // 初始化特殊量和特殊情况
    dp[0][0] = true
    // 当s为空字符串时
    for (let j = 1; j <= p_len; j++) {
        dp[0][j] = p[j - 1] == '*' && dp[0][j - 1];
    }

    // 迭代
    for (let i = 1; i <= s_len; i++) {
        for (let j = 1; j <= p_len; j++) {
            if (p[j - 1] == '?' || s[i - 1] == p[j - 1]) {
                dp[i][j] = dp[i - 1][j - 1]
            }
            else if (p[j - 1] == '*' && (dp[i - 1][j] || dp[i][j - 1])) {
                dp[i][j] = true
            }
        }
    }

    return dp[s_len][p_len]
};

console.log(isMatch("adcbeb", "a*b?b"))