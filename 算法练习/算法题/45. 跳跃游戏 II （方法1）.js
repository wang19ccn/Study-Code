/**
 * @param {number[]} nums
 * @return {number}
 */
var jump = function (nums) {
    let ans = 0
    let end = 0
    let max = 0
    for (let i = 0; i < nums.length - 1; i++) {
        max = Math.max(nums[i] + i, max)
        if (i == end) {
            end = max
            ans++
        }
    }
    return ans
};

console.log(jump([1,2,3]))