/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} key
 * @return {TreeNode}
 */
var deleteNode = function (root, key) {
    if (root == null) {
        return null
    }
    if (key < root.val) {
        root.left = deleteNode(root.left, key)
    }
    else if (key > root.val) {
        root.right = deleteNode(root.right, key)
    }
    else {
        if (root.left == null) {
            return root.right
        }
        else if (root.right == null) {
            return root.left
        }
        else {
            let node = root.right
            while (node.left != null) {
                node = node.left
            }
            node.left = root.left
            return root.right
        }
    }
    return root
};