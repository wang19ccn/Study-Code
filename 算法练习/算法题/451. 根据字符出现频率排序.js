/**
 * @param {string} s
 * @return {string}
 */
var frequencySort = function (s) {
    const len = s.length
    const arr = s.split('')

    let map = new Map()
    for (let i = 0; i < len; i++) {
        if (map.has(arr[i])) {
            map.set(arr[i], map.get(arr[i]) + 1)
        } else {
            map.set(arr[i], 1)
        }
    }

    let res = []
    map.forEach((v, k) => {
        res.push([k, v])
    })
    res.sort((a, b) => b[1] - a[1])


    let newStr = []
    for (let i = 0; i < res.length; i++) {
        for (let j = 0; j < res[i][1]; j++) {
            newStr.push(res[i][0])
        }
    }

    return newStr.join('')
};

console.log(frequencySort("tree"))
console.log(frequencySort("Aabb"))