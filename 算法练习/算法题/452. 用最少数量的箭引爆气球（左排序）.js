var findMinArrowShots = function (points) {
    if (points.length == 0) return 0;
    points.sort((a, b) => a[0] - b[0]);

    let count = 1;
    let i = 1;
    while (i < points.length) {
        if (points[i][0] > points[i - 1][1]) {
            count++
        } else {
            points[i][1] = Math.min(points[i - 1][1], points[i][1])
        }
        i++
    }

    return count;
};