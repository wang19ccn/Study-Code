/**
 * @param {number[]} A
 * @param {number[]} B
 * @param {number[]} C
 * @param {number[]} D
 * @return {number}
 */
var fourSumCount = function (A, B, C, D) {
    let map = new Map()
    for (let a of A) {
        for (let b of B) {
            if (map.has(a + b)) {
                map.set(a + b, map.get(a + b) + 1)
            } else {
                map.set(a + b, 1)
            }
        }
    }
    let count = 0
    for (let c of C) {
        for (let d of D) {
            let num = -(c + d)
            if (map.has(num)) {
                count += map.get(num)
            }
        }
    }
    return count
};

console.log(fourSumCount([-1, -1], [-1, 1], [-1, 1], [1, -1]))