/**
 * @param {number[]} g
 * @param {number[]} s
 * @return {number}
 */
var findContentChildren = function (g, s) {
    g.sort((a, b) => a - b)
    s.sort((a, b) => a - b)
    let count = 0
    for (let i = 0; count < g.length && i < s.length; i++) {
        if (g[count] <= s[i]) count++
    }
    return count
};