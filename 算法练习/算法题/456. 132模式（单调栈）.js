/**
 * 单调增栈
 * @param {number[]} nums
 * @return {boolean}
 */
var find132pattern = function (nums) {
    let len = nums.length

    let stack = []
    let k = -Infinity
    for (let i = len - 1; i >= 0; i--) {
        if (nums[i] < k) return true
        while (stack.length != 0 && stack[stack.length - 1] < nums[i]) {
            k = Math.max(k, stack.pop())
        }
        stack.push(nums[i])
    }

    return false
};

console.log(find132pattern([3, 3, 3, 4, 2]))