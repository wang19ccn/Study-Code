/**
 * 枚举3
 * @param {number[]} nums
 * @return {boolean}
 */
var find132pattern = function (nums) {
    let len = nums.length
    let num_i = nums[0]
    for (let j = 1; j < len; j++) {
        for (let k = j + 1; k < len; k++) {
            if (num_i < nums[k] && nums[k] < nums[j]) return true
        }
        num_i = Math.min(num_i, nums[j])
    }
    return false
};