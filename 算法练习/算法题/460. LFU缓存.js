/**
 * @param {number} capacity
 */
var LFUCache = function(capacity) {
    this.limit=capacity;
    this.map={};
    this.freqMap={};
    //console.log("调用构造函数===>"+"容量："+this.limit+"map："+this.map+"freqMap："+this.freqMap)
};

/*
map对象格式
{
    value:"value", //存值
    freq:1 //频率
}
*/
/** 
 * @param {number} key
 * @return {number}
 */
LFUCache.prototype.get = function(key) {
    let r=-1;
    if(typeof this.map[key]!="undefined"){
        let o = this.map[key];
        r=o.value;
        this.updateL(key,o);
    }
    return r;
};

/** 
 * @param {number} key 
 * @param {number} value
 * @return {void}
 */
LFUCache.prototype.put = function(key, value) {
    if(this.limit<=0) return;

    if(typeof key=="undefined"||typeof value=="undefined") throw new Error('key or value is undefined');

    if(typeof this.map[key]=="undefined"){ //key代表map的数组下标
        if(Object.keys(this.map).length == this.limit){ 
            let fkeys=Object.keys(this.freqMap);
            let freq=fkeys[0];
            let keys = this.freqMap[freq];
            delete this.map[keys.shift()];
            if(this.freqMap[freq].length==0) delete this.freqMap[freq]; 
        }  

        if(!this.freqMap[1]) this.freqMap[1] = [];

        this.freqMap[1].push(key);
        this.map[key]={
            value:value,
            freq:1
        };
        //console.log("put加入的"+"key="+key+this.map[key].value+"-----"+this.map[key].freq)
        
    }else{
        this.map[key].value=value;
        this.updateL(key,this.map[key]);
    }

    
    
};

/**
 * Your LFUCache object will be instantiated and called as such:
 * var obj = new LFUCache(capacity)
 * var param_1 = obj.get(key)
 * obj.put(key,value)
 */

LFUCache.prototype.updateL=function(key,obj){
    let freq = obj.freq;
    let arr=this.freqMap[freq];

    //删除原频率记录
    this.freqMap[freq].splice(arr.indexOf(key),1);
    //清理
    if(this.freqMap[freq].length==0){
        delete this.freqMap[freq];
    }
    //更新频率
    freq=obj.freq=obj.freq+1;
    if(!this.freqMap[freq]){
        this.freqMap[freq]=[]
    }
    this.freqMap[freq].push(key);
};


cache = new LFUCache(2);
cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // 返回 1
cache.put(3, 3);    // 去除 key 2
cache.get(2);       // 返回 -1 (未找到key 2)
cache.get(3);       // 返回 3
cache.put(4, 4);    // 去除 key 1
console.log(cache.get(1));      // 返回 -1 (未找到 key 1)
cache.get(3);       // 返回 3
console.log(cache.get(4));       // 返回 4

// cache = new LFUCache(3);
// cache.put(1, 1);
// cache.put(3, 2);
// cache.get(3)