/**
 * @param {number} x
 * @param {number} y
 * @return {number}
 */
var hammingDistance = function (x, y) {
    let n = x ^ y
    let distance = 0
    while (n) {
        ++distance
        n &= n - 1
    }
    return distance
};