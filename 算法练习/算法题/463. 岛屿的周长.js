/**
 * @param {number[][]} grid
 * @return {number}
 */
var islandPerimeter = function (grid) {
    let sum = 0
    for (let k = 0; k < grid.length; k++) {
        for (let i = 0; i < grid[0].length; i++) {
            if (grid[k][i] == 1) {
                if (k - 1 < 0 || grid[k - 1][i] == 0) {
                    ++sum
                }
                if (k + 1 >= grid.length || grid[k + 1][i] == 0) {
                    ++sum
                }
                if (i - 1 < 0 || grid[k][i - 1] == 0) {
                    ++sum
                }
                if (i + 1 >= grid[0].length || grid[k][i + 1] == 0) {
                    ++sum
                }
            }
        }
    }
    return sum
};

console.log(islandPerimeter([[0, 1, 0, 0],
[1, 1, 1, 0],
[0, 1, 0, 0],
[1, 1, 0, 0]]))