/**
 * @param {string} s1
 * @param {number} n1
 * @param {string} s2
 * @param {number} n2
 * @return {number}
 */
var getMaxRepetitions = function(s1, n1, s2, n2) {
    let str1 = s1.split("");
    let str2 = s2.split("");
    
    let index = 0
    let cnt = 0
    for(let i=0;i<n1;i++){
        for(let j=0;j<str1.length;j++){
            if(str1[j]==str2[index]){
                index++
            }
            if(index==str2.length){
                index=0
                ++cnt;
            }
        }
    }
    return parseInt(cnt/n2)
};

console.log(getMaxRepetitions("acb",4,"ab",2))

console.log(getMaxRepetitions("aaa",10,"aa",1))
