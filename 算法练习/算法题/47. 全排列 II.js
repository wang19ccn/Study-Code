/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var permuteUnique = function (nums) {
    let len = nums.length;
    let res = [];
    if (len == 0) {
        return res;
    }

    let path = []
    let used = new Array(len).fill(false)

    nums.sort((a, b) => a - b)
    dfs(nums, len, 0, path, used, res)

    return res;
};

var dfs = function (nums, len, depth, path, used, res) {
    if (depth == len) {
        res.push([...path])
        return;
    }

    for (let i = 0; i < len; i++) {
        if (used[i]) {
            continue;
        }

        // 剪枝条件：i > 0 是为了保证 nums[i - 1] 有意义
        // 写 !used[i - 1] 是因为 nums[i - 1] 在深度优先遍历的过程中刚刚被撤销选择
        if (i > 0 && nums[i] == nums[i - 1] && !used[i - 1]) {
            continue;
        }

        path.push(nums[i]);
        used[i] = true;
        dfs(nums, len, depth + 1, path, used, res);
        path.pop();
        used[i] = false;
    }
}

console.log(permuteUnique([3, 3, 0, 3]))