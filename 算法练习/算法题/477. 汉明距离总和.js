/**
 * @param {number[]} nums
 * @return {number}
 */
var totalHammingDistance = function (nums) {
    let ans = 0;
    let n = nums.length;
    for (let i = 0; i < 30; ++i) {
        let c = 0;
        for (let val of nums) {
            c += (val >> i) & 1;
        }
        ans += c * (n - c);
    }
    return ans;
};
