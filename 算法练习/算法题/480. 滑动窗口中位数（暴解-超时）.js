/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var medianSlidingWindow = function (nums, k) {
    let len = nums.length
    if (len < 1) return nums

    let left = 0
    let right = k

    let res = []
    while (right < len + 1) {
        let temp = nums.slice(left, right)
        temp.sort((a, b) => a - b)

        if (k % 2 == 0) {
            let new_k = k / 2
            let new_k1 = k / 2 - 1
            let mid = (temp[new_k] + temp[new_k1]) / 2
            console.log(mid)
            res.push(mid)
        } else {
            let new_k = (k - 1) / 2
            let mid = temp[new_k]
            res.push(mid)
        }

        left++
        right++
    }

    return res
};


console.log(medianSlidingWindow([1, 3, -1, -3, 5, 3, 6, 7], 3))