/**
 * @param {number[]} nums
 * @return {boolean}
 */
var PredictTheWinner = function (nums) {
    let len = nums.length

    let dp = new Array(len)
    for (let i = 0; i < dp.length; i++) {
        dp[i] = new Array(len)
    }

    for (let i = 0; i < len; i++) {
        dp[i][i] = nums[i]
    }

    for (let i = len - 2; i >= 0; i--) {
        for (let j = i + 1; j < len; j++) {
            let pickA = nums[i] - dp[i + 1][j] // 选择左端
            let pickB = nums[j] - dp[i][j - 1] // 选择右端
            dp[i][j] = Math.max(pickA, pickB)
        }
    }

    return dp[0][len - 1] >= 0
};