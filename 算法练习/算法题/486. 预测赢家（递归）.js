/**
 * @param {number[]} nums
 * @return {boolean}
 */
var PredictTheWinner = function (nums) {
    let len = nums.length
    let memo = new Array(len)
    for (let i = 0; i < memo.length; i++) {
        memo[i] = new Array(len)
    }
    // 利用差值计算
    var select = function (i, j) {
        if (memo[i][j] !== undefined) {
            return memo[i][j]
        }
        if (i == j) {
            memo[i][j] = nums[i]
            return nums[i]
        }
        let pickA = nums[i] - select(i + 1, j) // 选择左端
        let pickB = nums[j] - select(i, j - 1) // 选择右端
        memo[i][j] = Math.max(pickA, pickB)
        return memo[i][j]
    }
    return select(0, nums.length - 1) >= 0
};