/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var findSubsequences = function (nums) {
    let res = []
    let len = nums.length
    let set = new Set()

    dfs(nums, 0, [], res, len, set)
    return res
};

var dfs = function (nums, start, path, res, len, set) {
    if (path.length > 1) {
        let str = path.join(',')
        if (!set.has(str)) {
            res.push(path.slice())
            set.add(str)
        }
    }
    for (let i = start; i < len; i++) {
        let pre = path[path.length - 1]
        let cur = nums[i]
        if (path.length == 0 || pre <= cur) {
            path.push(cur)
            dfs(nums, i + 1, path, res, len, set)
            path.pop()
        }
    }
}