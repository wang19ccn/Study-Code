/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var findTargetSumWays = function (nums, target) {
    let len = nums.length
    let count = 0;

    var dfs = function (start, sum) {
        if (start == len - 1) {
            if (sum + nums[start] == target) {
                count++;
            }
            if (sum - nums[start] == target) {
                count++;
            }
            return;
        }
        dfs(start + 1, sum + nums[start]);
        dfs(start + 1, sum - nums[start]);
    };

    dfs(0, 0);

    return count;
};

console.log(findTargetSumWays([1,0], 1))