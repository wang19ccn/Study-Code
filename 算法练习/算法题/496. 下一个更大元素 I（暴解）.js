/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var nextGreaterElement = function (nums1, nums2) {
    let res = []
    for (let num of nums1) {
        let index = 0
        for (let i = 0; i < nums2.length; i++) {
            if (nums2[i] === num) index = i
        }
        let k = index + 1
        while (k < nums2.length && nums2[k] < num) {
            k++
        }
        k < nums2.length ? res.push(nums2[k]) : res.push(-1)
    }
    return res
};