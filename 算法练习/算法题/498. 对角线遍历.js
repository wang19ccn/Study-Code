/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var findDiagonalOrder = function (matrix) {
    if (matrix.length == 0) return []

    let dx = [1, 0];
    let dy = [0, 1];

    let m = matrix.length;
    let n = matrix[0].length;

    let queue_a = new Array();
    let queue_b = new Array();

    let point = []
    let res = []

    res.push(matrix[0][0])
    queue_a[0] = [0, 0]

    while (0 != queue_a.length || 0 != queue_b.length) {
        if (0 != queue_a.length) {
            let temp = []
            while (0 != queue_a.length) {
                point = queue_a.shift();
                let x = point[0];
                let y = point[1];
                for (let i = 0; i < 2; i++) {
                    let newX = x + dx[i];
                    let newY = y + dy[i];
                    if (newX < 0 || newX >= m || newY < 0 || newY >= n || matrix[newX][newY] == Infinity) {
                        continue;
                    }
                    temp.push(matrix[newX][newY])
                    matrix[newX][newY] = Infinity
                    queue_b.push([newX, newY]);
                }
            }
            temp.reverse()
            res.push.apply(res, temp)
        } else {
            let temp = []
            while (0 != queue_b.length) {
                point = queue_b.shift();
                let x = point[0];
                let y = point[1];
                for (let i = 0; i < 2; i++) {
                    let newX = x + dx[i];
                    let newY = y + dy[i];
                    if (newX < 0 || newX >= m || newY < 0 || newY >= n || matrix[newX][newY] == Infinity) {
                        continue;
                    }
                    temp.push(matrix[newX][newY])
                    matrix[newX][newY] = Infinity
                    queue_a.push([newX, newY]);
                }
            }
            res.push.apply(res, temp)
        }
    }
    return res;
};

console.log(findDiagonalOrder(
    [[477, 2647, -33], [289, 735, -277], [164, 5236, -320], [363, 1067, -306], [345, 483, -78], [307, 3905, 140], [260, 3676, 122], [195, 5003, 13], [331, 4447, 293], [40, 5282, 452], [335, 5225, 69], [128, 3733, -94], [186, 3251, -7], [245, 3908, 364], [278, 2093, 4], [334, 1553, 47], [459, 1267, 56], [399, 2584, 336], [411, 1136, 462], [428, 3119, 174], [317, 1050, -206], [303, 4985, 147], [429, 5453, -430], [132, 2588, -159], [351, 1119, 125], [116, 2360, -42], [72, 3934, -112], [486, 3681, 99], [153, 21, 329], [408, 3050, 75], [452, 2264, -13], [156, 2686, 205], [475, 838, -353], [420, 5311, 28], [265, 1870, 66], [502, 4905, 58], [99, 3282, 87], [143, 5092, -248], [512, 5357, -351], [272, 4311, 434], [458, 3121, 341], [225, 4819, -29], [493, 4613, -230], [99, 5133, -233], [97, 1897, 310], [508, 731, 389], [218, 1590, -143], [368, 2917, -311], [244, 1027, -130], [211, 5186, 155], [509, 3994, 38], [340, 2269, -9], [479, 3302, -358], [214, 3405, -213], [511, 2781, -285], [428, 481, 338], [222, 1628, -127], [480, 3728, -327], [176, 2755, 366], [182, 2682, -56], [446, 1498, -127], [172, 3842, -222], [192, 5507, -230], [30, 4423, 144], [186, 814, -64], [434, 689, -115], [128, 3875, 176], [179, 4241, 3], [34, 1224, 113], [285, 3759, -25], [345, 3123, -76], [481, 3800, 42], [478, 3587, 6], [315, 732, 315], [287, 986, -220], [285, 3605, 193], [381, 2592, 142], [405, 1911, 322], [388, 2085, -92], [193, 5521, 352], [131, 1606, -296], [518, 4054, -124], [224, 73, -30], [485, 463, -135], [1, 3975, 402]]
))
