/** 中心扩散
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function (s) {
    if (s == null || s.length < 1) return ""
    let left = 0
    let right = 0
    for (let i = 0; i < s.length - 1; i++) {
        let len1 = expandAroundCenter(s, i, i) //奇数中心
        let len2 = expandAroundCenter(s, i, i + 1) //偶数中心
        let len = Math.max(len1, len2)
        if (len > right - left) {
            left = i - parseInt((len - 1) / 2)
            right = i + parseInt(len / 2)
        }
    }
    return s.substring(left, right + 1)
};

var expandAroundCenter = function (s, left, right) {
    let start = left
    let end = right
    while (start >= 0 && end < s.length && s.charAt(start) == s.charAt(end)) {
        start--
        end++
    }
    return end - start - 1
}

console.log(longestPalindrome("babad"))