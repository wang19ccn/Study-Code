/** 动态规划
 * 状态：dp[i][j] 表示子串s[i...j]是否是回文子串
 * 状态转移方程：dp[i][j]=(s[i]==s[j]) and dp[i+1][j-1]
 * 边界条件：j-1-(i+1)+1<2 => j-i<3
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function (s) {
    if (s == null || s.length < 2) return s
    let maxLen = 1
    let start = 0

    let dp = new Array(s.length)
    for (let i = 0; i < s.length; i++) {
        dp[i] = new Array(s.length)
        dp[i][i] = true // 不会被参考
    }

    let str = s.split("")
    for (let j = 1; j < s.length; j++) {
        for (let i = 0; i < j; i++) {
            if (str[i] != str[j]) {
                dp[i][j] = false
            } else {
                if (j - i < 3) {
                    dp[i][j] = true
                } else {
                    dp[i][j] = dp[i + 1][j - 1]
                }
            }

            if (dp[i][j] && j - i + 1 > maxLen) {
                maxLen = j - i + 1
                start = i
            }
        }
    }

    return s.substring(start, start + maxLen)
};

console.log(longestPalindrome("babad"))