/** 迭代
 * @param {number} x
 * @param {number} n
 * @return {number}
 */
var myPow = function (x, n) {
    if (n == 0 || x == 1) return 1.0
    return n < 0 ? 1 / myPowHelper(x, Math.abs(n)) : myPowHelper(x, n)
};

var myPowHelper = function (x, n) {
    let ans = 1.0
    let xc = x;
    while (n > 0) {
        if (n % 2 == 1) {
            ans *= xc
        }
        xc *= xc
        n = parseInt(n / 2)
    }
    return ans
}

console.log(myPow(2, 10))