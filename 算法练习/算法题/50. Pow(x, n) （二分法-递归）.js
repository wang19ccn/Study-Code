/** 格式优化
 * @param {number} x
 * @param {number} n
 * @return {number}
 */
var myPow = function (x, n) {
    if (n == 0 || x == 1) return 1.0
    return n < 0 ? 1 / myPowHelper(x, Math.abs(n)) : myPowHelper(x, n)
};

var myPowHelper = function (x, n) {
    if (n == 1) return x
    let half = myPowHelper(x, parseInt(n / 2))
    return n % 2 != 0 ? half * half * x : half * half
}

// /** 递归法
//  * @param {number} x
//  * @param {number} n
//  * @return {number}
//  */
// var myPow = function (x, n) {
//     if (n == 0 || x == 1) {
//         return 1
//     }
//     if (n < 0) {
//         return 1 / myPowHelper(x, Math.abs(n))
//     }
//     return myPowHelper(x, n)
// };

// var myPowHelper = function (x, n) {
//     if (n == 1) {
//         return x
//     }
//     if (n % 2 != 0) {
//         let half = myPowHelper(x, parseInt(n / 2))
//         return half * half * x
//     } else {
//         let half = myPowHelper(x, parseInt(n / 2))
//         return half * half
//     }
// }