/** 超时
 * @param {number} x
 * @param {number} n
 * @return {number}
 */
var myPow = function (x, n) {
    let res = 1
    if (n < 0) {
        x = 1 / x
        n = -n
    }
    for (let i = 0; i < n; i++) {
        res = res * x
    }
    return res
};