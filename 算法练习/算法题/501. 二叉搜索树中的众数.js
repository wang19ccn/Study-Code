/**
 * 在不计算递归使用的系统栈，则当前符合O(1)的空间复杂度
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var findMode = function (root) {
    // 上一个节点
    let pre = null

    // 结果数组
    let res = []

    // 用于记录一共多少个众数，控制结果数组的长度和数组下标
    let resLen = 0

    // 数据中某数重复的最多数量
    let maxNum = 0

    // 当前该数的数量
    let curNum = 0

    // 递归函数
    var recursion = function (root) {
        if (!root) return

        // 中序遍历--先右
        recursion(root.left)

        // pre != null 阻止第一次进行判断时，pre.val报错
        // 判断是否与前一个数相等
        if (pre != null && pre.val == root.val) {
            ++curNum
        } else {
            curNum = 1
        }

        // 当前数目是否
        if (curNum > maxNum) {
            maxNum = curNum
            resLen = 1
        }
        else if (curNum == maxNum) {
            // 第一轮不会执行此处
            if (res != null) {
                res[resLen] = root.val
            }
            // 第一轮用于计算一共多少个众数
            // 第二轮用于控制结果数组的下标
            resLen++
        }

        // 前一个节点
        pre = root

        // 中序遍历--后左
        recursion(root.right)
    }


    // 启动函数
    var main = function () {
        // 第一轮递归，确定某众数的最大数量，确定数组长度
        recursion(root)

        // 初始化
        pre = null
        res = new Array(resLen)
        resLen = 0
        curNum = 0

        // 第二轮递归，寻找众数
        recursion(root)

        return res
    }

    return main()
};

console.log(findMode(
    root = {
        val: 2147483647,
        left: null,
        right: null
    }
))