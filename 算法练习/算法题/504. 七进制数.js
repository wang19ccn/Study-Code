/**
 * @param {number} num
 * @return {string}
 */
var convertToBase7 = function (num) {
    let sum = '';
    let temp = [];

    if (num == 0) {
        return '0';
    }

    if (num < 0) {
        sum = '-';
        num = -num;
    }

    while (num != 0) {
        temp.unshift(num % 7);
        num = parseInt(num / 7);
    }

    return sum + temp.join('');
};