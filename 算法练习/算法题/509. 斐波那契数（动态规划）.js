/**
 * @param {number} n
 * @return {number}
 */
var fib = function (n) {
    if (n < 2) return n

    let first = 0, second = 0
    let next = 1

    for (let i = 2; i <= n; i++) {
        first = second
        second = next
        next = first + second
    }

    return next
};