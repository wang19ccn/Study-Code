/**
 * @param {string} ring
 * @param {string} key
 * @return {number}
 */
var findRotateSteps = function (ring, key) {
    // 记录各个字母出现的位置
    let indexMap = {}
    for (let i = 0; i < ring.length; i++) {
        let c = ring[i]
        if (indexMap[c]) {
            indexMap[c].push(i)
        } else {
            indexMap[c] = [i]
        }
    }
    console.log(indexMap)

    let memo = new Array(ring.length)
    for (let i = 0; i < ring.length; i++) {
        memo[i] = new Array(key.length).fill(-1)
    }

    let dfs = function (ringIndex, keyIndex) {
        if (keyIndex == key.length) {
            return 0
        }
        if (memo[ringIndex][keyIndex] != -1) {
            return memo[ringIndex][keyIndex]
        }
        const cur = key[keyIndex]
        let res = Infinity
        for (let targetIndex of indexMap[cur]) {
            // 两种出现的位置
            let d1 = Math.abs(ringIndex - targetIndex)
            let d2 = ring.length - d1
            let curMin = Math.min(d1, d2)
            res = Math.min(res, curMin + dfs(targetIndex, keyIndex + 1))
        }
        memo[ringIndex][keyIndex] = res
        return res
    }

    return key.length + dfs(0, 0)
};

console.log(findRotateSteps("godding", "gd"))