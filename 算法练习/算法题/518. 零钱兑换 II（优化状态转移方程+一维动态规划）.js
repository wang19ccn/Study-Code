/**
 * @param {number} amount
 * @param {number[]} coins
 * @return {number}
 */
var change = function (amount, coins) {
    let len = coins.length
    if (len == 0) {
        if (amount == 0) {
            return 1
        }
        return 0
    }

    let dp = new Array(amount + 1).fill(0)

    // 初始化
    dp[0] = 1
    for (let i = coins[0]; i <= amount; i += coins[0]) {
        dp[i] = 1
    }

    for (let i = 1; i < len; i++) {
        for (let j = coins[i]; j <= amount; j++) {
            dp[j] += dp[j - coins[i]]
        }
    }

    return dp[amount]
};

console.log(change(3, [2]))