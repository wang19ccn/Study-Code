/**
 * @param {number} amount
 * @param {number[]} coins
 * @return {number}
 */
var change = function (amount, coins) {
    let len = coins.length
    if (len == 0) {
        if (amount == 0) {
            return 1
        }
        return 0
    }

    let dp = new Array(len)
    for (let i = 0; i < len; i++) {
        dp[i] = new Array(amount + 1).fill(0)
    }

    // 初始化
    dp[0][0] = 1
    for (let i = coins[0]; i <= amount; i += coins[0]) {
        dp[0][i] = 1
    }

    for (let i = 1; i < len; i++) {
        for (let j = 0; j <= amount; j++) {
            for (let k = 0; j - k * coins[i] >= 0; k++) {
                dp[i][j] += dp[i - 1][j - k * coins[i]]
            }
        }
    }

    return dp[len - 1][amount]
};

console.log(change(3, [2]))
console.log(change(5, [1, 2, 5]))