/**
 * @param {number[]} nums
 * @param {number} k
 * @return {boolean}
 */
var checkSubarraySum = function (nums, k) {
    let map = new Map();
    let len = nums.length;

    let pre = 0;
    map.set(0, -1);
    for (let i = 0; i < len; i++) {
        pre += nums[i];
        let remainder = pre % k;
        if (map.has(remainder)) {
            let index = map.get(remainder)
            if (i - index > 1) {
                return true;
            }
        } else {
            map.set(remainder, i);
        }
    }

    return false;
};

// console.log(checkSubarraySum([23, 2, 4, 6, 6], 7))
console.log(checkSubarraySum([23, 2, 6, 4, 7], 6))