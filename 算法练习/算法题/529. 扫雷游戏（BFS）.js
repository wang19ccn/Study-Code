/**
 * @param {character[][]} board
 * @param {number[]} click
 * @return {character[][]}
 */
var updateBoard = function (board, click) {
    // 点击到雷
    if (board[click[0]][click[1]] == 'M') {
        board[click[0]][click[1]] = 'X'
        return board
    }

    // 初始化
    let m = board.length
    let n = board[0].length
    let dx = [-1, 0, 1, 0, 1, 1, -1, -1]
    let dy = [0, -1, 0, 1, -1, 1, 1, -1]
    let queue = [[click[0], click[1]]]

    // 判断是否越界
    var isBorder = function (x, y) {
        return x >= 0 && x < m && y >= 0 && y < n
    }

    while (queue.length != 0) {
        let cur = queue.shift()
        let x = cur[0]
        let y = cur[1]
        // 扫到空白
        let count = 0
        for (let i = 0; i < 8; i++) {
            let new_x = x + dx[i]
            let new_y = y + dy[i]
            if (isBorder(new_x, new_y) && board[new_x][new_y] == 'M') {
                count++
            }
        }
        if (count == 0) {
            board[x][y] = 'B'
            for (let i = 0; i < 8; i++) {
                let new_x = x + dx[i]
                let new_y = y + dy[i]
                if (isBorder(new_x, new_y) && board[new_x][new_y] == 'E') {
                    board[new_x][new_y] = 'B' // 防止重复入队
                    queue.push([new_x, new_y])
                }
            }
        } else {
            board[x][y] = count + ''
        }
    }

    return board
};

