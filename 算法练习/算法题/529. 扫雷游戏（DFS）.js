/**
 * @param {character[][]} board
 * @param {number[]} click
 * @return {character[][]}
 */
var updateBoard = function (board, click) {
    // 初始化
    let m = board.length
    let n = board[0].length
    let dx = [-1, 0, 1, 0, 1, 1, -1, -1]
    let dy = [0, -1, 0, 1, -1, 1, 1, -1]

    // 判断是否越界
    var isBorder = function (x, y) {
        return x >= 0 && x < m && y >= 0 && y < n
    }

    var dfs = function (x, y) {
        if (!isBorder(x, y) || board[x][y] != 'E') {
            return
        }
        // 扫到空白
        let count = 0
        for (let i = 0; i < 8; i++) {
            let new_x = x + dx[i]
            let new_y = y + dy[i]
            if (isBorder(new_x, new_y) && board[new_x][new_y] == 'M') {
                count++
            }
        }
        if (count == 0) {
            board[x][y] = 'B'
            for (let i = 0; i < 8; i++) {
                let new_x = x + dx[i]
                let new_y = y + dy[i]
                dfs(new_x, new_y)
            }
        } else {
            board[x][y] = count + ''
        }
    }

    // 点击到雷/启动dfs
    if (board[click[0]][click[1]] == 'M') {
        board[click[0]][click[1]] = 'X'
        return board
    } else {
        dfs(click[0], click[1])
    }

    return board
};

