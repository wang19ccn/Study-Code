/** 
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function (nums) {
    let len = nums.length
    let max = nums[0]
    for (let i = 1; i < len; i++) {
        if (nums[i - 1] > 0) {
            nums[i] = nums[i] + nums[i - 1]
            max = Math.max(max, nums[i])
        } else {
            max = Math.max(max, nums[i])
        }
    }
    return max
};

console.log(maxSubArray([-2,-1]))