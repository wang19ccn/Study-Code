/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var getMinimumDifference = function (root) {
    let res = Infinity
    let pre = -1
    // let arr = []
    var dfs = function (root) {
        if (root == null) return
        dfs(root.left)
        if (pre == -1) {
            pre = root.val
            // arr.push(pre)
        } else {
            res = Math.min(res, root.val - pre)
            pre = root.val
            // arr.push(pre)
        }
        dfs(root.right)
    }
    dfs(root)
    // console.log(arr)
    return res
};