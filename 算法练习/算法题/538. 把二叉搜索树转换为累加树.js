/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var convertBST = function (root) {
    let sum = 0

    var recursion = function (root) {
        if (!root) return

        if (root.right) {
            recursion(root.right)
        }

        sum += root.val
        root.val = sum

        if (root.left) {
            recursion(root.left)
        }
    }

    recursion(root)
    return root
};