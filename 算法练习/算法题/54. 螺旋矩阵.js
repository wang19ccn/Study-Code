/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var spiralOrder = function (matrix) {
    let m = matrix[0].length, n = matrix.length

    if (!m || !n) return []

    let res = []

    let left = 0, right = m - 1, top = 0, bottom = n - 1
    while (left <= right && top <= bottom) {
        // 上
        for (let i = left; i <= right; i++) {
            res.push(matrix[top][i])
        }
        // 右
        for (let i = top + 1; i <= bottom; i++) {
            res.push(matrix[i][right])
        }
        if (left < right && top < bottom) {
            // 下
            for (let i = right - 1; i > left; i--) {
                res.push(matrix[bottom][i])
            }
            // 左
            for (let i = bottom; i > top; i--) {
                res.push(matrix[i][left])
            }
        }
        [left, right, top, bottom] = [left + 1, right - 1, top + 1, bottom - 1];
    }
    return res
};

console.log(spiralOrder([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]))