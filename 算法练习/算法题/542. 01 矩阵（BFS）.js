/**
 * @param {number[][]} matrix
 * @return {number[][]}
 */
var updateMatrix = function (matrix) {
    let dx = [-1, 1, 0, 0];
    let dy = [0, 0, -1, 1];

    let queue = new Array();
    let m = matrix.length; //获取所有元素数目
    let n = matrix[0].length; //获取单行元素数目

    //获取所有0，并把1改成-1表示未访问
    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            if (0 == matrix[i][j]) {
                queue.push([i, j]);
            } else {
                matrix[i][j] = -1;
            }
        }
    }

    console.log(queue);
    while (0 != queue.length) {
        point = queue.shift();
        let x = point[0];
        let y = point[1];
        for (let i = 0; i < 4; i++) {
            let newX = x + dx[i];
            let newY = y + dy[i];
            if (newX >= 0 && newX < m && newY >= 0 && newY < n && matrix[newX][newY] == -1) {
                matrix[newX][newY] = matrix[x][y]+1;
                queue.push([newX, newY]);
            }
        }
    }

    return matrix;
};

//console.log(updateMatrix([[0, 1, 1], [0, 1, 0], [1, 1, 1]]))
console.log(updateMatrix([[0,1,1,0,1,0,0,1,1,1,0,1,0,0,1,0,0,1,0,1,1,1,0,0,1,0,1,1,0,0,1,0,0]]))