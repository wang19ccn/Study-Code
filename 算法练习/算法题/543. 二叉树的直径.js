/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var diameterOfBinaryTree = function (root) {
    let sum = 0

    var recursion = function (root) {
        if (root == null) return 0
        let left = recursion(root.left)
        let right = recursion(root.right)
        sum = Math.max(sum, left + right)
        return Math.max(left, right) + 1
    }

    recursion(root)
    return sum
};