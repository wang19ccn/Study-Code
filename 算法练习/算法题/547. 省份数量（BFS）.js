/**
 * @param {number[][]} isConnected
 * @return {number}
 */
var findCircleNum = function (isConnected) {
    let n = isConnected.length
    let visited = new Array(n).fill(false)

    let cnt = 0
    let queue = []
    for (let i = 0; i < n; i++) {
        if (!visited[i]) {
            cnt++

            queue.push(i)
            visited[i] = true

            while (queue.length != 0) {
                let x = queue.shift()
                for (let y = 0; y < n; y++) {
                    if (isConnected[x][y] == 1 && !visited[y]) {
                        visited[y] = true
                        queue.push(y)
                    }
                }
            }

        }
    }

    return cnt
};
