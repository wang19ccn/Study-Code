/**
 * @param {number[][]} isConnected
 * @return {number}
 */
var findCircleNum = function (isConnected) {
    let n = isConnected.length
    let visited = new Array(n).fill(false)
    let cnt = 0
    for (let i = 0; i < n; i++) {
        if (!visited[i]) {
            cnt++
            dfs(i, isConnected, visited)
        }
    }
    return cnt
};

var dfs = function (i, isConnected, visited) {
    visited[i] = true
    for (let j = 0; j < isConnected.length; j++) {
        if (isConnected[i][j] == 1 && !visited[j]) {
            dfs(j, isConnected, visited)
        }
    }
}