class UnionFind {
    constructor(n) {
        this.size = n
        this.parent = new Array(n)
        for (let i = 0; i < n; i++) {
            this.parent[i] = i
        }
    }
    union(x, y) {
        let rootX = this.find(x)
        let rootY = this.find(y)
        if (rootX != rootY) {
            this.parent[rootX] = rootY
            this.size--
        }
    }
    /**
    * 路径压缩
    */
    find(x) {
        if (x != this.parent[x]) {
            this.parent[x] = this.find(this.parent[x])
        }
        return this.parent[x]
    }
}
/**
 * @param {number[][]} isConnected
 * @return {number}
 */
var findCircleNum = function (isConnected) {
    let n = isConnected.length
    let unionFind = new UnionFind(n)
    for (let i = 0; i < n; i++) {
        for (let j = i + 1; j < n; j++) {
            if (isConnected[i][j] == 1) {
                unionFind.union(i, j)
            }
        }
    }
    return unionFind.size
};

console.log(findCircleNum([[1, 1, 0], [1, 1, 0], [0, 0, 1]]))