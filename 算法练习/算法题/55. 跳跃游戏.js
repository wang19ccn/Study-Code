/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function(nums) {
    let maxstep=0;
    for(let i=0;i<nums.length;i++){
        if(i>maxstep) return false
        maxstep=Math.max(maxstep,i+nums[i])
    }
    return true;
};

console.log(canJump([2,3,1,1,4]))