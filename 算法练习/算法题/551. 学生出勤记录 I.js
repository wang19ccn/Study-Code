/**
 * @param {string} s
 * @return {boolean}
 */
var checkRecord = function (s) {
    let A_count = 0
    let L_count = 0
    for (let i = 0; i < s.length; i++) {
        if (s[i] == 'A') A_count++
        if (A_count >= 2) return false
        if (s[i] == 'L') {
            L_count++
            if (L_count >= 3) return false
        } else {
            L_count = 0
        }
    }
    return true
};