/** 92 ms
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var subarraySum = function (nums, k) {
    let check = new Map()
    check.set(0, 1)
    let count = 0, pre = 0
    for (num of nums) {
        pre += num
        if (check.has(pre - k)) count += check.get(pre - k)
        if (check.has(pre)) {
            check.set(pre, check.get(pre) + 1)
        } else {
            check.set(pre, 1)
        }
    }
    return count
};

console.log(subarraySum([3, 4, 7, 2, -3, 1, 1, -1, 3, -3], 7))