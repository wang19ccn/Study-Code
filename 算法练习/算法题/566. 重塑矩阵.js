/**
 * @param {number[][]} nums
 * @param {number} r
 * @param {number} c
 * @return {number[][]}
 */
var matrixReshape = function (nums, r, c) {
    let m = nums.length
    let n = nums[0].length

    if (m * n != r * c) {
        return nums
    }

    let ans = new Array(r).fill(0).map(() => new Array(c).fill(0))
    for (let x = 0; x < m * n; ++x) {
        ans[Math.floor(x / c)][x % c] = nums[Math.floor(x / n)][x % n];
    }

    return ans;
};

// 作者：LeetCode-Solution
// 链接：https://leetcode-cn.com/problems/reshape-the-matrix/solution/zhong-su-ju-zhen-by-leetcode-solution-gt0g/
