/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} s
 * @param {TreeNode} t
 * @return {boolean}
 */
var isEqual

var isSubtree = function (s, t) {
    isEqual = false
    dfs(s, t.val, t)
    return isEqual
};

var dfs = function (root, target, t) {
    if (isEqual) {
        return
    }
    if (!root) {
        return 
    }
    if (root.val === target) {
        isEqual = check(root, t)
    }
    dfs(root.left, target, t)
    dfs(root.right, target, t)
}

var check = function (root, t) {
    if (!root && !t) {
        return true
    }
    if (!root || !t) {
        return false
    }
    if (root.val !== t.val) {
        return false
    }
    return check(root.left, t.left) && check(root.right, t.right)
}