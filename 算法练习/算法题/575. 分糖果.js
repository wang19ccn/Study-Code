/**
 * @param {number[]} candies
 * @return {number}
 */
var distributeCandies = function (candies) {
    let set = new Set()
    for (let can of candies) {
        set.add(can)
    }
    return Math.min(set.size, parseInt(candies.length / 2))
};