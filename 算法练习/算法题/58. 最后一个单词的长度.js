/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLastWord = function (s) {
    let end = s.length - 1
    let start = 0
    while (s[end] == ' ') {
        --end
        if (end < 0) return 0
    }
    start = end
    while (s[start] != ' ' && start >= 0) {
        --start
    }
    return end - start
};

console.log(lengthOfLastWord("    "))