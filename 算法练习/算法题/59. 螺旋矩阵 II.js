/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var generateMatrix = function (n) {
    let matrix = new Array(n).fill(0).map(() => new Array(n).fill(0));

    let left = 0, right = n - 1, top = 0, bottom = n - 1

    let num = 1
    while (left <= right && top <= bottom) {
        // 上
        for (let i = left; i <= right; i++) {
            matrix[top][i] = num
            ++num
        }
        // 右
        for (let i = top + 1; i <= bottom; i++) {
            matrix[i][right] = num
            ++num
        }
        if (left < right && top < bottom) {
            // 下
            for (let i = right - 1; i > left; i--) {
                matrix[bottom][i] = num
                ++num
            }
            // 左
            for (let i = bottom; i > top; i--) {
                matrix[i][left] = num
                ++num
            }
        }
        [left, right, top, bottom] = [left + 1, right - 1, top + 1, bottom - 1];
    }
    return matrix
};
