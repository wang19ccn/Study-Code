/**
 * @param {number} n
 * @param {number} k
 * @return {string}
 */
var getPermutation = function (n, k) {
    let res = "";
    if (n == 0) {
        return res;
    }

    let count = 0
    let path = []
    let used = new Array(n + 1).fill(false)

    var dfs = function (len, depth) {
        if (depth == n) {
            count++
            if (count == k) {
                return res = path.join("")
            }
            return count;
        }

        for (let i = 1; i <= len; i++) {
            if (used[i]) {
                continue;
            }
            path.push(i);
            used[i] = true;
            dfs(len, depth + 1);
            path.pop();
            used[i] = false;
            if (res) {
                return res
            }
        }
    }

    dfs(n, 0)
    return res;
};

console.log(getPermutation(3, 2))