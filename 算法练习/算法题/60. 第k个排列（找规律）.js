/**
 * @param {number} n
 * @param {number} k
 * @return {string}
 */
var getPermutation = function (n, k) {
    let nums = []
    let factorial = 1

    for (let i = 1; i <= n; i++) {
        nums.push(i)
        factorial = factorial * i
    }

    k-- // nums中索引从0开始
    let res = ""

    while (nums.length > 0) {
        factorial = factorial / nums.length // 降阶
        let index = parseInt(k / factorial)
        res += nums[index]
        nums.splice(index, 1)
        k = k % factorial
    }

    return res
};

console.log(getPermutation(3, 2))