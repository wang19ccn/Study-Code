/**
 * @param {number[]} flowerbed
 * @param {number} n
 * @return {boolean}
 */
var canPlaceFlowers = function (flowerbed, n) {
    for (let i = 0, len = flowerbed.length; i < len && n > 0;) {
        if (flowerbed[i] == 1) {
            i += 2;
        } else if (i == flowerbed.length - 1 || flowerbed[i + 1] == 0) {
            n--;
            i += 2;
        } else {
            i += 3;
        }
    }
    return n <= 0
};