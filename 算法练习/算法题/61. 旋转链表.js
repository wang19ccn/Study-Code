/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var rotateRight = function (head, k) {
    if (!head || !head.next || k == 0) return head
    let pre = head
    let count = 0
    while (pre) {
        pre = pre.next
        count++
    }
    k = k % count
    while (k != 0) {
        let cur = head
        let two
        while (cur.next != null) {
            if (cur.next.next == null) two = cur
            cur = cur.next
        }
        two.next = null
        cur.next = head
        head = cur
        k--
    }
    return head
};