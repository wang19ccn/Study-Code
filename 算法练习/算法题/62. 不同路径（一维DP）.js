/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
var uniquePaths = function (m, n) {
    // dp initialize
    let dp = new Array(n).fill(1)

    // iterate
    for (let i = 1; i < m; i++) {
        for (let j = 1; j < n; j++) {
            dp[j] += dp[j - 1]
        }
    }

    return dp[n - 1]
};