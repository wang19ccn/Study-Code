/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */

 // 在 m+n−2 次 选择 m-1 个向下的方案数 （1，1，1，1，1）选择那几个来向下走
 // 选择 n-1 个向右的方案数 与选 m 一样
var uniquePaths = function (m, n) {
 
    // (m+n−2)(m+n−3)⋯n
    let member = 1
    for (let x = n; x <= m + n - 2; x++) {
        member *= x
    }

    // (m-1)!
    let denominator = 1
    for (let y = 1; y < m; y++) {
        denominator *= y
    }

    return member / denominator
};