/**
 * @param {character[]} tasks
 * @param {number} n
 * @return {number}
 */
var leastInterval = function (tasks, n) {

    let counts = new Array(26).fill(0)
    for (let task of tasks) {
        counts[task.charCodeAt() - 'A'.charCodeAt()]++
    }

    let max = 0
    for (let count of counts) {
        max = Math.max(max, count)
    }

    let maxCount = 0
    for (let count of counts) {
        if (count == max) maxCount++
    }

    return Math.max((n + 1) * (max - 1) + maxCount, tasks.length)
};