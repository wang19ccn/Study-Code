/**
 * @param {number[][]} nums
 * @return {number[]}
 */
var smallestRange = function (nums) {
    let all = new Array()
    let len = nums.length
    let map = new Array()
    for (let i = 0; i < len; i++) {
        map[i] = 0
        nums[i].forEach(v => {
            all.push({ value: v, from: i })
        })
    }
    all.sort((a, b) => { return a.value - b.value })

    let left = 0
    let count = 0
    let minLen = Infinity
    let minStart = 0
    for (let right = 0; right < all.length; right++) {
        if (map[all[right].from] == 0) count++
        map[all[right].from]++
        while (count == len && left <= right) {
            if (all[right].value - all[left].value < minLen) {
                minLen = all[right].value - all[left].value
                minStart = all[left].value
            }
            map[all[left].from]--
            if (map[all[left].from] == 0) count--
            left++
        }
    }
    return [minStart, minStart + minLen]
};

console.log(smallestRange([[4, 10, 15, 24, 26], [0, 9, 12, 20], [5, 18, 22, 30]]))