/**
 * @param {number} c
 * @return {boolean}
 */
var judgeSquareSum = function (c) {
    for (let i = 0; i * i <= c; i++) {
        let j = Math.sqrt(c - i * i);
        if (j === parseInt(j)) {
            return true;
        }
    }
    return false
};