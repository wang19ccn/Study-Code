/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var averageOfLevels = function (root) {
    if (!root) return []

    let averages = []
    let queue = [root]

    while (queue.length) {
        let sum = 0
        let len = queue.length
        for (let i = 0; i < len; i++) {
            let cur = queue.shift()
            sum += cur.val
            if (cur.left) {
                queue.push(cur.left)
            }
            if (cur.right) {
                queue.push(cur.right)
            }
        }
        averages.push(sum / len)
    }

    return averages
};

