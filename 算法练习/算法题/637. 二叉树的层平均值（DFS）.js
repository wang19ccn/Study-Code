/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var averageOfLevels = function (root) {
    let sum = []
    let res = []
    dfs(root, sum, 0)
    for (let s of sum) {
        let average = 0
        for (let i = 0; i < s.length; i++) {
            average += s[i]
        }
        average /= s.length
        res.push(average)
    }
    return res
};

var dfs = function (root, sum, level) {
    if (root == null) return

    if (sum[level] == undefined) {
        sum[level] = [root.val]
    } else {
        sum[level].push(root.val)
    }

    dfs(root.left, sum, level + 1)
    dfs(root.right, sum, level + 1)
}