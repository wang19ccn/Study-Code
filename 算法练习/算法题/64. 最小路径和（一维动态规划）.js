/**
 * @param {number[][]} grid
 * @return {number}
 */
var minPathSum = function (grid) {
    let m = grid.length
    let n = grid[0].length
    if (grid == null || m == 0 || n == 0) return 0
    let dp = Array(m)

    dp[0] = grid[0][0]
    for (let i = 1; i < m; i++) {
        dp[i] = dp[i - 1] + grid[i][0]
    }
    for (let j = 1; j < n; j++) {
        dp[0] = dp[0] + grid[0][j]
        for (let i = 1; i < m; i++) {
            dp[i] = Math.min(dp[i - 1], dp[i]) + grid[i][j]
        }
    }

    return dp[m - 1]
};


console.log(minPathSum([[1, 3, 1], [1, 5, 1], [4, 2, 1],[4, 2, 1]]))