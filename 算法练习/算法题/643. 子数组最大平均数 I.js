/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findMaxAverage = function (nums, k) {
    let left = 1
    let right = k

    let sum = 0
    for (let i = 0; i < k; i++) {
        sum += nums[i]
    }

    let average = sum / k
    while (right < nums.length) {
        sum = sum - nums[left - 1] + nums[right]
        average = Math.max(average, sum / k)
        left++
        right++
    }

    return average
};

console.log(findMaxAverage([1, 12, -5, -6, 50, 3], 4))