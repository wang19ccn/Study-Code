/**
 * @param {number[]} nums
 * @return {number[]}
 */
var findErrorNums = function (nums) {
    let err = 0
    let add = 0
    let map = new Map()
    for (let i = 0; i < nums.length; i++) {
        if (map.has(nums[i])) {
            map.set(nums[i], 2)
        } else {
            map.set(nums[i], 1)
        }
    }
    for (let i = 1; i <= nums.length; i++) {
        if (!map.has(i)) {
            add = i
        }
        if (map.has(i) && map.get(i) == 2) {
            err = i
        }
    }
    return [err, add]
};

console.log(findErrorNums([1, 2, 2, 4]))
console.log(findErrorNums([2, 2]))
console.log(findErrorNums([1, 1]))
console.log(findErrorNums([3, 2, 3, 4, 6, 5]))