/**
 * @param {string} s
 * @return {number}
 */
var countSubstrings = function (s) {
    let res = 0
    for (let center = 0; center < 2 * s.length - 1; center++) {
        let left = parseInt(center / 2)
        let right = left + center % 2
        while (left >= 0 && right < s.length && s.charAt(left) == s.charAt(right)) {
            res++
            left--
            right++
        }
    }
    return res
};

// 为什么有 2 * len - 1 个中心点？
//     aba 有5个中心点，分别是 a、b、c、ab、ba
//     abba 有7个中心点，分别是 a、b、b、a、ab、bb、ba
// 什么是中心点？
//     中心点即left指针和right指针初始化指向的地方，可能是一个也可能是两个
// 为什么不可能是三个或者更多？
//     因为3个可以由1个扩展一次得到，4个可以由两个扩展一次得到

// 作者：jawhiow
// 链接：https://leetcode-cn.com/problems/palindromic-substrings/solution/liang-dao-hui-wen-zi-chuan-de-jie-fa-xiang-jie-zho/
// 来源：力扣（LeetCode）
// 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

// 编号 i	回文中心左起始位置 l_i   回文中心右起始位置 r_i	
// 0	    0	                   0
// 1	    0	                   1
// 2	    1	                   1
// 3	    1	                   2
// 4	    2	                   2
// 5	    2	                   3
// 6	    3	                   3

// 作者：LeetCode-Solution
// 链接：https://leetcode-cn.com/problems/palindromic-substrings/solution/hui-wen-zi-chuan-by-leetcode-solution/
// 来源：力扣（LeetCode）
// 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。