/**
 * @param {string} s
 * @return {number}
 */
var countSubstrings = function (s) {
    let count = 0
    let len = s.length

    let dp = new Array(len)
    for (let i = 0; i < len; i++) {
        dp[i] = new Array(len).fill(false)
    }

    for (let j = 0; j < len; j++) {
        for (let i = 0; i <= j; i++) {
            if (i == j) {
                dp[i][j] = true
                count++
            }
            else if (j - i == 1 && s[i] == s[j]) {
                dp[i][j] = true
                count++
            }
            else if (j - i > 1 && s[i] == s[j] && dp[i + 1][j - 1]) {
                dp[i][j] = true
                count++
            }
        }
    }
    
    return count++
};
