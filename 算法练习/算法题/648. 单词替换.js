var replaceWords = function (dictionary, sentence) {
    return sentence
        .split(' ')
        .map(v => {
            let min = v
            for (const prefix of dictionary) {
                if (v.startsWith(prefix) && min.length > prefix.length) {
                    min = prefix
                }
            }
            return min
        })
        .join(' ')
}

//   作者：ZW-L
//   链接：https://leetcode-cn.com/problems/replace-words/solution/javascript-bao-li-sou-suo-qian-zhui-shu-2djb9/
