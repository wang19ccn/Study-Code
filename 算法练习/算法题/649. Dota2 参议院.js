/**
 * @param {string} senate
 * @return {string}
 */
var predictPartyVictory = function (senate) {
    let arr_senate = senate.split("")
    let R = true
    let D = true

    // 当 member 大于0时，R在D前出现，R可以消灭D。当flag小于0时，D在R前出现，D可以消灭R
    let member = 0

    while (R && D) {
        R = false
        D = false
        for (let i = 0; i < arr_senate.length; i++) {
            if (arr_senate[i] == 'R') {
                if (member < 0) arr_senate[i] = "x"; // 消灭R，R此时为false
                else R = true; // 如果没被消灭，本轮循环结束有R
                member++;
            }
            if (arr_senate[i] == 'D') {
                if (member > 0) arr_senate[i] = "x";
                else D = true;
                member--;
            }
        }
    }

    return R == true ? "Radiant" : "Dire";
};

console.log(predictPartyVictory("DDRRR"))