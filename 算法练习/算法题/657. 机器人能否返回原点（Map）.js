/**
 * @param {string} moves
 * @return {boolean}
 */
var judgeCircle = function (moves) {
    let map = new Map([
        ['R', 0],
        ['L', 0],
        ['U', 0],
        ['D', 0]
    ])

    for (let m of moves) {
        if (m == 'R') {
            if (map.get('L') != 0) {
                map.set('L', map.get('L') - 1)
            }
            else {
                map.set('R', map.get('R') + 1)
            }
            continue
        }
        if (m == 'L') {
            if (map.get('R') != 0) {
                map.set('R', map.get('R') - 1)
            }
            else {
                map.set('L', map.get('L') + 1)
            }
            continue
        }
        if (m == 'U') {
            if (map.get('D') != 0) {
                map.set('D', map.get('D') - 1)
            }
            else {
                map.set('U', map.get('U') + 1)
            }
            continue
        }
        if (m == 'D') {
            if (map.get('U') != 0) {
                map.set('U', map.get('U') - 1)
            }
            else {
                map.set('D', map.get('D') + 1)
            }
            continue
        }
    }

    return map.get('R') == 0 && map.get('L') == 0 && map.get('U') == 0 && map.get('D') == 0
};

console.log(judgeCircle("LLRR"))