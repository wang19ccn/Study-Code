/**
 * @param {number[]} digits
 * @return {number[]}
 */
// ES10基本类型BigInt
// 数值6145390195186705544超出Number基本类型的容纳范围，改用BigInt基本类型
// BigInt基本类型进行数学操作时，需要在数字字面量后加个n
var plusOne = function (digits) {
    return String(BigInt(digits.join(''))+1n).split('').map(str=>Number(str))
};

console.log(plusOne([9, 9, 9]))