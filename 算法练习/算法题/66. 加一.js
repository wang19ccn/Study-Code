/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function (digits) {
    if (digits[digits.length - 1] < 9) {
        digits[digits.length - 1] = digits[digits.length - 1] + 1
        return digits
    }

    let i = digits.length - 1
    while (i > -1) {
        if (9 == digits[i]) {
            digits[i] = 0
            i--
        } else {
            digits[i] = digits[i] + 1
            return digits
        }
    }

    let newDigits = new Array(digits.length + 1).fill(0)
    newDigits[0] = 1;

    return newDigits;
};

console.log(plusOne([9, 9, 9]))