/**
 * @param {number[]} nums
 * @return {boolean}
 */
var checkPossibility = function (nums) {
    let count = 0
    for (let i = 1; i < nums.length; i++) {
        if (nums[i - 1] > nums[i]) {
            count++
            if (count > 1) return false
            if (nums[i] < nums[i - 2]) {
                nums[i] = nums[i - 1]
            }
        }
    }
    return true
};