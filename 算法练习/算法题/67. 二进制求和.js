/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 */
var addBinary = function (a, b) {
    // 用0补齐a，b长度
    while (a.length > b.length) b = '0' + b
    while (a.length < b.length) a = '0' + a

    let res = new Array(a.length)
    let carryBefore = 0 // 上一位是否有进位，初始为0
    let val, carry
    for (let i = a.length - 1; i > -1; i--) {
        val = Number(a[i]) ^ Number(b[i]) // 异或是不带进位的相加 (当前不进位的和)
        carry = Number(a[i]) & Number(b[i]) // 求出当前位的进位 (当前位的进位)
        // 处理上一个进位
        if (carryBefore) {
            if (val) {
                carry = 1
                val = 0
            } else {
                val = 1
            }
        }
        carryBefore = carry
        res.unshift(val)
    }

    if (carry) res.unshift(1)
    return res.join('')
};

console.log(addBinary("1", "111"))