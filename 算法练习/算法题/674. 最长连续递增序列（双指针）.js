/**
 * @param {number[]} nums
 * @return {number}
 */
var findLengthOfLCIS = function (nums) {
    if (nums.length == 0) return 0
    let start = 0
    let end = 1
    let res = 1
    while (end < nums.length) {
        while (nums[end - 1] < nums[end]) {
            end++
        }
        res = Math.max(res, end - start)
        start = end
        end++
    }
    return res
};