/**
 * @param {number[]} nums
 * @return {boolean}
 */
var judgePoint24 = function (nums) {
    let len = nums.length
    if (len == 1) {
        let diff = nums[0] - 24
        return Math.abs(diff) < 0.000001
    }
    for (let i = 0; i < len; i++) {
        for (let j = i + 1; j < len; j++) {
            let copy = nums.slice()
            // 注意删的顺序
            copy.splice(j, 1)
            copy.splice(i, 1)

            let n1 = nums[i]
            let n2 = nums[j]
            let isValid = false

            isValid = isValid || judgePoint24(copy.concat(n1 + n2))
            isValid = isValid || judgePoint24(copy.concat(n1 - n2))
            isValid = isValid || judgePoint24(copy.concat(n2 - n1))
            isValid = isValid || judgePoint24(copy.concat(n1 * n2))
            if (n2 !== 0) {
                isValid = isValid || judgePoint24(copy.concat(n1 / n2))
            }
            if (n1 !== 0) {
                isValid = isValid || judgePoint24(copy.concat(n2 / n1))
            }
            if (isValid) return true
        }
    }
    return false
};

console.log(judgePoint24([1, 1, 8, 7]))