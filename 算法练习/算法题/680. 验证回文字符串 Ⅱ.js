/** 字符串只包含从 a-z 的小写字母。字符串的最大长度是50000。
 * @param {string} s
 * @return {boolean}
 */
var validPalindrome = function (s) {
    let i = 0
    let j = s.length - 1
    while (i < j) {
        if (s.charAt(i) != s.charAt(j)) {
            return (check(s, i + 1, j) || check(s, i, j - 1))
        }
        i++
        j--
    }
    return true
};

/**
 * 
 * @param {string} s 
 * @param {number} i 
 * @param {number} j 
 */
var check = function (s, i, j) {
    for (let k = i; k <= i + parseInt((j - i) / 2); k++) {
        if (s.charAt(k) != s.charAt(j - k + i)) return false
    }
    return true
}

console.log(validPalindrome("edaeadeasbccbaedaeadep"))