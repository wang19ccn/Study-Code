let father = [] // 记录父节点

var find = function (root) {
    let temp
    let son = root
    while (root != father[root]) {
        root = father[root]
    }
    while (son != root) {
        temp = father[son]
        father[son] = root
        son = temp
    }
    return root
}

/**
 * @param {number[][]} edges
 * @return {number[]}
 */
var findRedundantDirectedConnection = function (edges) {
    let len = edges.length
    let degree = new Array(len + 1).fill(0)
    for (let edge of edges) {
        degree[edge[1]]++
    }
    // 因为返回最后出现给定二维数组的答案，所以需要倒着遍历
    // 入度为2
    for (let i = len - 1; i >= 0; i--) {
        if (degree[edges[i][1]] == 2) {
            if (helper(edges, i)) return edges[i]
        }
    }
    // 入度为1，有环，删掉属于环的连线中在数组里最靠左的那条
    for (let i = len - 1; i >= 0; i--) {
        if (degree[edges[i][1]] == 1) { // 可以不判断，题意说明入度最少为1（除了根节点）
            if (helper(edges, i)) return edges[i]
        }
    }
    return []
};

var helper = function (edges, e) {
    for (let i = 1; i <= edges.length; i++) {
        father[i] = i
    }
    let count = edges.length // 根据有根树定义，n个节点有n-1，现在多一条，所以完整连起来且
    for (let i = 0; i < edges.length; i++) {
        if (i == e) continue // 跳过，相当于去除此条线
        let dx = find(edges[i][0])
        let dy = find(edges[i][1])
        if (dx != dy) {
            --count
            father[dy] = dx // 归于dx之后，因为dx是连到dy，dx的根节点==dy的根节点
        }
    }
    return count == 1
}

console.log(findRedundantDirectedConnection([[1, 3], [2, 3], [1, 2]]))