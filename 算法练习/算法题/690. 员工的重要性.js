/**
 * Definition for Employee.
 * function Employee(id, importance, subordinates) {
 *     this.id = id;
 *     this.importance = importance;
 *     this.subordinates = subordinates;
 * }
 */

/**
 * @param {Employee[]} employees
 * @param {number} id
 * @return {number}
 */
var GetImportance = function (employees, id) {
    for (let e of employees) {
        if (e.id == id) {
            if (e.subordinates.length == 0) {
                return e.importance
            }
            for (let subId of e.subordinates) {
                e.importance += GetImportance(employees, subId)
            }
            return e.importance
        }
    }
    return 0
};

function Employee(id, importance, subordinates) {
    this.id = id;
    this.importance = importance;
    this.subordinates = subordinates;
}