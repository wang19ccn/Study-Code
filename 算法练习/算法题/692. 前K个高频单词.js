/**
 * @param {string[]} words
 * @param {number} k
 * @return {string[]}
 */
var topKFrequent = function (words, k) {
    let map = new Map();

    for (let w of words) {
        if (!map.has(w)) {
            map.set(w, 1);
        } else {
            map.set(w, map.get(w) + 1);
        }
    }

    let set = new Set(words);
    let arr = Array.from(set);

    arr.sort((a, b) => {
        return map.get(a) == map.get(b) ? a.localeCompare(b) : map.get(b) - map.get(a);
    })

    return arr.slice(0, k);
};

console.log(topKFrequent(["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"],4))