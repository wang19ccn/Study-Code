/**
 * @param {number[][]} grid
 * @return {number}
 */
var maxAreaOfIsland = function (grid) {
    let dx = [0, 1, 0, -1]
    let dy = [1, 0, -1, 0]

    let m = grid.length
    let n = grid[0].length

    var isborder = function (x, y) {
        return x >= 0 && x < m && y >= 0 && y < n
    }

    let max = 0
    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            let sum = 0
            if (grid[i][j] == 1) {
                let queue = [[i, j]]
                while (queue.length) {
                    let cur = queue.shift()
                    let x = cur[0]
                    let y = cur[1]
                    if (!isborder(x, y) || grid[x][y] == 0) continue
                    ++sum
                    grid[x][y] = 0
                    for (let i = 0; i < 4; i++) {
                        queue.push([x + dx[i], y + dy[i]])
                    }
                }
                grid[i][j] = 0
            }
            max = Math.max(max, sum)
        }
    }

    return max
};

console.log(maxAreaOfIsland([[0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 1, 1],
    [0, 0, 0, 1, 1]]))