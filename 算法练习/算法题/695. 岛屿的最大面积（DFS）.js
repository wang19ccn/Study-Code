/**
 * @param {number[][]} grid
 * @return {number}
 */
var maxAreaOfIsland = function (grid) {
    let m = grid.length
    let n = grid[0].length

    var isborder = function (x, y) {
        return x >= 0 && x < m && y >= 0 && y < n
    }

    var dfs = function (i, j) {
        if (!isborder(i, j) || grid[i][j] == 0) return 0
        grid[i][j] = 0
        let sum = 1
        return sum + dfs(i - 1, j) + dfs(i + 1, j) + dfs(i, j - 1) + dfs(i, j + 1)
    }

    let max = 0
    for (let i = 0; i < m; i++) {
        for (let j = 0; j < n; j++) {
            if (grid[i][j] == 1) {
                max = Math.max(max, dfs(i, j))
            }
        }
    }

    return max
};

// console.log(maxAreaOfIsland([[1, 1, 0, 0, 0], [1, 1, 0, 0, 0], [0, 0, 0, 1, 1], [0, 0, 0, 1, 1]]))
console.log(maxAreaOfIsland([[0, 0, 0, 0, 0],
[0, 0, 0, 0, 0],
[0, 0, 0, 1, 1],
[0, 0, 0, 1, 1]]))