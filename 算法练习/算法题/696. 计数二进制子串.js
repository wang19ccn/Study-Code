/**
 * @param {string} s
 * @return {number}
 */
var countBinarySubstrings = function (s) {
    let n = 0
    let pre = 0 // 统计之前相同字符数
    let cur = 1 // 统计现在相同字符数
    for (let i = 0; i < s.length - 1; ++i) {
        if (s[i] == s[i + 1]) {
            ++cur
        } else {
            pre = cur
            cur = 1
        }
        if (pre >= cur) ++n
    }
    return n
};