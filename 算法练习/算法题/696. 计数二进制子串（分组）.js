/**
 * @param {string} s
 * @return {number}
 */
var countBinarySubstrings = function (s) {
    let sum = 0
    let pre = 0
    let ans = 0
    let len = s.length
    while (sum < len) {
        let c = s[sum]
        let count = 0
        while (sum < len && s[sum] == c) {
            ++sum
            ++count
        }
        ans += Math.min(count, pre)
        pre = count
    }
    return ans
};