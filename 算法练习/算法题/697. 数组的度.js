/**
 * @param {number[]} nums
 * @return {number}
 */
var findShortestSubArray = function (nums) {
    let map = new Map() // 计算 度 的哈希表
    let map1 = new Map() // 储存出现的位置

    let maxDegree = 0
    for (let i = 0; i < nums.length; i++) {
        if (!map.has(nums[i])) {
            map.set(nums[i], 1)
            map1.set(nums[i], [i])
        } else {
            map.set(nums[i], map.get(nums[i]) + 1)
            map1.get(nums[i]).push(i)
        }
        maxDegree = Math.max(maxDegree, map.get(nums[i]))
    }

    let minRes = Infinity
    for (let item of map1.values()) {
        if (item.length == maxDegree) {
            minRes = Math.min(minRes, item[item.length - 1] - item[0] + 1)
        }
    }

    return minRes
};

console.log(findShortestSubArray([1, 2, 2, 1, 2, 1, 1, 1, 1, 2, 2, 2]))