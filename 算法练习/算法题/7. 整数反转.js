/**
 * @param {number} x
 * @return {number}
 */
var reverse = function (x) {
    //方法一
    //let num=Math.abs(x).toString().split("").reverse().join("")

    //方法二
    let init = Math.abs(x)
    let num = 0
    while (init > 0) {
        num = num * 10 + init % 10
        init = Math.floor(init / 10)
    }

    if (x < 0) {
        return num <= Math.pow(2, 31) ? -num : 0
    } else {
        return num < Math.pow(2, 31) ? num : 0
    }

};