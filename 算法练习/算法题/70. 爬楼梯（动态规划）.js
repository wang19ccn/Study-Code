/**
 * @param {number} n
 * @return {number}
 */
var climbStairs = function (n) {
    let dp = [];
    //初始化数组
    for (let i = 0; i <= n; i++) {
        dp[i] = 0
    }
    dp[0] = 1;
    dp[1] = 1;
    for (let i = 2; i <= n; i++) {
        dp[i] = dp[i - 1] + dp[i - 2];
    }
    return dp[n]
};

console.log(climbStairs(10))