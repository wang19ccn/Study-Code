/**
 * @param {number} k
 * @param {number[]} nums
 */
var KthLargest = function (k, nums) {
    this.new_k = k
    this.new_nums = nums.sort((a, b) => a - b)
};

/** 
 * @param {number} val
 * @return {number}
 */
KthLargest.prototype.add = function (val) {
    this.new_nums.push(val)
    this.new_nums.sort((a, b) => a - b)
    return this.new_nums[this.new_nums.length - this.new_k]
};

/**
 * Your KthLargest object will be instantiated and called as such:
 * var obj = new KthLargest(k, nums)
 * var param_1 = obj.add(val)
 */