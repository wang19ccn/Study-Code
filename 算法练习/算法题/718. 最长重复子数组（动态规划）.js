/**
 * @param {number[]} A
 * @param {number[]} B
 * @return {number}
 */
var findLength = function (A, B) {
    let a_length = A.length + 1
    let b_length = B.length + 1

    let res = 0
    let dp = new Array(a_length)
    for (let k = 0; k < a_length; k++) {
        dp[k] = new Array(b_length).fill(0)
    }
    for (let i = 1; i < a_length; i++) {
        for (let j = 1; j < b_length; j++) {
            if (A[i - 1] == B[j - 1]) {
                dp[i][j] = dp[i - 1][j - 1] + 1
            }
            if (dp[i][j] > res) res = dp[i][j] // 不用max，减少赋值操作
        }
    }
    return res

};