/**
 * @param {string} word1
 * @param {string} word2
 * @return {number}
 */
var minDistance = function (word1, word2) {
    let word1arr = word1.split('');
    let word2arr = word2.split('');
    let dp = [];

    //初始化
    for (let i = 0; i <= word1arr.length; i++) {
        dp[i]=[];
        for (let j = 0; j <= word2arr.length; j++) {
            dp[i][j] = 0;
        }
    }
    for (let i = 0; i < word1arr.length + 1;i++) {
        dp[i][0] = i
    }
    for (let j = 0; j < word2arr.length + 1;j++) {
        dp[0][j] = j
    }

    //状态转移
    //i==>word1 对应位置index
    //j==>word2 对应位置index
    for (let i = 1; i < word1arr.length + 1;i++) {
        for (let j = 1; j < word2arr.length + 1;j++) {
            if(word1[i-1]==word2[j-1]){
                dp[i][j]=dp[i-1][j-1]
            }else{
                dp[i][j]=Math.min(dp[i-1][j-1],dp[i][j-1],dp[i-1][j])+1;
            }
        }
    }

    //console.log(dp)

    return dp[word1arr.length][word2arr.length]
};

console.log(minDistance("horse", "ros"))