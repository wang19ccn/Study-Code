/**
 * @param {number[]} nums
 * @return {number}
 */
var pivotIndex = function (nums) {
    if (0 == nums.length) return -1

    let leftSum = 0
    let rightSum = 0
    for (let i = 1; i < nums.length; i++) {
        rightSum += nums[i]
    }

    //中心索引在0上  
    //[-1,-1,-1,0,1,1]的测试用例结果为0
    if (0 == rightSum) return 0

    //中心索引不在0上(包含尾)
    for (let i = 0; i < nums.length - 1; i++) {
        leftSum += nums[i]
        rightSum -= nums[i + 1]
        if (leftSum == rightSum) {
            return i + 1
        }
    }

    return -1
};

console.log(pivotIndex([-1, -1, 0, 1, 1, 1]))