/**
 * @param {string} formula
 * @return {string}
 */
var countOfAtoms = function (formula) {
    // 1. 利用正则，区分原子名称
    let newFormula = formula.match(/[A-Z][a-z]|[A-Z]|[0-9]+|\(|\)/g)
    // console.log(newFormula)

    // 2. 栈，存[原子名称，数量]
    // 存在['(' , 1],会在后续操作中作个循环判断并删除
    let stack = []
    for (let i = 0; i < newFormula.length; i++) {
        if (newFormula[i] != ')') {
            if (!isNaN(newFormula[i + 1])) {
                stack.push([newFormula[i], Number(newFormula[i + 1])])
                i++
            } else {
                stack.push([newFormula[i], 1])
            }
        } else {
            let add = 1
            if (!isNaN(newFormula[i + 1])) {
                add = Number(newFormula[i + 1])
                i++
            }
            let temp = []
            let flag = ""
            // 利用 ['(' , 1] 来终止循环
            while (flag != "(") {
                arr = stack.pop()
                flag = arr[0]
                if (flag != "(") { temp.push([arr[0], arr[1] * add]) }
            }
            for (let a of temp) {
                stack.push(a)
            }
        }
    }

    // 3. 结果需按照字典序排序
    stack.sort()

    // 4. 利用哈希表合并重复的原子
    let map = new Map()
    for (let i = 0; i < stack.length; i++) {
        if (map.has(stack[i][0])) {
            map.set(stack[i][0], map.get(stack[i][0]) + stack[i][1])
        } else {
            map.set(stack[i][0], stack[i][1])
        }
    }

    // 5. 拼成结果
    let res = []
    map.forEach((v, k) => {
        if (v > 1) {
            res.push(k + v)
        } else {
            res.push(k)
        }
    })

    return res.join('')
};

console.log(countOfAtoms("Mg(OH)2"))
console.log(countOfAtoms("K4(ON(SO3)2)2"))
console.log(countOfAtoms("Be32"))