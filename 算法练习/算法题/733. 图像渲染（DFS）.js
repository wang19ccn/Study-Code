/**
 * @param {number[][]} image
 * @param {number} sr
 * @param {number} sc
 * @param {number} newColor
 * @return {number[][]}
 */
var floodFill = function (image, sr, sc, newColor) {
    let initColor = image[sr][sc]
    if (initColor == newColor) return image

    let dx = [-1, 1, 0, 0]
    let dy = [0, 0, 1, -1]
    let n = image.length
    let m = image[0].length

    image[sr][sc] = newColor
    var dfs = function (x, y) {
        for (let i = 0; i < 4; ++i) {
            let new_x = x + dx[i]
            let new_y = y + dy[i]
            if (new_x >= 0 && new_x < n && new_y >= 0 && new_y < m && image[new_x][new_y] == initColor) {
                image[new_x][new_y] = newColor
                dfs(new_x, new_y)
            }
        }
    }

    dfs(sr, sc)
    return image
};