/**
 * @param {number} N
 * @return {number}
 */
var monotoneIncreasingDigits = function (N) {
    const strN = N.toString().split('')
    let flag = strN.length
    for (let i = strN.length - 1; i > 0; i--) {
        if (strN[i - 1] > strN[i]) {
            flag = i
            strN[i - 1]--
        }
    }
    for (let i = flag; i < strN.length; i++) {
        strN[i] = '9'
    }
    return parseInt(strN.join(''))
};

console.log(monotoneIncreasingDigits(10))