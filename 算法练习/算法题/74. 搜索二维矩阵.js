/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function (matrix, target) {
    let up = 0
    let down = matrix.length - 1
    while (up < down) {
        // 注：此处要向上取整
        let mid = parseInt((down - up + 1) / 2 + up)
        if (matrix[mid][0] <= target) {
            up = mid
        } else {
            down = mid - 1
        }
    }

    let left = 0
    let right = matrix[0].length - 1
    while (left <= right) {
        let mid = parseInt((right - left) / 2 + left)
        if (matrix[up][mid] < target) {
            left = mid + 1
        } else if (matrix[up][mid] > target) {
            right = mid - 1
        } else {
            return true
        }
    }

    return false
};

console.log(searchMatrix([[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 60]], 13))