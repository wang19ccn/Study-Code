/**
 * @param {number[]} cost
 * @return {number}
 */
var minCostClimbingStairs = function (cost) {
    let sum = new Array(cost.length).fill(0)
    sum[0] = cost[0]
    sum[1] = cost[1]
    for (let i = 2; i < cost.length; i++) {
        sum[i] = Math.min(cost[i] + sum[i - 1], cost[i] + sum[i - 2])
    }
    return Math.min(sum[cost.length - 2], sum[cost.length - 1])
};

console.log(minCostClimbingStairs([1, 100, 1, 1, 1, 100, 1, 1, 100, 1, 100]))