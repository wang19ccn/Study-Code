/**
 * @param {number[]} cost
 * @return {number}
 */
var minCostClimbingStairs = function (cost) {
    let n = cost.length
    let pre_pre = cost[0]
    let pre = cost[1]
    for (let i = 2; i < n; i++) {
        let next = Math.min(cost[i] + pre, cost[i] + pre_pre)
        pre_pre = pre
        pre = next
    }
    return Math.min(pre, pre_pre)
}

console.log(minCostClimbingStairs([1, 100, 1, 1, 1, 100, 1, 1, 100, 1]))