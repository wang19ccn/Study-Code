/**
 * @param {number[]} nums
 * @return {number}
 * 每个 nums[i] 的整数范围在 [0, 100]
 */
var dominantIndex = function (nums) {
    if (nums.length == 0) return -1

    let firstMax = nums[0]
    let secondMax = 0
    let index = 0

    for (let i = 1; i < nums.length; i++) {
        if (firstMax < nums[i]) {
            secondMax = firstMax
            firstMax = nums[i]
            index = i
        }
        if (secondMax < nums[i] && firstMax > nums[i]) {
            secondMax = nums[i]
        }
    }

    if (firstMax / secondMax >= 2) return index

    return -1
};

console.log(dominantIndex([0, 0, 3, 2]))