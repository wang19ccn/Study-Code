/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var sortColors = function (nums) {
    let len = nums.length
    if (len < 2) return
    let zero = 0
    let two = len
    let i = 0
    while (i < two) {
        if (nums[i] == 0) {
            swap(nums, i, zero)
            zero++
            i++
        }
        else if (nums[i] == 1) {
            i++
        }
        else {
            two--
            swap(nums, i, two)
        }
    }
};

var swap = function (nums, x, y) {
    let temp = nums[x]
    nums[x] = nums[y]
    nums[y] = temp
}