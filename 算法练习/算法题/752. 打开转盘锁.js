/**
 * @param {string[]} deadends
 * @param {string} target
 * @return {number}
 */
var openLock = function (deadends, target) {
    // 向上拨动
    var plusOne = function (s, j) {
        let arr = s.split('')
        if (arr[j] == '9') {
            arr[j] = '0'
        } else {
            arr[j] = parseInt(arr[j]) + 1
        }
        return arr.join('')
    }
    // 向下拨动
    var minusOne = function (s, j) {
        let arr = s.split('')
        if (arr[j] == '0') {
            arr[j] = '9'
        } else {
            arr[j] = parseInt(arr[j]) - 1
        }
        return arr.join('')
    }
    // BFS
    var bfs = function () {
        // 记录死亡密码
        let deads = new Set()
        for (let d of deadends) deads.add(d)
        // 记录已经试过的密码
        let visited = new Set()
        // 初始化
        let step = 0
        let queue = []
        queue.push("0000")
        visited.add("0000")

        while (0 != queue.length) {
            let len = queue.length
            for (let i = 0; i < len; i++) {
                let cur = queue.shift()

                if (deads.has(cur)) continue
                if (cur == target) return step

                for (let j = 0; j < 4; j++) {
                    let up = plusOne(cur, j)
                    if (!visited.has(up)) {
                        queue.push(up)
                        visited.add(up)
                    }
                    let down = minusOne(cur, j)
                    if (!visited.has(down)) {
                        queue.push(down)
                        visited.add(down)
                    }
                }
            }
            ++step
        }
        return -1
    }

    return bfs()
};

console.log(openLock(["0201", "0101", "0102", "1212", "2002"], "0202"))
console.log(openLock(["8887", "8889", "8878", "8898", "8788", "8988", "7888", "9888"], "8888"))