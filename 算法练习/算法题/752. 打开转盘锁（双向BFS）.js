/**
 * @param {string[]} deadends
 * @param {string} target
 * @return {number}
 */
var openLock = function (deadends, target) {
    // 向上拨动
    var plusOne = function (s, j) {
        let arr = s.split('')
        if (arr[j] == '9') {
            arr[j] = '0'
        } else {
            arr[j] = parseInt(arr[j]) + 1
        }
        return arr.join('')
    }
    // 向下拨动
    var minusOne = function (s, j) {
        let arr = s.split('')
        if (arr[j] == '0') {
            arr[j] = '9'
        } else {
            arr[j] = parseInt(arr[j]) - 1
        }
        return arr.join('')
    }
    // BFS
    var bfs = function () {
        // 记录死亡密码
        let deads = new Set()
        for (let d of deadends) deads.add(d)
        // 记录已经试过的密码
        let visited = new Set()
        // 初始化
        let step = 0
        let queue1 = new Set()
        let queue2 = new Set()
        queue1.add("0000")
        queue2.add(target)

        while (0 != queue1.size && 0 != queue2.size) {
            if (queue1.length > queue2.length) {
                // 交换 q1 和 q2
                temp = queue1;
                queue1 = queue2;
                queue2 = temp;
            }

            let temp = new Set()
            for (let cur of queue1) {
                if (deads.has(cur)) continue
                if (queue2.has(cur)) return step
                visited.add(cur)
                for (let j = 0; j < 4; j++) {
                    let up = plusOne(cur, j)
                    if (!visited.has(up)) {
                        temp.add(up)
                    }
                    let down = minusOne(cur, j)
                    if (!visited.has(down)) {
                        temp.add(down)
                    }
                }
            }
            ++step
            queue1 = queue2
            queue2 = temp
        }

        return -1
    }

    return bfs()
};

console.log(openLock(["8887", "8889", "8878", "8898", "8788", "8988", "7888", "9888"], "8888"))