/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
var minWindow = function (s, t) {
    let sLen = s.length
    let tLen = t.length
    if (sLen == 0 || tLen == 0 || sLen < tLen) return ""

    let sArr = s.split('')
    let tArr = t.split('')

    let winFreq = new Array(128).fill(0) // 滑动窗口中，对应T中各个字符的频率
    let tFreq = new Array(128).fill(0) // T中各个字符的频率
    for (let c of tArr) {
        tFreq[c.charCodeAt()]++ 
    }

    // 滑动窗口内部包含多少T中的字符，对应字符频数超过不重复计算
    let distance = 0
    let minLen = sLen + 1
    let begin = 0

    let left = 0
    let right = 0
    //[left,right)
    while (right < sLen) {
        let rightIndex = sArr[right].charCodeAt()
        if (tFreq[rightIndex] == 0) {
            right++
            continue
        }
        if (winFreq[rightIndex] < tFreq[rightIndex]) {
            distance++
        }
        winFreq[rightIndex]++
        right++
        while (distance == tLen) {
            let leftIndex = sArr[left].charCodeAt()
            if (right - left < minLen) {
                minLen = right - left;
                begin = left;
            }
            if (tFreq[leftIndex] == 0) {
                left++
                continue
            }
            if (winFreq[leftIndex] == tFreq[leftIndex]) {
                distance--
            }
            winFreq[leftIndex]--
            left++
        }
    }
    if (minLen == sLen + 1) {
        return "";
    }
    return s.substring(begin, begin + minLen)
};

console.log(minWindow("ADOBECODEBANC", "ABC"))