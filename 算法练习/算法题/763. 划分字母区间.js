/**
 * @param {string} S
 * @return {number[]}
 */
var partitionLabels = function (S) {
    let last = new Array(26)
    let length = S.length
    let codePointA = 'a'.codePointAt(0)
    for (let i = 0; i < length; i++) {
        last[S.codePointAt(i) - codePointA] = i
    }
    let res = []
    let start = 0
    let end = 0
    for (let i = 0; i < length; i++) {
        end = Math.max(end, last[S.codePointAt(i) - codePointA])
        if (i == end) {
            res.push(end - start + 1)
            start = end + 1
        }
    }
    return res
};