/**
 * @param {string} S
 * @return {number[]}
 */
var partitionLabels = function (S) {
    let last = new Map()
    let length = S.length
    for (let i = 0; i < length; i++) {
        last.set(S[i], i)
    }
    let res = []
    let start = 0
    let end = 0
    for (let i = 0; i < length; i++) {
        end = Math.max(end, last.get(S[i]))
        if (i == end) {
            res.push(end - start + 1)
            start = end + 1
        }
    }
    return res
};