/** 
 * 回溯算法 + 剪枝
 * @param {number} n
 * @param {number} k
 * @return {number[][]}
 */
var combine = function (n, k) {
    let res = new Array()
    if (k <= 0 || n < k) {
        return res
    }
    let path = new Array()
    dfs(n, k, 1, path, res)
    return res
};

var dfs = function (n, k, begin, path, res) {
    if (path.length == k) {
        res.push([...path])
        return
    }
    // 剪枝，i <= n - (k - path.length) + 1
    for (let i = begin; i <= n - (k - path.length) + 1; i++) {
        path.push(i)
        dfs(n, k, i + 1, path, res)
        path.pop()
    }
}

console.log(combine(4, 2))