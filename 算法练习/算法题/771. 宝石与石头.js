/**
 * @param {string} J
 * @param {string} S
 * @return {number}
 */
var numJewelsInStones = function (J, S) {
    let map = new Map()
    for (let j of J) {
        map.set(j, '')
    }

    let count = 0
    for (let s of S) {
        if (map.has(s)) {
            count++
        }
    }
    return count
};