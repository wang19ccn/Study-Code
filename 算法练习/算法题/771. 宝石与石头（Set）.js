/**
 * @param {string} J
 * @param {string} S
 * @return {number}
 */
var numJewelsInStones = function (J, S) {
    let set = new Set(J.split(''))
    return S.split('').reduce((acc, val) => {
        return acc + set.has(val)
    }, 0)
};