/**
 * 回溯算法
 * @param {number[]} nums
 * @return {number[][]}
 */
var subsets = function (nums) {
    if (nums.length == 0) return []

    let res = new Array()
    let path = new Array()

    dfs(nums, 0, path, res)
    return res
};

var dfs = function (nums, start, path, res) {
    res.push([...path])

    if (start == nums.length) {
        return
    }

    for (let i = start; i < nums.length; i++) {
        path.push(nums[i])
        dfs(nums, i + 1, path, res)
        path.pop()
    }
}

console.log(subsets([1, 2, 3]))