/**
 * @param {number[][]} graph
 * @return {boolean}
 */
var isBipartite = function (graph) {
    let visited = new Array(graph.length)
    for (let i = 0; i < graph.length; i++) {
        if (visited[i]) continue
        let queue = [i]
        visited[i] = 1
        while (queue.length) {
            let cur = queue.shift()
            let curColor = visited[cur]
            let neighborColor = -curColor
            for (let i = 0; i < graph[cur].length; i++) {
                let neighbor = graph[cur][i]
                if (visited[neighbor] == undefined) {
                    visited[neighbor] = neighborColor;
                    queue.push(neighbor)
                } else if (visited[neighbor] != neighborColor) {
                    return false
                }
            }
        }
    }
    return true
};