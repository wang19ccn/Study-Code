/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function (board, word) {
    if (board.length == 0) return false

    let dx = [-1, 0, 1, 0]
    let dy = [0, -1, 0, 1]

    let m = board.length
    let n = board[0].length

    let marked = new Array(m)

    var isExist = function () {
        for (let i = 0; i < m; i++) {
            marked[i] = new Array(n).fill(false)
        }

        for (let i = 0; i < m; i++) {
            for (let j = 0; j < n; j++) {
                if (dfs(i, j, 0)) {
                    return true
                }
            }
        }
        return false
    }

    var dfs = function (i, j, start) {
        if (start == word.length - 1) {
            return board[i][j] == word.charAt(start)
        }
        if (board[i][j] == word.charAt(start)) {
            marked[i][j] = true
            for (let k = 0; k < 4; k++) {
                let new_x = i + dx[k]
                let new_y = j + dy[k]
                if (isBorder(new_x, new_y) && !marked[new_x][new_y]) {
                    if (dfs(new_x, new_y, start + 1)) {
                        return true
                    }
                }
            }
            marked[i][j] = false
        }
        return false
    }

    var isBorder = function (x, y) {
        return x >= 0 && x < m && y >= 0 && y < n
    }

    return isExist()
};

