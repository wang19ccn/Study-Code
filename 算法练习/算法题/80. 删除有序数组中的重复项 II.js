/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function (nums) {
    // if (nums.length <= 2) return nums.length;
    // s为slow指针 f为fast指针
    let s = 2//既代表索引(慢指针) 也代表最后记录的长度
    // 以1开始 是因为 nums一定有值 经过前端筛选之后 最后输出长度最少是2
    for (let f = 2; f < nums.length; f++) {//循环索引相当于快指针
        if (nums[s - 2] !== nums[f]) {
            nums[s] = nums[f]//原地修改
            s++//输出长度+1
        }
        // 相等的时候只移动快指针，即循环的索引
    }
    return s
};