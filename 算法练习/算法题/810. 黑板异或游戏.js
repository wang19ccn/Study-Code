/**
 * @param {number[]} nums
 * @return {boolean}
 */
var xorGame = function (nums) {
    let sumXOR = 0;
    for (let n of nums) {
        sumXOR ^= n
    }
    return sumXOR == 0 || nums.length % 2 == 0
};