/**
 * @param {string[]} words
 * @return {number}
 */
var minimumLengthEncoding = function(words) {
    let set=new Set(words);
    console.log(set);
    for(let word of set){   
        for(let i=1;i<word.length;i++){
            let target= word.slice(i);
            //console.log(target);
            set.has(target)&&set.delete(target);
        }
    }
    let result=0;
    set.forEach(item=>result+=item.length+1);
    return result;
};

minimumLengthEncoding(["time", "me", "bell",'e','i'])