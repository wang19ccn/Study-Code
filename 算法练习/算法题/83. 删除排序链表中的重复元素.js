/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var deleteDuplicates = function (head) {
    let init = head
    while (init != null && init.next != null) {
        if (init.next.val == init.val) {
            init.next = init.next.next
        } else {
            init = init.next
        }
    }
    return head
};