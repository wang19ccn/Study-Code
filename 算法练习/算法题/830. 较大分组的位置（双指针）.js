/**
 * @param {string} s
 * @return {number[][]}
 */
var largeGroupPositions = function (s) {
    let res = []
    let start = 0
    let end = 1
    while (start < s.length) {
        while (s[end - 1] == s[end]) {
            end++
        }
        if (end - start > 2) res.push([start, end - 1])
        start = end
        end++
    }
    return res
};