/**
 * @param {number[][]} A
 * @return {number[][]}
 */
var flipAndInvertImage = function (A) {
    for (let row of A) {
        row.reverse()
        for (let i = 0; i < row.length; i++) {
            row[i] = row[i] ^ 1
        }
    }
    return A
};