/**
 * @param {number} N
 * @param {number[][]} edges
 * @return {number[]}
 */
var sumOfDistancesInTree = function (N, edges) {
    // 建立映射表
    let graph = new Array(N)
    for (let i = 0; i < graph.length; ++i) {
        graph[i] = []
    }
    for (let edge of edges) {
        let [from, to] = edge
        graph[from].push(to)
        graph[to].push(from)
    }

    let distSum = new Array(N).fill(0)
    let nodeNum = new Array(N).fill(1)

    let postOrder = function (root, parent) {
        let neighbors = graph[root]
        for (let neighbor of neighbors) {
            if (neighbor == parent) {
                continue
            }
            postOrder(neighbor, root)
            nodeNum[root] += nodeNum[neighbor]
            distSum[root] += nodeNum[neighbor] + distSum[neighbor]
        }
    }

    let preOrder = function (root, parent) {
        let neighbors = graph[root]
        for (let neighbor of neighbors) {
            if (neighbor == parent) {
                continue
            }
            distSum[neighbor] = distSum[root] - nodeNum[neighbor] + (N - nodeNum[neighbor])
            preOrder(neighbor, root)
        }
    }

    postOrder(0, -1)
    console.log(distSum)
    preOrder(0, -1)
    console.log(distSum)
    return distSum
};