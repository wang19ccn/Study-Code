/**
 * @param {number} N
 * @param {number} K
 * @param {number} W
 * @return {number}
 */
var new21Game = function (N, K, W) {
    if (0 == K) return 1.0
    let temp = 0
    let dp = new Array(K + W).fill(0)
    for (let i = K; i <= N && i < K + W; i++) {
        dp[i] = 1.0
        temp += dp[i]
    }
    for (let i = K - 1; i > -1; i--) {
        dp[i] = temp / W
        temp = temp - dp[i + W] + dp[[i]]
    }
    return dp[0]
};

console.log(new21Game(21, 17, 10))