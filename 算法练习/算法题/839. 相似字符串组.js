// /**
//  * @param {string[]} strs
//  * @return {number}
//  */
// var numSimilarGroups = function (strs) {
//     const n = strs.length;
//     const m = strs[0].length;
//     const f = new Array(n).fill(0).map((element, index) => index);

//     for (let i = 0; i < n; i++) {
//         for (let j = i + 1; j < n; j++) {
//             const fi = find(i), fj = find(j);
//             if (fi === fj) {
//                 continue;
//             }
//             if (check(strs[i], strs[j], m)) {
//                 f[fi] = fj;
//             }
//         }
//     }
//     let ret = 0;
//     for (let i = 0; i < n; i++) {
//         if (f[i] === i) {
//             ret++;
//         }
//     }
//     return ret;
// };

// class UnionFind {
//     /**
//      * 初始化并查集，并使其顶点指向-1
//      * @param {number} num 顶点个数 
//      */
//     constructor(num) {
//         this.roots = new Array(num).fill(-1)
//         this.group = num
//     }

//     /**
//      * 查找顶点x的根节点
//      * @param {number} x 
//      */
//     findRoot(x) {
//         let x_root = x
//         while (this.roots[x_root] != -1) {
//             x_root = this.roots[x_root]
//         }
//         return x_root
//     }

//     /**
//      * 合并顶点x和顶点y所在的集合
//      * @param {number} x 
//      * @param {number} y 
//      */
//     union(x, y) {
//         // 查找各自根节点
//         let x_root = this.findRoot(x)
//         let y_root = this.findRoot(y)
//         // 如果根节点相同，说明已经在一个集合内
//         if (x_root != y_root) {
//             this.group--
//             this.roots[x_root] = y_root
//         }
//     }

//     check(a, b, len) {
//         let num = 0;
//         for (let i = 0; i < len; i++) {
//             if (a[i] !== b[i]) {
//                 num++;
//                 if (num > 2) {
//                     return false;
//                 }
//             }
//         }
//         return true;
//     }
// }


var numSimilarGroups = function (strs) {
    const n = strs.length;
    const m = strs[0].length;
    const f = new Array(n).fill(0).map((element, index) => index);

    for (let i = 0; i < n; i++) {
        for (let j = i + 1; j < n; j++) {
            const fi = find(i), fj = find(j);
            if (fi === fj) {
                continue;
            }
            if (check(strs[i], strs[j], m)) {
                f[fi] = fj;
            }
        }
    }
    let ret = 0;
    for (let i = 0; i < n; i++) {
        if (f[i] === i) {
            ret++;
        }
    }
    return ret;

    function find(x) {
        return f[x] === x ? x : (f[x] = find(f[x]));
    }

    function check(a, b, len) {
        let num = 0;
        for (let i = 0; i < len; i++) {
            if (a[i] !== b[i]) {
                num++;
                if (num > 2) {
                    return false;
                }
            }
        }
        return true;
    }
};

// 作者：LeetCode-Solution
// 链接：https://leetcode-cn.com/problems/similar-string-groups/solution/xiang-si-zi-fu-chuan-zu-by-leetcode-solu-8jt9/
