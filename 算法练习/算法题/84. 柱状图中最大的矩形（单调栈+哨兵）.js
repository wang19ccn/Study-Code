/**
 * @param {number[]} heights
 * @return {number}
 */
var largestRectangleArea = function (heights) {
    let len = heights.length
    if (0 == len) return 0
    if (1 == len) return heights[0]

    let area = 0
    // 在原数组首和尾增加0（哨兵）
    // heights = [0, ...heights, 0]          // ...是JS的展开运算符
    let newHeights = new Array(len + 2).fill(0)
    for (let i = 0; i < len; i++) {
        newHeights[i + 1] = heights[i]
    }
    len += 2
    heights = newHeights

    let stack = []
    stack.push(0)
    for (let i = 1; i < len; i++) {
        while (heights[stack[stack.length - 1]] > heights[i]) {
            let height = heights[stack.pop()]
            let width = i - stack[stack.length - 1] - 1
            area = Math.max(area, width * height)
        }
        stack.push(i)
    }

    return area
};

console.log(largestRectangleArea([2, 1, 5, 6, 2, 3]))