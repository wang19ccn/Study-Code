/**
 * @param {number[]} heights
 * @return {number}
 */
var largestRectangleArea = function (heights) {
    let len = heights.length
    if (0 == len) return 0
    if (1 == len) return heights[0]

    let area = 0
    let stack = []
    for (let i = 0; i < len; i++) {
        while (stack.length != 0 && heights[stack[stack.length - 1]] > heights[i]) {
            let height = heights[stack.pop()]
            while (stack.length != 0 && heights[stack[stack.length - 1]] == height) {
                stack.pop()
            }
            let width
            if (stack.length == 0) {
                width = i
            } else {
                width = i - stack[stack.length - 1] - 1
            }

            area = Math.max(area, width * height)
        }
        stack.push(i)
    }
    while (stack.length != 0) {
        let height = heights[stack.pop()]
        while (stack.length != 0 && heights[stack[stack.length - 1]] == height) {
            stack.pop()
        }
        let width
        if (stack.length == 0) {
            width = len
        } else {
            width = len - stack[stack.length - 1] - 1
        }

        area = Math.max(area, width * height)
    }

    return area
};

console.log(largestRectangleArea([2,1,5,6,2,3]))