/**
 * @param {number[][]} rooms
 * @return {boolean}
 */
var canVisitAllRooms = function (rooms) {
    let visit = new Set([0])
    let queue = ([0])
    while (queue.length) {
        let nextRoom = rooms[queue.shift()]
        for (let num of nextRoom) {
            if (!visit.has(num)) {
                queue.push(num)
                visit.add(num)
            }
        }
    }
    return visit.size == rooms.length ? true : false
};

console.log(canVisitAllRooms([[1], [2], [3], []]))