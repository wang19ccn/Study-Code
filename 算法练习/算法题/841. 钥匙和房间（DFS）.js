/**
 * @param {number[][]} rooms
 * @return {boolean}
 */
var canVisitAllRooms = function (rooms) {
    let visit = new Set([0])

    var dfs = function (roomNum) {
        visit.add(roomNum)
        let nextRoom = rooms[roomNum]
        for (let num of nextRoom) {
            if (!visit.has(num)) {
                dfs(num)
            }
        }
    }

    dfs(0)
    return visit.size == rooms.length ? true : false
};



console.log(canVisitAllRooms([[1], [2], [3], []]))