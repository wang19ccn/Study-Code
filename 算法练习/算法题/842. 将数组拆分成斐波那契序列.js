/**
 * @param {string} S
 * @return {number[]}
 */
var splitIntoFibonacci = function (S) {
    // const max = 2 ** 31 - 1;
    const max = Math.pow(2, 31) - 1

    let res = [];
    let len = S.length;

    const dfs = (start, S, res) => {
        let size = res.length;
        if (start == len) { return size > 2 }
        let num = 0;
        for (let i = start; i < len; i++) {
            num = 10 * num + Number(S.charAt(i));
            if (num >= max || num < 0) return false;    //是否超出范围
            if (size < 2 || num == res[size - 1] + res[size - 2]) {
                res.push(num);
                if (dfs(i + 1, S, res)) {
                    return true;
                }
                res.pop();
            }
            //判断是否以0开头
            if (Number(S.charAt(i)) == 0 && i == start) return false;
        }
        return false;
    }

    return dfs(0, S, res) ? res : [];
}