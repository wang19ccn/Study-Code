/**
 * @param {string} S
 * @param {string} T
 * @return {boolean}
 */
var backspaceCompare = function (S, T) {
    let s_stack = []
    let t_stack = []
    for (let s of S) {
        if (s_stack.length != 0 && s == '#') {
            s_stack.pop()
        }
        if (s != '#') {
            s_stack.push(s)
        }
    }
    for (let t of T) {
        if (t_stack.length != 0 && t == '#') {
            t_stack.pop()
        }
        if (t != '#') {
            t_stack.push(t)
        }
    }
    return s_stack.join('') == t_stack.join('')
};