/**
 * @param {number[]} A
 * @return {number}
 */
var longestMountain = function (A) {
    let len = A.length
    let res = 0
    let left = 0
    while (left + 2 < len) {
        let right = left + 1
        // 保证右侧是山脚
        if (A[left] < A[left + 1]) {
            // 上升坡
            while (right + 1 < len && A[right] < A[right + 1]) {
                ++right
            }
            // 下降坡
            if (right < len - 1 && A[right] > A[right + 1]) {
                while (right + 1 < len && A[right] > A[right + 1]) {
                    ++right
                }
                res = Math.max(res, right - left + 1)
            }
            // 平路 
            else {
                ++right
            }
        }
        left = right
    }
    return res
};