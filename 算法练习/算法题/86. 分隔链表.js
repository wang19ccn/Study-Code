function ListNode(val) {
    this.val = val;
    this.next = null;
}
/**
 * @param {ListNode} head
 * @param {number} x
 * @return {ListNode}
 */
var partition = function (head, x) {
    // 创建两个链表
    let small = new ListNode(0)
    let large = new ListNode(0)
    let smallHead = small
    let largeHead = large

    // 将链表分裂
    while (head != null) {
        let temp = head.next
        if (head.val < x) {
            small.next = head
            small = small.next
        } else {
            large.next = head
            large = large.next
        }
        head.next = null
        head = temp
    }

    small.next = largeHead.next
    return smallHead.next
};