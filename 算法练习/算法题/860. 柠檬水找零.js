/**
 * @param {number[]} bills
 * @return {boolean}
 */
var lemonadeChange = function (bills) {
    let five = 0
    let ten = 0
    for (let bill of bills) {
        if (bill == 10) {
            if (five != 0) {
                five -= 1
                ten += 1
            } else {
                return false
            }
        }
        else if (bill == 20) {
            // 10只能给20找零，所以优先使用10
            if (five > 0 && ten > 0) {
                ten -= 1
                five -= 1
            }
            else if (five >= 3) {
                five -= 3
            }
            else {
                return false
            }
        }
        else {
            five++
        }
    }
    return true
};