/**
 * @param {number[][]} A
 * @return {number}
 */
var matrixScore = function (A) {
    let m = A.length
    let n = A[0].length
    for (let i = 0; i < m; i++) {
        if (A[i][0] == 0) {
            for (let j = 0; j < n; j++) {
                A[i][j] ^= 1
            }
        }
    }
    let result = 0
    for (let j = 0; j < n; j++) {
        let OneCount = 0
        for (let i = 0; i < m; i++) {
            OneCount += A[i][j]
        }
        // 1 << x  二进制1往左移动x位 相当于二进制中1在第x位的十进制 0 0 x 0 0 
        result += Math.max(OneCount, m - OneCount) * (1 << (n - j - 1));
    }
    return result
};
// [0, 0, 1, 1]
// [1, 0, 1, 0]
// [1, 1, 0, 0]

// [1, 1, 1, 1]
// [1, 0, 0, 1]
// [1, 1, 1, 1]