/**
 * @param {number} N
 * @return {number}
 */
var binaryGap = function (N) {
    let last = -1
    let maxLen = 0
    for (let i = 0; i < 32; ++i) {
        if (((N >> i) & 1) > 0) {
            if (last >= 0) {
                maxLen = Math.max(maxLen, i - last)
            }
            last = i
        }
    }
    return maxLen
};


// 程序中if( (i>>j)&1)是什么意思
// https://zhidao.baidu.com/question/330520142.html

// i先转换成而bai经指数，然后右移j位，du然后和1进行与运算
// 详解
// >> 右移运算符
// 假设zhi i=2，j=1
// ①
// i的值 i的二进制形dao式 i >> 1(即i的二进制形式右移1位)
// 2 0010 0001
// ②& 按位相与运算符
// 参加运算的两个数据，按二进位进行“与”运算。如果两个相应的二进位都为1，则该位的结果值为1，否则为0。即0&0=0；0&1=0；1&0=0；1&1=1
// 引用①的i>>1的结果
// 表达式 (i>>1)&1
// i>>1 0001
// 1 0001
// 结果 0001 即结果为 1(运算逻辑请参考②)
// 希望对你有帮助