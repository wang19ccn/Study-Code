/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root1
 * @param {TreeNode} root2
 * @return {boolean}
 */
var leafSimilar = function (root1, root2) {
    let res1 = []
    let res2 = []

    var dfs = function (root, res) {
        if (root.left == null && root.right == null) {
            res.push(root.val);
            return;
        }
        if (root.left) { dfs(root.left, res); }
        if (root.right) { dfs(root.right, res); }
    }

    dfs(root1, res1);
    dfs(root2, res2);

    return res1.join() == res2.join()
};