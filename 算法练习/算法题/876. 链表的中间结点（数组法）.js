/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var middleNode = function (head) {
    let res = []
    res.push(head)
    while (head) {
        head = head.next
        if (head) res.push(head)
    }
    return res[parseInt(res.length / 2)]
};