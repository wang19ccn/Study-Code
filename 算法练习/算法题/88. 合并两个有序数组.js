/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
    let tmp_nums1 = nums1.slice(0, m)
    let tmp_nums2 = nums2.slice(0, n)
    let sum = []

    let point1 = 0
    let point2 = 0
    for (let i = 0; i < (m + n); i++) {
        if (tmp_nums1[point1] > tmp_nums2[point2] && point2 < n) {
            sum.push(tmp_nums2[point2])
            point2++
        } else if (tmp_nums1[point1] <= tmp_nums2[point2] && point1 < m) {
            sum.push(tmp_nums1[point1])
            point1++
        } else {
            while (point1 < m) {
                sum.push(tmp_nums1[point1])
                point1++
            }
            while (point2 < n) {
                sum.push(tmp_nums2[point2])
                point2++
            }
        }
    }
    for (let i = 0; i < sum.length; i++) {
        nums1[i] = sum[i];
    }
};

console.log(merge([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3))