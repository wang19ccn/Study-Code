/** 没写完
 * @param {number} K
 * @param {number} N
 * @return {number}
 */
var superEggDrop = function(K, N) {
    let dp=[];
    for(let i=0;i<=N;i++){
        dp[i]=[];
        for(let j=0;j<=K;j++){
            dp[i][j]=Infinity;
        }
    }

    for(let j=0;j<=K;j++){
        dp[0][j]=0;
    }

    dp[1][0]=0;
    for(let j=1;j<=K;j++){
        dp[1][j]=1;
    }

    for (let i = 0; i <= N; i++) {
        dp[i][0] = 0;
        dp[i][1] = i;
    }

    // for(let i=2;i<=N;i++){
    //     for(let j=2;j<=K;j++){
    //         for(let k=1;k<=i;k++){
    //             dp[i][j] = Math.min(dp[i][j], Math.max(dp[k - 1][j - 1], dp[i - k][j]) + 1);
    //         }
    //     }
    // }
    return dp;
};

console.log(superEggDrop(8,10000))