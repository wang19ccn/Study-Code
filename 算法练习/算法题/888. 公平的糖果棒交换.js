/**
 * @param {number[]} A
 * @param {number[]} B
 * @return {number[]}
 */
var fairCandySwap = function (A, B) {
    let sumA = getSum(A)
    let sumB = getSum(B)

    let setA = new Set(A)
    let target = parseInt((sumA - sumB) / 2)

    let res = []
    for (let y of B) {
        let x = y + target
        if (setA.has(x)) {
            res = [x, y]
            break
        }
    }

    return res
};

var getSum = function (num) {
    let sum = 0
    for (let i = 0; i < num.length; i++) {
        sum += num[i]
    }
    return sum
}