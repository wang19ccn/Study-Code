var surfaceArea = function(grid) {

	let N = grid.length; //获取数组长度
	let cube_sum = 0; //方块总数
	let faces = 0; //重叠的面

	for (let i = 0; i < N; i++) { //遍历 确定行
		for (let j = 0; j < N; j++) { //遍历 确定列

			cube_sum += grid[i][j]; //将重叠的方块计算在一起

			if (!cube_sum) { //如果为零，跳出循环
				continue;
			}

			//垂直
			if (grid[i][j] > 0) {
				faces += grid[i][j] - 1;
			}

			//一行，左侧挨着的方块，减少的面
			if (i > 0) {
				faces += Math.min(grid[i - 1][j], grid[i][j]);
			}

			//一列，前方挨着的方块，减少的面
			if (j > 0) {
				faces += Math.min(grid[i][j - 1], grid[i][j]);
			}
		}
	}

	return 6 * cube_sum - 2 * faces;
};

surfaceArea([
	[2]
]);
