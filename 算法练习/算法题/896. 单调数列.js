/**
 * @param {number[]} A
 * @return {boolean}
 */
var isMonotonic = function (A) {
    let add = true
    let reduce = true
    const n = A.length
    for (let i = 0; i < n - 1; ++i) {
        if (A[i] > A[i + 1]) {
            add = false
        }
        if (A[i] < A[i + 1]) {
            reduce = false
        }
    }
    return add || reduce
};