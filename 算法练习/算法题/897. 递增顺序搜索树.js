/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var increasingBST = function (root) {
    let res = []
    dfs(root, res)

    let new_root = new TreeNode()
    let temp = new_root

    for (let value of res) {
        temp.right = new TreeNode(value)
        temp = temp.right
    }

    return new_root.right
};

var dfs = function (root, res) {
    if (!root) {
        return;
    }
    dfs(root.left, res);
    res.push(root.val);
    dfs(root.right, res);
}