/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function (x) {
    if (x < 0) return false
    let arr = []
    let i = 0
    while (0 != x) {
        arr[i++] = x % 10
        x = parseInt(x / 10)
    }
    let left = 0
    let right = arr.length - 1
    for (let i = 0; i < parseInt(arr.length / 2); i++) {
        if (arr[left++] != arr[right--]) return false
    }
    return true
};