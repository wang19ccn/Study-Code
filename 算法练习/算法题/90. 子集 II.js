/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var subsetsWithDup = function (nums) {
    if (nums.length == 0) return []

    let res = new Array()
    let path = new Array()

    nums.sort((a, b) => a - b)
    dfs(nums, 0, path, res)
    return res
};

var dfs = function (nums, start, path, res) {
    res.push([...path])
    for (let i = start; i < nums.length; i++) {
        if (i > start && nums[i] == nums[i - 1]) {
            continue
        }
        path.push(nums[i])
        dfs(nums, i + 1, path, res)
        path.pop()
    }
}

console.log(subsetsWithDup([1, 2, 2]))