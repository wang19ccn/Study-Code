/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortArray = function (nums) {
    if (nums.length < 2) return nums
    quickSort(nums, 0, nums.length - 1)
    return nums
};

var quickSort = function (nums, left, right) {
    if (left >= right) return
    let index = partition(nums, left, right)
    quickSort(nums, left, index - 1)
    quickSort(nums, index + 1, right)
}

var partition = function (nums, left, right) {
    let pivot = nums[left]
    let j = left
    for (let i = left + 1; i <= right; i++) {
        if (nums[i] < pivot) {
            j++
            [nums[i], nums[j]] = [nums[j], nums[i]]
        }
    }
    [nums[left], nums[j]] = [nums[j], nums[left]]
    return j
}

//console.log(sortArray([3,6,0,0,8,9,10]))
console.log(sortArray([-2, 3, -5]))