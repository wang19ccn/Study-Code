/**
 * @param {number[]} deck
 * @return {boolean}
 */
var hasGroupsSizeX = function (deck) {

    let count = [0];
    let x = 0;

    //初始化数组，定义数组类型
    for (let num in deck) {
        x = deck[num];
        count[x] = 0;
        console.log(count)
    }

    for (let num in deck) {
        x = deck[num];
        count[x] = count[x] + 1;
        console.log(count)
    }

    let y = 0;
    for (let cnt in count) {
        if (count[cnt] > 0) {
            y = gcd(y, count[cnt]);
            console.log(y)
            if (1 === y) {
                return false;
            }
        }
    }

    return y >= 2;

};

var gcd = function (a, b) {
    return b === 0 ? a : gcd(b, a % b)
};

//hasGroupsSizeX([1,2,3,4,4,3,2,1]);

hasGroupsSizeX([1,1,1,2,2,2,3,3,3,3,3,3]);