/**
 * @param {number[]} A
 * @return {number[]}
 */
var sortArrayByParityII = function (A) {
    let res = new Array(A.length)
    let i = 0
    for (let a of A) {
        if (a % 2 == 0) {
            res[i] = a
            i += 2
        }
    }
    i = 1
    for (let a of A) {
        if (a % 2 == 1) {
            res[i] = a
            i += 2
        }
    }
    return res
};