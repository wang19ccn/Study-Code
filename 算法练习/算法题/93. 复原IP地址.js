/**
 * @param {string} s
 * @return {string[]}
 */
var restoreIpAddresses = function (s) {
    let res = []
    let dfs = function (subRes, start) {
        // 满4，且字符串用尽
        if (subRes.length == 4 && start == s.length) {
            res.push(subRes.join('.'))
            return
        }
        // 满4，但字符串未用尽，不符合ip要求
        if (subRes.length == 4 && start < s.length) {
            return
        }
        // 切割字符串
        for (let len = 1; len <= 3; len++) {
            // 判断是否越界
            if (s[start + len - 1] == undefined) {
                return
            }
            // 大于1位时，不能以0开头
            if (len != 1 && s[start] == '0') return
            // 切割字符串
            let str = s.substring(start, start + len)
            // 不能超过255
            if (len == 3 && Number.parseInt(str) > 255) {
                return
            }
            subRes.push(str)
            dfs(subRes, start + len)
            subRes.pop()
        }
    }
    dfs([], 0)
    return res
};

console.log(restoreIpAddresses("25525511135"))

