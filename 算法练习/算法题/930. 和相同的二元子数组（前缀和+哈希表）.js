/**
 * @param {number[]} nums
 * @param {number} goal
 * @return {number}
 */
// 尝试暴力求解
var numSubarraysWithSum = function (nums, goal) {
    let count = 0
    for (let left = 0; left < nums.length; left++) {
        let sum = 0
        for (let right = left; right < nums.length; right++) {
            sum += nums[right]
            if (sum == goal) {
                count++
            }
        }
    }
    return count
};

// 前缀和 + 哈希表
var numSubarraysWithSum = function (nums, goal) {
    let map = new Map()
    let sum = 0
    let res = 0
    for (let num of nums) {
        map.set(sum, (map.get(sum) || 0) + 1)
        sum += num
        res += map.get(sum - goal) || 0
    }
    return res
};
console.log(numSubarraysWithSum([1, 0, 1, 0, 1], 2))
console.log(numSubarraysWithSum([0, 0, 0, 0, 0], 0))