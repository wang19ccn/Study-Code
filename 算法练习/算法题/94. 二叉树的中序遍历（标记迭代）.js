/** 
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var inorderTraversal = function (root) {
    let white = 0
    let gray = 1
    let res = []
    let stack = [[white, root]]
    while (stack.length != 0) {
        let cur = stack.pop()
        let color = cur[0]
        let node = cur[1]
        if (!node) continue
        if (color == white) {
            stack.push([white, node.right])
            stack.push([gray, node])
            stack.push([white, node.left])
        } else {
            res.push(node.val)
        }
    }
    return res
};

console.log(inorderTraversal(
    root = {
        val: 1,
        left: { val: 2, left: null, right: null },
        right: {
            val: 3,
            left: { val: 4, left: null, right: null },
            right: { val: 5, left: null, right: null }
        }
    }
))

// 其核心思想如下：

// 使用颜色标记节点的状态，新节点为白色，已访问的节点为灰色。
// 如果遇到的节点为白色，则将其标记为灰色，然后将其右子节点、自身、左子节点依次入栈。
// 如果遇到的节点为灰色，则将节点的值输出。
// 使用这种方法实现的中序遍历如下：

// 作者：hzhu212
// 链接：https://leetcode-cn.com/problems/binary-tree-inorder-traversal/solution/yan-se-biao-ji-fa-yi-chong-tong-yong-qie-jian-ming/
// 来源：力扣（LeetCode）
// 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。