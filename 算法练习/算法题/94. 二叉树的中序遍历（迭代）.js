/** （这是不是算迭代？？）（四个月后的我回答道：是的）
 * 
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var inorderTraversal = function(root) {
    let stact = []
    let res = []

    while (stact.length || root != null) {
        //存右子树
        while (root !== null) {
            stact.push(root)
            root = root.left
        }

        root = stact.pop()
        res.push(root.val)

        root = root.right
    }

    return res
};
