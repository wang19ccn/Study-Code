/**
 * @param {number[]} A
 * @return {boolean}
 */
var validMountainArray = function (A) {
    if (A.length < 3) return false
    let index = 0
    while (index < A.length && A[index] < A[index + 1]) {
        index++
    }
    if (index == 0 || index == A.length - 1) return false
    while (index < A.length && A[index] > A[index + 1]) {
        index++
    }
    return index == A.length - 1
};