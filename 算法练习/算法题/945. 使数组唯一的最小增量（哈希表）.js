/**应该是线性探测
 * 时间太长（LeetCode 2000+ms）
 * @param {number[]} A
 * @return {number}
 */
var minIncrementForUnique = function (A) {
    let move = 0
    let res = new Array(80010).fill(0)
    for (let i = 0; i < A.length; i++) {
        res[A[i]]++
    }
    for (let i = 1; i < A.length; i++) {
        if (res[A[i]] == 1) {
            continue
        }
        else if (res[A[i]] == 0) {
            res[A[i]]++
        }
        else {
            res[A[i]]--
            while (res[A[i]] > 0) {
                A[i]++
                move++
            }
            res[A[i]]++
        }
    }
    return move
};

