/**
 * @param {number[]} A
 * @return {number}
 */
var minIncrementForUnique = function (A) {
    let check = new Array(80000).fill(-1)
    let move = 0

    var findSpace = function (a) {
        let b = check[a]
        if (b == -1) {
            check[a] = a
            return a
        }
        b = findSpace(b + 1)
        check[a] = b
        return b
    }

    for (let a of A) {
        let b = findSpace(a)
        move += b - a
    }

    return move
};


