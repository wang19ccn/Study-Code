/**
 * @param {number[]} A
 * @return {number}
 */
var minIncrementForUnique = function (A) {
    let move = 0
    let num = 0
    let count = new Array(80010).fill(0)
    for (let i = 0; i < A.length; i++) {
        count[A[i]]++
    }
    for (let i = 0; i < 80000; i++) {
        if (count[i] >= 2) {
            num += count[i] - 1
            move -= i * (count[i] - 1)
        }
        else if (num > 0 && count[i] == 0) {
            num--
            move += i
        }
    }
    return move
};

console.log(minIncrementForUnique([0, 1, 1, 1, 2]))

