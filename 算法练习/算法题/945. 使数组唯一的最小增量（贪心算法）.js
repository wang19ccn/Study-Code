/**
 * @param {number[]} A
 * @return {number}
 */
var minIncrementForUnique = function (A) {
    let move = 0
    A = A.sort((a, b) => a - b)
    for (let i = 1; i < A.length; i++) {
        if (A[i] <= A[i - 1]) {
            move += A[i - 1] - A[i] + 1
            A[i] += A[i - 1] - A[i] + 1
        }
    }
    return move
};

console.log(minIncrementForUnique([0, 1, 1, 1, 2]))