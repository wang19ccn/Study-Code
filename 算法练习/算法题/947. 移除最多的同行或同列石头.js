/**
 * @param {number[][]} stones
 * @return {number}
 */
/**
 * @param {string[][]} equations
 * @param {number[]} values
 * @param {string[][]} queries
 * @return {number[]}
 */
// class UnionFind {
//     constructor(n) {
//         this.parent = new Map()
//         this.
//     }

//     union(x, y, value) {
//         let rootX = this.find(x)
//         let rootY = this.find(y)
//         if (rootX == rootY) {
//             return
//         }
//         this.parent[rootX] = rootY
//         this.weight[rootX] = this.weight[y] * value / this.weight[x]
//     }
//     /**
//     * 路径压缩
//     * @param x
//     * @return 根结点的 id
//     */
//     find(x) {
//         if (x != this.parent.get(x)) {
//             this.parent.set(x, this.find(this.parent.get(x)))
//         }
//         return this.parent[x]
//     }
// }
// var removeStones = function (stones) {
//     let unionFind = new UnionFind()

//     for (let stone of stones) {
//         unionFind.union(stone[0] + 10000, stone[1])
//     }

//     return stones.length - unionFind.getCount()
// };

var removeStones = function(stones) {
    const n = stones.length;
    const edge = {};
    for (const [i, [x1, y1]] of stones.entries()) {
        for (const [j, [x2, y2]] of stones.entries()) {
            if (x1 === x2 || y1 === y2) {
                edge[i] ? edge[i].push(j) : edge[i] = [j];
            }
        }
    }

    vis = new Set();
    let num = 0;
    for (let i = 0; i < n; i++) {
        if (!vis.has(i)) {
            num++;
            dfs(i, vis, edge);
        }
    }
    return n - num;
};

const dfs = (x, vis, edge) => {
    vis.add(x);
    for (let y of edge[x]) {
        if (!vis.has(y)) {
            dfs(y, vis, edge);
        }
    }
}

// 作者：LeetCode-Solution
// 链接：https://leetcode-cn.com/problems/most-stones-removed-with-same-row-or-column/solution/yi-chu-zui-duo-de-tong-xing-huo-tong-lie-m50r/
