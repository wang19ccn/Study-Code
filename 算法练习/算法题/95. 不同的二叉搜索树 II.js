/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number} n
 * @return {TreeNode[]}
 */
var generateTrees = function (n) {
    if (0 == n) return []
    let getAllTrees = function (low, high) {
        if (low > high) return [null]
        if (low == high) return [new TreeNode(low)]
        let res = []
        for (let i = low; i <= high; i++) {
            let leftTrees = getAllTrees(low, i - 1)
            let rightTrees = getAllTrees(i + 1, high)
            for (let leftTree of leftTrees) {
                for (let rightTree of rightTrees) {
                    let root = new TreeNode(i)
                    root.left = leftTree
                    root.right = rightTree
                    res.push(root)
                }
            }
        }
        return res
    }
    return getAllTrees(1, n)
};

console.log(generateTrees(3))

function TreeNode(val, left, right) {
    this.val = (val === undefined ? 0 : val)
    this.left = (left === undefined ? null : left)
    this.right = (right === undefined ? null : right)
}
