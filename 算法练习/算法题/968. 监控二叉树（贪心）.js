/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var minCameraCover = function (root) {
    let result = 0
    let rerootsion = function (root) {
        // 空节点，该节点有覆盖
        if (root == null) return 2

        // 后序遍历，从下往上
        let left = rerootsion(root.left)
        let right = rerootsion(root.right)


        if (left == 2 && right == 2) return 0

        if (left == 0 || right == 0) {
            result++
            return 1
        }

        if (left == 1 || right == 1) return 2
    }

    return rerootsion(root) == 0 ? ++result : result
};

console.log(minCameraCover(
    root = {
        val: 0,
        left: null,
        right: null
    }
))