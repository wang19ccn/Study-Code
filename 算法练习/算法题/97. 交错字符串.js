/**
 * @param {string} s1
 * @param {string} s2
 * @param {string} s3
 * @return {boolean}
 */
var isInterleave = function (s1, s2, s3) {
    let m = s1.length
    let n = s2.length
    if (s3.length !== m + n) return false

    let dp = new Array(m + 1)
    for (let i = 0; i < m + 1; i++) {
        dp[i] = new Array(n + 1).fill(false)
    }
    dp[0][0] = true
    for (let i = 1; i <= m && s1.charAt(i - 1) == s3.charAt(i - 1); i++) dp[i][0] = true
    for (let i = 1; i <= n && s2.charAt(i - 1) == s3.charAt(i - 1); i++) dp[0][i] = true
    for (let i = 1; i <= m; i++) {
        for (let j = 1; j <= n; j++) {
            dp[i][j] = (dp[i - 1][j] && s1.charAt(i - 1) == s3.charAt(i + j - 1)) || (dp[i][j - 1] && s2.charAt(j - 1) == s3.charAt(i + j - 1))
        }
    }
    return dp[m][n]
};

console.log(isInterleave("aabcc", "dbbca", "aadbbbaccc"))