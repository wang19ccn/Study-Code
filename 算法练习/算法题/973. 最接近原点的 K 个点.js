/**
 * @param {number[][]} points
 * @param {number} K
 * @return {number[][]}
 */
var kClosest = function (points, K) {
    let arr = []
    for (let point of points) {
        let distance = point[0] * point[0] + point[1] * point[1]
        arr.push([distance, point])
    }
    arr.sort((a, b) => { return a[0] - b[0] })

    let res = []
    for (let i = 0; i < K; ++i) {
        res.push(arr[i][1])
    }

    return res
};

console.log(kClosest([[0, 1], [1, 0]], 2))