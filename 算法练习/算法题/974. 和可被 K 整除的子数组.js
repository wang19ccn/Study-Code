/**
 * @param {number[]} A
 * @param {number} K
 * @return {number}
 */
// 同余定理
// (preSum[ j ] - preSum[ i - 1 ]) mod K == 0
//  preSum[j] mod K == preSum[i-1] mod K

var subarraysDivByK = function (A, K) {
    let map = { 0: 1 } //预设边界
    let preSum = 0
    let count = 0
    for (let i = 0; i < A.length; i++) {
        preSum = (preSum + A[i]) % K
        if (preSum < 0) preSum += K // 处理负数情况，根据累加求余拆成分步求余之后，应该取正余数 （正余数和负余数实际为一类，故都转成负余数）
        if (map[preSum]) count += map[preSum]
        if (map[preSum]) {
            map[preSum]++
        } else {
            map[preSum] = 1
        }
    }
    return count
};

console.log(subarraysDivByK([4, 5, 0, -2, -3, 1], 5))