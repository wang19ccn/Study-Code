/**
 * @param {number[]} arr
 * @return {number}
 */
var maxTurbulenceSize = function (arr) {
    let n = arr.length

    let left = 0
    let right = 0

    let res = 0
    while (right < n - 1) {
        if (left == right) {
            if (arr[left] == arr[left + 1]) left++
            right++
        } else {
            if (arr[right - 1] < arr[right] && arr[right] > arr[right + 1]) {
                right++
            } else if (arr[right - 1] > arr[right] && arr[right] < arr[right + 1]) {
                right++
            } else {
                left = right
            }
        }
        res = Math.max(res, right - left + 1)
    }

    return res
};