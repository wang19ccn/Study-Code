/**
 * @param {number[]} arr
 * @return {number}
 */
var maxTurbulenceSize = function (arr) {
    let n = arr.length

    let up = new Array(n).fill(1)
    let down = new Array(n).fill(1)

    let res = 1
    for (let i = 0; i < n; i++) {
        if (arr[i - 1] < arr[i]) {
            up[i] = down[i - 1] + 1
        } else if (arr[i - 1] > arr[i]) {
            down[i] = up[i - 1] + 1
        }
        res = Math.max(res, Math.max(up[i], down[i]))
    }

    return res
};