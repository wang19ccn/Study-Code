/** 中序遍历
 * 
 * 二叉搜索树（二叉排列树）中序遍历一定是从低到高升序的。
 * 
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isValidBST = function (root) {
    let stact = []
    let inorder = -Infinity

    while (stact.length || root != null) {
        //存右子树
        while (root !== null) {
            stact.push(root)
            root = root.left
        }

        root = stact.pop()

        if (root.val <= inorder) return false

        inorder = root.val
        root = root.right
    }

    return true
};