/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var pre = -Infinity

var isValidBST = function (root) {
    pre = -Infinity //如果此处不赋值，则无法通过判题系统 本地不需要，chrome也不需要
    return isValidBST1(root)
};

var isValidBST1 = function (root) {
    if (root == null) {
        return true;
    }
    // 访问左子树
    if (!isValidBST1(root.left)) {
        return false;
    }

    if (root.val <= pre) {
        return false;
    }
    
    pre = root.val;

    // 访问右子树
    return isValidBST1(root.right);
}

function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}

console.log(isValidBST(new TreeNode(0)))