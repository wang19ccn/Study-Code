/**
 * @param {number[]} days
 * @param {number[]} costs
 * @return {number}
 */
var mincostTickets = function (days, costs) {
    let days_end = days[days.length - 1]
    let dp = new Array(days_end + 1).fill(0)
    let days_index = 0
    for (let i = 1; i < dp.length; i++) {
        if (i != days[days_index]) {
            dp[i] = dp[i - 1]
        } else {
            dp[i] = Math.min(dp[Math.max(0, i - 1)] + costs[0],
                dp[Math.max(0, i - 7)] + costs[1],
                dp[Math.max(0, i - 30)] + costs[2])
            days_index += 1
        }
    }
    return dp[dp.length - 1]
};