// 参考答案
// 链接：https://leetcode-cn.com/problems/recover-binary-search-tree/solution/san-chong-jie-fa-xiang-xi-tu-jie-99-hui-fu-er-cha-/
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {void} Do not return anything, modify root in-place instead.
 */
var recoverTree = function (root) {
    if (root == null) return
    let x = null
    let y = null
    let pre = null
    let tmp = null
    while (root != null) {
        if (root.left != null) {
            tmp = root.left
            while (tmp.right != null && tmp.right != root) {
                tmp = tmp.right
            }
            if (tmp.right == null) {
                tmp.right = root
                root = root.left
            } else {
                if (pre != null && pre.val > root.val) {
                    y = root
                    if (x == null) x = pre
                }
                pre = root
                tmp.right = null
                root = root.right
            }

        } else {
            if (pre != null && pre.val > root.val) {
                y = root
                if (x == null) x = pre
            }
            pre = root
            root = root.right
        }
    };
    [x.val, y.val] = [y.val, x.val]
};

