/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {void} Do not return anything, modify root in-place instead.
 */
var recoverTree = function (root) {
    let first = null
    let second = null
    let pre = new TreeNode(-Infinity)

    var dfs = function (node) {
        if (node == null) return
        dfs(node.left)
        if (first == null && pre.val >= node.val) {
            first = pre
        }
        if (first != null && pre.val >= node.val) {
            second = node
        }
        pre = node
        dfs(node.right)
    }

    dfs(root); // 注意分号，不然使用语法糖会报错
    // let temp = first.val
    // first.val = second.val
    // second.val = temp
    [first.val, second.val] = [second.val, first.val]
};