/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 */
var isCousins = function (root, x, y) {
    let xParent = null, xDepth = null, xHave = false;
    let yParent = null, yDepth = null, yHave = false;
    var dfs = function (root, depth, parent) {
        if (!root) {
            return;
        }
        if (root.val == x) {
            [xParent, xDepth, xHave] = [parent, depth, true];
        } else if (root.val == y) {
            [yParent, yDepth, yHave] = [parent, depth, true];
        }
        if (xHave && yHave) {
            return;
        }
        dfs(root.left, depth + 1, root);
        dfs(root.right, depth + 1, root);
    }
    dfs(root, 0, null);
    return xDepth == yDepth && xParent != yParent;
};