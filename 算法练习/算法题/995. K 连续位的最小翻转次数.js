/**
 * @param {number[]} A
 * @param {number} K
 * @return {number}
 */
var minKBitFlips = function (A, K) {
    let n = A.length
    let res = 0
    let queue = new Array()

    for (let i = 0; i < n; i++) {
        if (queue.length != 0 && i >= queue[0] + K) {
            queue.shift()
        }

        if (queue.length % 2 == A[i]) {
            if (i + K > n) {
                return -1
            }

            queue.push(i)
            res += 1
        }
    }

    return res
};