/**
 * @param {character[][]} board
 * @return {number}
 */
var numRookCaptures = function(board) {

	let num = 0;
	let dx = [-1, 1, 0, 0];
	let dy = [0, 0, -1, 1];

	for (let i = 0; i < 8; i++) {
		for (let j = 0; j < 8; j++) {
			if (board[i][j] == 'R') {

				for (let k = 0; k < 4; k++) {
					let x = i;
					let y = j;
					while (true) {
						x += dx[k];
						y += dy[k];
						if (x < 0 || x >= 8 || y < 0 || y >= 8 || board[x][y] == 'B') {
							break;
						}
						if (board[x][y] == 'p') {
							num++;
							break;
						}
					}
				}

				return num;
			}
		}
	}

	return 0;
};

numRookCaptures([
	[".", ".", ".", ".", ".", ".", ".", "."],
	[".", "p", "p", "p", "p", "p", ".", "."],
	[".", "p", "p", "B", "p", "p", ".", "."],
	[".", "p", "B", "R", "B", "p", ".", "."],
	[".", "p", "p", "B", "p", "p", ".", "."],
	[".", "p", "p", "p", "p", "p", ".", "."],
	[".", ".", ".", ".", ".", ".", ".", "."],
	[".", ".", ".", ".", ".", ".", ".", "."]
])
