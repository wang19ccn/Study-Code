/**理解错测试用例
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */
var mergeKLists = function (lists) {
    let len=lists.length
    let lensum=0
    for(let i=0;i<len;i++){
        lensum += lists[i].length
    }

    let arr=new Array(lensum).fill(0)
    let k = 0 
    for(let i=0;i<len;i++){
        for(let j=0;j<lists[i].length;j++){
            arr[k]=lists[i][j].val;
            k++
        }
    }

    arr.sort((a, b) => a - b); 

    let newLists = new Array(lensum)
    //let newLists = new Array(lensum).fill({val:null,next:null}) 
    //不可以这样，这样js默认认为所有数组都是指向这两个值，变一个全变。
    for(let i=0;i<lensum;i++){
        if(i==lensum-1){
            newLists[i]={val:arr[i],next:null}
        }else{
            newLists[i]={val:arr[i],next:arr[i+1]}
        }
    }
    return newLists
};

function ListNode(val,next) {
    this.val = val;
    this.next = next;
}

// console.log(mergeKLists([[{val:1,next:4},{val:4,next:5},{val:5,next:null}],
//     [{val:1,next:3},{val:3,next:4},{val:1,next:4}]
//     ,[{val:2,next:6},{val:6,next:null}]]))

    