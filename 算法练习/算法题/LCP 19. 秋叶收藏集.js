/**
 * @param {string} leaves
 * @return {number}
 */
var minimumOperations = function (leaves) {
    let len = leaves.length

    let dp = new Array(3)
    for (let i = 0; i < 3; i++) {
        dp[i] = new Array(len)
    }

    for (let i = 0; i < len; i++) {
        if (i < 1) {
            dp[0][i] = Number((leaves[i] != 'r'))
        } else {
            dp[0][i] = dp[0][i - 1] + Number((leaves[i] != 'r'))
        }

        if (i < 1) {
            dp[1][i] = dp[0][i]
        } else {
            dp[1][i] = Math.min(dp[0][i - 1] + Number((leaves[i] != 'y')), dp[1][i - 1] + Number((leaves[i] != 'y')))
        }

        if (i < 2) {
            dp[2][i] = dp[1][i]
        } else {
            dp[2][i] = Math.min(dp[1][i - 1] + Number((leaves[i] != 'r')), dp[2][i - 1] + Number((leaves[i] != 'r')))
        }
    }

    return dp[2][len - 1]
};

console.log(minimumOperations("rrryyyrryyyrr"))

// 状态     r  r  r  y  y  y  r  r  y  y  y  r  r
// 0      [ 0, 0, 0, 1, 2, 3, 3, 3, 4, 5, 6, 6, 6 ]
// 1      [ 0, 1, 1, 0, 0, 0, 1, 2, 2, 2, 2, 3, 4 ]
// 2      [ 0, 1, 1, 2, 1, 1, 0, 0, 1, 2, 3, 2, 2 ] 

// 思路
// 动态规划

// 使用 3 个 dp 数组记录状态

// dp[0][i] 代表从头开始全部修改成红色（纯红）需要修改几次
// dp[1][i] 代表从头开始是红色，然后现在是黄色（红黄），需要修改几次
// dp[2][i] 代表从头开始是红色，然后变成黄色，又变成红色（红黄红），需要修改几次
// 根据 i 是红是黄，判断转移情况

// dp[0][i] 就很简单，如果是黄的，就比之前加一
// dp[1][i] 可以从上一个纯红状态变化过来，也可以从上一个本身状态变化过来
// dp[2][i] 可以从上一个红黄状态变化过来，也可以从上一个本身状态变化过来
// 所以最后要求的答案即：dp[2].back()

// 作者：ikaruga
// 链接：https://leetcode-cn.com/problems/UlBDOe/solution/ulbdoe-by-ikaruga/