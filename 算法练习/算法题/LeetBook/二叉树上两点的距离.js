/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} p
 * @param {number} q
 * @return {number}
 */
var distanceInTree = function (root, p, q) {
    let ancestor = findAncestor(root, p, q)
    return getDistance(ancestor, p) + getDistance(ancestor, q)
};

var findAncestor = function (root, p, q) {
    if (root == null || root.val == p || root.val == q) {
        return root
    }
    let left = findAncestor(root.left, p, q)
    let right = findAncestor(root.right, p, q)
    if (left != null && right != null) {
        return root
    }

    return left == null ? right : left
}

var getDistance = function (ancestor, target) {
    if (ancestor == null) {
        return -Infinity
    }
    if (ancestor.val == target) {
        return 0
    }
    return Math.max(getDistance(ancestor.left, target) + 1, getDistance(ancestor.right, target) + 1)
}