/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} preorder
 * @return {TreeNode}
 */
var bstFromPreorder = function (preorder) {
    return createTree(preorder, 0, preorder.length - 1)
};

var createTree = function (preorder, left, right) {
    if (left > right) {
        return null
    }

    let rootNum = preorder[left]
    let root = new TreeNode(rootNum)
    let pivot = left + 1

    // 找到右子树的开始节点，第一个比当且节点大的节点。
    while (pivot <= right && preorder[pivot] < preorder[left]) {
        pivot++
    }

    root.left = createTree(preorder, left + 1, pivot - 1)
    root.right = createTree(preorder, pivot, right)

    return root
}