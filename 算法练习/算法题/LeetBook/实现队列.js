var LCQueue = function() {
    this.arr = new Array()
};

/** 
 * @param {number} x
 * @return {void}
 */
LCQueue.prototype.push = function(x) {
    this.arr.push()
};

/**
 * @return {void}
 */
LCQueue.prototype.pop = function() {
    return this.arr.pop()
};

/**
 * @return {number}
 */
LCQueue.prototype.size = function() {
    return this.arr.length
};

/**
 * @return {number}
 */
LCQueue.prototype.front = function() {
    return this.arr.shift()
};

/**
 * Your LCQueue object will be instantiated and called as such:
 * var obj = new LCQueue()
 * obj.push(x)
 * obj.pop()
 * var param_3 = obj.size()
 * var param_4 = obj.front()
 */