const curBar = (n, m) => {
    let slices = n;
    let cnt = 0;
    while (slices > 1) {
        ++cnt;
        slices -= Math.min(slices / 2, m);
    }
    return cnt;
}

// 作者：力扣 (LeetCode)
// 链接：https://leetcode-cn.com/circle/discuss/QGsghu/view/C7mWij/