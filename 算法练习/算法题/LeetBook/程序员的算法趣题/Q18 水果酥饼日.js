// 记录无向图
let edges = [];
// 记录搜索过程中每个数是否被使用过
let used = [];
// 记录搜索过程中的序列
const ans = [];
// 是否找到答案
let found = false;

const dfs = (pos, n) => {
    if (pos == n) {
        // 搜索完成，还需要判断首尾之和是否为完全平方数
        const sum = ans[0] + ans[ans.length - 1];
        const root = Math.floor(Math.sqrt(sum));
        if (root * root === sum) {
            found = true;
        }
    }
    else if (pos === 0) {
        // 由于酥饼是圆形，因此可以指定第一个数选择 1
        used[1] = true;
        ans.push(1);
        dfs(pos + 1, n);
        if (found) {
            return;
        }
        used[1] = false;
        ans.pop();
    }
    else {
        // 只能选与上一个数形成完全平方数，并且还未被选择的数
        for (const num of edges[ans[ans.length - 1]]) {
            if (!used[num]) {
                used[num] = true;
                ans.push(num);
                dfs(pos + 1, n);
                if (found) {
                    return;
                }
                used[num] = false;
                ans.pop();
            }
        }
    }
}

const minimalFruitPie = () => {
    // 从小到大枚举 n
    for (let n = 2; ; ++n) {
        edges = new Array(n + 1).fill(0).map(v => []);
        // 建立无向图
        for (let i = 1; i <= n; ++i) {
            // 枚举平方数 root^2
            for (let root = 1; root * root <= i + n; ++root) {
                const j = root * root - i;
                if (j > i) {
                    edges[i].push(j);
                    edges[j].push(i);
                }
            }
        }
        used = new Array(n + 1).fill(0);
        dfs(0, n);
        if (found) {
            return ans;
        }
    }
}

// 作者：力扣 (LeetCode)
// 链接：https://leetcode-cn.com/circle/discuss/amUE3E/view/cIvhGN/
