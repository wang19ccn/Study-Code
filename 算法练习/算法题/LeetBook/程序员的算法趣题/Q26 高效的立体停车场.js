// 停车场大小
const M = 10;
const N = 10;

class Status {
    constructor(car, space, step) {
        // 目标车所在的格子
        this.car = car;
        // 空位所在的格子
        this.space = space;
        // 移动的步数
        this.step = step;
    }

    hashCode() {
        return this.car * M * N + this.space;
    }

    // 空位左移
    goLeft() {
        // 在最左列
        if (this.space % N === 0) {
            return null;
        }
        return this.car === this.space - 1 ? new Status(this.space, this.car, this.step + 1) : new Status(this.car, this.space - 1, this.step + 1);
    }

    // 空位右移
    goRight() {
        // 在最右列
        if (this.space % N === N - 1) {
            return null;
        }
        return this.car === this.space + 1 ? new Status(this.space, this.car, this.step + 1) : new Status(this.car, this.space + 1, this.step + 1);
    }

    // 空位上移
    goUp() {
        // 在最上行
        if (Math.floor(this.space / N) === 0) {
            return null;
        }
        return this.car === this.space - N ? new Status(this.space, this.car, this.step + 1) : new Status(this.car, this.space - N, this.step + 1);
    }

    // 空位下移
    goDown() {
        // 在最下行
        if (Math.floor(this.space / N) === M - 1) {
            return null;
        }
        return this.car === this.space + N ? new Status(this.space, this.car, this.step + 1) : new Status(this.car, this.space + N, this.step + 1);
    }

    // 辅助函数，返回空位与四个方向的交换结果，方便编码
    getAdjacent() {
        const adjacent = [this.goLeft(), this.goRight(), this.goUp(), this.goDown()];
        const neighbors = [];
        for (const neighbor of adjacent) {
            if (neighbor !== null) {
                neighbors.push(neighbor);
            }
        }
        return neighbors;
    }

    // 判断车是否到达右下角
    finished() {
        return this.car === M * N - 1;
    }
}

const effectiveParkingArea = () => {
    const init = new Status(0, M * N - 1, 0);
    const seen = new Map();
    const queue = [];

    seen.set(init.hashCode(), init);
    queue.push(init);

    while (queue.length) {
        const cur = queue.shift();
        const neighbors = cur.getAdjacent();
        for (const neighbor of neighbors) {
            if (neighbor.finished()) {
                return neighbor.step;
            }
            if (!seen.has(neighbor.hashCode())) {
                seen.set(neighbor.hashCode(), neighbor);
                queue.push(neighbor);
            }
        }
    }
    // 不可能的情况
    return -1;
}

// 作者：力扣 (LeetCode)
// 链接：https://leetcode-cn.com/circle/discuss/ZhsB58/view/wSKf1M/
