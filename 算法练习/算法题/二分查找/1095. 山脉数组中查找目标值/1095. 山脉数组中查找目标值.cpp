/** 4ms,7.3MB
 * // This is the MountainArray's API interface.
 * // You should not implement it, or speculate about its implementation
 * class MountainArray {
 *   public:
 *     int get(int index);
 *     int length();
 * };
 */

// class Solution {
// public:
//     int findInMountainArray(int target, MountainArray &mountainArr) {
//         int len = mountainArr.length();

//         int peakIndex = findInMountainTop(mountainArr,0,len-1);
//         if(mountainArr.get(peakIndex)==target){
//             return peakIndex;
//         }

//         int res=findSortedArray(mountainArr,0,peakIndex-1,target);
//         if(res != -1){
//             return res;
//         }

//         return findReverseArray(mountainArr,peakIndex+1,len-1,target);

//     }

//     int findInMountainTop(MountainArray mountainArr,int left,int right){
//         while(left<right){
//             int mid = left + (right-left)/2;
//             if(mountainArr.get(mid)<mountainArr.get(mid+1)){
//                 left=mid+1;
//             }else{
//                 right=mid;
//             }
//         }
//         return left;
//     }

//     int findSortedArray(MountainArray mountainArr,int left,int right,int target){
//         while(left<right){
//             int mid = left + (right-left)/2;
//             if(mountainArr.get(mid)<target){
//                 left=mid+1;
//             }else{
//                 right=mid;
//             }
//         }
//         if(mountainArr.get(left)==target){
//             return left;
//         }
//         return -1;
//     }

//     int findReverseArray(MountainArray mountainArr,int left,int right,int target){
//         while(left<right){
//             int mid = left + (right-left+1)/2; //注意上取整
//             if(mountainArr.get(mid)<target){
//                 right = mid -1;
//             }else{
//                 //[keft,right(mid)]
//                 left=mid;
//             }
//         }
//         if(mountainArr.get(left)==target){
//             return left;
//         }
//         return -1;
//     }
// };