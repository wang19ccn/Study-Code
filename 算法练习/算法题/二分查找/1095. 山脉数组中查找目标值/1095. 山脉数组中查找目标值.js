/**
 * // This is the MountainArray's API interface.
 * // You should not implement it, or speculate about its implementation
 * function MountainArray() {
 *     @param {number} index
 *     @return {number}
 *     this.get = function(index) {
 *         ...
 *     };
 *
 *     @return {number}
 *     this.length = function() {
 *         ...
 *     };
 * };
 */

/** 64ms,34.4MB 接口未实现，只能在题库执行
 * @param {number} target
 * @param {MountainArray} mountainArr
 * @return {number}
 */
var findInMountainArray = function (target, mountainArr) {
    let len = mountainArr.length() //获取长度

    let peakIndex = findInMountainTop(mountainArr, 0, len - 1)
    if (mountainArr.get(peakIndex) == target) {
        return peakIndex
    }

    let res = findSortedArray(mountainArr, 0, peakIndex - 1, target) //因为找的是最小下标，如果右半侧找到就可以直接返回并退出程序
    if (res != -1) {
        return res;
    }

    return findReverseArray(mountainArr, peakIndex + 1, len - 1, target)
};

/** 寻找顶点
 * [left..right]
 * @param {*} mountainArr 
 * @param {*} left 
 * @param {*} right 
 */
var findInMountainTop = function (mountainArr, left, right) {
    while (left < right) {
        let mid = parseInt(left + (right - left) / 2)
        if (mountainArr.get(mid) < mountainArr.get(mid + 1)) {
            left = mid + 1
        } else {
            right = mid
        }
    }
    return left
}

/** 右侧有序，从低到高
 * [left..peakIndex-1]
 * @param {*} mountainArr 
 * @param {*} left 
 * @param {*} right 
 * @param {*} target 
 */
var findSortedArray = function (mountainArr, left, right, target) {
    while (left < right) {
        let mid = parseInt(left + (right - left) / 2)
        if (mountainArr.get(mid) < target) {
            left = mid + 1
        } else {
            right = mid
        }
    }
    if (mountainArr.get(left) == target) { //确保是找到目标值
        return left 
    }
    return -1
}

/** 左侧有序，从高到底
 * [peakIndex+1..right]
 * @param {*} mountainArr 
 * @param {*} left 
 * @param {*} right 
 * @param {*} target 
 */
var findReverseArray = function (mountainArr, left, right, target) {
    while (left < right) {
        let mid = parseInt(left + (right - left + 1) / 2) //取上整，即mid要在right
        if (mountainArr.get(mid) < target) {
            right = mid - 1
        } else {
            //[left,right(mid)]才能执行，否则成死循环
            left = mid
        }
    }
    if (mountainArr.get(left) == target) { //同理
        return left
    }
    return -1
}