/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var searchRange = function (nums, target) {
    if (nums == null) return [-1, -1]
    let firstIndex = find(true, nums, target)
    let lastIndex = find(false, nums, target)
    return [firstIndex, lastIndex]
};

var find = function (isFindFirst, nums, target) {
    let start = 0
    let end = nums.length - 1
    while (start < end) {
        let mid = start + parseInt((end - start) / 2)
        if (nums[mid] > target) {
            end = mid - 1
        }
        else if (nums[mid] < target) {
            start = mid + 1
        }
        else {
            if (isFindFirst) {
                if (mid > 0 && nums[mid] == nums[mid - 1]) {
                    end = mid - 1
                } else {
                    return mid
                }
            } else {
                if (mid < nums.length - 1 && nums[mid] == nums[mid + 1]) {
                    start = mid + 1
                } else {
                    return mid
                }
            }
        }
    }
    return nums[start] == target ? start : -1;
    //return -1 //如果start<=end 就可以进入到return mid
}

console.log(searchRange([5, 7, 7, 8, 8, 10], 8))