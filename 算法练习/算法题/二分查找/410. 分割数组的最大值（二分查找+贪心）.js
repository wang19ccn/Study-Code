/**
 * @param {number[]} nums
 * @param {number} m
 * @return {number}
 */
var splitArray = function (nums, m) {
    let max = 0
    let sum = 0

    for (let num of nums) {
        max = Math.max(max, num)
        sum += num
    }

    let left = max
    let right = sum
    while (left < right) {
        let mid = left + parseInt((right - left) / 2)
        let splits = split(nums, mid)
        if (splits > m) {
            left = mid + 1
        } else {
            right = mid
        }
    }
    return left
};

var split = function (nums, maxIntervalSum) {
    let splits = 1
    let curIntervalSum = 0
    for (let num of nums) {
        if (curIntervalSum + num > maxIntervalSum) {
            curIntervalSum = 0
            splits++
        }
        curIntervalSum += num
    }
    return splits
}

console.log(splitArray([7, 2, 5, 10, 8], 2))