/** 
 * @param {number[]} arr
 * @param {number} k
 * @param {number} x
 * @return {number[]}
 */
var findClosestElements = function (arr, k, x) {
    let size = arr.length

    //最优区间的左边界”的索引的搜索区间为 [0, size - k]
    let left = 0
    let right = size - k

    while (left < right) {
        let mid = left + parseInt((right - left) / 2)
        if (x - arr[mid] > arr[mid + k] - x) {
            left = mid + 1
        } else {
            right = mid
        }
    }

    return arr.slice(left, left + k)
};

console.log(findClosestElements([1, 1, 1, 10, 10, 10], 1, 9))