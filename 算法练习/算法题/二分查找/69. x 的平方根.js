/**
 * @param {number} x
 * @return {number}
 */
var mySqrt = function(x) {
    if(x<2) return parseInt(x)

    let num = 0
    let left = 2
    let right =parseInt(x/2)
    
    while(left<=right){
        let mid = left+parseInt((right-left)/2)
        num = mid * mid
        if(num>x) right=mid-1
        else if(num<x) left=mid+1
        else return mid
    }
    
    return right
};