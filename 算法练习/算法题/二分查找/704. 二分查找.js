/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function(nums, target) {
    let left = 0
    let right = nums.length - 1
    while(left<right){
        let mid = left+parseInt((right-left)/2)
        if(nums[mid]<target){
            left=mid+1
        }else{
            right=mid
        }
    }
    if(nums[left]==target){
        return left
    }else{
        return -1
    }
};