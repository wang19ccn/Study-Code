import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int W = Integer.parseInt(bufferedReader.readLine());

        String line1 = bufferedReader.readLine();
        String line2 = bufferedReader.readLine();
        String[] line1Array = line1.split(" ");
        String[] line2Array = line2.split(" ");

        int N = line1Array.length;
        int[] weights = new int[N];
        int[] values = new int[N];
        for (int i = 0; i < N; i++) {
            weights[i] = Integer.parseInt(line1Array[i]);
            values[i] = Integer.parseInt(line2Array[i]);
        }

        int[] dp = new int[W + 1];
        for (int i = 1; i <= N; i++) {
            for (int j = W; j >= weights[i - 1]; j--) {
                dp[j] = Math.max(dp[j], dp[j - weights[i - 1]] + values[i - 1]);
            }
        }
        System.out.println(dp[W]);
    }
}