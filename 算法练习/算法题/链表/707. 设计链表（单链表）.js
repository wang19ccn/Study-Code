/**
 * Initialize your data structure here.
 */
var MyLinkedList = function () {
    this.head = null
    this.tail = null
    this.length = 0
};

var listNode = function (val) {
    this.val = val
    this.next = null
}

/**
 * get(index)：获取链表中第 index 个节点的值。如果索引无效，则返回-1。
 * Get the value of the index-th node in the linked list. If the index is invalid, return -1. 
 * @param {number} index
 * @return {number}
 */
MyLinkedList.prototype.get = function (index) {
    if (index >= 0 && index < this.length) {
        let i = 0
        let cur = this.head
        while (i < index) {
            cur = cur.next
            i++
        }
        return cur.val
    } else {
        return -1
    }
};

/**
 * addAtHead(val)：在链表的第一个元素之前添加一个值为 val 的节点。插入后，新节点将成为链表的第一个节点。
 * Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtHead = function (val) {
    let lastHead = this.head
    let node = new listNode(val)
    this.head = node
    this.head.next = lastHead
    if (!this.tail) {
        this.tail = node
        this.tail.next = null
    }
    this.length++
};

/**
 * addAtTail(val)：将值为 val 的节点追加到链表的最后一个元素。
 * Append a node of value val to the last element of the linked list. 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtTail = function (val) {
    let lastTail = this.tail
    let node = new listNode(val)
    this.tail = node
    if (lastTail) {
        lastTail.next = this.tail
    }
    if (!this.head) {
        this.head = node
        this.head.next = null
    }
    this.length++
};

/**
 * addAtIndex(index,val)：在链表中的第 index 个节点之前添加值为 val  的节点。
 * 如果 index 等于链表的长度，则该节点将附加到链表的末尾。
 * 如果 index 大于链表长度，则不会插入节点。
 * 如果 index 小于0，则在头部插入节点。
 * Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. 
 * @param {number} index 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtIndex = function (index, val) {
    if (index === this.length) {
        this.addAtTail(val)
    }
    else if (index <= 0) {
        this.addAtHead(val)
    }
    else if (index > 0 && index < this.length) {
        let i = 0
        let pre = this.head
        while (i < index - 1) {
            pre = pre.next
            i++
        }
        let node = new listNode(val)
        node.next = pre.next
        pre.next = node
        this.length++
    }
};

/**
 * deleteAtIndex(index)：如果索引 index 有效，则删除链表中的第 index 个节点。
 * Delete the index-th node in the linked list, if the index is valid. 
 * @param {number} index
 * @return {void}
 */
MyLinkedList.prototype.deleteAtIndex = function (index) {
    if (index > 0 && index < this.length) {
        let i = 0
        let pre = null
        let cur = this.head
        while (i < index) {
            pre = cur
            cur = cur.next
            i++
        }
        pre.next = cur.next
        if (index === this.length - 1) {
            this.tail = pre
        }
        this.length--
    }
    else if (index === 0) {
        this.head = this.head.next
        this.length--
    }
};

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * var obj = new MyLinkedList()
 * var param_1 = obj.get(index)
 * obj.addAtHead(val)
 * obj.addAtTail(val)
 * obj.addAtIndex(index,val)
 * obj.deleteAtIndex(index)
 */