/**
 * Initialize your data structure here.
 */
var MyLinkedList = function () {
    this.length = 0
    this.head = new listNode(0)
    this.tail = new listNode(0)
    this.head.next = this.tail
    this.tail.prev = this.head
};

var listNode = function (val) {
    this.val = val
    this.next = null
    this.prev = null
}

/**
 * Get the value of the index-th node in the linked list. If the index is invalid, return -1. 
 * @param {number} index
 * @return {number}
 */
MyLinkedList.prototype.get = function (index) {
    if (index < 0 || index >= this.length) return -1
    let cur = null
    if (index + 1 < this.length - index) {
        cur = this.head
        for (let i = 0; i < index + 1; ++i) cur = cur.next
    }
    else {
        cur = this.tail
        for (let i = 0; i < this.length - index; ++i) cur = cur.prev
    }
    return cur.val
};

/**
 * Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtHead = function (val) {
    let pre = this.head
    let suc = this.head.next
    let add = new listNode(val)
    add.prev = pre
    add.next = suc
    pre.next = add
    suc.prev = add
    this.length++
};

/**
 * Append a node of value val to the last element of the linked list. 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtTail = function (val) {
    let suc = this.tail
    let pre = this.tail.prev
    let add = new listNode(val)
    add.prev = pre
    add.next = suc
    pre.next = add
    suc.prev = add
    this.length++
};

/**
 * Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. 
 * @param {number} index 
 * @param {number} val
 * @return {void}
 */
MyLinkedList.prototype.addAtIndex = function (index, val) {
    if (index > this.length) return
    if (index < 0) index = 0
    let pre = null
    let suc = null
    if (index < this.length - index) {
        pre = this.head
        for (let i = 0; i < index; i++) pre = pre.next
        suc = pre.next
    }
    else {
        suc = this.tail
        for (let i = 0; i < this.length - index; i++) suc = suc.prev
        pre = suc.prev
    }
    let add = new listNode(val)
    add.prev = pre
    add.next = suc
    pre.next = add
    suc.prev = add
    this.length++
};

/**
 * Delete the index-th node in the linked list, if the index is valid. 
 * @param {number} index
 * @return {void}
 */
MyLinkedList.prototype.deleteAtIndex = function (index) {
    if (index < 0 || index >= this.length) return
    let pre = null
    let suc = null
    if (index < this.length - index) {
        pre = this.head
        for (let i = 0; i < index; i++) pre = pre.next
        suc = pre.next.next
    }
    else {
        suc = this.tail
        for (let i = 0; i < this.length - index - 1; i++) suc = suc.prev
        pre = suc.prev.prev
    }
    pre.next = suc
    suc.prev = pre
    this.length--
};

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * var obj = new MyLinkedList()
 * var param_1 = obj.get(index)
 * obj.addAtHead(val)
 * obj.addAtTail(val)
 * obj.addAtIndex(index,val)
 * obj.deleteAtIndex(index)
 */


let linkedList = new MyLinkedList();
linkedList.addAtHead(2);
linkedList.deleteAtIndex(1);
linkedList.addAtHead(2);
linkedList.addAtHead(7);
linkedList.addAtHead(3);
linkedList.addAtHead(2);
linkedList.addAtHead(5);
linkedList.addAtTail(5); // 5-2-3-7-2-5
linkedList.get(5);
linkedList.addAtTail(3);
linkedList.deleteAtIndex(6);
linkedList.deleteAtIndex(4);