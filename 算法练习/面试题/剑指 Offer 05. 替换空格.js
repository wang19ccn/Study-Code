/**
 * @param {string} s
 * @return {string}
 */
var replaceSpace = function (s) {
    // return s.replace(/ /g, "%20")
    return s.replace(/\s/g, "%20")
};