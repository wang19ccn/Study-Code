/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {number[]}
 */
var reversePrint = function (head) {
    let res = []
    var dfs = function (head) {
        if (head == null) return
        dfs(head.next)
        res.push(head.val)
    }
    dfs(head)
    return res
};
