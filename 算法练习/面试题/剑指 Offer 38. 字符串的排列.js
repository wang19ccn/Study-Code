/**
 * @param {string} s
 * @return {string[]}
 */
var permutation = function (s) {
    let len = s.length
    let arr = s.split('')
    let res = []
    if (0 == len) {
        return res
    }

    let path = []
    let used = new Array(len).fill(false)

    arr.sort()
    dfs(arr, len, 0, path, used, res)

    return res
};

var dfs = function (arr, len, depth, path, used, res) {
    if (depth == len) {
        res.push([...path].join(''))
        return
    }

    for (let i = 0; i < len; i++) {
        if (used[i]) {
            continue
        }
        if (i > 0 && arr[i] == arr[i - 1] && !used[i - 1]) {
            continue;
        }
        path.push(arr[i])
        used[i] = true
        dfs(arr, len, depth + 1, path, used, res)
        path.pop()
        used[i] = false
    }
}

console.log(permutation("aab"))