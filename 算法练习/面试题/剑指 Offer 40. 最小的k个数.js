/**
 * @param {number[]} arr
 * @param {number} k
 * @return {number[]}
 */
var getLeastNumbers = function (arr, k) {
    sortArray(arr)
    return arr.slice(0, k)
};

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortArray = function (nums) {
    heapSort(nums);
    return nums;
};

//----------堆排序-----------------

//执行堆排序
var heapSort = function (nums) {
    buildMaxHeap(nums);

    for (let j = nums.length - 1; j > 0; j--) {
        change(nums, 0, j);
        adjustHeap(nums, 0, j);
    }

    return nums;
};

//建立大顶堆
var buildMaxHeap = function (nums) {
    for (let i = parseInt(nums.length / 2 - 1); i >= 0; i--) {
        adjustHeap(nums, i, nums.length);
    }
};

//调整大顶堆
var adjustHeap = function (nums, i, len) {
    let temp = nums[i];
    for (let k = i * 2 + 1; k < len; k = k * 2 + 1) { //遍历左结点
        if (k + 1 < len && nums[k] < nums[k + 1]) { //如果左子结点小于右子结点，k指向右子结点
            k++;
        }
        if (nums[k] > temp) { ////如果子节点大于父节点，将子节点值赋给父节点（不用进行交换）
            nums[i] = nums[k];
            i = k;
        } else {
            break;
        }
    }
    nums[i] = temp;
};

//交换元素
var change = function (nums, x, y) {
    let temp = nums[x];
    nums[x] = nums[y];
    nums[y] = temp;
};
