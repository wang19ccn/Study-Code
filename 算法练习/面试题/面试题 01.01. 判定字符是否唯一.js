/**
 * @param {string} astr
 * @return {boolean}
 */
var isUnique = function (astr) {
    let astr_new = astr.split('').sort()
    for (let i = 0; i < astr.length - 1; i++) {
        if (astr_new[i] === astr[i + 1]) {
            return false
        }
    }
    return true
};