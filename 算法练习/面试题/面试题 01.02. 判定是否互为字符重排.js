/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var CheckPermutation = function (s1, s2) {
    let s1_arr = s1.split('')
    let s2_arr = s2.split('')
    s1_arr.sort()
    s2_arr.sort()
    return s1_arr.join()  === s2_arr.join()
};