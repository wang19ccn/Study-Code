/**
 * @param {string} S
 * @return {string}
 */
var compressString = function (S) {
    let len = S.length
    let s = [S[0]]
    let count = 1
    for (let i = 1; i < len; i++) {
        if (S[i - 1] == S[i]) {
            count++
        } else {
            s.push(count)
            s.push(S[i])
            count = 1
        }
    }
    s.push(count)
    return s.length < S.length ? s.join("") : S
};