/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var rotate = function (matrix) {
    console.log(matrix)
    // 对角线翻转
    for (let i = 0; i < matrix.length; i++) {
        for (let j = i + 1; j < matrix[i].length; j++) {
            [matrix[i][j], matrix[j][i]] = [matrix[j][i], matrix[i][j]]
        }
    }
    // 水平翻转
    console.log(matrix)
    matrix.forEach(row => row.reverse());
    console.log(matrix)
};

console.log(rotate([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]))