/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var removeDuplicateNodes = function (head) {
    if (head == null) return head
    let set = new Set()
    set.add(head.val)

    let node = head.next
    let prev = head
    while (node) {
        if (set.has(node.val)) {
            prev.next = node.next
        } else {
            set.add(node.val)
            prev = node
        }
        node = node.next
    }
    return head
};