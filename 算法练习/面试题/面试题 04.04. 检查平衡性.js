/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */

var flag
var isBalanced = function (root) {
    if (root == null) return true
    flag = true
    depth(root, 1)
    console.log(depth(root, 1))
    return flag
};

var depth = function (root, tier) {
    if (root == null) return tier - 1

    let leftTier = depth(root.left, tier + 1)
    let rightTier = depth(root.right, tier + 1)

    if (Math.abs(leftTier - rightTier) > 1) return flag = false

    return Math.max(leftTier, rightTier)
};