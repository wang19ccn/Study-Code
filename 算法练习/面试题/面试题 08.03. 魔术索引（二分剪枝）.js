/**
 * @param {number[]} nums
 * @return {number}
 */

var findMagicIndex = function (nums) {
    var res = -1
    var getIndex = function (nums, start, end) {
        if (start > end) return
        let mid = parseInt(start + (end - start) / 2)
        if (nums[mid] == mid) {
            res = mid
            return getIndex(nums, start, mid - 1)
        } else {
            getIndex(nums, start, mid - 1)
            if (res == -1 || res > end) {
                return getIndex(nums, mid + 1, end)
            }
        }
    }
    getIndex(nums, 0, nums.length - 1)
    return res
};

console.log(findMagicIndex([1, 1, 2, 3]))