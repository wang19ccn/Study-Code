/**
 * @param {number} n
 * @return {string[][]}
 */
var solveNQueens = function (n) {
    // 初始化棋盘
    let board = new Array(n)
    for (let i = 0; i < n; i++) {
        board[i] = new Array(n).fill('.')
    }

    // 记录某一列是否放置了皇后
    let cols = new Set()
    // 记录主对角线上的单元格是否放置了皇后
    let main = new Set()
    // 记录副对角线上的单元格是否放置了皇后
    let sub = new Set()

    // 存储结果
    let res = []

    var dfs = function (row) {
        // 终止
        if (row == n) {
            let resBoard = board.slice()
            for (let i = 0; i < n; i++) {
                resBoard[i] = resBoard[i].join('')
            }
            res.push(resBoard)
            return
        }
        // 选中
        for (let col = 0; col < n; col++) {
            if (!cols.has(col) && !main.has(row - col) && !sub.has(row + col)) {
                // 选中该点
                board[row][col] = 'Q'
                cols.add(col)
                main.add(row - col)
                sub.add(row + col)
                // 在当前选中后，继续下一行
                dfs(row + 1)
                // 撤销该点选中
                board[row][col] = '.'
                cols.delete(col)
                main.delete(row - col)
                sub.delete(row + col)
            }
        }
    }

    // 执行
    dfs(0)

    return res
};