/**
 * @param {number} shorter
 * @param {number} longer
 * @param {number} k
 * @return {number[]}
 */
var divingBoard = function (shorter, longer, k) {
    if (k == 0) return []
    
    if (shorter == longer) return [longer * k]
    
    let res = []
    for (let i = 0; i <= k; i++) {
        res.push(i * shorter + (k - i) * longer)
    }
    
    return res.reverse()
};

console.log(divingBoard(2,4,3))