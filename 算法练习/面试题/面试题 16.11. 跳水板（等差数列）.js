/**
 * @param {number} shorter
 * @param {number} longer
 * @param {number} k
 * @return {number[]}
 */
var divingBoard = function (shorter, longer, k) {
    if (k == 0) return []

    let d = longer - shorter
    if (d == 0) return [longer * k]

    let res = [k * shorter]
    for (let i = 1; i <= k; i++) {
        res[i] = res[i - 1] + d
    }

    return res
};

console.log(divingBoard(2, 4, 3))