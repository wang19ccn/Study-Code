/**
 * @param {string} pattern
 * @param {string} value
 * @return {boolean}
 */
var patternMatching = function (pattern, value) {
    let map = new Map() // 保存a和b所对应的单词
    let set = new Set() // 用于确保map中的a和b不会对应相同的单词

    var match = function (pattern, value, map, set) {
        if (pattern == '') return value == ''
        let pChar = pattern[0]
        if (map.has(pChar)) {
            if (!value.startsWith(map.get(pChar))) return false
            return match(pattern.substring(1), value.substring(map.get(pChar).length), map, set)
        }
        for (let i = -1; i < value.length; i++) {
            let word = value.substring(0, i + 1)
            if (set.has(word)) continue
            map.set(pChar, word)
            set.add(word)
            if (match(pattern.substring(1), value.substring(i + 1), map, set)) return true
            map.delete(pChar)
            set.delete(word)
        }
        return false
    }
    // match(pattern, value, map, set)
    // console.log(set)
    return match(pattern, value, map, set)
};

console.log(patternMatching("aabaa", "dogcatcatdog"))