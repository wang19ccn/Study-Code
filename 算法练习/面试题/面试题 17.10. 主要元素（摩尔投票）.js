/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function (nums) {
    let candidate = -1
    let count = 0
    for (let num of nums) {
        if (count == 0) {
            candidate = num
        }
        if (num == candidate) {
            count++
        } else {
            count--
        }
    }
    count = 0
    const len = nums.length
    for (let num of nums) {
        if (num == candidate) {
            count++
        }
    }
    return count * 2 > len ? candidate : -1
};