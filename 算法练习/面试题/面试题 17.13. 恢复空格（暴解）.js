/**
 * @param {string[]} dictionary
 * @param {string} sentence
 * @return {number}
 */
var respace = function (dictionary, sentence) {
    let set = new Set()
    for (let dic of dictionary) set.add(dic)

    let len = sentence.length
    let dp = new Array(len + 1).fill(0)
    for (let i = 1; i <= len; i++) {
        dp[i] = dp[i - 1] + 1
        for (let j = 0; j < i; j++) {
            if (set.has(sentence.slice(j, i))) {
                dp[i] = Math.min(dp[i], dp[j])
            }
        }
    }

    return dp[len]
};

console.log(respace(["look"], "look"))