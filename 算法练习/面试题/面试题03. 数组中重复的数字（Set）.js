/**
 * @param {number[]} nums
 * @return {number}
 */
var findRepeatNumber = function (nums) {
    let check = new Set()
    for (let num of nums) {
        if (check.has(num)) return num
        check.add(num)
    }
};