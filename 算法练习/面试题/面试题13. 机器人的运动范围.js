/**
 * 1 <= n,m <= 100
 * 0 <= k <= 20
 * @param {number} m
 * @param {number} n
 * @param {number} k 
 * @return {number}
 */
var movingCount = function(m, n, k) {
    let sum=1;
    //方向数组
    dx=[-1,1,0,0];
    dy=[0,0,-1,1]
    
    //构造地图
    let arr=[];
    for(let i=0;i<m;i++){
        arr[i]=[];
        for(let j=0;j<n;j++){
            arr[i][j]=0;
        }
    }
    arr[0][0]=1
    //console.log(arr);

    //BFS
    let queue = new Array([0,0]);
    let point =[];
    while(0!=queue.length){
        point=queue.shift();
        let x=point[0];
        let y=point[1];
        //console.log(point);

        for(let i=0;i<4;i++){
            let newX = x + dx[i];
            let newY = y + dy[i];
            //console.log("x"+newX+"y"+newY)
            //计算位数之和
            let numX=placesNum(newX);
            let numY=placesNum(newY);
            if(newX<0||newX>=m||newY<0||newY>=n||arr[newX][newY]!=0||(numX+numY)>k){
                continue;
            }
            //console.log("arr["+newX+"]["+newY+"]="+arr[newX][newY]);
            arr[newX][newY]=arr[x][y]+1;
            //console.log("arr["+newX+"]["+newY+"]="+arr[newX][newY]);
            queue.push([newX,newY]);
            sum+=1;
        }
    }
    console.log(arr);
    return sum;
};

var placesNum = function(num){
    var sum=0;
    while(num){
        sum += num%10;
        num=parseInt(num/10);
    }
    return sum;
}
console.log(movingCount(11,8,16));
console.log(placesNum(10)+placesNum(7))