/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var spiralOrder = function (matrix) {
    if (matrix.length == 0) {
        return []
    }
    let m = matrix.length
    let n = matrix[0].length
    let total = m * n
    let res = []

    if (m == 1) {
        for (let i = 0; i < n; i++) {
            res.push(matrix[0][i])
        }
        return res
    }
    if (n == 1) {
        for (let i = 0; i < m; i++) {
            res.push(matrix[i][n - 1])
        }
        return res
    }

    let top = 0 // 上边界起始点
    let bottom = n - 2 //底边起始点
    let right = 1 //右边界起始点
    let left = m - 2 //左边界起始点

    while (m != 0 && n != 0) {
        if (m - top == 0 || n - top == 0) {
            m = 0
            n = 0
        }
        else if (m - top == 1) {
            for (let i = top; i < n; i++) {
                res.push(matrix[top][i])
            }
            m = 0
            n = 0
        }
        else if (n - top == 1) {
            for (let i = right - 1; i < m; i++) {
                res.push(matrix[i][n - 1])
            }
            n = 0
            m = 0
        }
        else {
            //遍历上层
            for (let i = top; i < n; i++) {
                res.push(matrix[top][i])
            }
            //遍历右侧
            for (let i = right; i < m; i++) {
                res.push(matrix[i][n - 1])
            }
            //遍历底层
            for (let i = bottom; i > -1 + top; i--) {
                if (res.length == total) break
                res.push(matrix[m - 1][i])
            }
            //遍历右侧
            for (let i = left; i > 0 + top; i--) {
                if (res.length == total) break
                res.push(matrix[i][top])
            }
            n--
            m--
            top++
            bottom--
            right++
            left--
        }
    }
    return res
};

// console.log(spiralOrder([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]))

// 1  2  3  4
// 5  6  7  8 
// 9  10 11 12

// console.log(spiralOrder([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]))

// 1  2  3  4
// 5  6  7  8 
// 9  10 11 12
// 13 14 15 16

// console.log(spiralOrder([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18, 19, 20], [21, 22, 23, 24, 25]]))

// 1  2  3  4  5
// 6  7  8  9  10
// 11 12 13 14 15
// 16 17 18 19 20
// 21 22 23 24 25

console.log(spiralOrder([[1, 2, 3, 4, 5, 6, 7, 8, 9, 10], [11, 12, 13, 14, 15, 16, 17, 18, 19, 20]]))

//  1  2  3  4  5  6  7  8  9 10
// 11 12 13 14 15 16 17 18 19 20

