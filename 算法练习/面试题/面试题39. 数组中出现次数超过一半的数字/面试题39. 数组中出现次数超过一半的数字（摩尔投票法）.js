/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function(nums) {
    let votes = 0;
    for(num of nums){
        if(votes==0){ 
            x = num;
        }
        votes += num == x?1:-1;
    }
    return x;
};

console.log(majorityElement([1, 2, 3, 2, 2, 2, 5, 4, 2]))
console.log(majorityElement([1, 2, 2, 2, 2, 2, 5, 4, 3]))
console.log(majorityElement([3,3,4]))
console.log(majorityElement([3,2,3]))
console.log(majorityElement([2,2,1,1,1,2,2]))