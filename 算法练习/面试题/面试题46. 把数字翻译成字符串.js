const translateNum = (num) => {
    const str = num.toString()
    const n = str.length
    const dp = new Array(n + 1)
    dp[0] = 1
    dp[1] = 1
    for (let i = 2; i < n + 1; i++) {
      const temp = Number(str[i - 2] + str[i - 1])
      if (temp >= 10 && temp <= 25) {
        dp[i] = dp[i - 1] + dp[i - 2]
      } else {
        dp[i] = dp[i - 1]
      }
    }
    return dp[n] // 翻译前n个数的方法数，即翻译整个数字
  }
  
 // 作者：hyj8
 // 链接：https://leetcode-cn.com/problems/ba-shu-zi-fan-yi-cheng-zi-fu-chuan-lcof/solution/shou-hui-tu-jie-dfsdi-gui-ji-yi-hua-di-gui-dong-ta/
 // 来源：力扣（LeetCode）
 // 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。