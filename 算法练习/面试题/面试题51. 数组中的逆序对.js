/**
 * @param {number[]} nums
 * @return {number}
 */
var reversePairs = function (nums) {
    let len = nums.length

    if (len < 2) {
        return 0;
    }

    let copy = [...nums]

    let temp = new Array(len).fill(0)

    return reversePairsSum(copy, 0, len - 1, temp)
};

/**
 * nums[left..right]计算逆序对个数并排序
 */
var reversePairsSum = function (nums, left, right, temp) {
    //递归终止条件
    if (left == right) return 0

    //不可以用 （left+right)/2 如果left和right很大，会出现整型溢出
    let mid = left + parseInt((right - left) / 2)

    let leftPairs = reversePairsSum(nums, left, mid, temp)

    let rightPairs = reversePairsSum(nums, mid + 1, right, temp)

    let crossPairs = mergeAndCount(nums, left, mid, right, temp)

    return leftPairs + rightPairs + crossPairs
}

/**
 * nums[left..mid]有序，nums[mid+1..right]有序
 */
var mergeAndCount = function (nums, left, mid, right, temp) {
    for (let i = left; i <= right; i++) {
        temp[i] = nums[i];
    }

    let i = left;
    let j = mid + 1;

    let count = 0;

    for (let k = left; k <= right; k++) {
        if (i == mid + 1) {
            nums[k] = temp[j];
            j++;
        } else if (j == right + 1) {
            nums[k] = temp[i]
            i++
        } else if (temp[i] <= temp[j]) { //严格小于的话就不是稳定的归并排序
            nums[k] = temp[i]
            i++;
        } else {
            nums[k] = temp[j];
            j++;
            count += (mid - i + 1);
        }
    }

    console.log(nums)

    return count;
}

console.log(reversePairs([1, 3, 2, 8, 6,12, 8]))