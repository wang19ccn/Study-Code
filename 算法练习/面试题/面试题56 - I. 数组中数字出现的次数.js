/**
 * @param {number[]} nums
 * @return {number[]}
 */
var singleNumbers = function(nums) {
    let s=0;
    for(num of nums){
        s ^=num
    }
    let k=s&(-s)
    let res=[null,null]
    for(num of nums){
        if(num & k){
            res[0] ^= num
        }else{
            res[1] ^= num
        }
    }
    return res;
};