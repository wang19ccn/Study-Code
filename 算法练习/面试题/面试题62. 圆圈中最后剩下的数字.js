/**
 * @param {number} n
 * @param {number} m
 * @return {number}
 */
var lastRemaining = function (n, m) {
    let flag = 0;

    for (let i = 2; i <= n; i++) {
        flag = (flag + m) % i;
        //console.log(m);
        //console.log(flag);
    }

    return flag;
};

console.log(lastRemaining(10, 17))

//lastRemaining(5, 3)