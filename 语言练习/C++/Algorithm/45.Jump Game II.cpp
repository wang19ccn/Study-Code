//
// Created by huihui on 2020/5/17.
//
#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int jump(vector<int> &nums) {
        int ans = 0;
        int end = 0;
        int maxPos = 0;
        for (int i = 0; i < nums.size() - 1; i++) {
            maxPos = max(nums[i] + i, maxPos);
            if (i == end) {
                end = maxPos;
                ans++;
            }
            // 并没有加快多少
            // if(end >= nums.size()){
            //     return ans;
            // }
        }
        return ans;
    }
};
