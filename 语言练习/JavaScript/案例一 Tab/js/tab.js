class Tab {
    constructor(id) {
        // 获取元素
        this.main = document.querySelector(id);
        this.lis = this.main.querySelector('li');
        this.sections = this.main.querySelector('section')
    }

    // 1. 切换功能
    toggleTab() {

    }

    // 2. 添加功能
    addTab() {

    }

    // 3. 删除功能
    removeTab() {

    }

    // 4. 修改功能
    editTab() {

    }

}
new Tab('#tab');