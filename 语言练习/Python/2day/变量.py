"""
2021-3-16
加减乘除

"""

a = 321
b = 12
print(a + b)
print(a - b)
print(a * b)
print(a / b)


a = 100
b = 12.345
c = 1 + 5j
d = 'hello, world'
e = True
print(type(a))    # <class 'int'>
print(type(b))    # <class 'float'>
print(type(c))    # <class 'complex'>
print(type(d))    # <class 'str'>
print(type(e))    # <class 'bool'>

"""
使用Python中内置的函数对变量类型进行转换
int()：将一个数值或字符串转换成整数，可以指定进制。
float()：将一个字符串转换成浮点数。
str()：将指定的对象转换成字符串形式，可以指定编码。
chr()：将整数转换成该编码对应的字符串（一个字符）。
ord()：将字符串（一个字符）转换成对应的编码（整数）。

"""


"""
使用input()函数获取键盘输入(字符串)
使用int()函数将输入的字符串转换成整数
使用print()函数输出带占位符的字符串

"""
a = int(input('a = '))
b = int(input('b = '))
print('%d + %d = %d' % (a, b, a + b))
print('%d - %d = %d' % (a, b, a - b))
print('%d * %d = %d' % (a, b, a * b))
print('%d / %d = %f' % (a, b, a / b))
print('%d // %d = %d' % (a, b, a // b))
print('%d %% %d = %d' % (a, b, a % b))
print('%d ** %d = %d' % (a, b, a ** b))

"""
说明：上面的print函数中输出的字符串使用了占位符语法，
其中%d是整数的占位符，
%f是小数的占位符，
%%表示百分号（因为百分号代表了占位符，所以带占位符的字符串中要表示百分号必须写成%%），
字符串之后的%后面跟的变量值会替换掉占位符然后输出到终端中，运行上面的程序，看看程序执行结果就明白啦。

"""

flag0 = False
flag1 = True
print('test1=', flag0 and flag1)

"""
练习1：华氏温度转换为摄氏温度。
华氏温度到摄氏温度的转换公式为：$C=(F - 32) \div 1.8$。

"""

f = float(input("请输入华氏温度："))
c = (f - 32) / 1.8
print("%.1f华氏度 = %.1f摄氏度" % (f, c))

"""
练习2：输入圆的半径计算计算周长和面积。

"""
radius = float(input('请输入圆的半径：'))
perimeter = 2 * 3.1416 * radius
area = 3.1416 * radius * radius
print('周长：%.2f' % perimeter)
print('面积：%.2f' % area)

"""
练习3：输入年份 如果是闰年输出True 否则输出False

"""

year = int(input('请输入年份：'))
# 如果代码太长写成一行不便于阅读 可以使用\对代码进行折行
isYear = year % 4 == 0 and year % 100 != 0 or \
         year % 400 == 0
print(isYear)

year = int(input('请输入年份：'))
isYear = (year % 4 == 0 and year % 100 != 0 or
         year % 400 == 0)
print(isYear)