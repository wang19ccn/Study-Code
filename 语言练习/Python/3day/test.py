"""
if:
else:

缩进要用四空格
"""

username = input("please input username")
password = input("please input password")
if username == "admin" and password == "123456":
    print("身份验证成功！")
else:
    print("身份验证失败！")


"""
练习1：英制单位英寸与公制单位厘米互换。
"""

value = float(input("请输入长度："))
unit = input("请输入单位：")
if unit == "in" or unit == "英寸":
    print("%f 英寸 = %f 厘米" % (value, value * 2.54))
elif unit == "cm" or unit == "英寸":
    print("%f 厘米 = %f 英寸" % (value, value / 2.54))
else:
    print("请输入有效单位")


"""
练习2：百分制成绩转换为等级制成绩。

要求：
90分以上（含90分）输出A；
80分-90分（不含90分）输出B；
70分-80分（不含80分）输出C；
60分-70分（不含70分）输出D；
60分以下输出E。

"""

score = float(input("请输入成绩："))
if score >= 90:
    print("A")
elif score >= 80:
    print("B")
elif score >= 70:
    print("C")
elif score >= 60:
    print("D")
else:
    print("E")


"""
练习3：输入三条边长，如果能构成三角形就计算周长和面积。

A={sqrt  {s(s-a)(s-b)(s-c)}} s=0.5*(a+b+c)
"""

a = float(input("a = "))
b = float(input("b = "))
c = float(input("c = "))
(a, b)
if a + b > c and a + c > b and b + c > a:
    print('周长：%f' % (a + b + c))
    # 海伦公式
    s = 0.5 * (a + b +c)
    print('面积：%f' % ((s*( s - a )*( s - b )*( s - c )) ** 0.5))
else:
    print('不能构成三角形') 