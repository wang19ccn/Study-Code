"""
for item in range:
"""
sum = 0
# range(101)：可以用来产生0到100范围的整数，需要注意的是取不到101。
for x in range(101):
    sum += x
print(sum)

"""
while condition :
猜数字
"""
import random

num = random.randint(1, 100)
count = 0  
while True:
    count += 1
    inputNum = int(input('请输入：'))
    if num < inputNum:
        print('big!')
    elif num > inputNum:
        print('small')
    else:
        print('right!')
        break
print('一共猜了%d' % count)


"""
练习3：打印如下所示的三角形图案。
"""
row = int(input('请输入行数: '))
for i in range(row):
    for _ in range(i + 1):
        print('*',end = '')
    print()
# 为末尾end传递一个空字符串,这样print函数不会在字符串末尾添加一个换行符,
# 而是添加一个空字符串,    
