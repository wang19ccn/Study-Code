#include <vector>
#include <map>
#include <iostream>
using namespace std;

class Solution {
public:
	vector<int> twoSum(vector<int>& nums, int target) {
		map<int, int> numsMap;
		for (int i = 0; i < nums.size(); ++i) {
			map<int, int>::iterator iter = numsMap.find(target - nums[i]);
			if (iter != numsMap.end()) {
				return { iter->second,i };
			}
			// numsMap[nums[i]] = i;
			numsMap.insert(pair<int, int>(nums[i], i));
		}
		return {};
	}
};

//int main() {
//	Solution solution;
//	vector<int> nums = { 2, 7, 11, 15 };
//	for (auto i : solution.twoSum(nums, 9)) {
//		cout << i << ' ';
//	}
//	return 0;
//}