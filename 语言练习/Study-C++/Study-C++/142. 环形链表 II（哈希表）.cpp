/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
#include <cstddef>
#include <unordered_set>

using namespace std;

struct ListNode {
	int val;
	ListNode* next;
	ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
	ListNode* detectCycle(ListNode* head) {
		unordered_set<ListNode*> visuted;
		while (head != nullptr)
		{
			if (visuted.count(head))
			{
				return head;
			}
			visuted.insert(head);
			head = head->next;
		}
		return nullptr;
	}
};