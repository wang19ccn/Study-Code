/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

struct ListNode {
	int val;
	ListNode* next;
	ListNode() : val(0), next(nullptr) {}
	ListNode(int x) : val(x), next(nullptr) {}
	ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
public:
	ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
		// 结构体和类只是模板，不占用内存空间
		// 但变量占用空间，new是创建一个新的并装在变量数据，给指针接收，寻址 （10月4日 批注）
		ListNode* pre = new ListNode(0);
		ListNode* cur = pre;
		int carry = 0;
		while (l1 != nullptr || l2 != nullptr) {
			int x = l1 ? l1->val : 0;
			int y = l2 ? l2->val : 0;
			int sum = x + y + carry;

			carry = sum / 10;
			cur->next = new ListNode(sum %= 10);

			cur = cur->next;
			if (l1 != nullptr) l1 = l1->next;
			if (l2 != nullptr) l2 = l2->next;
		}
		if (carry == 1) {
			cur->next = new ListNode(carry);
		}
		return pre->next;
	}
};


