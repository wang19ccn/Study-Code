#include <cstddef>
#include <iostream>
struct ListNode {
	int val;
	ListNode* next;
	ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
	bool isPalindrome(ListNode* head) {
		if (!head || !head->next) return true;
		ListNode* list_1 = head;
		// 注意此处并没有断开list_1和中间结点
		// 1. 初始 head 1-2-3-4-5
		// 2. list_1 1-2-3-4-5
		// 3. 找到中点后 list_2 4-5
		// 4. 反转list_2 5-4-null 
		// 5. 注意此时 list_1 1-2-3-4-null 按理说应该断开两个子链 3-null
		// 6. 共同指向4结点，不影响做题，但感觉不太规范
		ListNode* list_2 = reverseList(searchMid(head));
		bool res = isEqual(list_1, list_2);

		// 恢复链表
		reverseList(list_2);
		return res;
	}

	ListNode* searchMid(ListNode* head) {
		ListNode* slow = head;
		ListNode* fast = head;
		while (fast && fast->next)
		{
			slow = slow->next;
			fast = fast->next->next;
		}
		if (fast)  slow = slow->next;
		return slow;
	}

	ListNode* reverseList(ListNode* head) {
		ListNode* pre = nullptr;
		ListNode* cur = head;
		while (cur != nullptr) {
			ListNode* temp = cur->next;
			cur->next = pre;
			pre = cur;
			cur = temp;
		}
		return pre;
	}

	bool isEqual(ListNode* list1, ListNode* list2) {
		while (list2) {
			if (list2->val != list1->val) return false;
			list1 = list1->next;
			list2 = list2->next;
		}
		return true;
	}
};

//int main() {
//	Solution* solution = new Solution();
//	ListNode* list = new ListNode(1);
//	list->next = new ListNode(2);
//	list->next->next = new ListNode(3);
//	list->next->next->next = new ListNode(4);
//	list->next->next->next->next = new ListNode(5);
//	std::cout << solution->isPalindrome(list) << std::endl;
//	return 0;
//}