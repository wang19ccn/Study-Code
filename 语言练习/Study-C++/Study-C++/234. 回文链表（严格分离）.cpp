#include <cstddef>
#include <iostream>
struct ListNode {
	int val;
	ListNode* next;
	ListNode(int x) : val(x), next(NULL) {}
};
class Solution {
public:
	bool isPalindrome(ListNode* head) {
		ListNode* fast = head;
		ListNode* cur = head;
		ListNode* pre = NULL;
		while (fast && fast->next) {
			fast = fast->next->next;
			// 反转前半个表
			ListNode* temp = cur->next;
			cur->next = pre;
			pre = cur;
			cur = temp;
		}
		if (fast) cur = cur->next;
		while (pre) {
			if (pre->val != cur->val) return false;
			pre = pre->next;
			cur = cur->next;
		}
		return true;
	}
};