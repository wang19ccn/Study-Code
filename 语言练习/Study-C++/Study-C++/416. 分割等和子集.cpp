#include <vector>
using namespace std;

class Solution {
public:
	bool canPartition(vector<int>& nums) {
		int len = nums.size();
		if (len == 0) return false;

		int sum = 0;
		for (int num : nums) {
			sum += num;
		}
		if ((sum & 1) == 1) {
			return false;
		}
		int target = sum / 2;

		vector<vector<int>> dp(len, vector<int>(target + 1, false));

		if (nums[0] <= target) {
			dp[0][nums[0]] = true;
		}

		for (int i = 1; i < len; i++) {
			for (int j = 0; j <= target; j++) {
				dp[i][j] = dp[i - 1][j];

				if (nums[i] == j) {
					dp[i][j] = true;
					continue;
				}

				if (nums[i] < j) {
					dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i]];
				}
			}
		}

		return dp[len - 1][target];
	}
};