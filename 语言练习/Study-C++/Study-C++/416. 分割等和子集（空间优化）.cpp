#include <vector>
using namespace std;

class Solution {
public:
	bool canPartition(vector<int>& nums) {
		int len = nums.size();
		if (len == 0) return false;

		int sum = 0;
		for (int num : nums) {
			sum += num;
		}
		if ((sum & 1) == 1) {
			return false;
		}

		int target = sum / 2;
		vector<bool> dp(target + 1, false);
		dp[0] = true;

		if (nums[0] <= target) {
			dp[nums[0]] = true;
		}

		for (int i = 1; i < len; i++) {
			for (int j = target; nums[i] <= j; j--) {
				if (dp[target]) {
					return true;
				}
				dp[j] = dp[j] || dp[j - nums[i]];
			}
		}

		return dp[target];
	}
};