#include <unordered_set>
using namespace std;
class Solution {
public:
	int totalNQueens(int n) {
		unordered_set<int> cols;
		unordered_set<int> main;
		unordered_set<int> sub;
		return dfs(n, 0, cols, main, sub);
	}

	int dfs(int n, int row, unordered_set<int>& cols, unordered_set<int>& main, unordered_set<int>& sub) {
		if (row == n) {
			return 1;
		}
		int count = 0;
		for (int col = 0; col < n; col++) {
			if (cols.find(col) == cols.end() && main.find(row - col) == main.end() && sub.find(row + col) == sub.end()) {
				cols.insert(col);
				main.insert(row - col);
				sub.insert(col + row);
				count += dfs(n, row + 1, cols, main, sub);
				cols.erase(col);
				main.erase(row - col);
				sub.erase(col + row);
			}
		}
		return count;
	}
};