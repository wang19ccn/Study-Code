#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
	int bestTeamScore(vector<int>& scores, vector<int>& ages) {
		int n = ages.size();
		vector<pair<int, int>> info(n);
		for (int i = 0; i < n; ++i) {
			info[i].first = ages[i];
			info[i].second = scores[i];
		}
		sort(info.begin(), info.end()); // 会先比较first 然后在first相等时，根据second排序
		vector<int> dp(n);
		int res = 0;
		for (int i = 0; i < n; ++i) {
			dp[i] = info[i].second;
			for (int j = i - 1; j >= 0; --j) {
				if (info[j].second <= info[i].second) {
					dp[i] = max(dp[j] + info[i].second, dp[i]);
				}
			}
			res = max(res, dp[i]);
		}
		return res;
	}
};

//int main() {
//	Solution s;
//	vector<int> a = { 319776, 611683, 835240, 602298, 430007, 574, 142444, 858606, 734364, 896074 };
//	vector<int> b = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
//	s.bestTeamScore(a, b);
//}