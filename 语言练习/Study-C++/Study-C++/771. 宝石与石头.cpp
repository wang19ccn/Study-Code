#include <string>
#include <unordered_set>
#include <iostream>
using namespace std;

class Solution {
public:
	int numJewelsInStones(string J, string S) {
		int count = 0;
		unordered_set<char> set;
		for (int i = 0; i < J.length(); i++)
		{
			char j = J[i];
			set.insert(j);
		}
		for (int i = 0; i < S.length(); i++)
		{
			char s = S[i];
			if (set.count(s)) {
				++count;
			}
		}
		return count;
	}
};

//int main() {
//	Solution solution;
//	cout << solution.numJewelsInStones("aA", "aAAbbbb") << endl;
//	return 0;
//}