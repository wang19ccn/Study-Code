#include <stdlib.h>
typedef struct DNode {
	int data;
	struct DNode* prior, * next;
}DNode, * DLinkList;

// 初始化 双链表
bool InitDLinkList(DLinkList &L) {
	L = (DNode*)malloc(sizeof(DNode));
	if (L == NULL) return false;
	L->prior = NULL;
	L->next = NULL;
	return true;
}

// 判空
bool Empty(DLinkList L) {
	return L->next == NULL;
}

// 在p结点之后插入s结点（后插）
bool InsertNextDNode(DNode* p, DNode* s) {
	if (p == NULL || s == NULL) return false;
	s->next = p->next;
	if (p->next != NULL) {
		p->next->prior = s;
	}
	s->prior = p;
	p->next = s;
	return true;
}

// 删除p结点的后续节点
bool DeletNextDNode(DNode* p) {
	if (p == NULL) return false;
	DNode* q = p->next;
	if (q == NULL) return false;
	p->next = q->next;
	if (q->next != NULL)
		q->next->prior = p;
	free(q);
	return true;
}

// 释放链表
void DestoryList(DLinkList& L) {
	while (L->next!=NULL)
	{
		DeletNextDNode(L);
	}
	free(L);
	L = NULL;
}

// 遍历双链表
// while

void testDLinkList() {
	DLinkList L;
	InitDLinkList(L);
}