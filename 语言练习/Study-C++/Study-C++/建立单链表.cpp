#include <cstddef>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

typedef struct LNode {
	int data;
	struct LNode* next;
}LNode, * LinkList;

// 不带头结点的单链表
//bool InitList(LinkList& L) {
//	L = NULL; // 防止脏数据
//	return true;
//}
//
//
//bool Empty(LinkList L) {
//	return L == NULL;
//}

// 带头结点的单链表
bool InitList(LinkList& L) {
	L = (LNode*)malloc(sizeof(LNode));
	if (L == NULL) return false;  //内存不足，分配失败
	L->next = NULL;
	return true;
}

bool Empty(LinkList L) {
	return L->next == NULL;
}

bool ListInsert(LinkList& L, int i, int e) {
	if (i < 1) return false;

	// 不带头节点，需要特殊处理i == 1时候的情况
	//if (i == 1) {
	//	LNode* s = (LNode*)malloc(sizeof(LNode));
	//	s->data = e;
	//	s->next = L;
	//	L = s;
	//	return true;
	//}

	LNode* p;
	int j = 0;
	p = L;
	while (p != NULL && j < i - 1) { // 找到i - 1个结点
		p = p->next;
		j++;
	}
	if (p == NULL) return false;
	LNode* s = (LNode*)malloc(sizeof(LNode));
	s->data = e;
	s->next = p->next;
	p->next = s;
	return true;
}

// 后插操作
bool InsertNextNode(LNode* p, int e) {
	if (p == NULL) return false;
	LNode* s = (LNode*)malloc(sizeof(LNode));
	if (s == NULL) return false; // 内存分配失败
	s->data = e;
	s->next = p->next;
	p->next = s;
	return true;
}

// 前插操作
bool InsertPriorNode(LNode* p, int e) {
	if (p == NULL) return false;
	LNode* s = (LNode*)malloc(sizeof(LNode));
	if (s == NULL) return false; // 内存分配失败
	s->next = p->next;
	p->next = s;
	s->data = p->data;
	p->data = e;
	return true;
}

// 按位序删除（带头节点）
bool ListDelete(LinkList& L, int i, int& e) {
	if (i < 1) return false;
	LNode* p;
	int j = 0;
	p = L;
	while (p != NULL && j < i - 1) { // 找到i - 1个结点
		p = p->next;
		j++;
	}
	if (p == NULL) return false;
	if (p->next == NULL) return false;
	LNode* q = p->next;
	e = q->data;
	p->next = q->next;
	free(q);
	return true;
}

// 删除指定节点p
// 存在bug，无法处理最后一个节点。
bool DeleteNode(LNode* p) {
	if (p == NULL) return false;
	LNode* q = p->next;
	p->data = p->next->data;
	p->next = q->next;
	free(q);
	return true;
}


// 按位查找
LNode* GetElem(LinkList L, int i) {
	if (i < 1) return NULL;
	LNode* p;
	int j = 0;
	p = L;
	while (p != NULL && j < i) { // 找到i个结点
		p = p->next;
		j++;
	}
	return p;
}
// 按值查找
LNode* LocateElem(LinkList L, int e) {
	LNode* p = L->next;
	while (p != NULL && p->data != e) {
		p = p->next;
	}
	return p;
}

// 求表的长度
int Length(LinkList L) {
	int len = 0;
	LNode* p = L;
	// 如果没有头结点 判断条件改为 p != NULL
	while (p->next != NULL) {
		p = p->next;
		++len;
	}
	return len;
}

// 单链表建立
// 尾插法
LinkList List_TailInsert(LinkList& L) {
	int x;
	L = (LinkList)malloc(sizeof(LNode));
	LNode* s, * r = L;
	scanf_s("%d", &x);
	while (x != 9999) {
		s = (LNode*)malloc(sizeof(LNode));
		s->data = x;
		r->next = s;
		r = s;
		scanf_s("%d", &x);
	}
	r->next = NULL;
	return L;
}
// 头插法
// 逆置
LinkList List_HeadInsert(LinkList& L) {
	int x;
	LNode* s = L;
	L = (LinkList)malloc(sizeof(LNode));
	L->next = NULL;
	scanf_s("%d", &x);
	while (x != 9999) {
		s = (LNode*)malloc(sizeof(LNode));
		s->data = x;
		s->next = L->next;
		L->next = s;
		scanf_s("%d", &x);
	}
	return L;
}

void test() {
	// LNode* newNode = new LNode();
	LinkList L;
	InitList(L);
}

//int main() {
//	LinkList L;
//	// List_TailInsert(L);
//	List_HeadInsert(L);
//	GetElem(L, 2);
//	std::cout << GetElem(L, 2)->data << std::endl;
//	printf_s("%d", Length(L));
//}