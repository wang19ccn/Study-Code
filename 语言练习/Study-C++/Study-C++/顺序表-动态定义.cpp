#include <stdlib.h>

#define InitSize 10

typedef struct {
	int* data;
	int MaxSize;
	int length;
}SeqList;

void InitList(SeqList& L) {
	L.data = (int*)malloc(InitSize * sizeof(int));
	L.length = 0;
	L.MaxSize = InitSize;
}

void IncreaseSize(SeqList& L, int len) {
	int* p = L.data;
	L.data = (int*)malloc((L.MaxSize + len) * sizeof(int));
	for (int i = 0; i < L.length; i++) {
		L.data[i] = p[i];
	}
	L.MaxSize = L.MaxSize + len;
	free(p);
}


// 按位查找
int GetElem(SeqList L, int i) {
	return L.data[i - 1];
	// 为什么可以用数组下标来访问？
	// data其实是malloc函数分配的一大块内存空间的起始地址
	// 假设地址起始为 2000 
	// 若sizeof(ElemType)==6
	// date[0] 此2000开始的6B内存空间 2000-2005
	// date[1] 此2006开始的6B内存空间 2006-2012

	// 计算机背后的操作，每次取几个字节，和数字类型有关
	// 所以需要强制转换malloc函数返回的存储空间起始地址（类型）和数据元素的数据类型相对应
}



//int main() {
//	SeqList L;
//	InitList(L);
//	IncreaseSize(L, 5);
//	return 0;
//}