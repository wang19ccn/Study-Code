#include <stdio.h>
#define MaxSize 10
typedef struct {
	int data[MaxSize];
	int length;
}SqList;

void InitList(SqList& L) {
	for (int i = 0; i < MaxSize; i++) {
		L.data[i] = 0; //初始化，默认值
	}
	L.length = 0;
}

bool ListInsert(SqList& L, int i, int e) {
	if (i < 1 || i > L.length + 1) {
		return false;
	}
	if (L.length >= MaxSize) {
		return false;
	}
	for (int j = L.length; j >= i; j--) {
		L.data[j] = L.data[j - 1];
	}
	L.data[i - 1] = e;
	L.length++;
	return true;
}

bool ListDelete(SqList& L, int i, int& e) {
	if (i<1 || i>L.length)
		return false;
	e = L.data[i - 1];
	for (int j = i; i < L.length; j++) {
		L.data[j - 1] = L.data[j];
	}
	L.length--;
	return true;
}

// 按位查找
int GetElem(SqList L, int i) {
	return L.data[i - 1];
}

// 按值查找
int LocateElem(SqList L, int e) {
	for (int i = 0; i < L.length; i++) {
		if (L.data[i] == e) {
			// 注意这里是否能用 == 来判断相等
			// 要根据题意具体分析
			// 结构体不能用 == ， 要分别对比各个分量 （否则根本无法编译）
			return i + 1;
		}
	}
	return 0;
}

//int main() {
//	SqList L;
//	InitList(L);
//	ListInsert(L, 1, 1);
//	ListInsert(L, 2, 2);
//	ListInsert(L, 3, 3);
//
//	int e = -1; // 用变量e把删除的元素“带回来”
//	if (ListDelete(L, 2, e)) {
//		printf("已经删除第3个元素，删除元素值为=%d\n", e);
//	}
//	else {
//		printf("位序i不合法，删除失败\n");
//	}
//	// 违规访问，如果用MaxSize，再有没初始化默认值时，会出现脏数据
//	// 实际应该根据length来循环，所以没有初始化也可以
//	for (int i = 0; i < MaxSize; i++) {
//		printf("data[%d] = %d \n", i, L.data[i]);
//	}
//	return 0;
//}