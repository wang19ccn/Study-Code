let str:string="hello"

let str1:string="你好typescript"

let str2:string="你好typescript"

var flag:boolean=true

var a:number=123
console.log(a)

var a=12.3

//数组
let arr:number[]=[12,34,5,5]
console.log(arr)

let arr1:string[]=["js","es5",'es6']

let arr3:Array<number>=[11,23,4356]

//元组类型
let arr4:[string,number,boolean]=["ts",3.18,true]

//枚举类型
enum Flag{success=1,error=-1}
var f:Flag=Flag.success

enum Color{red,blue,orange}
var c:Color=Color.orange
console.log(c) //3 不赋值，默认是索引值

enum Color1{red,blue=5,orange}
var c1:Color1=Color1.orange
console.log(c1) //6 

//任意类型（any）
var num:any=123;
num='str'
num=true

var num1:number|null|undefined

// ES5写法
// function run(){
//     console.log('run')
// }
// run();

// 表示方法没有任何返回类型
function run():void{
    console.log('run')
}
run();

// var a:never