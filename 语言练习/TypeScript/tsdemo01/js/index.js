"use strict";
var str = "hello";
var str1 = "你好typescript";
var str2 = "你好typescript";
var flag = true;
var a = 123;
console.log(a);
var a = 12.3;
//数组
var arr = [12, 34, 5, 5];
console.log(arr);
var arr1 = ["js", "es5", 'es6'];
var arr3 = [11, 23, 4356];
//元组类型
var arr4 = ["ts", 3.18, true];
//枚举类型
var Flag;
(function (Flag) {
    Flag[Flag["success"] = 1] = "success";
    Flag[Flag["error"] = -1] = "error";
})(Flag || (Flag = {}));
var f = Flag.success;
var Color;
(function (Color) {
    Color[Color["red"] = 0] = "red";
    Color[Color["blue"] = 1] = "blue";
    Color[Color["orange"] = 2] = "orange";
})(Color || (Color = {}));
var c = Color.orange;
console.log(c); //3 不赋值，默认是索引值
var Color1;
(function (Color1) {
    Color1[Color1["red"] = 0] = "red";
    Color1[Color1["blue"] = 5] = "blue";
    Color1[Color1["orange"] = 6] = "orange";
})(Color1 || (Color1 = {}));
var c1 = Color1.orange;
console.log(c1); //6 
//任意类型（any）
var num = 123;
num = 'str';
num = true;
var num1;
// ES5写法
// function run(){
//     console.log('run')
// }
// run();
// 表示方法没有任何返回类型
function run() {
    console.log('run');
}
run();
// var a:never
